<?php

class Mfu extends CI_Controller {

    private $css;
    private $locale_month;
    private $locale_short_month;

    public function __construct() {
        parent::__construct();
        $this->css = css_asset('mfu.css');
        $this->template->set('site_detail', LANG == 'en' ? 'Mae Fah Luang University' : 'มหาวิทยาลัยแม่ฟ้าหลวง');
        $this->template->set('widget', $this->post->get_widget());
        $this->template->set('fix_link', $this->template->element('mfu/fixlink.php'));
        $this->template->set('sidebar', $this->template->element('mfu/sidebar.php'));

        $this->locale_month = unserialize(FULL_MONTH);
        $this->locale_short_month = unserialize(SHORT_MONTH);

        $this->template->set('locale_month', $this->locale_month);
        $this->template->set('locale_short_month', $this->locale_short_month);

        //Create cache 1 day
//        $this->output->cache(1440);
    }

    public function index() {
        $this->template->set('title', LANG == 'th' ? 'ยินดีต้อนรับสู่มหาวิทยาลัยแม่ฟ้าหลวง' : "Welcome to Mae Fah Luang University");
        $this->css .= css_asset("nivo-slider.css") . css_asset("themes/default/default.css");
        $js = js_asset("jquery.nivo.slider.pack.js");
        $this->template->set('css', $this->css);
        $this->template->set('js', $js);
        $this->template->set('slide', $this->post->get_all_slide());
        $this->template->load(null, 'default_plain.php');
    }

    public function school($post_id) {
        $school = $this->post->get_post_by_id($post_id);
        $this->template->set('title', $school->post_title);
        $this->template->set('css', $this->css);
        $this->template->set('school', $school);
        $this->template->set('school_info', json_decode($school->post_detail));
        $this->template->load(null, 'default_plain.php');
    }

    public function post($post_id) {
//        $post = $this->post->get_post_by_id($post_id);        
        $post = get_latest_post_revision($post_id);
        $this->template->set('title', $post->post_title);
        if ($post->cate_id != 0) {
            $this->template->set('cate_info', $this->post->get_cate_info($post->cate_id));
        }
        $this->template->set('css', $this->css);
        $this->template->set('post', $post);
        $this->template->set('post_stat_id', "post_{$post_id}");
        $this->template->load(null, 'default_plain.php');
    }

    public function page($post_id) {
//        $post = $this->post->get_post_by_id($post_id);
        $post = get_latest_post_revision($post_id);
        $this->template->set('title', $post->post_title);
        if ($post->cate_id != 0) {
            $this->template->set('cate_info', $this->post->get_cate_info($post->cate_id));
        }
        $this->template->set('css', $this->css);
        $this->template->set('post', $post);
        $this->template->set('post_stat_id', "post_{$post_id}");
        $this->template->load(null, 'default_plain.php');
    }

    public function category($cate_id) {
        $page = $this->input->get('page');
        $page = is_numeric($page) ? $page : 0;

        $year = $this->input->get('year');
        $year = is_numeric($year) ? $year : 0;

        $month = $this->input->get('month');
        $month = is_numeric($month) ? $month : 0;

        $search = $this->input->get('q');

        $offset = POST_PER_PAGE * $page;

        $cate_info = get_category_by_cate_id($cate_id);
        $all_post = get_all_post_in_cate($cate_id, POST_PER_PAGE, $offset, TYPE_POST, false, $month, $year, $search);

        $total_post = get_number_of_post_in_cate($cate_id, TYPE_POST, false, $month, $year, $search);
        $total_page = ceil($total_post / POST_PER_PAGE);

        $all_year = $this->post->get_group_year_of_post_by_cate($cate_id);
        $all_month = $this->post->get_group_month_of_post_by_cate($cate_id);

        $this->template->set('css', $this->css);
        $this->template->set('title', $cate_info->cate_name . " | " . (LANG == 'th' ? "หมวดหมู่" : "Category"));

        $this->template->set('all_year', $all_year);
        $this->template->set('all_month', $all_month);

        $this->template->set('year', $year);
        $this->template->set('month', $month);
        $this->template->set('q', $search);

        $this->template->set('cate_id', $cate_id);
        $this->template->set('cate_info', $cate_info);
        $this->template->set('all_post', $all_post);
        $this->template->set('total_page', $total_page);
        $this->template->set('page', $page);
        $this->template->load(null, 'default_plain.php');
    }

    public function archive($post_id) {
        $startpage = $this->input->get('startpage');
        $startpage = is_numeric($startpage) ? $startpage : 0;

        $page = $this->input->get('page');
        $page = is_numeric($page) ? $page : (0 + $startpage);

        $api_detail = get_post_by_id($post_id);

        $get_post = $api_detail->post_detail . "&page={$page}";
        $page_info = $get_post . "&num_rows=1";

        $all_post = json_decode(file_get_contents($get_post));
        $page_data = json_decode(file_get_contents($page_info));

        $this->template->set('title', "{$api_detail->post_title} | " . (LANG == 'th' ? "ข่าวจากระบบเว็บไซต์เดิม" : "Archive news"));
        $this->template->set('css', $this->css);
        $this->template->set('api_id', $post_id);
        $this->template->set('api_detail', $api_detail);
        $this->template->set("all_post", $all_post);
        $this->template->set("startpage", $startpage);
        $this->template->set("page", $page);
        $this->template->set("page_data", $page_data);
        $this->template->load(null, 'default_plain.php');
    }

    public function readarchive($id) {
        $read_api = "http://demo.mfu.ac.th/read_api.php?id={$id}";
        $post = file_get_contents($read_api);
        $post = json_decode($post);
        $post = $post[0];

        $ref_id = $this->input->get("ref");
        $ref_id = is_numeric($ref_id) ? $ref_id : 0;

        $this->template->set("post", $post);
        $this->template->set('post_stat_id', "post_api_{$post->id}");
        $this->template->set('css', $this->css);
        $this->template->set('ref_id', $ref_id);
        $this->template->set('title', "{$post->title} | " . (LANG == 'th' ? "ข่าวจากระบบเว็บไซต์เดิม" : "Archive news"));
        $this->template->load(null, 'default_plain.php');
    }

    public function all_event() {
        $page = $this->input->get('page');
        $page = is_numeric($page) ? $page : 1;

        $cate_id = $this->input->get('cate_id');
        $cate_id = is_numeric($cate_id) ? $cate_id : 0;

        $css = css_asset('datepicker.css');
        $js = js_asset('bootstrap-datepicker.js');
        if (LANG == 'th') {
            $js .= js_asset('locales/bootstrap-datepicker.th.js');
        }

        $this->template->set('js', $js);

        $month = $this->input->get('month');
        $day = $this->input->get('day');
        $year = $this->input->get('year');

        if ($cate_id != 0) {
            $cate_info = get_category_by_cate_id($cate_id);
            $page_title = $cate_info->cate_name . " ";
            if ($cate_info->parent_id == 0) {
                $all_child_cate = get_category_by_parent_cate_id($cate_id);
                $concern_cate = array();
                foreach ($all_child_cate as $each_cate) {
                    array_push($concern_cate, $each_cate->cate_id);
                }
                $post = $this->post->get_post_by_status(POST_PUBLISH, TYPE_EVENT, 0, $concern_cate, $page, 9, $order_by = 'desc', $schedule = false, $day, $month, $year);
            } else {
                $post = $this->post->get_post_by_status(POST_PUBLISH, TYPE_EVENT, 0, $cate_id, $page, 9, $order_by = 'desc', $schedule = false, $day, $month, $year);
            }
        } else {
            $page_title = "รายการทั้งหมด ";
            $post = $this->post->get_post_by_status(POST_PUBLISH, TYPE_EVENT, 0, $cate_id, $page, 9, $order_by = 'desc', $schedule = false, $day, $month, $year);
        }

        if (is_numeric($month) && is_numeric($year)) {
            if (is_numeric($day)) {
                $unix = mktime(0, 0, 0, $month, $day, $year);
                $page_title .= (int) $day . " " . $this->locale_month[LANG][(int) $month] . " " . $year;
            } else {
                $unix = mktime(0, 0, 0, $month, 1, $year);
                $page_title .= $this->locale_month[LANG][(int) $month] . " " . $year;
            }
        } else {
            $unix = date('U');
        }

        $this->template->set('title', "{$page_title} | " . (LANG == 'th' ? "ปฏิทินกิจกรรม" : "Event and Activity calendar"));
        $this->template->set('css', $this->css . $css);
        $this->template->set('cate_id', $cate_id);
        $this->template->set('date_set', $unix);
        $this->template->set('page_title', $page_title);
        $this->template->set('page', $page);
        $category = get_all_category(CATE_EVENT);
        $re_order = array();
        foreach ($category as $each_cate) {
            $re_order[$each_cate->cate_id] = $each_cate;
        }
        $this->template->set('category', $category);
        $this->template->set('re_cate', group_cate_by_parent($category));
        $this->template->set('re_order', $re_order);
        $this->template->set('post', $post);
        $this->template->set('total_page', $this->post->get_number_of_page(POST_PUBLISH, TYPE_EVENT, 0, $cate_id, 9, $schedule = false, $day, $month, $year));
        $this->template->load(null, 'default_plain.php');
    }

    public function event($post_id) {
        $post = $this->post->get_post_by_id($post_id);
        $this->template->set('title', $post->post_title . " | " . (LANG == 'th' ? "ปฏิทินกิจกรรม" : "Event and Activity calendar"));
        $this->template->set('css', $this->css);
        $this->template->set('post', $post);
        $this->template->set('post_stat_id', "post_{$post_id}");
        $this->template->load(null, 'default_plain.php');
    }

    public function audience($post_id) {
        $post = $this->post->get_post_by_id($post_id);
        $this->template->set('title', $post->post_title . " | " . (LANG == 'th' ? "แนะนำตามกลุ่มผู็ชม" : "Audience Guideline"));
        $this->template->set('css', $this->css);
        $this->template->set('post', $post);
        $this->template->set('all_link', json_decode($post->post_detail));
        $this->template->set('post_stat_id', "post_{$post_id}");
        $this->template->load(null, 'default_plain.php');
    }

    public function visitor_info() {
        $this->css .= css_asset("nivo-slider.css") . css_asset("themes/default/default.css");
        $js = js_asset("jquery.nivo.slider.pack.js");
        $js .= '<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>';
        $js .= js_asset("jquery-ui-1.8.18.min.js") . js_asset("jquery.ui.map.js");
        $this->template->set('css', $this->css);
        $this->template->set('js', $js);
        $this->template->set('title', LANG == 'th' ? "ผู้เยี่ยมชมทั่วไป" : "Visitor Information");
        $this->template->load(null, 'default_plain.php');
    }

    public function research() {
        $this->template->set('css', $this->css);
        $this->template->set('title', LANG == 'th' ? "ผลงานวิจัย" : "Research Information");

        //Research Database Temporary

        $url = "http://research-database.mfu.ac.th/index2.php";
        $html = file_get_contents($url);
        phpQuery::newDocumentHTML($html);
        $root = "http://research-database.mfu.ac.th/";

        foreach (pq('.droplet_here')->find('a') as $each_anchor) {
            $link = pq($each_anchor)->attr('href');
            pq($each_anchor)->attr('target', '_blank');
            pq($each_anchor)->attr('href', $root . $link);
        }

        foreach (pq('.droplet_here')->find('img') as $each_anchor) {
            $link = pq($each_anchor)->attr('src');
            pq($each_anchor)->attr('src', $root . $link);
            if (pq($each_anchor)->is("[width=11]")) {
                pq($each_anchor)->removeAttr("width");
                pq($each_anchor)->removeAttr("height");
                pq($each_anchor)->attr('style', " padding:0px 5px; ");
            }
        }

        pq("[bgcolor=#013F96]")->attr("bgcolor", "#fecbcb");
        pq('.droplet_here')->find("form#frm2>table")->addClass("table table-condensed");
        $research_table = pq('.droplet_here')->find("form#frm2")->html();
        $this->template->set('research_table', $research_table);

        //End Research Database

        $this->template->load(null, 'default_plain.php');
    }

    public function researcher() {
        $position = $this->input->get('position');
        $page = $this->input->get('page');
        $cate_id = $this->input->get('cate_id');
        $user_id = $this->input->get('user_id');

        $position = is_numeric($position) ? $position : 0;
        $page = is_numeric($page) ? $page : 1;
        $cate_id = is_numeric($cate_id) ? $cate_id : 0;
        $user_id = is_numeric($user_id) ? $user_id : 0;

        $this->template->set('css', $this->css);
        $this->template->set('title', LANG == 'th' ? "รายนามนักวิจัย" : "Reseacher list");
        $this->template->set('position', $position);
        $this->template->set('cate_id', $cate_id);
        $this->template->set('page', $page);
        $this->template->set('category', $this->post->get_post_by_status(POST_PUBLISH, TYPE_SCHOOL, $user_id, 0, 1, 200));

        $this->template->set('post', $this->post->get_post_by_status(POST_PUBLISH, TYPE_RESEARCH, 0, $cate_id, $page, $total_per_page = 15, $order_by = 'desc', $schedule = true, $special_year = 0, $month = 0, $year = 0, $position));
        $this->template->set('total_page', $this->post->get_number_of_page(POST_PUBLISH, TYPE_RESEARCH, 0, $cate_id, $total_per_page = 15, $schedule = true, $special_year = 0, $month = 0, $year = 0, $position));

        $this->template->load(null, 'default_plain.php');
    }

    public function pr() {
        $this->template->set('css', $this->css);
        $this->template->set('title', LANG == 'th' ? "ข่าวสาร" : "General News");
        $this->template->load(null, 'default_plain.php');
    }

    public function admission() {
        $this->template->set('css', $this->css);
        $this->template->set('title', LANG == 'th' ? "ส่วนรับนักศึกษา" : "Admission");
        $this->template->load(null, 'default_plain.php');
    }

    public function gallery() {
        $cate_id = $this->input->get('cate_id');
        $page = $this->input->get('page');
        $month = $this->input->get('month');
        $year = $this->input->get('year');
        $q = $this->input->get('q');

        $month = is_numeric($month) ? $month : 0;
        $year = is_numeric($year) ? $year : 0;
        $cate_id = is_numeric($cate_id) ? $cate_id : 0;
        $page = is_numeric($page) ? $page : 1;

        $this->template->set('month', $month);
        $this->template->set('year', $year);
        $this->template->set('cate_id', $cate_id);
        $this->template->set('page', $page);
        $this->template->set('q', $q);

        $this->template->set('css', $this->css);

        $this->template->set('group_year', $this->post->get_group_year_of_post_by_type(TYPE_GALLERY, POST_PUBLISH));
        $this->template->set('group_category', $this->admin->get_all_category(CATE_ALBUM));

        $this->template->set('title', LANG == 'th' ? "อัลบั้มภาพ" : "All Album");
        $this->template->set('order_time', $this->post->get_group_month_year_special_post(TYPE_GALLERY, $cate_id));
        $this->template->set('total_page', $this->post->get_number_of_page(POST_PUBLISH, TYPE_GALLERY, 0, $cate_id, 21, $schedule = false, $special_year = 0, $month, $year, 0, false, $q));
        $this->template->set('album', $this->post->get_post_by_status(POST_PUBLISH, TYPE_GALLERY, 0, $cate_id, $page, 21, $order_by = 'desc', $schedule = false, $special_year = 0, $month, $year, 0, false, $q));
        $this->template->load(null, 'default_plain.php');
    }

    public function album($post_id) {
        $post = get_post_by_id($post_id);
        $this->template->set('css', $this->css . css_asset("colorbox.css"));
        $this->template->set('js', js_asset("jquery.colorbox.js"));
        $this->template->set('title', $post->post_title . (LANG == 'th' ? "อัลบั้มภาพ" : "Album"));
        $this->template->set('post', $post);
        $this->template->set('all_image', get_all_media_sort_by_name($post_id));
        $this->template->set('post_stat_id', "post_{$post_id}");
        $this->template->load(null, 'default_plain.php');
    }

    public function news_mashup() {
        $page = $this->input->get('page');
        $page = is_numeric($page) ? $page : 0;
        $offset = POST_PER_PAGE * $page;
        $post = get_latest_news(POST_PER_PAGE, $offset);
        $total_page = get_number_latest_news();
        $total_page = ceil($total_page / POST_PER_PAGE);
        $this->template->set('css', $this->css);
        $this->template->set('page', $page);
        $this->template->set('post', $post);
        $this->template->set('title', "ข่าวสารทั้งหมด");
        $this->template->set('total_page', $total_page);
        $this->template->load(null, 'default_plain.php');
    }

    public function category_group($post_id) {
        if (is_numeric($post_id)) {
            $cate_id = $this->input->get('cate_id');
            $post = get_post_by_id($post_id);
            $raws_element = json_decode($post->post_detail);
            $category = array();

            foreach ($raws_element as $each_element) {
                if ($each_element->type == "category") {
                    array_push($category, $each_element);
                }
            }


            foreach ($category as &$each_cate) {
                $c = explode("/", $each_cate->url);
                $each_cate->cate_id = $c[5];
            }

            $cate_id = is_numeric($cate_id) ? $cate_id : $category[0]->cate_id;

            $page = $this->input->get('page');
            $page = is_numeric($page) ? $page : 0;

            $offset = POST_PER_PAGE * $page;

            $cate_info = get_category_by_cate_id($cate_id);
            $all_post = get_all_post_in_cate($cate_id, POST_PER_PAGE, $offset);
            $total_post = get_number_of_post_in_cate($cate_id);
            $total_page = ceil($total_post / POST_PER_PAGE);

            $this->template->set('css', $this->css);
            $this->template->set('title', $post->post_excerp . " &raquo; " . $cate_info->cate_name);
            $this->template->set('cate_id', $cate_id);
            $this->template->set('category', $category);

            $this->template->set('all_post', $all_post);
            $this->template->set('total_page', $total_page);
            $this->template->set('page', $page);

            $this->template->load(null, 'default_plain.php');
        } else {
            redirect(site_url());
        }
    }

    public function mfu_reader($mid) {
        $media_info = get_media_mid($mid);
        $this->template->set('css', $this->css);
        $this->template->set('js', js_asset("bookflip.js"));
        $this->template->set('title', "{$media_info->media_name} | PDF Reader");
        $this->template->set('media_info', $media_info);
        $this->template->set('post_stat_id', "mid_{$mid}");
        $this->template->load(null, 'default_plain.php');
    }

    public function newsletter() {
        $cate_id = $this->input->get('cate_id');
        $page = $this->input->get('page');
        $user_id = $this->input->get('user_id');
        $year = $this->input->get('year');
        $month = $this->input->get('month');
        $search = $this->input->get('q');

        $all_month = $this->post->get_group_month_of_post_by_type(TYPE_NEWSLETTER);
        $all_year = $this->post->get_group_year_of_post_by_type(TYPE_NEWSLETTER);

        $cate_id = is_numeric($cate_id) ? $cate_id : 0;
        $page = is_numeric($page) ? $page : 1;
        $user_id = is_numeric($user_id) ? $user_id : 0;
        $month = is_numeric($month) ? $month : 0;
        $year = is_numeric($year) ? $year : 0;

        $this->template->set("title", "Newsletter");
        $this->template->set('css', $this->css);
        $this->template->set('page', $page);

        $this->template->set('month', $month);
        $this->template->set('year', $year);
        $this->template->set('q', $search);

        $this->template->set('all_month', $all_month);
        $this->template->set('all_year', $all_year);
        $this->template->set('cate_id', $cate_id);

        $this->template->set('post', $this->post->get_post_by_status(POST_PUBLISH, TYPE_NEWSLETTER, $user_id, $cate_id, $page, 21, 'desc', false, $day = 0, $month, $year, $post_excerp = 0, $backend = false, $search));
        $this->template->set('total_page', $this->post->get_number_of_page(POST_PUBLISH, TYPE_NEWSLETTER, $user_id, $cate_id, 21, false, $day = 0, $month, $year));
        $this->template->load(null, 'default_plain.php');
    }

    public function clipping() {
        $cate_id = $this->input->get('cate_id');
        $page = $this->input->get('page');
        $user_id = $this->input->get('user_id');
        $year = $this->input->get('year');
        $month = $this->input->get('month');
        $search = $this->input->get('q');

        $all_month = $this->post->get_group_month_of_post_by_type(TYPE_CLIPPING);
        $all_year = $this->post->get_group_year_of_post_by_type(TYPE_CLIPPING);

        $cate_id = is_numeric($cate_id) ? $cate_id : 0;
        $page = is_numeric($page) ? $page : 1;
        $user_id = is_numeric($user_id) ? $user_id : 0;
        $month = is_numeric($month) ? $month : 0;
        $year = is_numeric($year) ? $year : 0;

        $this->template->set("title", "News Clipping");
        $this->template->set('css', $this->css);
        $this->template->set('cate_id', $cate_id);
        $this->template->set('page', $page);

        $this->template->set('month', $month);
        $this->template->set('year', $year);
        $this->template->set('q', $search);

        $this->template->set('all_month', $all_month);
        $this->template->set('all_year', $all_year);

//        $this->template->set('clipping_year', $this->post->get_group_year_special_post(TYPE_CLIPPING));

        $this->template->set('post', $this->post->get_post_by_status(POST_PUBLISH, TYPE_CLIPPING, $user_id, $cate_id, $page, 25, 'desc', false, $day = 0, $month, $year, $post_excerp = 0, $backend = false, $search));

        $this->template->set('total_page', $this->post->get_number_of_page(POST_PUBLISH, TYPE_CLIPPING, $user_id, $cate_id, 25, false, $day = 0, $month, $year));
        $this->template->load(null, 'default_plain.php');
    }

    public function organization() {
        $this->template->set('title', 'Organization');
        $this->template->set('css', $this->css);
        $this->template->load(null, 'default_plain.php');
    }

    public function search() {
        $q = $this->input->get('q');
        $this->template->set('title', isset($q) ? "Search: {$q}" : "Search");
        $this->template->set('css', $this->css);
        $this->template->load(null, 'default_plain.php');
    }

    public function bachelor() {
        $url = "bachelor.php";
        $title = LANG == 'th' ? "หลักสูตรปริญญาตรี" : "Bachelor program";
        $this->program_school($url, $title);
    }

    public function doctorate() {
        $url = "doctorate.php";
        $title = LANG == 'th' ? "หลักสูตรปริญญาเอก" : "Doctorate program";
        $this->program_school($url, $title);
    }

    public function master() {
        $url = "master.php";
        $title = LANG == 'th' ? "หลักสูตรปริญญาโท" : "Master program";
        $this->program_school($url, $title);
    }

    public function program_school($url, $title) {
        $root = "http://www.mfu.ac.th/2012/";
        $url = $root . $url;
        $html = file_get_contents($url);
        phpQuery::newDocumentHTML($html);

        pq('.droplet_here')->addClass("table table-striped");

        foreach (pq('.droplet_here')->find('a') as $each_anchor) {
            $link = pq($each_anchor)->attr('href');
            pq($each_anchor)->attr('target', '_blank');
            pq($each_anchor)->attr('href', $root . $link);
        }

        foreach (pq('.droplet_here')->find('img') as $each_anchor) {
            $link = pq($each_anchor)->attr('src');
            pq($each_anchor)->attr('src', $root . $link);
            if (pq($each_anchor)->is("[width=11]")) {
                pq($each_anchor)->removeAttr("width");
                pq($each_anchor)->removeAttr("height");
                pq($each_anchor)->attr('style', " padding:0px 5px; ");
            }
        }

        foreach (pq('[bgcolor=#5c1605]') as $each_rec) {
            pq($each_rec)->attr('style', "color: #fff; ");
        }

        foreach (pq('[bgcolor=#06364a]') as $each_rec) {
            pq($each_rec)->attr('style', "color: #fff; ");
        }

        foreach (pq('[bgcolor=#06364A]') as $each_rec) {
            pq($each_rec)->attr('style', "color: #fff; ");
        }

        foreach (pq('.center') as $each_rec) {
            pq($each_rec)->addClass('txt_center th_san font_24');
        }

        foreach (pq("[width=95%]") as $each_rec) {
            pq($each_rec)->removeAttr("width");
        }

        foreach (pq("[width=680]") as $each_rec) {
            pq($each_rec)->removeAttr("width");
        }

        $tb = pq('.droplet_here')->clone();

        $this->template->set('title', $title);
        $this->template->set('css', $this->css);
        $this->template->set('data', $tb);
        $this->template->load('mfu/program.php', 'default_plain.php');
    }

    public function debug() {
        
    }

    public function testx() {
        $this->template->set('title', "ชื่อหน้าที่ต้องการสร้าง");
        $this->template->set('css', $this->css);
        $this->template->load(null, 'default_plain.php');
    }

}

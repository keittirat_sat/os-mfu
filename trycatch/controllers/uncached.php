<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of uncached
 *
 * @author TryCatch
 */
class Uncached extends CI_Controller {

    public function stat() {
        $key = $this->input->get('key', true);
        if (!$key) {
            $key = "main_stat";
        }


        $user_route = $this->session->userdata('user_route');

        if (!is_array($user_route)) {
            $user_route = array();
        }

        if (!array_key_exists($key, $user_route)) {
            $counter = $this->admin->set_stat($key);
        } else {
            $stat = $this->admin->get_stat($key);
            $counter = $stat[0]->counter;
        }
        $user_route[$key] = $counter;
        $this->session->set_userdata('user_route', $user_route);

        echo json_encode(array('ref_id' => $key, 'counter' => number_format($counter, 0, '.', ',')));
    }

    public function sendmail() {
        $post = $this->input->post();
        if ($post) {
            $contact = $this->post->get_meta('contactus');
            $target = json_decode($contact[0]->meta_info);

            $subject = "[Presidential Hotline] {$name} " . date('d F Y');
            if (server_mail($target, $subject, $post['contact_detail'])) {
                $data['status'] = "success";
            } else {
                $data['status'] = "fail";
            }
            echo json_encode($data);
        } else {
            redirect(site_url());
        }
    }

    public function test_mail() {
        print_r(server_mail("keittirat@trycatch.in.th", "TEST SMTP Mail", "ทดสอบระบบ"));
    }

    public function clear_cache() {
        $this->output->clear_all_cache();
    }

}

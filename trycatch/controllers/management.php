<?php

class Management extends CI_Controller {

    private $locale_month;
    private $locale_short_month;

    public function __construct() {
        parent::__construct();
        $this->locale_month = unserialize(FULL_MONTH);
        $this->locale_short_month = unserialize(SHORT_MONTH);

        $this->template->set('locale_month', $this->locale_month);
        $this->template->set('locale_short_month', $this->locale_short_month);
    }

    public function index() {
        //login Page
        if (loged()) {
            redirect(site_url('management/welcome'));
        } else {
            $this->template->set('css', css_asset("trycatch_admin.css"));
            $this->template->load();
        }
    }

    public function welcome() {
        if (loged()) {
            $this->load->library('site_analytic');
            $this->template->set('title', 'Welcome : ' . get_user_realname_loged());
            $this->template->set('visitor_pageview', $this->site_analytic->get_visitor_pageview());
            $this->template->set('keyword', $this->site_analytic->get_keyword());
            $this->template->set('site_referrer', $this->site_analytic->get_user_referrer());
            $stat = $this->admin->get_stat('main_stat');
            $this->template->set('local_stat', number_format($stat[0]->counter));
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function all_category() {
        if (loged()) {
            $cate_type = $this->input->get('cate_type');
            $cate_type = is_numeric($cate_type) ? $cate_type : CATE_POST;

            $this->template->set('title', 'Category');
            $this->template->set('cate_type', $cate_type);
            $this->template->set('all_api', $this->post->get_all_api());
            $this->template->set('temp', get_all_category($cate_type));
            $this->template->load('management/all_category_2');
        } else {
            redirect(site_url('management'));
        }
    }

    public function all_slide() {
        if (loged()) {
            $status = $this->input->get("status");
            $status = empty($status) ? "allslide" : trim($status);

            $q = $this->input->get("q");
            $q = empty($q) ? "" : trim($q);

            $type = $this->input->get("type");
            $type = is_numeric($type) ? $type : 0;

            $page = $this->input->get('page');
            $month = $this->input->get('month');
            $year = $this->input->get('year');

            $month = is_numeric($month) ? $month : 0;
            $year = is_numeric($year) ? $year : 0;
            $page = is_numeric($page) ? $page : 1;

            switch ($status) {
                case "allslide":
                    $slide_real_status = "allslide";
                    break;
                case "online":
                    $slide_real_status = 1;
                    break;
                default:
                    $slide_real_status = 0;
                    break;
            }

            $group_year = $this->post->get_group_year_of_post_by_type(TYPE_SLIDE);

            $css = css_asset('datepicker.css');
            $js = js_asset('bootstrap-datepicker.js');
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('status', $status);
            $this->template->set('month', $month);
            $this->template->set('q', $q);
            $this->template->set('year', $year);
            $this->template->set('type', $type);
            $this->template->set('group_year', $group_year);
            $this->template->set('title', 'Slide');
            $this->template->set('slide', $this->post->get_all_slide_by_status($slide_real_status, $q, $type, $month, $year));
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function post_operation() {
        $this->post_page_operation(TYPE_POST);
    }

//    public function all_post() {
//        $this->post_page_all(TYPE_POST);
//    }

    public function all_post() {
        if (loged()) {
            $this->template->set('title', "All Post");
            $this->template->set('all_user_in_system', $this->admin->get_all_users_info(get_user_level() == USER_ADMIN ? 0 : get_user_uid() ));

            //Default filter
            $users = $this->input->get('users');
            $post_status = $this->input->get('post_status');
            $page = $this->input->get('page');
            $cate_id = $this->input->get('cate_id');

            //Apply custom filter
            $users = is_numeric($users) ? $users : (get_user_level() == USER_WRITER ? get_user_uid() : 0);
            $post_status = is_numeric($post_status) ? $post_status : POST_PUBLISH;
            $page = is_numeric($page) ? $page : 1;
            $cate_id = is_numeric($cate_id) ? $cate_id : 0;

            $this->template->set('users', $users);
            $this->template->set('post_status', $post_status);
            $this->template->set('page', $page);
            $this->template->set('cate_id', $cate_id);

            $this->template->set('operation_mode', "Post");

            //Set Pagination
            $this->template->set('total_page', $this->post->get_number_of_page($post_status, TYPE_POST, $users, $cate_id, 20, false, $special_year = 0, $month = 0, $year = 0, $post_excerp = 0, $backend = true));

            //Retrieve Post
            $this->template->set('post', $this->post->get_post_by_status($post_status, TYPE_POST, $users, $cate_id, $page, 20, 'desc', false, $special_year = 0, $month = 0, $year = 0, $post_excerp = 0, $backend = true));

            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function page_operation() {
        $this->post_page_operation(TYPE_PAGE);
    }

//    public function all_page() {
//        $this->post_page_all(TYPE_PAGE);
//    }

    public function all_page() {
        if (loged()) {
            $this->template->set('title', "All Page");
            $this->template->set('all_user_in_system', $this->admin->get_all_users_info());

            //Default filter
            $users = $this->input->get('users', true);
            $post_status = $this->input->get('post_status', true);
            $page = $this->input->get('page', true);

            //Apply custom filter
            $users = is_numeric($users) ? $users : 0;
            $post_status = is_numeric($post_status) ? $post_status : POST_PUBLISH;
            $page = is_numeric($page) ? $page : 1;

            $this->template->set('users', $users);
            $this->template->set('post_status', $post_status);
            $this->template->set('page', $page);

            $this->template->set('operation_mode', "Page");

            //Set Pagination
//            $this->template->set('total_page', $this->post->get_number_of_page($post_status, TYPE_PAGE, $users));
            //Retrieve Post
//            $this->template->set('post', $this->post->get_post_by_status($post_status, TYPE_PAGE, $users, 0, $page));
            //Set Pagination
            $this->template->set('total_page', $this->post->get_number_of_page($post_status, TYPE_PAGE, $users, 0, 20, false));

            //Retrieve Post
            $this->template->set('post', $this->post->get_post_by_status($post_status, TYPE_PAGE, $users, 0, $page, 20, 'desc', false));


            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function all_school() {
//        $this->post_page_all(TYPE_SCHOOL);
        if (loged()) {
            $this->template->set('title', "All School");
            $this->template->set('all_user_in_system', $this->admin->get_all_users_info());

            //Default filter
            $users = $this->input->get('users', true);
            $post_status = $this->input->get('post_status', true);
            $page = $this->input->get('page', true);

            //Apply custom filter
            $users = is_numeric($users) ? $users : 0;
            $post_status = is_numeric($post_status) ? $post_status : POST_PUBLISH;
            $page = is_numeric($page) ? $page : 1;

            $this->template->set('users', $users);
            $this->template->set('post_status', $post_status);
            $this->template->set('page', $page);

            $this->template->set('operation_mode', "Page");

            //Set Pagination
            $this->template->set('total_page', $this->post->get_number_of_page($post_status, TYPE_SCHOOL, $users));

            //Retrieve Post
            $this->template->set('post', $this->post->get_post_by_status($post_status, TYPE_SCHOOL, $users, 0, $page));

            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function new_gallery() {
        if (loged()) {
            $post_id = $this->input->get('post_id');
            if (!is_numeric($post_id)) {
                $post_info = $this->post->reserv_post(TYPE_GALLERY);
                $post_id = $post_info->post_id;
                $this->template->set("title", "New Album");
            } else {
                $post_info = $this->post->get_post_by_id($post_id);
                if ($post_info->post_title) {
                    $this->template->set("title", "Edit Album: {$post_info->post_title}");
                } else {
                    $this->template->set("title", "Darft mode");
                }
            }

            $this->template->set('category', get_all_category(CATE_ALBUM));
            $this->template->set('reserv_post', $post_info);
            $all_image = group_obj_by_rows(get_all_media_sort_by_name($post_id));
            $this->template->set('all_image', $all_image);
            $this->template->load();
        } else {
            redirect('management');
        }
    }

    private function post_page_all($type = TYPE_POST) {
        if (loged()) {
            $this->template->set('title', ($type == TYPE_POST) ? "All Posts" : "All Pages");
            $this->template->set('all_user_in_system', $this->admin->get_all_users_info());

            //Default filter
            $users = $this->input->get('users', true);
            $category = $this->input->get('category', true);
            $post_status = $this->input->get('post_status', true);
            $page = $this->input->get('page', true);

            //Apply custom filter
            $users = ($users === false) ? 0 : $users;
            $category = ($category === false) ? 0 : $category;
            $post_status = ($post_status === false) ? 1 : $post_status;
            $page = ($page === false) ? 1 : $page;

            $this->template->set('users', $users);
            $this->template->set('post_status', $post_status);
            $this->template->set('category', $category);
            $this->template->set('page', $page);
            $this->template->set('type_post', $type);

            switch ($type) {
                case TYPE_POST:
                    $this->template->set('type_link', "post_operation");
                    $this->template->set('operation_mode', "Post");
                    break;
                case TYPE_PAGE:
                    $this->template->set('type_link', "page_operation");
                    $this->template->set('operation_mode', "Page");
                    break;
                case TYPE_SCHOOL:
                    $this->template->set('type_link', "school_operation");
                    $this->template->set('operation_mode', "School");
                    break;
            }

            //Set Pagination
            $this->template->set('total_page', $this->post->get_number_of_page($post_status, $type, $users, $category));

            //Retrieve Post
            $this->template->set('post', $this->post->get_post_by_status($post_status, $type, $users, $category, $page));

            $this->template->load('management/all_post');
        } else {
            redirect(site_url('management'));
        }
    }

    private function post_page_operation($type = TYPE_POST) {
        if (loged()) {
            $post_id = $this->input->get('post_id');
            $display = $this->input->get('display');

            $css = css_asset('datepicker.css');
            $js = js_asset('bootstrap-datepicker.js');
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('display', $display);

            if (is_numeric($post_id)) {
                $post_info = $this->post->get_post_by_id($post_id);

                if ($post_info->post_title) {
                    $this->template->set("title", "Edit " . (($type == TYPE_PAGE) ? "Page" : "Post") . ": " . $post_info->post_title);
                } else {
                    $this->template->set("title", "Darfting mode");
                }

                //Add New Revision
                $this->template->set("form_url", site_url("api/add_custom_post"));
                $this->template->set("revision", TYPE_REVISION);

                //GET Revision
                $post_revision = $this->post->get_revision($display == "revision" && $post_info->revision != 0 ? $post_info->revision : $post_id);

                if (count($post_revision)) {

                    $post_revision = array_reverse($post_revision);

                    //When retriev data in revison
                    if ($display != "revision" || $post_info->revision == 0) {
                        array_push($post_revision, $post_info);
                    } else {
                        //State Normal
                        $master_post = $this->post->get_post_by_id($post_info->revision);
                        array_push($post_revision, $master_post);
                    }

                    if ($display != "revision") {
                        $post_info = clone $post_revision[0];

                        $this->template->set('real_revison_post_id', $post_info->post_id);

                        $post_info->post_id = $post_id;
                    } else {
                        $this->template->set('real_revison_post_id', $post_id);
                    }

                    $this->template->set("post_revision", $post_revision);
                }
            } else {
                $post_info = $this->post->reserv_post($type);
                $this->template->set("title", "New " . (($type == TYPE_PAGE) ? "Page" : "Post"));

                //Add new POST
                $this->template->set("form_url", site_url('api/update_post'));
            }

            $post_info->post_type = $type;
            $this->template->set('reserv_post', $post_info);
            $this->template->load('management/post_operation');
        } else {
            redirect(site_url('management'));
        }
    }

    public function new_account() {
        if (loged()) {
            $this->template->set('title', 'Add new account');
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function all_account() {
        if (loged()) {
            $level = $this->input->get('level');
            $level = is_numeric($level) ? $level : 0;
            $this->template->set('title', 'All Users account');
            $this->template->set('level', $level);
            $this->template->set('users', $this->admin->get_all_account($level));
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function school_operation() {
        if (loged()) {
            $post_id = $this->input->get('post_id');
            if (!empty($post_id)) {
                $post_info = $this->post->get_post_by_id($post_id);

                if ($post_info->post_title) {
                    $this->template->set("title", "Edit School: " . $post_info->post_title);
                } else {
                    $this->template->set("title", "Darfting mode");
                }

                if (!empty($post_info->post_detail)) {
                    $post_detail = (array) json_decode($post_info->post_detail);
                } else {
                    $post_detail = "";
                }

                $this->template->set('post_detail', $post_detail);
            } else {
                $post_info = $this->post->reserv_post(TYPE_SCHOOL);
                $this->template->set("title", "New School");
            }
            $this->template->set('reserv_post', $post_info);
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function menu_main() {
        if (loged()) {
            $all_link = $this->post->get_group_link();
            $this->template->set("all_link", $all_link);
            $this->template->set("title", "Menu configuration");
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function widget_blocking() {
        if (loged()) {
            $this->template->set("title", "Widget and Blocking");
            $all_link = $this->post->get_group_link();
            $this->template->set("all_link", $all_link);
            $this->template->set("widget", $this->post->get_widget());
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function external_link() {
        if (loged()) {
            $this->template->set("title", "External Links");
            $post_id = $this->input->get('post_id');
            $all_link = $this->post->get_group_link();
            if (count($all_link)) {
                $post_id = (!empty($post_id)) ? $post_id : $all_link[0]->post_id;
            } else {
                $post_id = false;
            }
            $this->template->set("all_link", $all_link);
            $this->template->set("post_id", $post_id);
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function audience_guideline() {
        if (loged()) {
            $this->template->set("title", "Audience Guildline System");
            $post_id = $this->input->get('post_id');
            $all_link = $this->post->get_audience();
            if (count($all_link)) {
                $post_id = (!empty($post_id)) ? $post_id : $all_link[0]->post_id;
            } else {
                $post_id = false;
            }
            $this->template->set("all_link", $all_link);
            $this->template->set("post_id", $post_id);
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function all_event() {
        if (loged()) {
            $post_status = $this->input->get('post_status');
            $post_status = is_numeric($post_status) ? $post_status : POST_PUBLISH;

            $uid = $this->input->get('uid');
            $uid = is_numeric($uid) ? $uid : 0;

            $cate_id = $this->input->get('cate_id');
            $cate_id = is_numeric($cate_id) ? $cate_id : 0;

            $page = $this->input->get('page');
            $page = is_numeric($page) ? $page : 1;

            $this->template->set("title", "Event management");
            $this->template->set("cate_id", $cate_id);
            $this->template->set("uid", $uid);
            $this->template->set("page", $page);
            $category = get_all_category(CATE_EVENT);
            $this->template->set("category", $category);

            foreach ($category as $each_cate) {
                $re_cate[$each_cate->cate_id] = $each_cate;
            }

            $this->template->set("re_cate", $re_cate);
            $this->template->set("post_status", $post_status);
            $this->template->set('all_user_in_system', $this->admin->get_all_users_info());

            $this->template->set('total_page', $this->post->get_number_of_page($post_status, TYPE_EVENT, $uid, $cate_id));
            $this->template->set("post", $this->post->get_post_by_status($post_status, TYPE_EVENT, $uid, $cate_id, $page));
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function event_operation() {
        $this->post_page_operation(TYPE_EVENT);
    }

    public function all_media() {
        if (loged()) {
            //Search
            $q = $this->input->get('q');
            $q = empty($q) ? null : trim($q);

            //Start Page
            $startPage = $this->input->get('startpage');
            $startPage = is_numeric($startPage) ? $startPage : 0;

            //Page
            $page = $this->input->get('page');
            $page = is_numeric($page) ? $page : (0 + $startPage);
            $perpage = POST_PER_PAGE * 2;
            //$perpage = 5;
            $offset = $perpage * $page;

            //file_type
            $type = $this->input->get("type");
            $type = empty($type) ? "all" : $type;
            $type = $type == "all" ? 0 : $type;

            $total_page = $this->post->get_number_of_media($type, $q);
            $total_page = ceil($total_page / $perpage);

            $media = $this->post->get_all_media(0, 'desc', $perpage, $offset, $type, $q);

            $this->template->set("title", "Media store");
            $this->template->set("startpage", $startPage);
            $this->template->set("page", $page);
            $this->template->set("total_page", $total_page);
            $this->template->set("post", $media);
            $this->template->set("type", $type);
            $this->template->set("q", $q);
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function all_gallery() {
        if (loged()) {
            $post_status = $this->input->get('post_status');
            $post_status = is_numeric($post_status) ? $post_status : POST_PUBLISH;

            $page = $this->input->get('page');
            $page = is_numeric($page) ? $page : 0;

            $this->template->set('title', 'Gallery');
            $this->template->set('page', $page);
            $this->template->set('post_status', $post_status);
            $this->template->set('post', $this->post->get_post_by_status($post_status, TYPE_GALLERY, 0, 0, $page + 1));
            $this->template->set('total_page', $this->post->get_number_of_page($post_status, TYPE_GALLERY));
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function api() {
        if (loged()) {
            $this->template->set('title', 'Application programming interface');
            $this->template->set('post', $this->post->get_all_api());
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function clipping() {
        if (loged()) {
            $cate_id = $this->input->get('cate_id');
            $page = $this->input->get('page');
            $user_id = $this->input->get('user_id');

            $cate_id = is_numeric($cate_id) ? $cate_id : 0;
            $page = is_numeric($page) ? $page : 1;
            $user_id = is_numeric($user_id) ? $user_id : 0;

            $css = css_asset('datepicker.css');
            $js = js_asset('bootstrap-datepicker.js');

            $this->template->set('css', $css);
            $this->template->set('js', $js);

            $this->template->set('title', 'News Clipping');
            $this->template->set('all_user_in_system', $this->admin->get_all_users_info());
            $this->template->set('cate_id', $cate_id);
            $this->template->set('page', $page);
            $this->template->set('user_id', $user_id);
            $this->template->set('category', get_all_category(CATE_CLIPPING));
            $this->template->set('post', $this->post->get_post_by_status(POST_PUBLISH, TYPE_CLIPPING, $user_id, $cate_id, $page));
            $this->template->set('total_page', $this->post->get_number_of_page(POST_PUBLISH, TYPE_CLIPPING, $user_id, $cate_id, 20));
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function newsletter() {
        if (loged()) {
            $cate_id = $this->input->get('cate_id');
            $page = $this->input->get('page');
            $user_id = $this->input->get('user_id');

            $cate_id = is_numeric($cate_id) ? $cate_id : 0;
            $page = is_numeric($page) ? $page : 1;
            $user_id = is_numeric($user_id) ? $user_id : 0;

            $css = css_asset('datepicker.css');
            $js = js_asset('bootstrap-datepicker.js');

            $this->template->set('css', $css);
            $this->template->set('js', $js);

            $this->template->set('title', 'Newsletter');
            $this->template->set('all_user_in_system', $this->admin->get_all_users_info());
            $this->template->set('cate_id', $cate_id);
            $this->template->set('page', $page);
            $this->template->set('user_id', $user_id);
            $this->template->set('post', $this->post->get_post_by_status(POST_PUBLISH, TYPE_NEWSLETTER, $user_id, $cate_id, $page));
            $this->template->set('total_page', $this->post->get_number_of_page(POST_PUBLISH, TYPE_NEWSLETTER, $user_id, $cate_id, 20));
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function edit_account() {
        if (loged()) {
            $current_uid = get_user_uid();
            $current_level = get_user_level();
            $uid = $this->input->get('uid');
            if (is_numeric($uid)) {
                $user_info = get_user_by_uid($uid);
                if ($uid == $current_uid || $current_level == USER_ADMIN || $current_level == USER_DIRECTOR) {
                    $permission = false;
                    if ($uid != $current_uid) {
                        if ($user_info->level == USER_WRITER) {
                            $permission = true;
                        } else if ($user_info->level == USER_DIRECTOR) {
                            if ($current_level == USER_ADMIN) {
                                $permission = true;
                            }
                        }
                    } else {
                        $permission = true;
                    }

                    if ($permission) {
                        //Permission Passed
                        $this->template->set('all_director', $this->admin->get_all_account(USER_DIRECTOR));
                        $this->template->set('title', "Update: {$user_info->real_name}");
                        $this->template->set('user_info', $user_info);
                        $this->template->load();
                    } else {
                        redirect('management/all_account');
                    }
                } else {
                    redirect('management/all_account');
                }
            } else {
                redirect('management/all_account');
            }
        } else {
            redirect(site_url('management'));
        }
    }

    public function research() {
        if (loged()) {
            $position = $this->input->get('position');
            $page = $this->input->get('page');
            $cate_id = $this->input->get('cate_id');
            $user_id = $this->input->get('user_id');

            $position = is_numeric($position) ? $position : 0;
            $page = is_numeric($page) ? $page : 1;
            $cate_id = is_numeric($cate_id) ? $cate_id : 0;
            $user_id = is_numeric($user_id) ? $user_id : 0;

            $this->template->set('title', "Research Management");
            $this->template->set('position', $position);
            $this->template->set('cate_id', $cate_id);
            $this->template->set('page', $page);
            $this->template->set('category', $this->post->get_post_by_status(POST_PUBLISH, TYPE_SCHOOL, $user_id, 0, 1, 200));

            $this->template->set('post', $this->post->get_post_by_status(POST_PUBLISH, TYPE_RESEARCH, 0, $cate_id, $page, $total_per_page = 20, $order_by = 'desc', $schedule = true, $special_year = 0, $month = 0, $year = 0, $position));
            $this->template->set('total_page', $this->post->get_number_of_page(POST_PUBLISH, TYPE_RESEARCH, 0, $cate_id, $total_per_page = 20, $schedule = true, $special_year = 0, $month = 0, $year = 0, $position));
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function new_researcher() {
        if (loged()) {
            $post_id = $this->input->get('post_id');
            if (is_numeric($post_id)) {
                $post_info = get_post_by_id($post_id, false);
                $name = json_decode($post_info->post_title);
                $this->template->set('title', "Update: " . (is_object($name) ? $name->en . " | " . $name->th : "Anonymous"));
            } else {
                $post_info = $this->post->reserv_post(TYPE_RESEARCH);
                $post_id = $post_info->post_id;
                $this->template->set('title', "New Researcher");
            }
            $this->template->set('reserve_post', $post_info);
            $this->template->set('pdf_file', $this->post->get_all_media($post_id, 'desc', null, null, 'pdf'));
            $this->template->set('category', $this->post->get_post_by_status(POST_PUBLISH, TYPE_SCHOOL, 0, 0, 1, 200));

            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

}

?>

<?php

class operation extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-Type: text/plain; charset=utf-8");
        header('Access-Control-Allow-Origin: *');
        if (!loged()) {
            //if doesn't login
            echo json_encode(array('status' => 'relogin'));
            exit;
        } else {
            $this->output->clear_all_cache();
        }
    }

    public function store_category() {
        $category = $this->input->post();
        if ($this->admin->store_category($category)) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function edit_category() {
        $category = $this->input->post();
        $cate_id = $category['cate_id'];
        unset($category['cate_id']);
        if ($this->admin->update_cate_name($cate_id, $category)) {
            $post['status'] = 'success';
        } else {
            $post['status'] = 'fail';
        }
        echo json_encode($post);
    }

    public function del_category() {
        $cate_id = $this->input->get_post('cate_id');
        if ($this->admin->del_cate($cate_id)) {
            $post['status'] = 'success';
        } else {
            $post['status'] = 'fail';
        }
        echo json_encode($post);
    }

}

?>

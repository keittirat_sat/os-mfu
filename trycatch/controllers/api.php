<?php

class Api extends CI_Controller {

    private $clear_cache;

    public function __construct() {
        parent::__construct();
        header("Content-Type: text/plain; charset=utf-8");
        header('Access-Control-Allow-Origin: *');
        $this->output->clear_all_cache();
    }

    public function login() {
        $data = $this->input->post();

        $auth_result = $this->auth->take_login($data);
        if ($auth_result) {
            $this->auth->set_login_session($auth_result);
            echo json_encode(array('status' => 'success', 'routing' => site_url('management/welcome')));
        } else {
            echo json_encode(array('status' => 'fail'));
        }
    }

    public function get_all_category() {
        $all_cat = get_all_category();
        foreach ($all_cat as &$rec) {
            $rec->total_post = get_number_of_post_in_cate($rec->cate_id);
            $rec->all_post = site_url('management/all_post/' . $rec->cate_id);
            $rec->edit_link = site_url('operation/edit_category/' . $rec->cate_id);
            $rec->del_link = site_url('operation/del_category/' . $rec->cate_id);
        }
        echo json_encode($all_cat);
    }

    public function logout() {
        signout();
        redirect(site_url('management'));
    }

    /* Administrator Operation */

    public function reserve_post_record() {
        $type = $this->input->get_post('type');
        $post_info = $this->post->insert_empty_post($type);
        echo json_encode($post_info);
    }

    public function add_custom_post() {
        $post = $this->input->post();
        $rev_id = 0;

        if (array_key_exists('real_revison_post_id', $post)) {
            $rev_id = $post['real_revison_post_id'];
            unset($post['real_revison_post_id']);
        }

        if (array_key_exists('enable_schedule', $post)) {
            unset($post['enable_schedule']);
        }

        if (array_key_exists('hidden_expire', $post)) {
            unset($post['hidden_expire']);
        }

        //Check post_status
        //Drop display

        if (get_user_level() != USER_WRITER && (($post['post_status'] == POST_PUBLISH) || ($post['post_status'] == POST_REVIEW)) && $post['revision'] != 0) {
            //Update Master
            $post_id = $post['revision'];
            unset($post['revision']);
            unset($post['post_type']);
            unset($post['post_author']);

            if ($rev_id != $post_id) {
//                $this->post->delete_post($rev_id, true);
                $this->post->delete_all_revision_by_parent_id($post_id);
            }

            $post_info = $this->post->update_post($post, $post_id);
        } else if ($post['post_status'] == POST_DRAFT) {
            $post_id = $rev_id == 0 ? $post['revision'] : $rev_id;
            $check_post = get_post_by_id($post_id);

            if ($check_post->post_type != TYPE_REVISION) {
                //Old condition compare with POST_PUBLISH
                //if current post is Publish system will make a new revision
                $post_info = $this->post->insert_post_with_data($post);
            } else {
                //if current post isn't Publish system will update
                if ($post_id == $post['revision']) {
                    $post['revision'] = 0;
                }
                $post_info = $this->post->update_post($post, $post_id);
            }
        } else if ($post['post_status'] == POST_REVIEW && get_user_level() == USER_WRITER) {
            //Send post to director
            $update_post_id = $post['revision'];

//            if ($rev_id == $post['revision']) {
//                $post['revision'] = 0;
//            }
            //$post_info = $this->post->update_post($post, $rev_id);
            $post_info = $this->post->insert_post_with_data($post);

            $data['method'] = "reviwe";
            notify_post_update($update_post_id);
        } else {
            //if new version isn't draft system will make a new revision
            $post_info = $this->post->insert_post_with_data($post);
        }

        if ($post_info) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }

        echo json_encode($data);
    }

    private function resize_image($image_path, $size, $new_image = null) {
        //Setup Image manipulation procedure
        $config_x = array();
        //$config['image_library'] = 'gd2';
        $config_x['source_image'] = $image_path;
        if ($new_image) {
            $config_x['new_image'] = $new_image;
        }
        $config_x['create_thumb'] = FALSE;
        $config_x['maintain_ratio'] = TRUE;
        $config_x['width'] = $size;
        $config_x['height'] = $size;

        //clear old config
        $this->image_lib->clear();

        //Re initial
        $this->image_lib->initialize($config_x);

        //Resize
        return $this->image_lib->resize();
    }

    public function get_media_by_post_id() {
        $post_id = $this->input->get_post('post_id');
        $media_type = $this->input->get_post('media_type');
        if (isset($media_type)) {
            if ($media_type == 'pdf' || $media_type == 'img' || $media_type == 'other' || $media_type == 'vdo') {
                
            } else {
                $media_type = 0;
            }
        } else {
            $media_type = 0;
        }
        $media = $this->post->get_all_media($post_id, $order_by = 'desc', $limit = null, $offset = null, $media_type);
        foreach ($media as $rec) {
            get_media_block($rec, $post_id);
        }
    }

    public function get_raws_upload_media() {
        $post_id = $this->input->get_post('post_id');
        $media_type = $this->input->get_post('media_type');
        if (isset($media_type)) {
            if ($media_type == 'pdf' || $media_type == 'img' || $media_type == 'other' || $media_type == 'vdo') {
                
            } else {
                $media_type = 0;
            }
        } else {
            $media_type = 0;
        }
        $media = $this->post->get_all_media($post_id, $order_by = 'desc', $limit = null, $offset = null, $media_type);
        echo json_encode($media);
    }

    public function get_raws_media_by_post_id() {
        $post_id = $this->input->get_post('post_id');
        $media = $this->post->get_all_media($post_id);

        foreach ($media as $rec) {
            get_media_block_2($rec, $post_id);
        }
    }

    public function upload_clip() {
        $this->upload_special_method(TYPE_CLIPPING);
    }

    public function upload_slide() {
        $this->upload_special_method(TYPE_SLIDE);
    }

    public function upload_special_method($type) {
        $post = $this->input->post();

        if (array_key_exists('enable_schedule', $post)) {
            unset($post['enable_schedule']);
        }

        if (array_key_exists('hidden_expire', $post)) {
            unset($post['hidden_expire']);
        }

        $this->post->insert_post_with_data($post);

        $post_id = $this->post->get_last_record(POST_PUBLISH, $type);
        $this->upload_single_media($post_id->post_id);
    }

    //Update Special Post
    public function update_slide() {
        $this->update_special_method(TYPE_SLIDE);
    }

    public function update_special_method($type) {
        $post = $this->input->post();
        $post_id = $post['post_id'];
        unset($post['post_id']);
        if (array_key_exists('enable_schedule', $post)) {
            unset($post['enable_schedule']);
        }

        if (array_key_exists('hidden_expire', $post)) {
            unset($post['hidden_expire']);
        }

        //Clear uploaded media
        $s = $_FILES['img_upload']['size'];
        if ($s > 0) {
            $all_media = $this->post->get_all_media($post_id);
            foreach ($all_media as $media) {
                $this->post->delete_media($media->mid);
            }

            //Re-Upload
            $this->upload_single_media($post_id);
        }

        if ($this->post->update_post($post, $post_id)) {
            if ($s == 0) {
                echo json_encode(array("status" => "success"));
            }
        } else {
            if ($s == 0) {
                echo json_encode(array("status" => "fail"));
            }
        }
    }

    public function upload_newsletter() {
        $this->upload_special_method(TYPE_NEWSLETTER);
    }

    public function upload_single_media($post_id = null) {
        $path = './uploads';
        $post_id = is_numeric($post_id) ? $post_id : $this->input->get_post('post_id');
        $path .= "/" . $post_id;

        $config['allowed_types'] = '*';
        $config['upload_path'] = $path;
        $uid = get_user_uid();
        $current_date = date('U');

        if (!is_dir($path)) {
            mkdir($path);
        }
        @chmod($path, 0777);

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('img_upload')) {
            //Fail to upload.
            $data['status'] = 'fail';
            $flag = $this->upload->display_errors();
        } else {
            //Upload success
            $data['status'] = "success";
            $flag = $this->upload->data();

            //Setup data object to insert into DB
            $file_detail = array(
                'header_media' => $flag['file_type'],
                'media_name' => $flag['file_name'],
                'media_info' => json_encode($flag),
                'uid' => $uid,
                'post_id' => $post_id,
                'upload_date' => $current_date
            );

            switch (strtolower($flag['file_ext'])) {
                case '.jpg':
                case '.png':
                case '.jpeg':
                    //Image display
                    //Create Thumbnail
                    $this->resize_image($flag['full_path'], 150, $flag['file_path'] . "s_" . $flag['file_name']);

                    //Create Medium size   
                    $this->resize_image($flag['full_path'], 300, $flag['file_path'] . "m_" . $flag['file_name']);

                    //Resize image
                    $this->resize_image($flag['full_path'], 1024);

                    $file_detail['media_type'] = "img";
                    $file_detail['path'] = json_encode(array(
                        's' => $flag['file_path'] . "s_" . $flag['file_name'],
                        'm' => $flag['file_path'] . "m_" . $flag['file_name'],
                        'l' => $flag['full_path']
                    ));

                    $mylink = array(
                        's' => site_url('uploads/' . $post_id . '/s_' . $flag['file_name']),
                        'm' => site_url('uploads/' . $post_id . '/m_' . $flag['file_name']),
                        'l' => site_url('uploads/' . $post_id . '/' . $flag['file_name'])
                    );

                    $file_detail['link'] = json_encode($mylink);
                    $data['link'] = $mylink;
                    break;
                case '.mp4':
                case '.ogg':
                case '.webm':
                    //Video player
                    $file_detail['media_type'] = "vdo";
                    $file_detail['path'] = $flag['full_path'];
                    $file_detail['link'] = site_url('uploads/' . $post_id . '/' . $flag['file_name']);
                    break;
                case '.pdf':
                    //PDF Document
                    $file_detail['media_type'] = "pdf";
                    $file_detail['path'] = $flag['full_path'];
                    $file_detail['link'] = site_url('uploads/' . $post_id . '/' . $flag['file_name']);

                    //Generate
                    //Require imagemagick package
                    $file_path = $flag['full_path'];
                    $pdf_folder = do_hash($flag['file_name'] . date('U'));
                    $folder_target = "./uploads/{$post_id}/{$pdf_folder}";

                    if (!is_dir($folder_target)) {
                        mkdir($folder_target);
                        chmod($folder_target, 0777);
                    }

                    //Convert file
                    $str = "convert -quality 100 -density 300x300 {$file_path} {$folder_target}/page.jpg";
                    shell_exec($str);
                    $temp = get_dir_file_info($folder_target, false);

                    $pdf_img_info = array();
                    foreach ($temp as $each_file) {
                        $cad = array(
                            'name' => $each_file['name'],
                            'path' => $each_file['relative_path'],
                            'url' => site_url("uploads/{$post_id}/{$pdf_folder}/" . $each_file['name']),
                            'thumb' => site_url("uploads/{$post_id}/{$pdf_folder}/m_" . $each_file['name'])
                        );

                        $this->resize_image($each_file['server_path'], 300, "./uploads/{$post_id}/{$pdf_folder}/m_" . $each_file['name']);
                        $this->resize_image($each_file['server_path'], 1024);
                        array_push($pdf_img_info, $cad);
                    }
                    $pdf_img_info = array_reverse($pdf_img_info);
                    $flag['pdf_image'] = $pdf_img_info;
                    $file_detail['media_info'] = json_encode($flag);
                    break;
                default:
                    //Unidentify make treat it as direct link download
                    $file_detail['media_type'] = "other";
                    $file_detail['path'] = $flag['full_path'];
                    $file_detail['link'] = site_url('uploads/' . $post_id . '/' . $flag['file_name']);
                    break;
            }
            $this->post->insert_media($file_detail);
            $mid = $this->post->get_lates_media($post_id);
            $data['mid'] = $mid->mid;
        }
        $data['flag'] = $flag;
        echo json_encode($data);
    }

    public function upload_media() {
        $path = './uploads';
        $post_id = $this->input->get_post('post_id');
        $folder = $path . "/" . $post_id;
        $current_date = date('U');

        $uid = get_user_uid();

        if (!is_dir($folder)) {
            mkdir($folder);
        }

        chmod($folder, 0777);

        $obj = array();
        for ($i = 0; $i < count($_FILES['upload_btn']['tmp_name']); $i++) {
            //Setup file destination with correct format file name pattern
            $destination = $_FILES['upload_btn']["name"][$i];

            //Get file info
            $file_info = pathinfo($destination);

            //Reform file name
            $file_name = url_title($file_info['filename']);
            $file_name = (empty($file_name)) ? $current_date : $file_name;

            //Check File has existed ?
            if (is_file($folder . "/" . $destination)) {
                $file_name .= "_" . $current_date;
            }

            //set file name to save
            $destination = $file_name . "." . $file_info['extension'];

            //Generate save point and thumbnail
            $save_place = $folder . "/" . $destination;
            $save_place_m = $folder . "/m_" . $destination;
            $save_place_s = $folder . "/s_" . $destination;

            //Move upload file into destination
            move_uploaded_file($_FILES['upload_btn']['tmp_name'][$i], $save_place);

            //Setup data object to insert into DB
            $file_detail = array(
                'header_media' => $_FILES['upload_btn']['type'][$i],
                'media_name' => $destination,
                'media_info' => json_encode($file_info),
                'uid' => $uid,
                'post_id' => $post_id,
                'upload_date' => $current_date
            );

            switch (strtolower($file_info['extension'])) {
                case 'jpg':
                case 'png':
                case 'jpeg':
                    //Image display
                    //Create Thumbnail
                    $this->resize_image($save_place, 150, $save_place_s);

                    //Create Medium size   
                    $this->resize_image($save_place, 300, $save_place_m);

                    //Resize image
                    $this->resize_image($save_place, 1024);

                    $file_detail['media_type'] = "img";
                    $file_detail['path'] = json_encode(array(
                        's' => $save_place_s,
                        'm' => $save_place_m,
                        'l' => $save_place
                    ));
                    $file_detail['link'] = json_encode(array(
                        's' => site_url('uploads/' . $post_id . '/s_' . $destination),
                        'm' => site_url('uploads/' . $post_id . '/m_' . $destination),
                        'l' => site_url('uploads/' . $post_id . '/' . $destination)
                    ));
                    break;
                case 'mp4':
                case 'ogg':
                case 'webm':
                    //Video player
                    $file_detail['media_type'] = "vdo";
                    $file_detail['path'] = $save_place;
                    $file_detail['link'] = site_url('uploads/' . $post_id . '/' . $destination);
                    break;
                case 'pdf':
                    //PDF Document
                    $file_detail['media_type'] = "pdf";
                    $file_detail['path'] = $save_place;
                    $file_detail['link'] = site_url('uploads/' . $post_id . '/' . $destination);

                    //Generate
                    //Require imagemagick package
                    $file_path = "./uploads/{$post_id}/{$destination}";
                    $pdf_folder = do_hash($destination . date('U'));
                    $folder_target = "./uploads/test/{$pdf_folder}";

                    if (!is_dir($folder_target)) {
                        mkdir($folder_target);
                        chmod($folder_target, 0777);
                    }

                    //Convert file
                    $str = "convert -quality 100 -density 300x300 {$file_path} {$folder_target}/page.jpg";
                    shell_exec($str);
                    $temp = get_dir_file_info($folder_target, false);
                    $pdf_img_info = array();
                    foreach ($temp as $each_file) {
                        $cad = array(
                            'name' => $each_file['name'],
                            'path' => $each_file['relative_path'],
                            'url' => site_url("uploads/test/{$folder}/" . $each_file['name'])
                        );
                        array_push($pdf_img_info, $cad);
                    }
                    $pdf_img_info = array_reverse($pdf_img_info);
                    $file_info['pdf_image'] = $pdf_img_info;
                    $file_detail['media_info'] = json_encode($file_info);
                    break;
                default:
                    //Unidentify make treat it as direct link download
                    $file_detail['media_type'] = "other";
                    $file_detail['path'] = $save_place;
                    $file_detail['link'] = site_url('uploads/' . $post_id . '/' . $destination);
                    break;
            }
            array_push($obj, $file_detail);
            $this->post->insert_media($file_detail);
        }

        echo json_encode($obj);
    }

    public function delete_media() {
        $mid = $this->input->get_post('mid');
        echo $this->post->delete_media($mid);
    }

    public function update_post() {
        $post = $this->input->post();
        $post_id = $post['post_id'];

        if (array_key_exists('post_slide', $post)) {
            $post['post_slide'] = is_numeric($post['post_slide']) ? $post['post_slide'] : 0;
        } else {
            $post['post_slide'] = 0;
        }

        unset($post['post_id']);
        unset($post['enable_schedule']);
        unset($post['hidden_expire']);

        if ($this->post->update_post($post, $post_id)) {
            $data['status'] = "success";
            $data['post_id'] = $post_id;
            $data['redirect'] = site_url('management/post_operation?post_id=' . $post_id);
        } else {
            $data['status'] = "fail";
        }

        //In case of new post and set post type to reviwe system will automatic send email to director
        if (array_key_exists('post_status', $post) && $post['post_status'] == POST_REVIEW && get_user_level() == USER_WRITER) {
            //Send post to director
            notify_post_update($post_id);
        }
        echo json_encode($data);
    }

    public function remove_slide() {
        $post_id = $this->input->get_post("post_id");
        $post = get_post_by_id($post_id);
        if ($post->post_type == TYPE_SLIDE) {
            //slide
            $resp = $this->post->delete_post($post_id, true);
        } else {
            //post
            $resp = $this->post->update_post(array("post_slide" => 0), $post_id);
        }

        if ($resp) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        
        echo json_encode($data);
    }

    public function update_post_school() {
        $post = $this->input->post();
        $post_id = $post['post_id'];
        $post_detail = array(
            'post_other_home' => $post['post_other_home'],
            'post_other_about' => $post['post_other_about'],
            'post_other_research' => $post['post_other_research'],
            'post_other_career' => $post['post_other_career'],
            'post_other_collaboration' => $post['post_other_collaboration'],
            'post_other_programmes' => $post['post_other_programmes'],
            'post_other_phone' => $post['post_other_phone'],
            'post_other_fax' => $post['post_other_fax'],
            'post_other_email' => $post['post_other_email'],
            'post_other_website' => $post['post_other_website'],
            'post_other_address' => $post['post_other_address']
        );

        $post_obj = array(
            'post_title' => $post['post_title'],
            'post_excerp' => $post['post_excerp'],
            'post_detail' => json_encode($post_detail),
            'post_status' => $post['post_status']
        );

        if ($this->post->update_post($post_obj, $post_id)) {
            $data['status'] = "success";
            $data['post_id'] = $post_id;
            $data['redirect'] = site_url('management/post_operation?post_id=' . $post_id);
        } else {
            $data['status'] = "fail";
        }

        echo json_encode($data);
    }

    public function update_slide_order() {
        $data = $this->input->post();
        $order = json_decode($data['order']);

        foreach ($order as $each_post) {
            $post_id = $each_post->post_id;
            $post_obj['post_thumbnail'] = $each_post->post_thumbnail;
            $this->post->update_post($post_obj, $post_id);
        }

        echo json_encode(array("status" => "success"));
    }

    public function add_users() {
        $user_data = $this->input->post();
        if ($this->admin->check_user_on_exist($user_data['email']) === 0) {
            //Email does not exist
            $user_data['password'] = do_hash($user_data['password']);

            //this user level
            $user_level = get_user_level();
            $form_rejection = true;

            switch (strtolower($user_data['level'])) {
                case 'admin': $user_data['level'] = USER_ADMIN;
                    if ($user_level == USER_ADMIN) {
                        //Admin can add Admin
                        $form_rejection = false;
                    }
                    break;
                case 'director': $user_data['level'] = USER_DIRECTOR;
                    if ($user_level == USER_ADMIN) {
                        //Admin can add Director
                        $form_rejection = false;
                    }
                    break;
                case 'writer': $user_data['level'] = USER_WRITER;
                    if ($user_level != USER_WRITER) {
                        //Admin and Director can add Writer
                        $form_rejection = false;
                    }
                    break;
            }

            if ($form_rejection) {
                echo json_encode(array(
                    'status' => 'fail',
                    'error' => 'Form rejected'
                ));
                exit(1);
            }
            if ($this->admin->create_account($user_data)) {
                $response['status'] = "success";
            } else {
                $response['status'] = "fail";
            }
            $response['info'] = $user_data;
            $response['link'] = site_url('management/all_account');
        } else {
            //This Email on exist
            $response['status'] = "fail";
            $response['error'] = "Email on existed";
        }
        echo json_encode($response);
    }

    public function delete_clip() {
        $this->delete_post();
        $this->delete_media();
    }

    public function delete_post() {
        $user_level = get_user_level();
        $uid = get_user_uid();
        $post_id = $this->input->post('post_id');
        $trucate = $this->input->post('trucate');
        $trucate = ($trucate == 0) ? false : true;
        if (!empty($user_level) && !empty($uid) && !empty($post_id)) {
            $post_info = get_post_by_id($post_id);
            if ($user_level == USER_WRITER) {
                //Writer
                if ($post_info->post_author == $uid) {
                    $response = $this->post->delete_post($post_id, $trucate);
                }
            } else if ($user_level == USER_DIRECTOR || $user_level == USER_ADMIN) {
                $response = $this->post->delete_post($post_id, $trucate);
            }
        } else {
            redirect(site_url('management'));
            exit(0);
        }

        if ($response) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }

        $data['response'] = $this->input->post();

        echo json_encode($data);
    }

    public function update_group() {
        $post = $this->input->post();
        $post_id = $post['post_id'];
        $obj = array();
        if (array_key_exists('dataset', $post)) {
            $obj['post_detail'] = json_encode($post['dataset']);
        }
        $obj['post_excerp'] = $post['post_excerp'];
        $response = $this->post->update_post($obj, $post_id);

        if ($response) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }

        echo json_encode($data);
    }

    public function update_audience() {
        $post = $this->input->post();
        $post_id = $post['post_id'];
        $dataset = $post['dataset'];
        $response = $this->post->update_post(array(
            'post_detail' => json_encode($dataset)
                ), $post_id);

        if ($response) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }

        echo json_encode($data);
    }

    public function update_menu() {
        $post = $this->input->post('dataset');
        $post = json_encode($post);
        $response = $this->post->update_menu($post);
        if ($response) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }

        $data['response'] = $post;

        echo json_encode($data);
    }

    public function add_link_group() {
        $post = $this->input->post('group_name');
        $f = false;
        if (!empty($post)) {
            $post_obj = array(
                'post_title' => $post,
                'post_modify' => date('U'),
                'post_status' => POST_PUBLISH
            );
            $f = $this->post->add_group_link($post_obj);
        }

        if ($f['status']) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }

        $data['post_id'] = $f['post_id'];
        echo json_encode($data);
    }

    public function add_audience() {
        $post = $this->input->post('group_name');
        $f = false;
        if (!empty($post)) {
            $post_obj = array(
                'post_title' => $post,
                'post_modify' => date('U'),
                'post_status' => POST_PUBLISH
            );
            $f = $this->post->add_audience($post_obj);
        }

        if ($f['status']) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }

        $data['post_id'] = $f['post_id'];
        echo json_encode($data);
    }

    public function update_block() {
        $widget = $this->input->post('widget');
        if ($this->post->update_widget($widget)) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function update_account() {
        $post = $this->input->post();
        if ($post['password_1'] == $post['password_2']) {
            $repack['real_name'] = $post['real_name'];

            if (array_key_exists('level', $post)) {
                $repack['level'] = $post['level'];
            }

            if (array_key_exists('status', $post)) {
                $repack['status'] = $post['status'];
            }

            if (array_key_exists('disallow', $post)) {
                $repack['disallow'] = $post['disallow'];
            }

            if (array_key_exists('parent_uid', $post)) {
                $repack['parent_uid'] = $post['parent_uid'];
            }

            if ($post['password_2'] != "") {
                $repack['password'] = do_hash($post['password_2']);
            }
        }

        if ($this->admin->update_account($repack, $post['uid'])) {
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'fail'));
        }
    }

    public function test() {
        echo $this->config->item('base_url') . $this->config->item('index_page') . $this->uri->uri_string() . $this->output->_get_request();
    }

}

?>

<?php

function loged() {
    $ci = get_instance();
    $auth = $ci->session->userdata('email');
    if (empty($auth)) {
        return false;
    } else {
        return true;
    }
}

function signout() {
    $ci = get_instance();
    $ci->session->unset_userdata('email');
}

function get_all_session_info() {
    $ci = get_instance();
    $auth = $ci->session->all_userdata();
    return $auth;
}

function get_user_realname_loged() {
    $auth = get_all_session_info();
    return $auth['real_name'];
}

function get_user_uid() {
    $auth = get_all_session_info();
    return $auth['uid'];
}

function get_user_level() {
    $auth = get_all_session_info();
    return $auth['level'];
}

function get_user_post_reviwer() {
    $auth = get_all_session_info();
    return $auth['parent_uid'];
}

function template_section_block3($block1, $block2, $block3) {
    $temp = ((count($block1) > 0) ? 1 : 0) + ((count($block2) > 0) ? 1 : 0) + ((count($block3) > 0) ? 1 : 0);
    switch ($temp) {
        case 3: return "col-xs-4";
            break;
        case 2: return "col-xs-6";
            break;
        case 1: return "col-xs-12";
            break;
    }
}

function notify_post_update($post_id) {
    $post_info = get_post_by_id($post_id);
    $writer_uid = get_user_uid();

    switch ($post_info->post_type) {
        case TYPE_POST:
            $url_to_type = site_url("management/all_post?users={$writer_uid}&post_status=" . POST_REVIEW);
            $url_to_post = site_url("management/post_operation?post_id={$post_id}");
            break;
        case TYPE_PAGE:
            $url_to_type = site_url("management/all_page?users={$writer_uid}&post_status=" . POST_REVIEW);
            $url_to_post = site_url("management/page_operation?post_id={$post_id}");
            break;
        case TYPE_EVENT:
            $url_to_type = site_url("management/all_event?users={$writer_uid}&post_status=" . POST_REVIEW);
            $url_to_post = site_url("management/event_operation?post_id={$post_id}");
            break;
        case TYPE_REVISION:
            if ($post_info->revision != 0) {
                notify_post_update($post_info->revision);
            }
            return;
            break;
        default:
            $url_to_type = "";
            $url_to_post = "";
            break;
    }

    //Get Post reviwer information
    $parent_uid = get_user_post_reviwer();
    $reviwer = get_user_by_uid($parent_uid);
    $writer = get_user_realname_loged();
    //Generate email body
    $email_reviwer = $reviwer->email;

    $subject = "MFU system post review: {$post_info->post_title}";

    $body_txt = "Hello {$reviwer->real_name}<br/>";
    $body_txt .= "{$writer} have sent you a post to review: <a href='{$url_to_post}'>{$post_info->post_title}</a><br/>";
    $body_txt .= "or you can check all your post review just <a href='{$url_to_type}'>click here</a><br/>";
    $body_txt .= "Thank you";

    return server_mail($email_reviwer, $subject, $body_txt);
}

function get_disallow_section() {
    $auth = get_all_session_info();
    return $auth['disallow']->section;
}

function get_disallow_permission() {
    $auth = get_all_session_info();
    return $auth['disallow']->permission;
}

function mfu_header() {
    echo css_asset("admin_panel.css");
    ?>
    <script type="text/javascript">
        $(function () {
            $('a.clear_cache_btn').click(function () {
                var href = $(this).attr('href');
                $.get(href, function () {
                    alert("Cache file cleared");
                });
                return false;
            });
        });
    </script>
    <?php
}

function get_all_media($post_id = 0, $order_by = 'desc', $limit = null, $offset = null, $media_type = 0) {
    $ci = get_instance();
    return $ci->post->get_all_media($post_id, $order_by, $limit, $offset, $media_type);
}

function get_all_media_sort_by_name($post_id = 0, $order_by = 'desc') {
    $ci = get_instance();
    return $ci->post->get_all_media_sort_by_name($post_id, $order_by);
}

function mfu_body_class() {
    if (loged()) {
        echo "class='logged_user'";
    }
}

function get_latest_news($limit = 20, $offset = 0) {
    $ci = get_instance();
    return $ci->post->get_latest_news($limit, $offset);
}

function get_number_latest_news() {
    $ci = get_instance();
    return $ci->post->get_number_latest_news();
}

function get_latest_admission_news($limit = 20, $offset = 0, $schedule = true, $group = 1) {
    $ci = get_instance();
    return $ci->post->get_latest_admission_news($limit, $offset, $schedule, $group);
}

function get_number_latest_admission_news() {
    $ci = get_instance();
    return $ci->post->get_number_latest_admission_news();
}

function get_media_mid($mid) {
    $ci = get_instance();
    return $ci->post->get_media_mid($mid);
}

function get_category_by_cate_id($cate_id) {
    $ci = get_instance();
    return $ci->post->get_category_cate_id($cate_id);
}

function get_post_in_multiple_cate($array_of_cate, $limit = 20) {
    $ci = get_instance();
    return $ci->post->get_post_in_multiple_cate($array_of_cate, $limit);
}

function get_category_by_parent_cate_id($cate_id) {
    $ci = get_instance();
    return $ci->post->get_category_by_parent_cate_id($cate_id);
}

function get_group_link_by_id($post_id) {
    $ci = get_instance();
    return $ci->post->get_group_link_by_id($post_id);
}

function get_user_by_uid($uid) {
    $ci = get_instance();
    return $ci->post->get_user_uid($uid);
}

function get_post_by_id($post_id, $userinfo = true) {
    $ci = get_instance();
    return $ci->post->get_post_by_id($post_id, $userinfo);
}

function get_media_by_post_id($post_id) {
    $ci = get_instance();
    return $ci->post->get_all_media($post_id);
}

function get_media_block($obj_media, $post_id) {
    $media_info = json_decode($obj_media->media_info);
    ?>

    <div class="img-thumbnail relative media-lib-block <?php echo ($post_id == $obj_media->post_id) ? "media_in_this_post" : "media_other_post"; ?>" id="block_<?php echo $obj_media->mid; ?>">
        <?php switch ($obj_media->media_type): case 'img': ?>
                <?php $links = json_decode($obj_media->link); ?>
                <?php $paths = json_decode($obj_media->path); ?>
                <?php $mid_info = is_file($paths->s) ? $paths->s : "./uploads/{$obj_media->post_id}/s_{$obj_media->media_name}"; ?>
                <?php $dimenstion = getimagesize($mid_info); ?>

                <?php $class = ($dimenstion[0] > $dimenstion[1]) ? "height_enclose" : "width_enclose"; ?>
                <?php $real_link = $links->l; ?>
                <?php $permalink = "#thumbnail_{$obj_media->mid}"; ?>
                <?php $bg_link = $links->s; ?>
                <?php $img_m = $links->m; ?>
                <?php break; ?>
            <?php default: ?>
                <?php $class = "height_enclose"; ?>
                <?php $real_link = $obj_media->link; ?>
                <?php $permalink = "#file_{$obj_media->mid}"; ?>
                <?php $bg_link = image_asset_url($obj_media->media_type . '.png'); ?>
                <?php $img_m = $bg_link; ?>
                <?php break; ?>
        <?php endswitch; ?>
        <div class="filter_media_block">
            <i class="glyphicon glyphicon-ok-circle"></i>
        </div>
        <a href="<?php echo $permalink; ?>" data-mime="<?php echo $obj_media->header_media; ?>" data-link="<?php echo $real_link; ?>" data-thumbnail='<?php echo $bg_link; ?>' data-img-m='<?php echo $img_m; ?>' class="box-gallery <?php echo $class; ?>" style="background-image: url('<?php echo $bg_link; ?>');" title="<?php echo isset($media_info->basename) ? $media_info->basename : $media_info->file_name; ?>" data-type="<?php echo $obj_media->media_type ?>" data-mid="<?php echo $obj_media->mid; ?>"></a>
    </div>
    <?php
}

function get_media_block_2($obj_media) {
    $permalink = json_decode($obj_media->link);
    ?>
    <div class="col-md-3" id="block_<?php echo $obj_media->mid; ?>">
        <div data-mime="<?php echo $obj_media->header_media; ?>" data-link="<?php echo $permalink->l; ?>" data-thumbnail='<?php echo $permalink->s; ?>' data-img-m='<?php echo $permalink->m; ?>' class="thumbnail" title="<?php echo $obj_media->media_name; ?>" data-type="<?php echo $obj_media->media_type ?>" data-mid="<?php echo $obj_media->mid; ?>">
            <button class="btn btn-danger btn-xs del_btn" data-mid="<?php echo $obj_media->mid; ?>" style="position: absolute;"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>
            <img data-src="holder.js/100%x180" src="<?php echo $permalink->m; ?>">
        </div>
    </div>
    <?php
}

function mfu_footer() {
    if (loged()) {
        echo css_asset("trycatch_admin.css");
        echo css_asset("admin_panel.css");
        $permission_section = get_disallow_section();
        ?>
        <nav class="navbar navbar-inverse navbar-fixed-top mfu_admin_panel" role="navigation">            
            <div class="container">
                <div class="row relative">
                    <div class="col-md-3 hidden-sm">
                        <a href="<?php echo site_url(); ?>"><img src="<?php echo image_asset_url("backend_logo.png"); ?>" class="img-responsive"></a>
                    </div>
                    <div class="col-md-9 col-sm-12">
                        <div class="collapse navbar-collapse operation_menu">
                            <ul class="nav navbar-nav">
                                <li class="hidden-md hidden-lg">
                                    <a href="<?php echo site_url(); ?>">
                                        <span class='hidden-sm hidden-md hidden-xs'><i class="glyphicon glyphicon-globe"></i>&nbsp;Visit Site</span>
                                        <span class='hidden-lg'>Visit Site</span>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="<?php echo site_url('management/welcome'); ?>" class="dropdown-toggle">
                                        <span class='hidden-sm hidden-md hidden-xs'><i class="glyphicon glyphicon-home"></i>&nbsp;Summary</span>
                                        <span class='hidden-lg'>Summary</span>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span class='hidden-sm hidden-md hidden-xs'><i class="glyphicon glyphicon-pencil"></i>&nbsp;Post&nbsp;<b class="caret"></b></span>
                                        <span class='hidden-lg'>Post&nbsp;<b class="caret"></b></span>
                                    </a>
                                    <ul class="dropdown-menu">     
                                        <?php if (!in_array("research", $permission_section)): ?>
                                            <li><a href="<?php echo site_url('management/research'); ?>">Research</a></li>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                        <?php if (!in_array("post", $permission_section)): ?>
                                            <li><a href="<?php echo site_url('management/post_operation'); ?>">New Post</a></li>
                                            <li><a href="<?php echo site_url('management/all_post'); ?>">All Posts</a></li>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                        <?php if (!in_array("category", $permission_section)): ?>  
                                            <li><a href="<?php echo site_url('management/all_category'); ?>">All Categories</a></li>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                        <?php if (!in_array("event", $permission_section)): ?>    
                                            <li><a href="<?php echo site_url('management/all_event'); ?>">All Event</a></li>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                        <?php if (!in_array("school", $permission_section)): ?>     
                                            <li><a href="<?php echo site_url('management/school_operation'); ?>">New School</a></li>
                                            <li><a href="<?php echo site_url('management/all_school'); ?>">All Schools</a></li>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                        <?php if (!in_array("album", $permission_section)): ?>      
                                            <li><a href="<?php echo site_url('management/new_gallery'); ?>">New Album</a></li>
                                            <li><a href="<?php echo site_url('management/all_gallery'); ?>">Gallery</a></li>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                        <?php if (!in_array("newsletter", $permission_section)): ?>
                                            <li><a href="<?php echo site_url('management/newsletter'); ?>">Newsletter</a></li>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                        <?php if (!in_array("clipping", $permission_section)): ?>
                                            <li><a href="<?php echo site_url('management/clipping'); ?>">Clipping news</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                                <?php if (get_user_level() != USER_WRITER): ?>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class='hidden-sm hidden-md hidden-xs'><i class="glyphicon glyphicon-user"></i>&nbsp;Account&nbsp;<b class="caret"></b></span>
                                            <span class='hidden-lg'>Account&nbsp;<b class="caret"></b></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo site_url('management/new_account'); ?>">New Account</a></li>
                                            <li><a href="<?php echo site_url('management/all_account'); ?>">All Account</a></li>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span class='hidden-sm hidden-md hidden-xs'><i class="glyphicon glyphicon-link"></i>&nbsp;Template&nbsp;<b class="caret"></b></span>
                                        <span class='hidden-lg'>Template&nbsp;<b class="caret"></b></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <?php if (get_user_level() != USER_WRITER && !in_array("menu_main", $permission_section)): ?>
                                            <li><a href="<?php echo site_url('management/menu_main'); ?>">Main Menu</a></li>
                                        <?php endif; ?>

                                        <?php if (get_user_level() == USER_ADMIN): ?>
                                            <li><a href="<?php echo site_url('management/widget_blocking'); ?>">Widget & Blocking</a></li>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                        <?php if (!in_array("clipping", $permission_section)): ?>
                                            <li><a href="<?php echo site_url('management/page_operation'); ?>">New page</a></li>
                                            <li><a href="<?php echo site_url('management/all_page'); ?>">All Pages</a></li>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                        <?php if (!in_array("post", $permission_section)): ?>
                                            <li><a href="<?php echo site_url('management/all_slide'); ?>">Slide</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span class='hidden-sm hidden-md hidden-xs'><i class="glyphicon glyphicon-bell"></i>&nbsp;Grouping&nbsp;<b class="caret"></b></span>
                                        <span class='hidden-lg'>Grouping&nbsp;<b class="caret"></b></span>
                                    </a>
                                    <ul class="dropdown-menu">

                                        <?php if (get_user_level() != USER_WRITER): ?>
                                            <?php if (!in_array("audience_guideline", $permission_section)): ?>
                                                <li><a href="<?php echo site_url('management/audience_guideline'); ?>">Audience Guideline</a></li>
                                            <?php endif; ?>

                                            <?php if (!in_array("external_link", $permission_section)): ?>
                                                <li><a href="<?php echo site_url('management/external_link'); ?>">External Link</a></li>
                                            <?php endif; ?>

                                            <?php if (!in_array("api", $permission_section)): ?>
                                                <li><a href="<?php echo site_url('management/api'); ?>">API</a></li>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <li><a href="<?php echo site_url('management/all_media'); ?>">Media Library</a></li>

                                        <?php if (get_user_level() != USER_WRITER): ?>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo site_url('uncached/clear_cache'); ?>" class="clear_cache_btn">Clear all cached</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="<?php echo get_user_realname_loged(); ?>">
                                        <span class="hidden-sm hidden-md hidden-xs"><?php echo get_user_realname_loged(); ?>&nbsp;<span class="glyphicon glyphicon-list"></span><b class="caret"></b></span>
                                        <span class="hidden-lg"><span class="glyphicon glyphicon-list"></span><b class="caret"></b></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo site_url('management/edit_account?uid=' . get_user_uid()); ?>"><span class="glyphicon glyphicon-wrench"></span> Profile setting</a></li>
                                        <li><a href="<?php echo site_url('api/logout'); ?>"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </div>
            </div>
        </nav>
        <?php
    }
}

function get_all_category($cate_type = CATE_POST) {
    $ci = get_instance();
    return $ci->admin->get_all_category($cate_type);
}

function get_all_post_in_cate($cate_id, $limit = null, $offset = null, $post_type = TYPE_POST, $schedule = true, $month = 0, $year = 0, $search = null) {
    $ci = get_instance();
    return $ci->admin->get_all_post_in_cate($cate_id, $limit, $offset, $post_type, $schedule, $month, $year, $search);
}

function get_all_post_by_type($post_type = TYPE_POST, $post_status = POST_PUBLISH) {
    $ci = get_instance();
    return $ci->admin->get_all_post_by_type($post_type, $post_status);
}

function get_number_of_post_in_cate($cate_id, $post_type = TYPE_POST, $post_status = POST_PUBLISH, $schedule = true, $month = 0, $year = 0, $search = null) {
    $ci = get_instance();
    return $ci->admin->get_number_of_post_in_cate($cate_id, $post_type, $post_status, $schedule, $month, $year, $search);
}

function get_number_event_by_status($type = POST_PUBLISH) {
    $ci = get_instance();
    return $ci->post->get_number_event_by_status($type);
}

function widget_backend($widget) {
    if (count($widget)) {
        foreach ($widget as $each_widget) {
            if (count((array) $each_widget)) {
                switch ($each_widget->data_type) {
                    case 'event':
                    case 'category':
                        if ($each_widget->data_id != 0) {
                            $ele_name = get_category_by_cate_id($each_widget->data_id);
                            $ele_name = $ele_name->cate_name;
                        } else {
                            $ele_name = "All Event";
                        }
                        break;
                    case 'ext_link':
                        $ele_name = get_group_link_by_id($each_widget->data_id);
                        $ele_name = $ele_name->post_title;
                        break;
                    default:
                        $ele_name = $each_widget->data_type;
                        break;
                }
                echo "<li><a href='#' data-id='{$each_widget->data_id}' data-type='{$each_widget->data_type}' class='each_elemet_widget'><i class='glyphicon glyphicon-minus-sign'></i>&nbsp;<span>{$ele_name}<i>&nbsp;&dash;&nbsp;{$each_widget->data_type}</i></span></a></li>";
            }
        }
    }
}

function re_arrange_menu($main_menu, $parent = -1) {
    $arranged = array();
    foreach ($main_menu as $menu) {
        if ($parent == $menu->menu_parent_id) {
            $menu->sub_menu = re_arrange_menu($main_menu, $menu->menu_id);
            array_push($arranged, $menu);
        }
    }
    return $arranged;
}

function get_main_menu_obj() {
    $ci = get_instance();
    $main_menu = $ci->post->get_meta('menu');
    $main_menu = $main_menu[0];
    $main_menu = json_decode($main_menu->meta_info);

    return re_arrange_menu($main_menu);
}

function get_main_menu_gen_format($obj, $class = 'nav nav-pills', $i = 0) {

    if (count($obj)) {
        $str = "<!--{$i}-->";
        if ($i != 2) {
            $str .= "<ul class='{$class}'>";
        }

        foreach ($obj as $each_menu) {
            $sub = get_main_menu_gen_format($each_menu->sub_menu, 'dropdown-menu', $i + 1);
            $str .= "<li " . (trim($sub) == "" ? "" : "class='dropdown'") . ">";
            $str .= "<a href='{$each_menu->menu_link}' " . (trim($sub) == "" ? "" : "class='dropdown-toggle' data-toggle='dropdown'") . ">" . (($i == 2) ? "&raquo;&nbsp;" : "") . "{$each_menu->menu_title} " . (trim($sub) == "" ? "" : "<span class='caret'></span>") . "</a>";
            $str .= $sub;
            $str .= "</li>";
        }

        if ($i != 2) {
            $str .= "</ul>";
        } else {
            $str .= "<li class='divider'></li>";
        }
        return $str;
    }
    return "";
}

function get_main_menu_with_format() {
    $obj = get_main_menu_obj();
    return get_main_menu_gen_format($obj);
}

function group_audience_content($data) {
    $group = array();
    foreach ($data as $each_menu) {
        if (!array_key_exists($each_menu->menu_parent_id, $group)) {
            $group[$each_menu->menu_parent_id] = array();
        }

        array_push($group[$each_menu->menu_parent_id], $each_menu);
    }
    return $group;
}

function widget_event($obj) {
    return array(
        'date' => $obj->post_date,
        'post_title' => $obj->post_title,
        'url' => site_url("mfu/event/{$obj->post_id}/" . slug($obj->post_title))
    );
}

function widget_audience_guideline($obj) {
    return array(
        'post_title' => $obj->post_title,
        'url' => site_url("mfu/audience/{$obj->post_id}/" . slug($obj->post_title))
    );
}

function get_excerp($obj, $limit = 150) {
    $obj->post_detail = str_replace("&nbsp;", "", $obj->post_detail);
    return (trim($obj->post_excerp) == "") ? (trim(strip_tags($obj->post_detail)) == "" ? "No content" : ellipsize(strip_tags($obj->post_detail), $limit)) : $obj->post_excerp . '&hellip;';
}

function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}

function widget_cate($obj) {
    $ci = get_instance();
    $dim = 'portrait';
    $thumbnail = image_asset_url('Blank_thumbnail.jpg');
    $title = ellipsize($obj->post_title, 81, 0.5);
    $url_to_post = site_url("mfu/post/{$obj->post_id}/" . slug($obj->post_title));

    if (is_numeric($obj->post_thumbnail)) {
        if ($obj->post_thumbnail != 0) {
            $img = $ci->post->get_media_mid($obj->post_thumbnail);
            $url = json_decode($img->link);
            $path = json_decode($img->path);
            if (is_file($path->s)) {
                $thumbnail = $url->s;
                $img_info = getimagesize($path->s);
                if ($img_info[0] > $img_info[1]) {
                    $dim = 'langscape';
                }
            }
        }
    } else {
        $thumbnail = @get_http_response_code($obj->post_thumbnail) == 200 ? $obj->post_thumbnail : $thumbnail;
        $url_to_post = $obj->ext_link;
    }

    return array(
        'date' => $obj->post_date,
        'thumbnail' => $thumbnail,
        'dim' => $dim,
        'post_title' => $title,
        'alt_title' => $obj->post_title,
        'post_excerp' => get_excerp($obj),
        'cate_id' => isset($obj->cate_id) ? $obj->cate_id : 0,
        'url' => $url_to_post
    );
}

function widget_cate_api($obj) {
    $ci = get_instance();
    $dim = 'portrait';
    $thumbnail = image_asset_url('Blank_thumbnail.jpg');
    $title = ellipsize($obj->post_title, 81, 0.5);
    $url_to_post = site_url("mfu/readarchive/{$obj->post_id}/" . slug($obj->post_title));

    if (is_numeric($obj->post_thumbnail)) {
        if ($obj->post_thumbnail != 0) {
            $img = $ci->post->get_media_mid($obj->post_thumbnail);
            $url = json_decode($img->link);
            $path = json_decode($img->path);
            if (is_file($path->s)) {
                $thumbnail = $url->s;
                $img_info = getimagesize($path->s);
                if ($img_info[0] > $img_info[1]) {
                    $dim = 'langscape';
                }
            }
        }
    } else {
        $thumbnail = @get_http_response_code($obj->post_thumbnail) == 200 ? $obj->post_thumbnail : $thumbnail;
        //$url_to_post = $obj->ext_link;
    }

    return array(
        'date' => $obj->post_date,
        'thumbnail' => $thumbnail,
        'dim' => $dim,
        'post_title' => $title,
        'alt_title' => $obj->post_title,
        'post_excerp' => get_excerp($obj),
        'cate_id' => isset($obj->cate_id) ? $obj->cate_id : 0,
        'url' => $url_to_post
    );
}

function get_event_by_status($post_status = POST_PUBLISH, $limit = 5, $offset = 0, $event_type = EVENT_OTHER) {
    $ci = get_instance();
    return $ci->post->get_event_by_status($post_status, $limit, $offset, $event_type);
}

function widget_process($widget) {
    $ci = get_instance();
    $obj['type'] = $widget->data_type;
    switch ($widget->data_type) {
        case 'school':
            $obj['title'] = "school";
            $obj['post'] = $ci->post->get_post_by_status(POST_PUBLISH, TYPE_SCHOOL);
            $obj['more_btn'] = site_url('mfu/all_school');
            break;
        case 'event':
            if ($widget->data_id != 0) {

                //Query for event ID
                $event_info = get_category_by_cate_id($widget->data_id);
                $all_child_cate = get_category_by_parent_cate_id($widget->data_id);

                $concern_cate = array();
                foreach ($all_child_cate as $each_cate) {
                    array_push($concern_cate, $each_cate->cate_id);
                }

                $obj['title'] = $event_info->cate_name;
                $obj['more_btn'] = site_url("mfu/all_event?cate_id={$widget->data_id}");
            } else {
                //All Event
                $event_info = get_all_category(CATE_EVENT);
                $concern_cate = array();
                foreach ($event_info as $each_cate) {
                    array_push($concern_cate, $each_cate->cate_id);
                }
                $obj['title'] = LANG == 'th' ? "กิจกรรมมหาวิทยาลัย" : "Calendar";
                $obj['more_btn'] = site_url("mfu/all_event");
            }

//            $post = get_post_in_multiple_cate($concern_cate);
            //Adjust date to query 5 days next
            $post = array();
            $current_time = date("U");
            $current_month = date("n");
            $i = 0;
            do {
                $temp = $ci->post->get_post_by_status(POST_PUBLISH, TYPE_EVENT, $users = 0, $concern_cate, 1, 5, 'desc', true, date('d', $current_time), date('n', $current_time), date('Y', $current_time));
                $post = array_merge($post, $temp);

                $i++;
                $affix = 86400 * $i;
                $current_time += $affix;

                //Check all event in this month and collect 5 event to display in frontend
            } while ((count($post) < 5) && ($current_month == date('n', $current_time)));

            if (count($post) < 5) {
                $post = $ci->post->get_post_by_status(POST_PUBLISH, TYPE_EVENT, $users = 0, $concern_cate, 1, 5, 'desc', true, 0, date('n'), date('Y'));
            }

            $obj['post'] = $post;
            break;
        case 'audience_guideline':
            $obj['title'] = "Audience Guideline";
            $obj['post'] = $ci->post->get_audience();
            $obj['more_btn'] = "";
            break;
        case 'search':
            $obj['title'] = 'search';
            $obj['post'] = "<script>";
            $obj['post'] .= "(function() {";
            $obj['post'] .= "var cx = '001450871320183011274:o9gcchahnzk';";
            $obj['post'] .= "var gcse = document.createElement('script');";
            $obj['post'] .= "gcse.type = 'text/javascript';";
            $obj['post'] .= "gcse.async = true;";
            $obj['post'] .= "gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;";
            $obj['post'] .= "var s = document.getElementsByTagName('script')[0];";
            $obj['post'] .= "s.parentNode.insertBefore(gcse, s);";
            $obj['post'] .="})();";
            $obj['post'] .= "</script>";
            $obj['post'] .= "<gcse:search></gcse:search>";
            break;
        case 'ext_link':
            $info = $ci->post->get_group_link_by_id($widget->data_id);
            $obj['title'] = $info->post_title;
            $obj['post'] = $info->post_detail;
            $obj['more_btn'] = "";
            break;
        case 'category':
        default :
            $cate_obj = $ci->post->get_cate_info($widget->data_id);
            $obj['title'] = $cate_obj->cate_name;
            $obj['post'] = array_reverse(get_all_post_in_cate($widget->data_id, 3));
            $obj['more_btn'] = site_url("mfu/{$widget->data_type}/{$widget->data_id}/" . slug($cate_obj->cate_name));
            break;
    }
    return $obj;
}

function get_random_image_from_gallery($total = 5) {
    $ci = get_instance();
    $image = $ci->post->get_random_image_from_gallery();
    $slide = array();
    $index = 0;
    shuffle($image);
    foreach ($image as $each_image) {
        $info = json_decode($each_image->media_info);
        if ($info->is_image) {
            if ($info->image_width > $info->image_height) {
                $obj = array(
                    'path' => json_decode($each_image->path),
                    'link' => json_decode($each_image->link),
                    'info' => $info
                );
                array_push($slide, $obj);
                if (count($slide) == $total) {
                    return $slide;
                }
                $index++;
            }
        }
    }
    return $slide;
}

function group_obj_by_rows($img_set, $per_rows = 4) {
    $obj = array();
    $rows = -1;
    foreach ($img_set as $index => $each_image) {

        if (($index % $per_rows) == 0) {
            $rows++;
        }

        if (!is_array($obj[$rows])) {
            $obj[$rows] = array();
        }
        array_push($obj[$rows], $each_image);
    }
    return $obj;
}

function slug($pre_slug) {
    $slug = url_title($pre_slug);
    $slug = (trim($slug) == "") ? urlencode($pre_slug) : $slug;
    return str_replace("+", "_", $slug);
}

function get_stat($key) {
    $ci = get_instance();
    return $ci->admin->get_stat($key);
}

function group_cate_by_parent($category) {
    $cate = array();
    foreach ($category as $each_cate) {
        $cate[$each_cate->parent_id][$each_cate->cate_id] = $each_cate;
    }
    return $cate;
}

function get_revision($post_id) {
    $ci = get_instance();
    return $ci->post->get_revision($post_id);
}

function server_mail($to, $subject, $detail) {
    $ci = get_instance();
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.gmail.com',
        'smtp_port' => 465,
        'smtp_user' => google_user,
        'smtp_pass' => google_pass,
        'mailtype' => 'html',
        'charset' => 'utf-8'
    );

    $name = empty($name) ? "Anonymous" : $name;

    $ci->load->library('email', $config);
    $ci->email->set_newline("\r\n");
    $ci->email->from(google_user, "MFU System");

    $ci->email->to($to);

    $ci->email->subject($subject);
    $ci->email->message(auto_typography($detail));

    return $ci->email->send();
}

function get_latest_post_revision($post_id) {
    $post_revision = get_revision($post_id);
    $post_revision = array_reverse($post_revision);

    $master_post = get_post_by_id($post_id);
    if (count($post_revision)) {
        array_push($post_revision, $master_post);
        foreach ($post_revision as $each_post) {
            if ($each_post->post_status == POST_PUBLISH) {
                return $each_post;
            }
        }
    }

    return $master_post;
}

function get_longdate_format($date) {
    $locale_month = unserialize(FULL_MONTH);
    if (LANG != "th") {
        return date('F d Y', $date);
    } else {
        return date("d", $date) . " " . $locale_month[LANG][date("n", $date)] . " " . (date("Y", $date) + 543);
    }
}

function get_shortdate_format($date) {
    $locale_short_month = unserialize(SHORT_MONTH);
    if (LANG != "th") {
        returndate("d", $date) . " " . $locale_short_month[LANG][date("n", $date)] . " " . date("Y", $date);
    } else {
        return date("d", $date) . " " . $locale_short_month[LANG][date("n", $date)] . " " . (date("Y", $date) + 543);
    }
}

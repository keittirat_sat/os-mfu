<?php

function lang_set() {
    return array(
        "ไม่มีข่าวในหมวดนี้" => "Not found any news in this section",
        "ไม่มีข้อมูลในส่วนนี้" => "Not found any information",
        "อ่านต่อ" => "Read more",
        "ดูทั้งหมด" => "View all",
        "จำนวนผู้เยี่ยมชม" => "Visitor",
        "จำนวนผู้เข้าชม" => "View",
        "ข่าวอื่นๆ" => "Other news",
        "สำนักวิชา" => "School",
        "ข่าวสาร" => "News",
        "ข่าวสารทั่วไป" => "General news",
        "ข่าวประชาสัมพันธ์" => "Information",
        "กิจกรรมนักศึกษา" => "Activity",
        "รางวัล" => "Reward",
        "ข่าวสารล่าสุด" => "Recent news",
        "อ่านข่าวทั้งหมด" => "View all news",
        "ข้อมูลที่น่าสนใจ" => "Suggest",
        "แผนที่และตำแหน่ง" => "Map and Location",
        "สภาพอากาศ" => "Weather",
        "เหตการณ์และกิจกรรมในวันนี้" => "Event and Activity in Today",
        "แสดงหัวข้อทั้งหมดในระบบ" => "All Event and Activity",
        "หน้าแรก" => "Home",
        "แกลลอรี่" => "Gallery",
        "ขออภัยประกาศนี้หมดอายุแล้ว" => "Sorry this post already expired",
        "หมวดหมู่บทความ" => "Category",
        "ทุกหมวดหมู่" => "All Category",
        "ลิ้งค์ต่างๆ" => "Link",
        "หน้าภายในเว็บไซต์" => "Internal Page",
        "ลำดับที่" => "No.",
        "โครงการวิจัย" => "Project",
        "วันที่เผยแพร่" => "Published date",
        "ดูเพิ่ม" => "Read more",
        "ข่าวรับสมัครล่าสุด" => "Recent Admission news",
        "ข่าวการรับสมัครอื่นๆ" => "Other news",
        "ปริญญาตรี" => "Bachelor's news",
        "ปริญญาโท" => "Master's news",
        "ปริญญาเอก" => "Doctoral news",
        "รายละเอียด" => "Information",
        "ไฮไลต์" => "Highlight",
        "การบริการในมหาวิทยาลัย" => "Student Support Services",
        "เกี่ยวกับมหาวิทยาลัย" => "About University",
        "ศิลปะวัฒนธรรม" => "Culture",
        "สุขภาพ" => "Health",
        "ธรรมชาติ" => "Natural",
        "ที่พัก" => "Dormitory",
        "การบริการ" => "Service",
        "อาคาร" => "Building",
        "เกี่ยวกับ มฟล." => "About MFU.",
        "ทุกเดือน" => "All Month",
        "ทุกปี" => "All Years",
        "แหล่งข่าว" => "Source",
        "ปี" => "Year",
        "หัวข้อ" => "Title",
        "อ่านข่าวจากเว็บไซต์เดิม" => "Archive news",
        "ระบบข่าวจากเว็บไซต์เดิม" => "Archieve",
        "ดูข่าวจากระบบเดิม" => "Go to previous version",
        "คำอธิบายสำนักวิชา" => "Overview",
        "ข้อมูลเกี่ยวกับสำนักวิชา" => "About the school & Vision",
        "ผลงานวิจัย" => "Research Interrest",
        "เส้นทางอาชีพของบัณฑิต" => "Career Opertunities",
        "รายชื่อหน่วยงานที่มีความร่วมมือ (ภาครัฐและเอกชน)" => "International Collaboration",
        "หลักสูตรที่เปิดสอนและรายละเอียด" => "Programes",
        "ช่องทางติดต่อ" => "Contact Information",
    );
}

function lang($key) {
    if (LANG == 'th') {
        return $key;
    } else {
        $lang = lang_set();
        return $lang[$key];
    }
}

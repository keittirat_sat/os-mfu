<?php

class admin extends CI_Model {

    private $ci;
    private $lang;

    public function __construct() {
        parent::__construct();
        $this->ci = get_instance();
        $this->lang = LANG;
    }

    public function store_category($category) {
        $cate_type = isset($category['cate_type']) ? $category['cate_type'] : CATE_POST;
        $total = $this->ci->db->from('category')->where('cate_name', $category['cate_name'])->where('cate_type', $cate_type)->count_all_results();
        if ($total == 0) {
            return $this->ci->db->insert('category', $category);
        } else {
            return false;
        }
    }

    public function get_all_category($cate_type = CATE_POST) {
        return $this->ci->db->from('category')->where('cate_type', $cate_type)->order_by('cate_name', 'asc')->get()->result();
    }

    public function get_all_post_in_cate($cate_id, $limit = null, $offset = null, $post_type = TYPE_POST, $schedule = true, $month = 0, $year = 0, $search = null) {
        $cate = $this->ci->db->from('category')->where('cate_id', $cate_id)->get()->result();
        $cate = $cate[0];
//        if ($cate->api_id == 0) {
            //Feed from Local system
            $this->ci->db->from('post')->where('cate_id', $cate_id)->where('post_status', POST_PUBLISH)->where('post_type', $post_type);
            if (is_numeric($limit) && is_numeric($offset)) {
                $this->ci->db->limit($limit, $offset);
            } else if (is_numeric($limit)) {
                $this->ci->db->limit($limit);
            }

            if ($schedule) {
                //Scheduling
                $today = date('U');
//            $this->ci->db->where_in('sch_start', array(0, '<=' . $today))->where_in('sch_exp', array(0, '>' . $today));
                $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
                $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
            }

            if ($month > 0) {
                if ($month < 10) {
                    $month = "0" . $month;
                }
                $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`,'%m')", $month);
            }

            if ($year > 0) {
                $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`,'%Y')", $year);
            }

            if ($search != null) {
                $this->ci->db->where("((`{$this->lang}_post`.`post_title` LIKE '%{$search}%') OR (`{$this->lang}_post`.`post_detail` LIKE '%{$search}%') OR (`{$this->lang}_post`.`post_excerp` LIKE '%{$search}%'))");
            }

            $debug = $this->ci->db->get()->result();
//            echo $this->ci->db->last_query();
            return $debug;
//        } else {
//            $post = $this->ci->post->get_post_by_id($cate->api_id);
//            $api_url = $post->post_detail;
//            $json = json_decode(file_get_contents($api_url));
//            $post = array();
//            foreach ($json as $index => $each_post) {
//                if ($index < $limit) {
//                    $obj = new STDClass();
//                    $obj->post_id = $each_post->id;
//                    $obj->cate_id = $each_post->cat_id;
//                    $obj->post_title = $each_post->title;
//                    $obj->post_thumbnail = $each_post->thumbnail;
//                    $obj->ext_link = $each_post->link;
//                    $obj->post_date = strtotime($each_post->publish);
//                    $obj->post_excerp = $each_post->intro;
//                    $obj->post_detail = $each_post->detail;
//                    array_push($post, $obj);
//                } else {
//                    break;
//                }
//            }
//            return $post;
//        }
    }

    public function get_number_of_post_in_cate($cate_id, $post_type = TYPE_POST, $post_status = POST_PUBLISH, $schedule = true, $month = 0, $year = 0, $search = null) {
        $this->ci->db->from('post')->where('cate_id', $cate_id)->where('post_status', $post_status);
        if ($post_type != null) {
            $this->ci->db->where('post_type', $post_type);
        }

        if ($schedule) {
            //Scheduling
            $today = date('U');
//            $this->ci->db->where_in('sch_start', array(0, '<=' . $today))->where_in('sch_exp', array(0, '>' . $today));
            $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
            $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
        }

        if ($month > 0) {
            if ($month < 10) {
                $month = "0" . $month;
            }
            $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`,'%m')", $month);
        }

        if ($year > 0) {
            $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`,'%Y')", $year);
        }

        if ($search != null) {
            $this->ci->db->where("((`{$this->lang}_post`.`post_title` LIKE '%{$search}%') OR (`{$this->lang}_post`.`post_detail` LIKE '%{$search}%') OR (`{$this->lang}_post`.`post_excerp` LIKE '%{$search}%'))");
        }

        return $this->ci->db->count_all_results();
    }

    public function get_all_post_by_type($post_type = TYPE_POST, $post_status = POST_PUBLISH) {
        $post = $this->ci->db->from('post')->where('post_type', $post_type)->where('post_status', $post_status)->get();
        return $post->result();
    }

    public function update_cate_name($cate_id, $cate_info) {
        return $this->ci->db->where('cate_id', $cate_id)->update('category', $cate_info);
    }

    public function del_cate($cate_id) {
        $f = $this->ci->db->where('cate_id', $cate_id)->delete('category');
        if ($f) {
            $this->ci->db->where('cate_id', $cate_id)->update('post', array('cate_id' => 0));
            return true;
        }
        return false;
    }

    public function get_all_users_info($parent_uid = 0) {
        $this->ci->db->from('users')->order_by('real_name');
        if ($parent_uid != 0) {
            $this->ci->db->where('parent_uid', $parent_uid);
            $this->ci->db->or_where('uid', $parent_uid);
        }
        return $this->ci->db->get()->result();
    }

    public function check_user_on_exist($email) {
        return $this->ci->db->from('users')->where('email', $email)->count_all_results();
    }

    public function create_account($user_obj) {
        $user_obj['disallow'] = '{"section":[],"permission":[]}';
        return $this->ci->db->insert('users', $user_obj);
    }

    public function get_all_account($level = 0) {
        $this->ci->db->from('users')->order_by('real_name');
        if ($level != 0) {
            $this->ci->db->where('level', $level);
        }
        $user = $this->ci->db->get()->result();
        foreach ($user as &$each_user) {
            $each_user->total_post = $this->get_total_of_publish_post_by_uid($each_user->uid, TYPE_POST);
            $each_user->total_page = $this->get_total_of_publish_post_by_uid($each_user->uid, TYPE_PAGE);
        }
        return $user;
    }

    public function update_account($obj, $uid) {
        return $this->ci->db->where('uid', $uid)->update('users', $obj);
    }

    public function get_total_of_publish_post_by_uid($uid, $post_type = TYPE_POST) {
        return $this->ci->db->from('post')->where('post_status', POST_PUBLISH)->where('post_type', $post_type)->where('post_author', $uid)->count_all_results();
    }

    public function set_stat($key) {
        $stat = $this->get_stat($key);
        if ($stat) {
            $counter = $stat[0]->counter + 1;
            $this->ci->db->where('ref_id', $key)->update('stat', array('counter' => $counter));
        } else {
            $counter = 1;
            $this->ci->db->insert('stat', array('ref_id' => $key, 'counter' => $counter));
        }
        return $counter;
    }

    public function get_stat($key) {
        $stat = $this->ci->db->from('stat')->where('ref_id', $key)->get()->result();
        return count($stat) > 0 ? $stat : false;
    }

}

?>

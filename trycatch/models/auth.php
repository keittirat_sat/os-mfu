<?php

class Auth extends CI_Model {

    private $ci;

    public function __construct() {
        parent::__construct();
        $this->ci = get_instance();
    }

    public function take_login($obj) {
        if (valid_email($obj['email'])) {
            $password = do_hash($obj['password']);
            $user = $this->ci->db->from('users')->where('email', $obj['email'])->where('password', $password)->where('status', 1)->get();
            $user = $user->result();
            if (count($user)) {
                return $user[0];
            }
            return false;
        }
        return false;
    }

    public function set_login_session($user_record) {
        $this->ci->session->set_userdata(array(
            'uid' => $user_record->uid,
            'real_name' => $user_record->real_name,
            'email' => $user_record->email,
            'status' => $user_record->status,
            'disallow' => json_decode($user_record->disallow),
            'level' => $user_record->level,
            'parent_uid' => $user_record->parent_uid
        ));
    }

}

?>

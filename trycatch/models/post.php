<?php

class Post extends CI_Model {

    private $ci;
    private $lang;

    public function __construct() {
        parent::__construct();
        $this->ci = get_instance();
        $this->lang = LANG;
    }

    public function get_latest_news($limit = 20, $offset = 0, $schedule = true) {
        $this->ci->db->select('post.*, media.link, media.media_name, media.post_id as mid_post_id')->from('post')->join('media', 'post.post_thumbnail = media.mid', 'left');
        $this->ci->db->where('post.post_type', TYPE_POST);
        $this->ci->db->where('post.post_status', POST_PUBLISH);
        $this->ci->db->where_in('post.cate_id', array(10, 21, 15, 12, 14, 22, 24, 23));
        if ($schedule) {
            $today = date('U');
//            $this->ci->db->where_in('sch_start', array(0, '<=' . $today))->where_in('sch_exp', array(0, '>' . $today));
            $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
            $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
        }
        return $this->ci->db->order_by('post.post_date', 'desc')->limit($limit, $offset)->get()->result();
    }

    public function get_number_latest_news($schedule = true) {
        $this->ci->db->select('post.*, media.link, media.media_name, media.post_id as mid_post_id')->from('post')->join('media', 'post.post_thumbnail = media.mid', 'left');
        $this->ci->db->where('post.post_type', TYPE_POST);
        $this->ci->db->where('post.post_status', POST_PUBLISH);
        $this->ci->db->where_in('post.cate_id', array(10, 21, 15, 12, 14, 22, 24, 23));
        if ($schedule) {
            $today = date('U');
//            $this->ci->db->where_in('sch_start', array(0, '<=' . $today))->where_in('sch_exp', array(0, '>' . $today));
            $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
            $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
        }
        return $this->ci->db->order_by('post.post_date', 'desc')->count_all_results();
    }

    public function get_latest_admission_news($limit = 20, $offset = 0, $schedule = true, $group = 1) {
        $this->ci->db->select('post.*, media.link, media.media_name, media.post_id as mid_post_id')->from('post')->join('media', 'post.post_thumbnail = media.mid', 'left');
        $this->ci->db->where('post.post_type', TYPE_POST);
        $this->ci->db->where('post.post_status', POST_PUBLISH);
        if ($group == 1) {
            $this->ci->db->where_in('post.cate_id', array(25, 26, 27));
        } else {
            $this->ci->db->where_in('post.cate_id', array(7, 8, 9));
        }
        if ($schedule) {
            $today = date('U');
//            $this->ci->db->where_in('sch_start', array(0, '<=' . $today))->where_in('sch_exp', array(0, '>' . $today));
            $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
            $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
        }
        return $this->ci->db->order_by('post.post_date', 'desc')->limit($limit, $offset)->get()->result();
    }

    public function get_number_latest_admission_news($limit = 20, $offset = 0, $schedule = true, $group = 1) {
        $this->ci->db->select('post.*, media.link, media.media_name, media.post_id as mid_post_id')->from('post')->join('media', 'post.post_thumbnail = media.mid', 'left');
        $this->ci->db->where('post.post_type', TYPE_POST);
        $this->ci->db->where('post.post_status', POST_PUBLISH);
        if ($group == 1) {
            $this->ci->db->where_in('post.cate_id', array(25, 26, 27));
        } else {
            $this->ci->db->where_in('post.cate_id', array(7, 8, 9));
        }
        if ($schedule) {
            $today = date('U');
//            $this->ci->db->where_in('sch_start', array(0, '<=' . $today))->where_in('sch_exp', array(0, '>' . $today));
            $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
            $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
        }
        return $this->ci->db->order_by('post.post_date', 'desc')->count_all_results();
    }

    public function get_post_by_id($post_id, $userinfo = true) {
        $this->ci->db->from('post');

        if ($userinfo) {
            $this->ci->db->join('users', 'users.uid = post.post_author', 'left');
        }

        $temp = $this->ci->db->where('post.post_id', $post_id)->get()->result();

        return $temp[0];
    }

    public function get_revision($post_id) {
        return $this->ci->db->from('post')->join('users', 'users.uid = post.post_author', 'left')->where('post.post_type', TYPE_REVISION)->where('post.revision', $post_id)->order_by('post.post_id', 'asc')->get()->result();
    }

    public function get_post_by_status($post_status = POST_PUBLISH, $post_type = TYPE_POST, $users = 0, $category = 0, $page = 1, $total_per_page = 20, $order_by = 'desc', $schedule = true, $day = 0, $month = 0, $year = 0, $post_excerp = 0, $backend = false, $search = NULL) {
        $offset = ($page - 1) * $total_per_page;
        $this->ci->db->from('post')->join('users', 'users.uid = post.post_author', 'left')->where('post.post_status', $post_status)->where('post.post_type', $post_type)->where('post.post_modify >', 0);

        if ($post_type == TYPE_POST) {
            $this->ci->db->join('category', 'post.cate_id = category.cate_id', 'left');
        }

        if ($users != 0) {
            $this->ci->db->where('post.post_author', $users);
        } else if (loged()) {
            if (get_user_level() == USER_DIRECTOR && $backend) {
                $this->ci->db->where('users.parent_uid', get_user_uid());
                $this->ci->db->or_where('post.post_author', get_user_uid());
            }
        }

        if (is_array($category)) {
            if (count($category) > 0) {
                $this->ci->db->where_in('post.cate_id', $category);
            }
        } else if ($category != 0) {
            $this->ci->db->where('post.cate_id', $category);
        }

        if ($schedule && $post_type == TYPE_POST) {
            $today = date('U');
//            $this->ci->db->where_in('post.sch_start', array(0, '<=' . $today))->where_in('post.sch_exp', array(0, '>' . $today));
            $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
            $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
        }

        if ($post_excerp != 0 && $post_type == TYPE_RESEARCH) {
            $this->ci->db->where('post.post_excerp', $post_excerp);
        }

        if ($post_type == TYPE_CLIPPING || $post_type == TYPE_GALLERY || $post_type == TYPE_EVENT || $post_type == TYPE_NEWSLETTER) {
            if ($day > 0) {
                $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`,'%d')", $day);
            }

            if ($month != 0) {
                $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`, '%m' ) = " . ($month < 10 ? "0" . $month : $month));
            }

            if ($year != 0) {
                $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`, '%Y' ) = " . $year);
            }
        }

        if ($search != NULL) {
            $this->ci->db->where("((`{$this->lang}_post`.`post_title` LIKE '%{$search}%') OR (`{$this->lang}_post`.`post_detail` LIKE '%{$search}%') OR (`{$this->lang}_post`.`post_excerp` LIKE '%{$search}%'))");
        }

        if ($post_type == TYPE_CLIPPING || $post_type == TYPE_EVENT) {
            $this->ci->db->order_by('post.post_date', $order_by);
        } else if ($post_type == TYPE_SCHOOL) {
            $this->ci->db->order_by('post.post_thumbnail', "asc");
        } else {
            $this->ci->db->order_by('post.post_id', $order_by);
        }

        $this->ci->db->limit($total_per_page, $offset);

        return $this->ci->db->get()->result();
    }

    public function get_group_year_of_post_by_cate($cate_id, $post_status = POST_PUBLISH) {
        $sql = "SELECT `post_date` as post_year FROM (`{$this->lang}_post`) WHERE `post_status` = {$post_status} and `cate_id` = {$cate_id} GROUP BY FROM_UNIXTIME(`post_date`, '%Y' )";
        return $this->ci->db->query($sql)->result();
    }

    public function get_group_month_of_post_by_cate($cate_id, $post_status = POST_PUBLISH) {
        $sql = "SELECT `post_date` as post_month FROM (`{$this->lang}_post`) WHERE `post_status` = {$post_status} and `cate_id` = {$cate_id} GROUP BY FROM_UNIXTIME(`post_date`, '%m' )";
        return $this->ci->db->query($sql)->result();
    }

    public function get_group_year_of_post_by_type($post_type, $post_status = POST_PUBLISH) {
        $sql = "SELECT `post_date` as post_year, FROM_UNIXTIME(`{$this->lang}_post`.post_date, '%Y') as year FROM (`{$this->lang}_post`) WHERE `post_status` = {$post_status} and `post_type` = {$post_type} GROUP BY FROM_UNIXTIME(`post_date`, '%Y' )";
        return $this->ci->db->query($sql)->result();
    }

    public function get_group_month_of_post_by_type($post_type, $post_status = POST_PUBLISH) {
        $sql = "SELECT `post_date` as post_month FROM (`{$this->lang}_post`) WHERE `post_status` = {$post_status} and `post_type` = {$post_type} GROUP BY FROM_UNIXTIME(`post_date`, '%m' )";
        return $this->ci->db->query($sql)->result();
    }

    public function get_all_api() {
        return $this->ci->db->from('post')->where('post.post_status', POST_PUBLISH)->join('users', 'users.uid = post.post_author', 'left')->where('post.post_type', TYPE_API)->order_by('post.post_date', 'desc')->get()->result();
    }

    public function get_event_by_status($post_status = POST_PUBLISH, $limit = 5, $offset = 0, $event_type = EVENT_OTHER) {
        $this->ci->db->from('post')->where('post.post_status', $post_status)->join('users', 'users.uid = post.post_author', 'left')->where('post.post_type', TYPE_EVENT);
        if (is_numeric($event_type)) {
            $this->ci->db->where('post.cate_id', $event_type);
        }
        return $this->ci->db->order_by('post.post_date', 'desc')->limit($limit, $offset)->get()->result();
    }

    public function get_number_event_by_status($post_status = POST_PUBLISH) {
        return $this->ci->db->from('post')->where('post.post_status', $post_status)->join('users', 'users.uid = post.post_author', 'left')->where('post.post_type', TYPE_EVENT)->count_all_results();
    }

    public function get_all_slide_by_status($status = 1, $q = null, $type = null, $month = null, $year = null) {
        $today = date('U');


        //Slide from POST
        $this->ci->db->select('post.*, media.link')->from('post')->join('media', 'media.mid = post.post_thumbnail')->where('post.post_slide', 1)->where('post.post_status', POST_PUBLISH);

        if (is_numeric($status)) {
            if ($status) {
                //Slide Online
                $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
                $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
            } else {
                //Slide Expire
                $this->ci->db->where("(`{$this->lang}_post`.`sch_start` > {$today} OR `{$this->lang}_post`.`sch_exp` < {$today}) AND (`{$this->lang}_post`.`sch_start` != 0 OR `{$this->lang}_post`.`sch_exp` != 0)");
            }
        }

        if ((is_numeric($month) && ($month > 0)) && (is_numeric($year) && ($year > 0))) {
            $this->ci->db->where("(FROM_UNIXTIME(`{$this->lang}_post`.post_date, '%Y') = {$year} AND FROM_UNIXTIME(`{$this->lang}_post`.post_date, '%m') = {$month})");
        } else if (is_numeric($month) && ($month > 0)) {
            $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.post_date, '%m') = {$month}");
        } else if (is_numeric($year) && ($year > 0)) {
            $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.post_date, '%Y') = {$year}");
        }

        if (isset($q)) {
            $this->ci->db->like("post.post_title", $q);
        }

        $normal_post = $this->ci->db->get()->result();

        //--------------------------------------------------------------------------------------------------------------------------------------------
        //Slide from Custom System
        $this->ci->db->select('post.*, media.link')->from('post')->join('media', 'media.post_id = post.post_id')->where('post.post_type', TYPE_SLIDE)->where('post.post_status', POST_PUBLISH);

        if (is_numeric($status)) {
            if ($status) {
                //Slide Online
                $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
                $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
            } else {
                //Slide Expire   
                $this->ci->db->where("(`{$this->lang}_post`.`sch_start` > {$today} OR `{$this->lang}_post`.`sch_exp` < {$today}) AND (`{$this->lang}_post`.`sch_start` != 0 OR `{$this->lang}_post`.`sch_exp` != 0)");
            }
        }

        if (isset($q)) {
            $this->ci->db->like("post.post_title", $q);
        }

        if ((is_numeric($month) && ($month > 0)) && (is_numeric($year) && ($year > 0))) {
            $this->ci->db->where("(FROM_UNIXTIME(`{$this->lang}_post`.post_date, '%Y') = {$year} AND FROM_UNIXTIME(`{$this->lang}_post`.post_date, '%m') = {$month})");
        } else if (is_numeric($month) && ($month > 0)) {
            $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.post_date, '%m') = {$month}");
        } else if (is_numeric($year) && ($year > 0)) {
            $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.post_date, '%Y') = {$year}");
        }

        $slide_post = $this->ci->db->get()->result();

        $new_post = array();

        foreach ($normal_post as $each_post) {
            $new_post[$each_post->post_id] = $each_post;
        }

        foreach ($slide_post as $each_post) {
            $new_post[$each_post->post_id] = $each_post;
        }

        krsort($new_post);
        return $new_post;
    }

    public function get_all_slide($schedule = true) {
        $this->ci->db->select('post.*, media.link')->from('post')->join('media', 'media.mid = post.post_thumbnail')->where('post.post_slide', 1)->where('post.post_status', POST_PUBLISH);

        if ($schedule) {
            $today = date('U');
            $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
            $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
        }

        $normal_post = $this->ci->db->get()->result();

        $this->ci->db->select('post.*, media.link')->from('post')->join('media', 'media.post_id = post.post_id')->where('post.post_type', TYPE_SLIDE)->where('post.post_status', POST_PUBLISH);

        if ($schedule) {
            $today = date('U');
//            $this->ci->db->where_in('post.sch_start', array(0, '<=' . $today))->where_in('post.sch_exp', array(0, '>' . $today));
            $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
            $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
        }

        $slide_post = $this->ci->db->get()->result();

        $new_post = array();

        foreach ($normal_post as $each_post) {
            $new_post[$each_post->post_id] = $each_post;
        }

        foreach ($slide_post as $each_post) {
            $new_post[$each_post->post_id] = $each_post;
        }
        krsort($new_post);
        return $new_post;
    }

    public function get_number_of_page($post_status = POST_PUBLISH, $post_type = TYPE_POST, $users = 0, $category = 0, $total_per_page = 20, $schedule = true, $day = 0, $month = 0, $year = 0, $post_excerp = 0, $backend = false, $search = NULL) {
        $this->ci->db->from('post')->join('users', 'users.uid = post.post_author', 'left')->where('post.post_status', $post_status)->where('post.post_type', $post_type)->where('post.post_modify >', 0);

        if ($post_type == TYPE_POST) {
            $this->ci->db->join('category', 'post.cate_id = category.cate_id', 'left');
        }

        if ($users != 0) {
            $this->ci->db->where('post.post_author', $users);
        } else if (loged()) {
            if (get_user_level() == USER_DIRECTOR && $backend) {
                $this->ci->db->where('users.parent_uid', get_user_uid());
                $this->ci->db->or_where('post.post_author', get_user_uid());
            }
        }

        if (is_array($category)) {
            if (count($category) > 0) {
                $this->ci->db->where_in('post.cate_id', $category);
            }
        } else if ($category != 0) {
            $this->ci->db->where('post.cate_id', $category);
        }

        if ($schedule && $post_type == TYPE_POST) {
            $today = date('U');
//            $this->ci->db->where_in('post.sch_start', array(0, '<=' . $today))->where_in('post.sch_exp', array(0, '>' . $today));
            $this->ci->db->where("(`{$this->lang}_post`.`sch_start` = 0 OR `{$this->lang}_post`.`sch_start` <= {$today})");
            $this->ci->db->where("(`{$this->lang}_post`.`sch_exp` = 0 OR  `{$this->lang}_post`.`sch_exp` >= {$today})");
        }

        if ($post_excerp != 0 && $post_type == TYPE_RESEARCH) {
            $this->ci->db->where('post.post_excerp', $post_excerp);
        }

        if ($post_type == TYPE_CLIPPING || $post_type == TYPE_GALLERY || $post_type == TYPE_EVENT || $post_type == TYPE_NEWSLETTER) {
            if ($day > 0) {
                $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`,'%d')", $day);
            }

            if ($month != 0) {
                $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`, '%m' ) = " . ($month < 10 ? "0" . $month : $month));
            }

            if ($year != 0) {
                $this->ci->db->where("FROM_UNIXTIME(`{$this->lang}_post`.`post_date`, '%Y' ) = " . $year);
            }
        }

        if ($search != NULL) {
            $this->ci->db->where("((`{$this->lang}_post`.`post_title` LIKE '%{$search}%') OR (`{$this->lang}_post`.`post_detail` LIKE '%{$search}%') OR (`{$this->lang}_post`.`post_excerp` LIKE '%{$search}%'))");
        }

        $total_post = $this->ci->db->count_all_results();


        return ceil($total_post / $total_per_page);
    }

    public function get_last_record($post_status, $post_type = TYPE_POST) {
        $temp = $this->ci->db->from('post')->where('post_status', $post_status)->where('post_type', $post_type)->order_by('post_id', 'DESC')->get
                ()->result();
        return $temp[0];
    }

    public function insert_empty_post($post_type = 1) {
        /*
         * Default type is POST
         */
        $user_info = get_all_session_info();
        $args = array(
            'post_type' => $post_type,
            'post_author' => $user_info['uid'],
            'post_date' => date('U'),
            'post_modify' => 0,
            'post_status' => 0
        );

        $this->ci->db->insert('post', $args);
    }

    public function insert_post_with_data($post) {
        return $this->ci->db->insert('post', $post);
    }

    public function get_group_year_special_post($post_type = TYPE_CLIPPING) {
        return $this->ci->db->select('post_excerp as clipping_year')->from('post')->where('post_type', $post_type)->group_by('post_excerp')->get()->result();
    }

    public function get_group_month_special_post($post_type = TYPE_CLIPPING) {
        return $this->ci->db->select('post_detail as clipping_month')->from('post')->where('post_type', $post_type)->group_by('post_detail')->get()->result();
    }

    public function get_group_month_year_special_post($post_type = TYPE_GALLERY, $cate_id = 0) {
        $this->ci->db->select('post_detail as month, post_excerp as year')->from('post')->where('post_status', POST_PUBLISH)->where('post_type', $post_type);
        if ($cate_id != 0) {
            $this->ci->db->where('cate_id', $cate_id);
        }
        return $this->ci->db->group_by('month, year')->
                        order_by('post_id', 'desc')->get()->result();
    }

    public function update_post($post_obj, $post_id) {
        $post_obj['post_modify'] = date('U');
        return $this->ci->db->where('post_id', $post_id)->update('post', $post_obj);
    }

    public function reserv_post($post_type = 1) {
        $this->insert_empty_post($post_type);

        return $this->get_last_record(0, $post_type);
    }

    public function insert_media($obj) {
        return

                $this->ci->db->insert('media', $obj);
    }

    public function get_lates_media($post_id = 0) {
        $this->ci->db->from('media')->limit(1)->order_by('mid', 'desc');
        if ($post_id != 0) {
            $this->ci->db->where('post_id', $post_id);
        }
        $record = $this->ci->db->get()->result();

        return

                count($record) > 0 ? $record[0] : false;
    }

    public function delete_post($post_id, $truncate = false) {
        if (!$truncate) {
            //Just temporary
            return $this->ci->db->where('post_id', $post_id)->update('post', array(
                        'post_status' => 0
            ));
        } else {
            //Permanently delete
            return $this->ci->db->where('post_id', $post_id)->delete('post');
        }
    }

    public function delete_all_revision_by_parent_id($post_id) {
        return $this->ci->db->where('revision', $post_id)->delete('post');
    }

    public function get_media_mid($mid) {
        $media = $this->ci->db->from('media')->where('mid', $mid)->get()->result();
        return $media[0];
    }

    public function get_all_media_sort_by_name($post_id = 0, $order_by = 'desc') {
        $this->ci->db->from('media')->join('users', 'users.uid = media.uid');
        if ($post_id != 0) {
            $this->ci->db->where('media.post_id', $post_id);
        }

        return $media = $this->ci->db->order_by('media.media_name', $order_by)->get()->result();
    }

    public function get_all_media($post_id = 0, $order_by = 'desc', $limit = null, $offset = null, $media_type = 0, $q = null) {
        $this->ci->db->from('media')->join('users', 'users.uid = media.uid');
        if ($post_id != 0) {
            $this->ci->db->where('media.post_id', $post_id);
        }

        if (!is_numeric($media_type)) {
            $this->ci->db->where('media.media_type', $media_type);
        }

        if ($q) {
            $this->ci->db->like('media.media_name', $q);
        }

        if (is_numeric($limit) && is_numeric($offset)) {
            $this->ci->db->limit($limit, $offset);
        } else if (is_numeric($limit)) {
            $this->ci->db->limit($limit);
        }

        return $media = $this->ci->db->order_by('media.mid', $order_by)->get()->result();
    }

    public function get_number_of_media($type = 0, $q = null) {
        $this->ci->db->from('media');
        if (!is_numeric($type)) {
            $this->ci->db->where("media_type", $type);
        }

        if ($q) {
            $this->ci->db->like('media_name', $q);
        }

        return $this->ci->db->count_all_results();
    }

    public function delete_media($mid) {
        $media = $this->ci->db->from('media')->where('mid', $mid)->get()->result();
        $media = $media[0];

        if ($media->media_type === "img") {
            $path = json_decode($media->path);
            unlink(is_file($path->s) ? $path->s : "./uploads/{$media->post_id}/s_{$media->media_name}");
            unlink(is_file($path->m) ? $path->m : "./uploads/{$media->post_id}/m_{$media->media_name}");
            unlink(is_file($path->l) ? $path->l : "./uploads/{$media->post_id}/{$media->media_name}");
        } else if ($media->media_type === "pdf") {
            $path = $media->path;
            unlink($path);
            $pdf_image = json_decode($media->media_info);
            foreach ($pdf_image->pdf_image as $each_image) {
                delete_files($each_image->path, true);
            }
        } else {
            $path = $media->path;
            unlink($path);
        }

        $flag = array();
        if ($this->ci->db->where('mid', $mid)->delete('media')) {
            $flag['status'] = "success";
        } else {
            $flag['status'] = "fail";
        }
        return json_encode($flag);
    }

    public function get_category_cate_id($cate_id) {
        $cate = $this->ci->db->from('category')->where('cate_id', $cate_id)->get()->result();
        return $cate[0];
    }

    public function get_post_in_multiple_cate($array_of_cate, $limit = 20) {
        return $this->ci->db->from('post')->where_in('cate_id', $array_of_cate)->order_by('post_date', 'desc')->limit($limit)->get()->result();
    }

    public function get_category_by_parent_cate_id($cate_id) {
        return $this->ci->db->from('category')->where('parent_id', $cate_id)->get()->result();
    }

    public function get_user_uid($uid) {
        $user = $this->ci->db->from('users')->where('uid', $uid)->get
                ()->result();
        return $user[0];
    }

    public function get_meta($meta_id) {
        $meta = $this->ci->db->from('metadata')->where('meta_id', $meta_id)->get()->result();
        if ($meta) {
            return

                    $meta;
        }
        return null;
    }

    public function update_menu($json) {
        if ($this->ci->db->from('metadata')->where('meta_id', 'menu')->count_all_results()) {
            $meta = $this->ci->db->where('meta_id', 'menu')->update('metadata', array('meta_info' => $json));
        } else {
            $meta = $this->ci->db->insert('metadata', array('meta_id' => 'menu', 'meta_info' =>
                $json));
        }
        return $meta;
    }

    public function add_group_link($post_obj) {
        $post_id = $this->reserv_post(TYPE_MENUGROUP);
        $status['status'] = $this->update_post($post_obj, $post_id->post_id);
        $status['post_id'] = $post_id->post_id;
        return $status;
    }

    public function update_group_link($post_obj, $post_id) {
        return

                $this->update_post($post_obj, $post_id);
    }

    public function get_group_link() {
        return $this->get_post_by_status(POST_PUBLISH, TYPE_MENUGROUP, $users = 0, $category = 0, $page = 1, $total_per_page = 2000);
    }

    public function get_group_link_by_id($post_id) {
        return $this->get_post_by_id($post_id);
    }

    public function add_audience($post_obj) {
        $post_id = $this->reserv_post(TYPE_AUDIENCE);
        $status['status'] = $this->update_post($post_obj, $post_id->post_id);
        $status['post_id'] = $post_id->post_id;
        return $status;
    }

    public function get_audience() {
        return $this->get_post_by_status(POST_PUBLISH, TYPE_AUDIENCE);
    }

    public function update_widget($widget) {
        return $this->ci->db->where('meta_id', 'widget')->update('metadata', array
                    ('meta_info' => $widget));
    }

    public function get_widget() {
        $widget = $this->ci->db->from('metadata')->where('meta_id', 'widget')->get()->result();
        $widget = $widget[0];

        return json_decode($widget->meta_info);
    }

    public function get_cate_info($cate_id) {
        $cate = $this->ci->db->from('category')->where('cate_id', $cate_id)->get
                ()->result();
        return $cate[0];
    }

    public function get_random_image_from_gallery() {
        return $this->ci->db->select('media.*')->from('post')->join('media', 'media.post_id = post.post_id')->where('post.post_type', TYPE_GALLERY)->limit(100)->get()->result();
    }

}

?>

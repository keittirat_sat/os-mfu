<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-9">
            <h1 class="red th_san"><?php echo $school->post_title; ?></h1>
        </div>
        <div class="col-xs-3">
            <a href="<?php echo $school_info->post_other_website; ?>" class="btn_gotosite <?php echo LANG ?>"></a>
        </div>
    </div>

    <?php if (trim($school->post_excerp) != ""): ?>
        <div class="row">
            <div class="col-xs-12">
                <?php echo $school->post_excerp; ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-xs-9">
            <div class="panel-group" id="schoolinfo">
                <div class="panel panel-mfu">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#schoolinfo" href="#overview">
                                <?php echo lang("คำอธิบายสำนักวิชา"); ?>
                            </a>
                        </h4>
                    </div>
                    <div id="overview" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <?php echo trim($school_info->post_other_home) != "" ? $school_info->post_other_home : "No Content"; ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-mfu">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#schoolinfo" href="#vision">
                                <?php echo lang("ข้อมูลเกี่ยวกับสำนักวิชา"); ?>
                            </a>
                        </h4>
                    </div>
                    <div id="vision" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php echo trim($school_info->post_other_about) != "" ? $school_info->post_other_about : "No Content"; ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-mfu">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#schoolinfo" href="#research">
                                <?php echo lang("ผลงานวิจัย"); ?>
                            </a>
                        </h4>
                    </div>
                    <div id="research" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php echo trim($school_info->post_other_research) != "" ? $school_info->post_other_research : "No Content"; ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-mfu">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#schoolinfo" href="#career">
                                <?php echo lang("เส้นทางอาชีพของบัณฑิต"); ?>
                            </a>
                        </h4>
                    </div>
                    <div id="career" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php echo trim($school_info->post_other_career) != "" ? $school_info->post_other_career : "No Content"; ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-mfu">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#schoolinfo" href="#inter">
                                <?php echo lang("รายชื่อหน่วยงานที่มีความร่วมมือ (ภาครัฐและเอกชน)"); ?>
                            </a>
                        </h4>
                    </div>
                    <div id="inter" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php echo trim($school_info->post_other_collaboration) != "" ? $school_info->post_other_collaboration : "No Content"; ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-mfu">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#schoolinfo" href="#program">
                                <?php echo lang("หลักสูตรที่เปิดสอนและรายละเอียด"); ?>
                            </a>
                        </h4>
                    </div>
                    <div id="program" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php echo trim($school_info->post_other_programmes) != "" ? $school_info->post_other_programmes : "No Content"; ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-mfu">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#schoolinfo" href="#info">
                                <?php echo lang("ช่องทางติดต่อ"); ?>
                            </a>
                        </h4>
                    </div>
                    <div id="info" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><b>Phone:</b> <?php echo trim($school_info->post_other_phone) != "" ? $school_info->post_other_phone : " - "; ?></p>
                            <p><b>Fax:</b> <?php echo trim($school_info->post_other_fax) != "" ? $school_info->post_other_fax : " - "; ?></p>
                            <p><b>Email:</b> <?php echo trim($school_info->post_other_email) != "" ? $school_info->post_other_email : " - "; ?></p>
                            <p><b>Website:</b> <?php echo trim($school_info->post_other_website) != "" ? $school_info->post_other_website : " - "; ?></p>
                            <p><b>Address</b></p>
                            <p><?php echo trim($school_info->post_other_address) != "" ? $school_info->post_other_address : " - "; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
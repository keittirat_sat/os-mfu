<div class="container margin_top_20">    
    <div class="row">
        <div class="col-xs-12">
            <h1 class="red th_san margin_top_0 margin_bottom_20"><?php echo $api_detail->post_title; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-9">
            <ol class="breadcrumb mfu">
                <li><a href="<?php echo site_url(); ?>"><?php echo lang("หน้าแรก"); ?></a></li>
                <li class="active"><?php echo ellipsize($api_detail->post_title, 60, 0.5) . " | " . lang('ระบบข่าวจากเว็บไซต์เดิม'); ?></li>
            </ol>
            <div class="row">
                <?php if (count($all_post)): ?>

                    <?php foreach ($all_post as $each_post): ?>

                        <?php
                        $post = new STDClass();
                        $post->post_id = $each_post->id;
                        $post->cate_id = $each_post->cat_id;
                        $post->post_title = $each_post->title;
                        $post->post_thumbnail = $each_post->thumbnail;
                        $post->ext_link = $each_post->link;
                        $post->post_date = strtotime($each_post->publish);
                        $post->post_excerp = html_entity_decode($each_post->intro);
                        $post->post_detail = html_entity_decode($each_post->detail);
                        ?>

                        <div class="row" style='margin-bottom: 15px;'>
                            <?php $cate_post = widget_cate_api($post); ?>
                            <div class="widget_cate_thumbnail">
                                <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] . "?ref={$api_id}" ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                            </div>
                            <div class="widget_cate_info cate_page">
                                <h3 class="th_san margin_top_0 red_1">
                                    <a title="<?php echo $cate_post['alt_title'] ?>" href='<?php echo $cate_post['url'] . "?ref={$api_id}"; ?>' class="link_inherit"><?php echo $cate_post['post_title']; ?></a> &dash;
                                    <small><i><?php echo get_longdate_format($cate_post['date']); ?></i></small>
                                </h3>
                                <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h3 class="txt_center th_san"><?php echo lang("ไม่มีข่าวในหมวดนี้"); ?></h3>
                <?php endif; ?>
            </div>
            <div class="row">
                <ul class="pagination pagination-sm">
                    <?php $total_page = $page_data->total_page; ?>
                    <?php $endPage = ($startpage + 10) < $total_page ? ($startpage + 10) : $total_page ?>
                    <?php for ($i = $startpage; $i < $endPage; $i++): ?>
                        <li <?php echo $page == $i ? "class='active'" : ""; ?>>
                            <a href="<?php echo "?page={$i}&startpage={$startpage}" ?>"><?php echo $i + 1 ?></a>
                        </li>
                    <?php endfor; ?>

                    <?php if ($total_page > 10): ?>
                        <li class="dropdown">
                            <a href="#" class="" id="dropdownMenu1" data-toggle="dropdown">
                                Jump to page (<?php echo $total_page ?>)
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" style="margin: 30px 0 0 -72px;">
                                <?php for ($p = 0; $p < $total_page; $p+=10): ?>    
                                    <li role="presentation" <?php echo $p == $startpage ? "class='active'" : ""; ?>>
                                        <?php $p_index = $p + 1; ?>
                                        <?php $p_endpoint = ($p + 10) <= $total_page ? ($p + 10) : $total_page ?>
                                        <a role="menuitem" tabindex="-1" href="<?php echo "?page={$p}&startpage={$p}"; ?>"><?php echo "{$p_index} - {$p_endpoint}"; ?></a>
                                    </li>
                                <?php endfor ?>
                            </ul>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
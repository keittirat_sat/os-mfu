<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="red th_san margin_top_0 margin_bottom_20"><?php echo $post->post_title; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-9">
            <ol class="breadcrumb mfu">
                <li><a href="<?php echo site_url(); ?>">หน้าแรก</a></li>
                <?php switch ($post->cate_id): case EVENT_ADMISSION: ?>
                        <li><a href="<?php echo site_url("mfu/all_event/admission"); ?>">ปฏิทินการรับสมัคร</a></li>
                        <?php break; ?>
                    <?php case EVENT_GUEST: ?>
                        <li><a href="<?php echo site_url("mfu/all_event/visitor"); ?>">ปฏิทินผู้เยี่ยมชม</a></li>
                        <?php break; ?>
                    <?php case EVENT_OTHER: ?>
                        <li><a href="<?php echo site_url("mfu/all_event/general"); ?>">ปฏิทินทั่วไป</a></li>
                        <?php break; ?>
                    <?php default: ?>
                        <li><a href="<?php echo site_url("mfu/all_event"); ?>">ปฏิทินทั้งหมด</a></li>
                        <?php break; ?>
                <?php endswitch; ?>
                <li class="active"><?php echo ellipsize($post->post_title, 60, 0.5); ?></li>
            </ol>
            <?php echo auto_typography($post->post_detail); ?>
            <p class="txt_right"><span class="badge badge-important"><i class="glyphicon glyphicon-user"></i> <?php echo lang("จำนวนผู้เข้าชม") ?>: <font id="id_holder_stat"></font></span></p>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
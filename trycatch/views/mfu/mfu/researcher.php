<?php $re_cate = array(); ?>
<?php foreach ($category as $each_cate): ?>
    <?php $re_cate[$each_cate->post_id] = $each_cate ?>
<?php endforeach; ?>
<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-3">
            <ul class="nav nav-pills nav-stacked">
                <li class="dropdown active">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="glyphicon glyphicon-user"></i>&nbsp;
                        <?php if ($position == 0): ?>
                            ทุกตำแหน่ง
                        <?php else: ?>
                            <?php switch ($position): case POSITION_PROF: ?>
                                    ศาสตราจารย์
                                    <?php break; ?>
                                <?php case POSITION_ASSIS_PROF: ?>
                                    รองศาสตราจารย์
                                    <?php break; ?>
                                <?php case POSITION_ASSOC_PROF: ?>
                                    ผู้ช่วยศาสตราจารย์
                                    <?php break; ?>
                            <?php endswitch; ?>
                        <?php endif; ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li <?php echo $position == 0 ? "class='active'" : ""; ?>><a href="?position=0">ทุกตำแหน่ง</a></li>
                        <li <?php echo $position == POSITION_PROF ? "class='active'" : ""; ?>><a href="<?php echo "?position=" . POSITION_PROF . "&cate_id={$cate_id}"; ?>">ศาสตราจารย์</a></li>
                        <li <?php echo $position == POSITION_ASSIS_PROF ? "class='active'" : ""; ?>><a href="<?php echo "?position=" . POSITION_ASSIS_PROF . "&cate_id={$cate_id}"; ?>">รองศาสตราจารย์</a></li>
                        <li <?php echo $position == POSITION_ASSOC_PROF ? "class='active'" : ""; ?>><a href="<?php echo "?position=" . POSITION_ASSOC_PROF . "&cate_id={$cate_id}"; ?>">ผู้ช่วยศาสตราจารย์</a></li>
                    </ul>
                </li>
                <li class="dropdown active">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="glyphicon glyphicon-home"></i>&nbsp;
                        <?php if ($cate_id == 0): ?>
                            ทุกสำนัก
                        <?php else: ?>
                            <?php foreach ($category as $each_cate): ?>
                                <?php if ($each_cate->post_id == $cate_id): ?>
                                    <?php echo $each_cate->post_title; ?>
                                    <?php break ?>
                                <?php endif; ?>
                            <?php endforeach ?>
                        <?php endif; ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li <?php echo $cate_id == 0 ? "class='active'" : ""; ?>><a href="<?php echo "?cate_id=0&position={$position}"; ?>">ทุกสำนัก</a></li>
                        <?php foreach ($category as $each_cate): ?>
                            <li <?php echo $cate_id == $each_cate->post_id ? "class='active'" : ""; ?>><a href="<?php echo "?cate_id={$each_cate->post_id}&position={$position}"; ?>"><?php echo $each_cate->post_title ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-12">
                    <?php foreach ($post as $each_post): ?>

                        <!--Start Repeat-->
                        <?php $link = "#"; ?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="thumbnail">
                                            <a href="<?php echo $link; ?>"  data-id="<?php echo $each_post->post_id ?>" class="get_more_info">
                                                <?php if ($each_post->post_thumbnail != 0): ?>
                                                    <?php $media = get_media_mid($each_post->post_thumbnail); ?>
                                                    <?php $thumb = json_decode($media->link) ?>
                                                    <?php $url_thumb = $thumb->m; ?>
                                                <?php else: ?>
                                                    <?php $url_thumb = image_asset_url("Blank_thumbnail.jpg"); ?>
                                                <?php endif; ?>
                                                <img src="<?php echo $url_thumb ?>" class="img-responsive" id="<?php echo "thumb_{$each_post->post_id}"; ?>">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <?php $title = json_decode($each_post->post_title); ?>
                                        <h4 class="red_1 th_san margin_top_0 margin_bottom_5">
                                            <b><a href="<?php echo $link ?>" class="link_inherit get_more_info" id="<?php echo "name_{$each_post->post_id}"; ?>"   data-id="<?php echo $each_post->post_id ?>"><?php echo "{$title->en} ({$title->th})"; ?></a></b>
                                        </h4>
                                        <div id="<?php echo "info_temp_{$each_post->post_id}"; ?>">
                                            <p class="margin_bottom_0 font_11"><b>ตำแหน่ง</b></p>
                                            <p>
                                                <a href="<?php echo "?position={$each_post->post_excerp}"; ?>">
                                                    <?php switch ($each_post->post_excerp): case POSITION_PROF: ?>
                                                            ศาสตราจารย์
                                                            <?php break; ?>
                                                        <?php case POSITION_ASSIS_PROF: ?>
                                                            รองศาสตราจารย์
                                                            <?php break; ?>
                                                        <?php case POSITION_ASSOC_PROF: ?>
                                                            ผู้ช่วยศาสตราจารย์
                                                            <?php break; ?>
                                                    <?php endswitch; ?>
                                                </a>
                                            </p>
                                            <p class="margin_bottom_0 font_11"><b>สำนักวิชา</b></p>
                                            <p>
                                                <a href="<?php echo "?cate_id={$each_post->cate_id}"; ?>"><?php echo $re_cate[$each_post->cate_id]->post_title; ?></a>
                                            </p>
                                            <?php $post_detail = json_decode($each_post->post_detail); ?>
                                            <?php if ($post_detail->tel_work): ?>
                                                <p>
                                                    <i class="glyphicon glyphicon-phone-alt"></i>&nbsp;<?php echo $post_detail->tel_work; ?>
                                                </p>
                                            <?php endif; ?>

                                            <?php if ($post_detail->tel_mobile && ($post_detail->tel_mobile_flag == 1)): ?>
                                                <p>
                                                    <i class="glyphicon glyphicon-phone"></i>&nbsp;<?php echo $post_detail->tel_mobile; ?>
                                                </p>
                                            <?php endif; ?>
                                        </div>
                                        <?php if ($post_detail->email && ($post_detail->email_flag == 1)): ?>
                                            <p id="<?php echo "mail_info_{$each_post->post_id}"; ?>">
                                                <i class="glyphicon glyphicon-envelope"></i>&nbsp;<?php echo safe_mailto($post_detail->email); ?>
                                            </p>
                                        <?php endif; ?>

                                        <p>
                                            <button class="get_more_info btn btn-danger btn-sm" data-id="<?php echo $each_post->post_id ?>"><i class="glyphicon glyphicon-bookmark"></i>&nbsp;รายละเอียดพร้อมผลงานวิจัย</button>
                                        </p>
                                        <textarea style="display: none;" id="<?php echo "spc_{$each_post->post_id}"; ?>"><?php echo $post_detail->special_ability ?></textarea>

                                        <div id="<?php echo "pdf_{$each_post->post_id}"; ?>" style="display:none;">
                                            <?php $pdf = get_all_media($each_post->post_id, 'desc', null, null, 'pdf') ?>
                                            <?php if (count($pdf) > 0): ?>
                                                <ul style="padding-left:15px;">
                                                    <?php foreach ($pdf as $each_pdf): ?>
                                                        <li>
                                                            <a target="_blank" href="<?php echo site_url("mfu/mfu_reader/{$each_pdf->mid}/" . slug($each_pdf->media_name)) ?>"><?php echo str_replace('_', ' ', $each_pdf->media_name); ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php else: ?>
                                                <p class="txt_center">No PDF found</p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End Repeat-->

                    <?php endforeach; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php if ($total_page > 0): ?>
                        <ul class="pagination pagination-sm pagination-centered">
                            <?php for ($i = 1; $i <= $total_page; $i++): ?>
                                <li class="<?php echo $i == $page ? "active" : ""; ?>"><a href="<?php echo "?page={$i}&cate_id={$cate_id}&position={$position}"; ?>"><?php echo $i; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-xs-3 sidebar">
            <?php $event_obj = new stdClass(); ?>
            <?php $event_obj->data_type = "event"; ?>
            <?php $event_obj->data_id = 47; ?>
            <?php $all_event = widget_process($event_obj); ?>
            <div class="widget_header_type_1 txt_right th_san" style="margin-bottom: 20px;">
                <p>Events</p>
                <p><?php echo $all_event['title']; ?></p>
            </div>

            <?php foreach ($all_event['post'] as $event_raws): ?>
                <?php $obj_event = widget_event($event_raws); ?>
                <div class="row">
                    <div class="event_date txt_center th_san">
                        <span style="margin: 0px; font-size: 36px; line-height: 20px;" class="txt_center"><?php echo date('d', $obj_event['date']) ?></span>
                        <!--<span class="red" style="font-size: 16px; line-height: 14px; margin-top: -5px; display: block;"><?php echo date('M y', $obj_event['date']) ?></span>-->
                        <span class="red" style="font-size: 16px; line-height: 14px; margin-top: -5px; display: block;"><?php echo $locale_short_month[LANG][date('n', $obj_event['date'])] . " " . date('y', $obj_event['date']) ?></span>
                    </div>
                    <div class="event_info">
                        <p class="txt_justify" style="font-size: 11px; margin-bottom: 5px;"><?php echo $obj_event['post_title']; ?></p>
                        <p class="txt_right" style=" margin-bottom: 15px;"><a class="btn btn-xs btn-default" href="<?php echo $obj_event['url']; ?>">อ่านต่อ &raquo;</a></p>
                    </div>
                </div>
            <?php endforeach; ?>
            <p class="txt_right">
                <a class="btn btn-default" href="<?php echo $all_event['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> ดูทั้งหมด</a>
            </p>

            <?php $ext_link = new stdClass(); ?>
            <?php $ext_link->data_type = "ext_link"; ?>
            <?php $ext_link->data_id = 447; ?>
            <?php $block = widget_process($ext_link); ?>
            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
            <?php $link = json_decode($block['post']) ?>
            <ul class="ext_link_list">
                <?php foreach ($link as $post): ?>
                    <li><a target="_blank" href="<?php echo $post->url; ?>"><?php echo $post->title; ?></a></li>
                <?php endforeach; ?>
            </ul>

            <!--Fix link-->
            <?php echo $fix_link; ?>
            <!--/Fix Link-->
        </div>
    </div>
</div>

<div class="modal fade" id="more_info">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title th_san red" id="modal_person_name">รายละเอียดเพิ่มเติม</h1>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="txt_center">
                            <div class="thumbnail" style="max-width:200px; max-height: 200px; margin: 0px auto 10px;">
                                <img src="" id="thumbnail_id" class="img-reponsive">
                            </div>
                        </div>
                        <h2 class="th_san red margin_top_0 margin_bottom_5">รายละเอียด</h2>
                        <div id="modal_personal_info"></div>
                        <h2 class="th_san red margin_top_0 margin_bottom_5">เอกสารงานวิจัย</h2>
                        <div id="modal_list_pdf" class="font_11"></div>
                    </div>
                    <div class="col-xs-8">
                        <h2 class="th_san red margin_top_10 margin_bottom_10">ความเชี่ยวชาญ / ชำนาญ ด้านการวิจัย</h2>
                        <div id="modal_info"></div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(function() {
        $('.get_more_info').click(function() {
            var id = $(this).attr('data-id');
            $('#modal_person_name').text($('#name_' + id).text());
            $('#modal_list_pdf').html($('#pdf_' + id).html());
            $('#modal_info').html($('#spc_' + id).html());

            var mail = "";
            if ($('#mail_info_' + id).length > 0) {
                $('#mail_info_' + id).find('script').remove();
                mail = $('#mail_info_' + id).html();
            }

            var phone = "";
            if ($('#info_temp_' + id).length > 0) {
                phone = $('#info_temp_' + id).html();
            }

            $('#modal_personal_info').html(phone + mail);
            $('#thumbnail_id').attr('src', $('#thumb_' + id).attr('src'));

            $('#more_info').modal();
        });
    });
</script>
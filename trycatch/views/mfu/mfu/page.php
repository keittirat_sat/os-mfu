<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="red th_san"><?php echo $post->post_title; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-9">
            <!--No Breadcrum-->
            <?php echo auto_typography($post->post_detail); ?>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
<?php $post = get_post_by_id(428); ?>
<?php $ext = json_decode(file_get_contents($post->post_detail)); ?>
<table class="table table-condensed">
    <thead>
        <tr>
            <th class="txt_center"><?php echo lang("ลำดับที่"); ?></th>
            <th class="txt_center"><?php echo lang("โครงการวิจัย"); ?></th>
            <th class="txt_center"><?php echo lang("วันที่เผยแพร่"); ?></th>
            <th class="txt_center"><?php echo lang("ดูเพิ่ม"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($ext as $index => $each_ext): ?>
            <tr>
                <td class="txt_center"><?php echo $index + 1; ?></td>
                <td width="450"><?php echo $each_ext->title; ?></td>
                <td class="txt_center"><?php echo date('d/m/Y', strtotime($each_ext->publish)); ?></td>
                <td class="txt_center">
                    <a class="btn btn-danger btn-xs" href="<?php echo $each_ext->link; ?>" target="_blank"><i class="glyphicon glyphicon-download-alt"></i>&nbsp;<?php echo lang("ดูเพิ่ม"); ?></a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <th class="txt_center"><?php echo lang("ลำดับที่"); ?></th>
            <th class="txt_center"><?php echo lang("โครงการวิจัย"); ?></th>
            <th class="txt_center"><?php echo lang("วันที่เผยแพร่"); ?></th>
            <th class="txt_center"><?php echo lang("ดูเพิ่ม"); ?></th>
        </tr>
    </tfoot>
</table>
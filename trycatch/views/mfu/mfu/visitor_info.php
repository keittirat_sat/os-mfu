<?php

function block_gen($id) { ?>
    <?php $post = get_post_by_id($id) ?>
    <?php echo auto_typography($post->post_excerp) ?>
    <?php $link = json_decode($post->post_detail) ?>
    <?php if (count($link)): ?>
        <p class='red' style='margin-bottom: 0px;'><?php echo lang("ข้อมูลที่น่าสนใจ"); ?></p>
        <ul class='hilight_link'>
            <?php foreach ($link as $each_post): ?>
                <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url ?>'><?php echo $each_post->title; ?></a></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
<?php } ?>

<div class="img_top_holder visitor_info">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('visitorinfo_header_text_' . LANG . '.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>
<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-9">
            <?php $slide = get_random_image_from_gallery(); ?>
            <?php if (count($slide)): ?>
                <div class='row'>
                    <div class='relative col-lg-12' style='margin-bottom: 30px;'>
                        <div class="visitor_slide">
                            <?php foreach ($slide as $each_slide): ?>
                                <?php $link = $each_slide['link']; ?>
                                <img src="<?php echo $link->l ?>">
                            <?php endforeach; ?>
                        </div>
                        <img src='<?php echo image_asset_url('gallery_text_' . LANG . '.png') ?>' class='gallery_txt'>
                        <a href='<?php echo site_url('mfu/gallery'); ?>' class='link_btn_gallery'><?php echo image_asset("VisitorInfo_Gallery_link_" . LANG . ".png"); ?></a>
                    </div>
                </div>
            <?php endif; ?>

            <!--hilight-->
            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("ไฮไลต์"); ?></span></h4>
            <!--ศิลปะวัฒนธรรม-->
            <div class="row">
                <div class="col-xs-4">
                    <div class="hilight_thumb" style="background-image:url('<?php echo image_asset_url('VisitorInfo_hilight_01.png') ?>');">
                        <h2 class="white th_san margin_bottom_0 margin_top_0 absolute_bottom bold"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("ศิลปะวัฒนธรรม"); ?></h2>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?php block_gen(659); ?>
                </div>
            </div>
            <!--/ศิลปะวัฒนธรรม-->

            <!--สุขภาพ-->
            <div class="row">
                <div class="col-xs-4">
                    <div class="hilight_thumb" style="background-image:url('<?php echo image_asset_url('VisitorInfo_hilight_02.png') ?>');">
                        <h2 class="white th_san margin_bottom_0 margin_top_0 absolute_bottom bold"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("สุขภาพ"); ?></h2>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?php block_gen(660); ?>
                </div>
            </div>
            <!--/สุขภาพ-->

            <!--ที่พัก-->
            <div class="row">
                <div class="col-xs-4">
                    <div class="hilight_thumb" style="background-image:url('<?php echo image_asset_url('VisitorInfo_hilight_04.png') ?>');">
                        <h2 class="white th_san margin_bottom_0 margin_top_0 absolute_bottom bold"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("ที่พัก"); ?></h2>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?php block_gen(662); ?>
                </div>
            </div>
            <!--/ที่พัก-->

            <!--ธรรมชาติ-->
            <div class="row">
                <div class="col-xs-4">
                    <div class="hilight_thumb" style="background-image:url('<?php echo image_asset_url('VisitorInfo_hilight_03.png') ?>');">
                        <h2 class="white th_san margin_bottom_0 margin_top_0 absolute_bottom bold"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("ธรรมชาติ"); ?></h2>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?php block_gen(661); ?>
                </div>
            </div>
            <!--/ธรรมชาติ-->
            <!--/hilight-->

            <!--การบริการในมหาวิทยาลัย-->
            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("การบริการในมหาวิทยาลัย"); ?></span></h4>
            <!--การบริการ-->
            <div class="row">
                <div class="col-xs-4">
                    <div class="hilight_thumb" style="background-image:url('<?php echo image_asset_url('VisitorInfo_hilight_07.png') ?>');">
                        <h2 class="white th_san margin_bottom_0 margin_top_0 absolute_bottom bold"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("การบริการ"); ?></h2>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?php block_gen(663); ?>
                </div>
            </div>
            <!--/การบริการ-->
            <!--/การบริการในมหาวิทยาลัย-->

            <!--อาคาร-->
            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("อาคาร"); ?></span></h4>
            <div class="row">
                <div class="col-xs-4">
                    <div class="hilight_thumb" style="background-image:url('<?php echo image_asset_url('VisitorInfo_hilight_05.png') ?>');">
                        <h2 class="white th_san margin_bottom_0 margin_top_0 absolute_bottom bold"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("อาคาร"); ?></h2>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?php block_gen(664); ?>
                </div>
            </div>
            <!--/อาคาร-->

            <!--เกี่ยวกับมหาวิทยาลัย-->
            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("เกี่ยวกับมหาวิทยาลัย"); ?></span></h4>
            <div class="row">
                <div class="col-xs-4">
                    <div class="hilight_thumb" style="background-image:url('<?php echo image_asset_url('VisitorInfo_hilight_06.png') ?>');">
                        <h2 class="white th_san margin_bottom_0 margin_top_0 absolute_bottom bold"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("เกี่ยวกับ มฟล."); ?></h2>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?php block_gen(665); ?>
                </div>
            </div>
            <!--/เกี่ยวกับมหาวิทยาลัย-->

            <div class='row'>
                <div class='col-xs-8'>
                    <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("แผนที่และตำแหน่ง"); ?></span></h4>
                    <div id='map_canvas'></div>
                    <h3 class="txt_left th_san margin_top_10 margin_bottom_0">
                        <?php echo image_asset("bullet.png") ?>&nbsp;<?php echo lang("รายละเอียด"); ?>
                    </h3>
                    <div class="row">
                        <?php $cate = get_category_by_cate_id(53); ?>
                        <div class="col-xs-8 font_11">
                            <?php echo auto_typography($cate->cate_info) ?>
                        </div>
                        <?php $howto_post = get_all_post_in_cate(53); ?>
                        <div class="col-xs-4 th_san">
                            <?php foreach ($howto_post as $index => $each_post): ?>
                                <a href="#" class="howtoget_btn" data-toggle="modal" data-target="<?php echo "#howto{$index}" ?>"><?php echo $each_post->post_title; ?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class='col-xs-4'>
                    <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("สภาพอากาศ"); ?></span></h4>
                    <a href="http://www.accuweather.com/en/th/chiang-rai/317549/weather-forecast/317549" class="aw-widget-legal">
                        <!--
                        By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
                        -->
                    </a><div id="awcc1393175948856" class="aw-widget-current"  data-locationkey="317549" data-unit="c" data-language="en-us" data-useip="false" data-uid="awcc1393175948856"></div><script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
                </div>
            </div>
        </div>
        <div class="col-xs-3 sidebar">
            <?php $event_obj = new stdClass(); ?>
            <?php $event_obj->data_type = "event"; ?>
            <?php $event_obj->data_id = 48; ?>
            <?php $all_event = widget_process($event_obj); ?>
            <div class="widget_header_type_1 txt_right th_san" style="margin-bottom: 20px;">
                <p>Visitor Calendar</p>
                <p><?php echo $all_event['title']; ?></p>
            </div>

            <?php foreach ($all_event['post'] as $event_raws): ?>
                <?php $obj_event = widget_event($event_raws); ?>
                <div class="row">
                    <div class="event_date txt_center th_san">
                        <span style="margin: 0px; font-size: 36px; line-height: 20px;" class="txt_center"><?php echo date('d', $obj_event['date']) ?></span>
                        <span class="red" style="font-size: 16px; line-height: 14px; margin-top: -5px; display: block;"><?php echo $locale_short_month[LANG][date('n', $obj_event['date'])] . " " . date('y', $obj_event['date']) ?></span>
                    </div>
                    <div class="event_info">
                        <p class="txt_justify" style="font-size: 11px; margin-bottom: 5px;"><?php echo $obj_event['post_title']; ?></p>
                        <p class="txt_right" style=" margin-bottom: 15px;"><a class="btn btn-xs btn-default" href="<?php echo $obj_event['url']; ?>"><?php echo lang("อ่านต่อ"); ?> &raquo;</a></p>
                    </div>
                </div>
            <?php endforeach; ?>
            <p class="txt_right">
                <a class="btn btn-default" href="<?php echo $all_event['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
            </p>

            <!--Fix link-->
            <?php echo $fix_link; ?>
            <!--/Fix Link-->
        </div>
    </div>
</div>

<!-- Modal -->
<?php foreach ($howto_post as $index => $each_post): ?>
    <div class="modal fade" id="<?php echo "howto{$index}"; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title red" id="myModalLabel1"><?php echo $each_post->post_title; ?></h4>
                </div>
                <div class="modal-body">
                    <?php echo $each_post->post_detail; ?>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<style>
    .nivo-directionNav{
        display: none;
    }

    .nivo-controlNav {
        position: absolute;
        top: 0px;
        left: 0px;
        z-index: 99;
        padding: 15px 0px 0px 30px;
    }

    .nivo-controlNav a.nivo-control{
        display: inline-block;
        width: 14px;
        height: 14px;
        text-indent: 100px;
        overflow: hidden;
        background: url('<?php echo image_asset_url('VisitorInfo_Gallery_bullet_inactive.png') ?>') no-repeat center;
        margin: 0px 8px 0px 0px;
    }


    .nivo-controlNav a.nivo-control.active{
        background: url('<?php echo image_asset_url('VisitorInfo_Gallery_bullet_active.png') ?>') no-repeat center;
    }

    .link_btn_gallery{
        position: absolute;
        bottom: -22px;
        z-index: 99;
        right: 33px;
    }

    .gallery_txt{
        position: absolute;
        top: 47px;
        left: 30px;
        z-index: 99;
    }

    .font_11{
        font-size: 11px;
        text-align: justify;
        margin: 5px 0px 10px;
    }
</style>

<script type="text/javascript">
    $(function() {
        $('.visitor_slide').nivoSlider();

        $('#map_canvas').gmap({
            'mapTypeId': google.maps.MapTypeId.HYBRID,
            'zoom': 14,
            'center': '20.045974,99.893056'
        }).bind('init', function(ev, map) {
            $('#map_canvas').gmap('addMarker', {'position': '20.045974,99.893056'});
        });
    });
</script>


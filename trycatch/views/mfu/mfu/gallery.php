<div class="img_top_holder visitor_info">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('visitorinfo_header_text.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>

<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-9">   
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span>แกลลอรี่</span></h4>
                    <form>
                        <div class="input-group input-group-sm box_search">
                            <!--month-->
                            <div class="input-group-btn">                            
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="replace_here"><?php echo lang("ทุกเดือน") ?></span>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu month_list" role="menu">
                                    <?php foreach ($locale_month[LANG] as $i => $each_month): ?>
                                        <li <?php echo $month == $i ? "class='active'" : ""; ?>><a href="<?php echo "#{$i}"; ?>" data-month="<?php echo $i; ?>"><?php echo $i == 0 ? lang("ทุกเดือน") : $each_month ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div><!--month-->

                            <!--year-->
                            <div class="input-group-btn">      
                                <button type="button" class="btn btn-default dropdown-toggle border_radius_0 margin_right_minus_1" data-toggle="dropdown">
                                    <span class="replace_here"><?php echo lang("ทุกปี") ?></span>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu year_list" role="menu">
                                    <li <?php echo $year == 0 ? "class='active'" : ""; ?>><a href="<?php echo "#0"; ?>" data-year="0"><?php echo lang("ทุกปี") ?></a></li>
                                    <?php foreach ($group_year as $each_year): ?>
                                        <li <?php echo $year == $each_year->year ? "class='active'" : ""; ?>><a href="<?php echo "#{$each_year->year}"; ?>" data-year="<?php echo $each_year->year; ?>"><?php echo $i == 0 ? lang("ทุกปี") : $each_year->year ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div><!--year-->

                            <!--category-->
                            <div class="input-group-btn">      
                                <button type="button" class="btn btn-default dropdown-toggle border_radius_0 margin_right_minus_1" data-toggle="dropdown">
                                    <span class="replace_here"><?php echo lang("ทุกหมวดหมู่") ?></span>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu cate_list" role="menu">
                                    <li <?php echo $cate_id == 0 ? "class='active'" : ""; ?>><a href="<?php echo "#0"; ?>" data-cate="0"><?php echo lang("ทุกหมวดหมู่") ?></a></li>
                                    <?php foreach ($group_category as $each_cate): ?>
                                        <li <?php echo $cate_id == $each_cate->cate_id ? "class='active'" : ""; ?>><a href="<?php echo "#{$each_cate->cate_id}"; ?>" data-cate="<?php echo $each_cate->cate_id; ?>"><?php echo $i == 0 ? lang("ทุกหมวดหมู่") : $each_cate->cate_name ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div><!--category-->
                            <input type="text" class="form-control" name="q" value="<?php echo $q; ?>" placeholder="Search in this section">
                            <span class="input-group-btn">
                                <button class="btn btn-default"><i class=" glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                        <input type="hidden" id="search_cate_id" name="cate_id" value="<?php echo $cate_id ?>">
                        <input type="hidden" id="search_month" name="month" value="<?php echo $month ?>">
                        <input type="hidden" id="search_year" name="year" value="<?php echo $year ?>">
                    </form>

                </div>
            </div>

            <div class="row">
                <?php if (count($album)): ?>
                    <?php foreach ($album as $each_album): ?>
                        <?php $image = get_media_by_post_id($each_album->post_id); ?>
                        <?php $media_info = json_decode($image[0]->media_info); ?>
                        <?php $media_link = json_decode($image[0]->link); ?>
                        <?php $dim = 'landscape'; ?>
                        <?php if ($media_info->image_width > $media_info->image_height): ?>
                            <?php $dim = 'portrait'; ?>
                        <?php endif; ?>
                        <div class="col-xs-4 margin_bottom_20">
                            <a href="<?php echo site_url("mfu/album/{$each_album->post_id}/" . slug($each_album->post_title)); ?>" class="cubic-md-4 <?php echo $dim; ?>" style="background-image: url('<?php echo $media_link->m ?>');">
                                <?php echo image_asset('Gallery_icon.png', '', array('class' => 'magnify')); ?>
                                <div class="gallery_title">
                                    <span><?php echo $each_album->post_title; ?></span>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h2 class="txt_center th_san"><?php echo lang("ไม่มีข้อมูลในส่วนนี้"); ?></h2>
                <?php endif; ?>
            </div>

            <div class="row">
                <div class="col-xs-12 txt_center">
                    <ul class="pagination pagination-sm pagination-centered">
                        <?php for ($i = 1; $i <= $total_page; $i++): ?>
                            <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo "?page={$i}&cate_id={$cate_id}&month={$month}&year={$year}"; ?>"><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-3 sidebar">
            <?php $all_event = get_event_by_status(); ?>
            <div class="widget_header_type_1 txt_right th_san" style="margin-bottom: 20px;">
                <p>Information&nbsp;&amp;&nbsp;Overview</p>
                <p>ประชาสัมพันธ์ข้อมูลทั่วไป</p>
            </div>

            <?php $list = get_post_by_id(446); ?>
            <?php $side_list = json_decode($list->post_detail) ?>
            <ul class="hilight_link">
                <?php foreach ($side_list as $link): ?>
                    <li><font class='red'>&raquo;&nbsp;</font><a href="<?php echo $link->url; ?>"><?php echo $link->title; ?></a></li>
                <?php endforeach; ?>
            </ul>

            <?php $widget->data_id = 0; ?>
            <?php $widget->data_type = 'school'; ?>
            <?php $block = widget_process($widget); ?>
            
            <div class="widget_header_type_1 txt_right th_san">
                <p>Schools &amp; Departments</p>
                <p>สำนักวิชาและหน่วยงาน</p>
            </div>
            
            <p class="label_widget_type_1 th_san">สำนักวิชา</p>
            <ul class="school_list">
                <?php foreach ($block['post'] as $post): ?>
                    <li><a href="<?php echo site_url("mfu/school/{$post->post_id}/" . slug($post->post_title)); ?>"><?php echo $post->post_title; ?></a></li>
                <?php endforeach; ?>
            </ul>

            <?php $list = get_post_by_id(447); ?>
            <?php $side_list = json_decode($list->post_detail) ?>
            <h2 class="margin_top_0 th_san red"><?php echo $list->post_title; ?></h2>
            <ul class="hilight_link">
                <?php foreach ($side_list as $link): ?>
                    <li><font class='red'>&raquo;&nbsp;</font><a href="<?php echo $link->url; ?>"><?php echo $link->title; ?></a></li>
                    <?php endforeach; ?>
            </ul>

            <!--Fix link-->
            <?php echo $fix_link; ?>
            <!--/Fix Link-->
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(function() {
        $('a.cubic-md-4').mouseenter(function() {
            $(this).find('.magnify').stop().animate({opacity: 1}, 200);
        }).mouseleave(function() {
            $(this).find('.magnify').stop().animate({opacity: 0}, 200);
        });

        $("ul.month_list li a").click(function() {
            var info = $(this).attr('data-month');
            var label = $(this).text();

            $("ul.month_list li").removeClass('active');
            $(this).parent('li').addClass('active');
            $("ul.month_list").siblings("button").find(".replace_here").text(label);
            $('#search_month').val(info);
            return false;
        });

        $("ul.year_list li a").click(function() {
            var info = $(this).attr('data-year');
            var label = $(this).text();

            $("ul.year_list li").removeClass('active');
            $(this).parent('li').addClass('active');
            $("ul.year_list").siblings("button").find(".replace_here").text(label);
            $('#search_year').val(info);
            return false;
        });

        $("ul.cate_list li a").click(function() {
            var info = $(this).attr('data-cate');
            var label = $(this).text();

            $("ul.cate_list li").removeClass('active');
            $(this).parent('li').addClass('active');
            $("ul.cate_list").siblings("button").find(".replace_here").text(label);
            $('#search_cate_id').val(info);
            return false;
        });
        
        $.each($('.box_search li.active'), function(idx, ele){
            $(ele).find("a").trigger("click");
        });
    });
</script>
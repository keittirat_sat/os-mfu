<div class="container margin_top_20">
    <!--title head1-->
    <h1>Dev Page Guide <small>หน้าต้นแบบพัฒนา</small></h1><!--title head1-->

    <!--Content Start here-->

    <!--first row-->
    <!--Grid set to 12 colum-->
    <div class="row margin_top_20">
        <!--grid_4-->
        <div class="col-xs-4">
            <div class="panel panel-default">
                <div class="panel-heading">Codeigniter Manual</div>
                <div class="panel-body">
                    <img src="http://ellislab.com/asset/css/img/products/ci-introduction-flame.png" class="img-responsive" style="margin: 15px auto;">
                </div>
                <div class="panel-footer">
                    <a href="http://ellislab.com/codeigniter" class="btn btn-danger">Read Codeigniter</a>
                </div>
            </div>
        </div><!--grid_4-->

        <!--grid_4-->
        <div class="col-xs-4">
            <div class="panel panel-default">
                <div class="panel-heading">Bootstrap Manual v3.1.1</div>
                <div class="panel-body">
                    <img src="http://logonoid.com/images/bootstrap-logo.png" class="img-responsive" style="margin: 0px auto;">
                </div>
                <div class="panel-footer">
                    <a href="http://getbootstrap.com/" class="btn btn-danger">Read Bootstrap v3.1.1</a>
                </div>
            </div>
        </div><!--grid_4-->

        <!--grid_4-->
        <div class="col-xs-4">
            <div class="panel panel-default">
                <div class="panel-heading">jQuery Manual v1.7</div>
                <div class="panel-body">
                    <img src="http://cazaresluis.com/wp-content/uploads/2012/09/ventana-modal-con-jquery-ui.png" class="img-responsive" style="margin: 0px auto;">
                </div>
                <div class="panel-footer">
                    <a href="http://api.jquery.com/category/deprecated/deprecated-1.7/" class="btn btn-danger">Read jQuery v1.7</a>
                </div>
            </div>  
        </div><!--grid_4-->

    </div><!--first row-->

    <!--Second rows-->
    <div class="row">
        <?php for ($i = 0; $i < 2; $i++): ?>
            <div class="col-xs-6">
                <div class="well well-sm">
                    Hello
                </div>
            </div>
        <?php endfor; ?>
    </div><!--Second rows-->

    <!--rows-->
    <div class="row">
        <?php for ($i = 0; $i < 3; $i++): ?>
            <div class="col-xs-4">
                <div class="well well-sm">
                    Hello
                </div>
            </div>
        <?php endfor; ?>
    </div><!--rows-->

    <!--rows-->
    <div class="row">
        <?php for ($i = 0; $i < 4; $i++): ?>
            <div class="col-xs-3">
                <div class="well well-sm">
                    Hello
                </div>
            </div>
        <?php endfor; ?>
    </div><!--rows-->

    <!--rows-->
    <div class="row">
        <?php for ($i = 0; $i < 6; $i++): ?>
            <div class="col-xs-2">
                <div class="well well-sm">
                    Hello
                </div>
            </div>
        <?php endfor; ?>
    </div><!--rows-->

    <!--rows-->
    <div class="row">
        <?php for ($i = 0; $i < 12; $i++): ?>
            <div class="col-xs-1">
                <div class="well well-sm">
                    Hello
                </div>
            </div>
        <?php endfor; ?>
    </div><!--rows-->

    <!--Content End here-->
</div>
<div class="img_top_holder pr_info">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('news_header_text.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>
<div class="container margin_top_20">

    <div class="row">
        <div class="col-xs-12">
            <h1 class="red th_san margin_top_0 margin_bottom_0">MFU Newsletter</h1>

            <!--Form search-->
            <form>
                <div class="input-group input-group-sm box_search">
                    <!--month-->
                    <div class="input-group-btn">                            
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="replace_here"><?php echo lang("ทุกเดือน") ?></span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu month_list" role="menu">
                            <?php foreach ($locale_month[LANG] as $i => $each_month): ?>
                                <li <?php echo $month == $i ? "class='active'" : ""; ?>><a href="<?php echo "#{$i}"; ?>" data-month="<?php echo $i; ?>"><?php echo $i == 0 ? lang("ทุกเดือน") : $each_month ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div><!--month-->
                    <!--year-->
                    <div class="input-group-btn">      
                        <button type="button" class="btn btn-default dropdown-toggle border_radius_0 margin_right_minus_1" data-toggle="dropdown">
                            <span class="replace_here"><?php echo lang("ทุกปี") ?></span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu year_list" role="menu">
                            <li <?php echo $year == 0 ? "class='active'" : ""; ?>><a href="<?php echo "#0"; ?>" data-year="0"><?php echo lang("ทุกปี") ?></a></li>
                            <?php foreach ($all_year as $each_year): ?>
                                <?php $temp_year = date("Y", $each_year->post_year); ?>
                                <li <?php echo $year == $temp_year ? "class='active'" : ""; ?>><a href="<?php echo "#{$temp_year}"; ?>" data-year="<?php echo $temp_year; ?>"><?php echo $i == 0 ? lang("ทุกปี") : $temp_year ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div><!--year-->

                    <input type="text" class="form-control" name="q" value="<?php echo $q; ?>" placeholder="Search in this section">
                    <span class="input-group-btn">
                        <button class="btn btn-default"><i class=" glyphicon glyphicon-search"></i></button>
                    </span>
                </div>
                <input type="hidden" id="search_cate_id" name="cate_id" value="<?php echo $cate_id ?>">
                <input type="hidden" id="search_month" name="month" value="<?php echo $month ?>">
                <input type="hidden" id="search_year" name="year" value="<?php echo $year ?>">
            </form>            
            <!--/Form search-->
        </div>
    </div>

    <div class="row">
        <div class="col-xs-9">            

            <!--Generate Rows of Book-->

            <?php if (count($post)): ?>

                <?php $rows = 0; ?>
                <?php $new_obj[$rows] = array(); ?>
                <?php foreach ($post as $i => $each_post): ?>
                    <?php array_push($new_obj[$rows], $each_post); ?>
                    <?php if ($i % 3 == 2): ?>
                        <?php $rows++; ?>
                        <?php $new_obj[$rows] = array(); ?>
                    <?php endif; ?>
                <?php endforeach; ?>

                <?php foreach ($new_obj as $rows): ?>
                    <div class="row margin_bottom_20">
                        <?php foreach ($rows as $each_post): ?>
                            <?php $media = get_media_by_post_id($each_post->post_id) ?>
                            <div class="col-xs-4 txt_center">
                                <?php if (array_key_exists(0, $media)): ?>
                                    <?php $mid = $media[0]->mid; ?>
                                    <?php $media_name = $media[0]->media_name; ?>
                                    <?php $pdf_info = json_decode($media[0]->media_info); ?>

                                    <?php $link = site_url("mfu/mfu_reader/{$mid}/" . slug($media_name)); ?>

                                    <?php if (count($pdf_info->pdf_image) > 1): ?>
                                        <?php foreach ($pdf_info->pdf_image as $each_page): ?>
                                            <?php if ($each_page->name == "page-0.jpg"): ?>
                                                <?php $thumb = $each_page ?>
                                                <?php break; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <?php $thumb = $pdf_info->pdf_image; ?>
                                        <?php $thumb = $thumb[0] ?>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php $thumb = new stdClass() ?>
                                    <?php $thumb->thumb = image_asset_url("Blank_thumbnail.jpg") ?>
                                    <?php $link = "#"; ?>
                                <?php endif; ?>

                                <!--Information-->
                                <h3 class="red th_san margin_bottom_0 margin_top_0"><?php echo $each_post->post_title; ?></h3>
                                <p><?php echo $each_post->post_excerp ?></p>
                                <a href="<?php echo $link; ?>" target="_blank" class="thumbnail">
                                    <img src="<?php echo $thumb->thumb ?>" style="max-height: 285px; max-width: 203px;">
                                </a>
                                <!--Information-->

                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>            
                <!--/Generate Rows of Book-->

                <div class="row">
                    <div class="col-xs-12 txt_center">
                        <ul class="pagination pagination-centered pagination-sm">
                            <?php for ($i = 1; $i <= $total_page; $i++): ?>
                                <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>

            <?php else: ?>
                <h3 class="txt_center red th_san"><?php echo lang("ไม่มีข้อมูลในส่วนนี้"); ?></h3>
            <?php endif; ?>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $("ul.month_list li a").click(function() {
            var info = $(this).attr('data-month');
            var label = $(this).text();

            $("ul.month_list li").removeClass('active');
            $(this).parent('li').addClass('active');
            $("ul.month_list").siblings("button").find(".replace_here").text(label);
            $('#search_month').val(info);
            return false;
        });

        $("ul.year_list li a").click(function() {
            var info = $(this).attr('data-year');
            var label = $(this).text();

            $("ul.year_list li").removeClass('active');
            $(this).parent('li').addClass('active');
            $("ul.year_list").siblings("button").find(".replace_here").text(label);
            $('#search_year').val(info);
            return false;
        });

        $.each($('.box_search li.active'), function(idx, ele) {
            $(ele).find("a").trigger("click");
        });
    });
</script>
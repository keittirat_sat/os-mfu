<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-3">
            <div id="datepicker" data-date="<?php echo date('d-m-Y', $date_set); ?>"data-date-format="dd-mm-yyyy"></div>
            <div class="margin_top_10">
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="<?php echo "?month=" . date('m') . "&day=" . date('d') . "&year=" . date('Y') ?>"><?php echo lang("เหตการณ์และกิจกรรมในวันนี้") ?></a></li>
                    <li><a href="<?php echo current_url(); ?>"><?php echo lang("แสดงหัวข้อทั้งหมดในระบบ"); ?></a></li>
                </ul>
            </div>
            <div style="padding: 0px 10px;" class="margin_top_10">
                <?php foreach ($re_cate[0] as $parent): ?>
                <label><a href="<?php echo "?cate_id={$parent->cate_id}"; ?>" style="color: #222;"><?php echo $parent->cate_name ?></a></label>
                    <?php if (array_key_exists($parent->cate_id, $re_cate)): ?>
                        <ul>
                            <?php foreach ($re_cate[$parent->cate_id] as $each_cate): ?>
                                <li><a href="<?php echo "?cate_id={$each_cate->cate_id}"; ?>"><?php echo $each_cate->cate_name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="page-header margin_top_0">
                <h1 class="th_san margin_top_0 red margin_bottom_0"><?php echo $page_title ?></h1>
            </div>
            <?php if (count($post) > 0): ?>
                <?php foreach ($post as $each_post): ?>
                    <div class="row" style='margin-bottom: 15px;'>
                        <?php $cate_post = widget_cate($each_post); ?>
                        <div class="widget_cate_thumbnail">
                            <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                        </div>
                        <div class="widget_cate_info">
                            <h4 class="th_san red_1"><a title="<?php echo $cate_post['alt_title'] ?>" class="link_inherit" href='<?php echo $cate_post['url']; ?>'><?php echo $cate_post['post_title']; ?></a></h4>
                            <p><a href='<?php echo "?cate_id={$cate_post['cate_id']}"; ?>'><?php echo $re_order[$cate_post['cate_id']]->cate_name ?></a>&dash;<a href='<?php echo "?month=" . str_pad(date('n', $cate_post['date']), 2, '0', STR_PAD_LEFT) . "&day=" . str_pad(date('d', $cate_post['date']), 2, '0', STR_PAD_LEFT) . "&year=" . date('Y', $cate_post['date']); ?>'><?php echo date('d', $cate_post['date']) . " " . $locale_month[LANG][date('n', $cate_post['date'])] . " " . date('Y'); ?></a></p>
                            <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class='row'>
                    <div class='col-xs-12'>
                        <ul class='pagination pagination-sm'>
                            <?php for ($i = 1; $i <= $total_page; $i++): ?>
                                <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href='<?php echo "?page={$i}&cate_id={$cate_id}&month={$month}&day={$day}&year={$year}"; ?>'><?php echo $i ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>
            <?php else: ?>
                <h3 class="red_1 th_san txt_center"><?php echo lang("ไม่มีข่าวในหมวดนี้"); ?></h3>
            <?php endif; ?>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
<input type="hidden" value="<?php echo $cate_id ?>" id="cate_id">
<input type="hidden" value="<?php echo $page ?>" id="page">

<style type="text/css">
    .pagination-sm{
        margin: 10px 0px;
    }
</style>

<script type="text/javascript">
    $(function() {
        $('#datepicker').datepicker({
            language: "<?php echo LANG ?>"
        }).on('changeDate', function(e) {
            var day = e.date.getDate();
            var month = e.date.getMonth();
            var year = e.date.getFullYear();

            month++;

            if (day < 10) {
                day = "0" + day;
            }

            if (month < 10) {
                month = "0" + month;
            }

            var url = location.origin + location.pathname + "?month=" + month + "&day=" + day + "&year=" + year + "&cate_id=" + $('#cate_id').val();
            location.href = url;

        }).on('changeMonth', function(e) {
            var month = e.date.getMonth();
            var year = e.date.getFullYear();

            month++;

            if (month < 10) {
                month = "0" + month;
            }

            var url = location.origin + location.pathname + "?month=" + month + "&year=" + year + "&cate_id=" + $('#cate_id').val();
            location.href = url;
        });
    });
</script>
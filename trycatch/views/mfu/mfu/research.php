<div class="img_top_holder research_info">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('research_header_text_' . LANG . '.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>
<div class="container margin_top_40">
    <div class="row">
        <div class="col-xs-9">
            <div class='row'>
                <div class='col-xs-4'>
                    <a href='http://www.mfu.ac.th/excellent/'><?php echo image_asset("Research_cs_icon.png", '', array('class' => 'img-responsive')); ?></a>
                    <a href='http://www.mfu.ac.th/fungalsymposium09'><?php echo image_asset("Research_fr_icon.png", '', array('class' => 'img-responsive margin_top_40')); ?></a>
                    <a href='<?php echo site_url('mfu/researcher'); ?>'><?php echo image_asset("allresearcher_" . LANG . ".png", '', array('class' => 'img-responsive margin_top_40')); ?></a>
                </div>
                <div class='col-xs-8'>
                    <div class="each_widget">
                        <?php $widget->data_id = 20; ?>
                        <?php $widget->data_type = 'category'; ?>
                        <?php $block = widget_process($widget); ?>
                        <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
                        <?php if (count($block['post'])): ?>
                            <?php foreach ($block['post'] as $post): ?>
                                <div class="row" style='margin-bottom: 15px;'>
                                    <?php $cate_post = widget_cate($post); ?>
                                    <div class="widget_cate_thumbnail">
                                        <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                                    </div>
                                    <div class="widget_cate_info">
                                        <h4><a title="<?php echo $cate_post['alt_title'] ?>" href='<?php echo $cate_post['url']; ?>'><?php echo $cate_post['post_title']; ?></a> &dash; <small><i><?php echo date('F d Y', $cate_post['date']); ?></i></small></h4>
                                        <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <p class="txt_right">
                                <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                            </p>
                        <?php else: ?>
                            <h3 class="txt_center margin_top_10 red th_san">&dash;<?php echo lang("ไม่มีข่าวในหมวดนี้"); ?>&dash;</h3>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class='row margin_top_20'>
                <div class='col-xs-12'>
                    <?php echo $research_table ?>
                </div>
            </div>
        </div>
        <div class="col-xs-3 sidebar">
            <?php $event_obj = new stdClass(); ?>
            <?php $event_obj->data_type = "event"; ?>
            <?php $event_obj->data_id = 47; ?>
            <?php $all_event = widget_process($event_obj); ?>
            <div class="widget_header_type_1 txt_right th_san" style="margin-bottom: 20px;">
                <p>Events</p>
                <p><?php echo $all_event['title']; ?></p>
            </div>

            <?php foreach ($all_event['post'] as $event_raws): ?>
                <?php $obj_event = widget_event($event_raws); ?>
                <div class="row">
                    <div class="event_date txt_center th_san">
                        <span style="margin: 0px; font-size: 36px; line-height: 20px;" class="txt_center"><?php echo date('d', $obj_event['date']) ?></span>
                        <!--<span class="red" style="font-size: 16px; line-height: 14px; margin-top: -5px; display: block;"><?php echo date('M y', $obj_event['date']) ?></span>-->
                        <span class="red" style="font-size: 16px; line-height: 14px; margin-top: -5px; display: block;"><?php echo $locale_short_month[LANG][date('n', $obj_event['date'])] . " " . date('y', $obj_event['date']) ?></span>
                    </div>
                    <div class="event_info">
                        <p class="txt_justify" style="font-size: 11px; margin-bottom: 5px;"><?php echo $obj_event['post_title']; ?></p>
                        <p class="txt_right" style=" margin-bottom: 15px;"><a class="btn btn-xs btn-default" href="<?php echo $obj_event['url']; ?>"><?php echo lang("อ่านต่อ"); ?> &raquo;</a></p>
                    </div>
                </div>
            <?php endforeach; ?>
            <p class="txt_right">
                <a class="btn btn-default" href="<?php echo $all_event['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
            </p>

            <?php $ext_link = new stdClass(); ?>
            <?php $ext_link->data_type = "ext_link"; ?>
            <?php $ext_link->data_id = 447; ?>
            <?php $block = widget_process($ext_link); ?>
            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
            <?php $link = json_decode($block['post']) ?>
            <ul class="ext_link_list">
                <?php foreach ($link as $post): ?>
                    <li><a target="_blank" href="<?php echo $post->url; ?>"><?php echo $post->title; ?></a></li>
                <?php endforeach; ?>
            </ul>

            <!--Fix link-->
            <?php echo $fix_link; ?>
            <!--/Fix Link-->
        </div>
    </div>
</div>

<style>
    thead, tfoot{
        background-color: rgba(255,0,0,0.2);
    }
    td{
        vertical-align: middle !important;
        padding: 12px 5px !important;
    }
</style>
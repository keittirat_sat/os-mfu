<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-12">
            <?php if ($media_info->media_type == "pdf"): ?>
                <p>                    <a href="#x"></a>
                    <a href="<?php echo $media_info->link; ?>" class="btn btn-success"><i class="glyphicon glyphicon-download"></i> Download original file</a>
                </p>
                <?php $pdf_json = json_decode($media_info->media_info); ?>
                <?php $total = count($pdf_json->pdf_image); ?>

                <?php $all_img = array(); ?>
                <?php foreach ($pdf_json->pdf_image as $img): ?>
                    <?php $all_img[$img->name] = $img; ?>
                <?php endforeach; ?>

                <?php if (count($all_img) > 1): ?>
                    <div align=center>
                        <div class="margin_bottom_20">
                            <button class="btn btn-default btn-sm"  onclick="clipmeR();"><i class="glyphicon glyphicon-arrow-left"></i> Previous</button>
                            <button class="btn btn-default btn-sm"  onclick="clipmeL();">Next <i class="glyphicon glyphicon-arrow-right"></i></button>
                            Speed: <input id=speedButton style="width:30px;" value="15" onchange="pSpeed = parseInt(this.value);">
                            <select id="flipSelect" style="display:none;"></select>
                        </div>
                        <div id="bookflip"></div>
                    </div>
                    <div id="pages" style="width:1px; height:1px; overflow:hidden;">
                        <div><?php echo image_asset("page.jpg", '', array('class' => 'img-responsive')); ?></div>

                        <?php for ($i = 0; $i < $total; $i++): ?>
                            <?php $img = $all_img["page-{$i}.jpg"]; ?>
                            <div <?php echo $i == 0 ? 'name="Home"' : ""; ?> style="background: url('<?php echo $img->url; ?>'); background-size: 100% auto; background-repeat: no-repeat;"></div>
                        <?php endfor; ?>
                    </div>
                <?php else: ?>
                    <?php foreach ($all_img as $img): ?>
                        <img src="<?php echo $img->url; ?>" class="img-responsive" style="margin: 10px auto;">
                    <?php endforeach; ?>
                <?php endif; ?>

            <?php else: ?>
                <div class="jumbotron">
                    <h1>Cannot read this file</h1>
                    <p>Extension not support</p>
                    <p><a href="<?php echo site_url(); ?>"><i class="glyphicon glyphicon-home"></i> Home</a></p>
                </div>
            <?php endif; ?>
            <p class="txt_right margin_top_10"><span class="badge badge-important"><i class="glyphicon glyphicon-user"></i> จำนวนผู้เข้าชม: <font id="id_holder_stat"></font></span></p>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
//        $('#flipbook').pageFlip();
    });


    pWidth = 470; //width of each page
    pHeight = 665; //height of each page

    numPixels = 20;  //size of block in pixels to move each pass
    pSpeed = 15; //speed of animation, more is slower

    startingPage = "0";//select page to start from, for last page use "e", eg. startingPage="e"
    allowAutoflipFromUrl = true; //true allows querystring in url eg bookflip.html?autoflip=5

    pageBackgroundColor = "#CCCCCC";
    pageFontColor = "#ffffff";

    pageBorderWidth = "1";
    pageBorderColor = "#3D4D5D";
    pageBorderStyle = "solid";  //dotted, dashed, solid, double, groove, ridge, inset, outset, dotted solid double dashed, dotted solid

    pageShadowLeftImgUrl = "black_gradient.png";
    pageShadowWidth = 80;
    pageShadowOpacity = 60;
    pageShadow = 1 //0=shadow off, 1= shadow on left page

    allowPageClick = true; //allow page turn by clicking the page directly
    allowNavigation = true; //this builds a drop down list of pages for auto navigation.
    pageNumberPrefix = "page "; //displays in the drop down list of pages if enabled

    ini();
</script>
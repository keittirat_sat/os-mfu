<div class="img_top_holder pr_info">
    <div class="container">
        <div class="row">
            
        </div>
    </div>
</div>

<!--.body_display-->
<div class="body_display margin_top_20">
    <div class="container">
        <div class="row">
            <div class="col-xs-9">
                <h2 class="th_san red">สำนักวิชา</h2>
                <?php $post = get_all_post_by_type(TYPE_SCHOOL) ?>
                <table class="table table-striped organization-table">
                    <thead>
                        <tr>
                            <th class="txt_center"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;ชื่อหน่วยงาน</th>
                            <th width="250" class="txt_center"><i class="glyphicon glyphicon-send"></i>&nbsp;&nbsp;อีเมลล์</th>
                            <th width="150" class="txt_center"><i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;เบอร์โทรศัพท์</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <?php foreach ($post as $each_post): ?>
                            <?php $each_link = json_decode($each_post->post_detail) ?>
                            <tr>
                                <td><b><a href="<?php echo site_url("mfu/school/{$each_post->post_id}/" . slug($each_post->post_title)) ?>"><?php echo $each_post->post_title ?></a></b></td>
                                <td><?php echo auto_link(str_replace(",", "<br/>", $each_link->post_other_email)) ?></td>
                                <td><?php echo str_replace(",", "<br/>", $each_link->post_other_phone) ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <h2 class="th_san red">ศูนย์</h2>
                <?php $post = get_post_by_id(640); ?>
                <?php $all_link = json_decode($post->post_detail); ?>
                <table class="table table-striped organization-table">
                    <thead>
                        <tr>
                            <th class="txt_center"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;ชื่อหน่วยงาน</th>
                            <th width="250" class="txt_center"><i class="glyphicon glyphicon-send"></i>&nbsp;&nbsp;อีเมลล์</th>
                            <th width="150" class="txt_center"><i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;เบอร์โทรศัพท์</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <?php foreach ($all_link as $each_link): ?>
                            <tr>
                                <td><b><a href="<?php echo $each_link->url ?>"><?php echo $each_link->title ?></a></b></td>
                                <td><?php echo auto_link(str_replace(",", "<br/>", $each_link->detail2)) ?></td>
                                <td><?php echo str_replace(",", "<br/>", $each_link->detail) ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <h2 class="th_san red">สำนักงานวิชาการ</h2>
                <?php $post = get_post_by_id(641); ?>
                <?php $all_link = json_decode($post->post_detail); ?>
                <table class="table table-striped organization-table">
                    <thead>
                        <tr>
                            <th class="txt_center"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;ชื่อหน่วยงาน</th>
                            <th width="250" class="txt_center"><i class="glyphicon glyphicon-send"></i>&nbsp;&nbsp;อีเมลล์</th>
                            <th width="150" class="txt_center"><i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;เบอร์โทรศัพท์</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <?php foreach ($all_link as $each_link): ?>
                            <tr>
                                <td><b><a href="<?php echo $each_link->url ?>"><?php echo $each_link->title ?></a></b></td>
                                <td><?php echo auto_link(str_replace(",", "<br/>", $each_link->detail2)) ?></td>
                                <td><?php echo str_replace(",", "<br/>", $each_link->detail) ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <h2 class="th_san red">สำนักงานบริหารกลาง</h2>
                <?php $post = get_post_by_id(642); ?>
                <?php $all_link = json_decode($post->post_detail); ?>
                <table class="table table-striped organization-table">
                    <thead>
                        <tr>
                            <th class="txt_center"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;ชื่อหน่วยงาน</th>
                            <th width="250" class="txt_center"><i class="glyphicon glyphicon-send"></i>&nbsp;&nbsp;อีเมลล์</th>
                            <th width="150" class="txt_center"><i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;เบอร์โทรศัพท์</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <?php foreach ($all_link as $each_link): ?>
                            <tr>
                                <td><b><a href="<?php echo $each_link->url ?>"><?php echo $each_link->title ?></a></b></td>
                                <td><?php echo auto_link(str_replace(",", "<br/>", $each_link->detail2)) ?></td>
                                <td><?php echo str_replace(",", "<br/>", $each_link->detail) ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <h2 class="th_san red">หน่วยบริการและหารายได้</h2>
                <?php $post = get_post_by_id(643); ?>
                <?php $all_link = json_decode($post->post_detail); ?>
                <table class="table table-striped organization-table">
                    <thead>
                        <tr>
                            <th class="txt_center"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;ชื่อหน่วยงาน</th>
                            <th width="250" class="txt_center"><i class="glyphicon glyphicon-send"></i>&nbsp;&nbsp;อีเมลล์</th>
                            <th width="150" class="txt_center"><i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;เบอร์โทรศัพท์</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <?php foreach ($all_link as $each_link): ?>
                            <tr>
                                <td><b><a href="<?php echo $each_link->url ?>"><?php echo $each_link->title ?></a></b></td>
                                <td><?php echo auto_link(str_replace(",", "<br/>", $each_link->detail2)) ?></td>
                                <td><?php echo str_replace(",", "<br/>", $each_link->detail) ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <h2 class="th_san red">โครงการ / หน่วยงานพิเศษ</h2>
                <?php $post = get_post_by_id(645); ?>
                <?php $all_link = json_decode($post->post_detail); ?>
                <table class="table table-striped organization-table">
                    <thead>
                        <tr>
                            <th class="txt_center"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;ชื่อหน่วยงาน</th>
                            <th width="250" class="txt_center"><i class="glyphicon glyphicon-send"></i>&nbsp;&nbsp;อีเมลล์</th>
                            <th width="150" class="txt_center"><i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;เบอร์โทรศัพท์</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <?php foreach ($all_link as $each_link): ?>
                            <tr>
                                <td><b><a href="<?php echo $each_link->url ?>"><?php echo $each_link->title ?></a></b></td>
                                <td><?php echo auto_link(str_replace(",", "<br/>", $each_link->detail2)) ?></td>
                                <td><?php echo str_replace(",", "<br/>", $each_link->detail) ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <?php $post = get_post_by_id(646); ?>
                <?php $all_link = json_decode($post->post_detail); ?>
                <?php foreach ($all_link as $each_link): ?>
                    <h2 class="th_san red"><?php echo $each_link->title ?></h2>
                    <p>
                        <i class="glyphicon glyphicon-home"></i>&nbsp;<?php echo $each_link->title ?>
                    </p>
                    <?php if ($each_link->detail): ?>
                        <p>
                            <i class="glyphicon glyphicon-earphone"></i>&nbsp;<b>เบอร์โทรศัพท์&nbsp;</b><?php echo $each_link->detail ?>
                        </p>
                    <?php endif; ?>
                    <?php if ($each_link->detail2): ?>
                        <p>
                            <i class="glyphicon glyphicon-send"></i>&nbsp;<b>อีเมลล์&nbsp;</b><?php echo auto_link($each_link->detail2); ?>
                        </p>
                    <?php endif; ?>
                    <?php if ($each_link->url != "#"): ?>
                        <p class="txt_right">
                            <a href="<?php echo $each_link->url ?>" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-share-alt"></i>&nbsp;เว็บไซต์หน่วยงาน</a>
                        </p>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="col-xs-3">
                <?php echo $sidebar ?>
            </div>
        </div>
    </div>
</div>
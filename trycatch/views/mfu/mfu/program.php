<div class="img_top_holder programmes">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('programmes_header_text_' . LANG . '.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>
<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-9">
            <?php echo $data; ?>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
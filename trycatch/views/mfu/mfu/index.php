<!--<div class="img_top_holder index">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('home_header_text.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>-->

<div class="img_top_holder index_mini">
    <div class="container">
        <div class="row">
            <!--<div class="col-lg-12"><?php echo image_asset('home_header_text.png', '', array('class' => 'img-responsive')); ?></div>-->
        </div>
    </div>
</div>

<!--.body_display-->
<div class="body_display margin_top_20">
    <div class="container">
        <div class="row">
            <div class="col-xs-9">

                <!--.slide_index-->
                <div class="row slide_index">
                    <div class="col-xs-12">
                        <!--.Slide-->
                        <div class="slide slider-wrapper theme-default">
                            <div class="ribbon"></div>
                            <div id="slider" style=' height: 280px;' class="nivoSlider">
                                <?php foreach ($slide as $each_slide): ?>
                                    <?php $img = json_decode($each_slide->link); ?>
                                    <?php if ($each_slide->post_type == TYPE_POST): ?>
                                        <?php $link = site_url("mfu/post/{$each_slide->post_id}/" . slug($each_slide->post_title)); ?>
                                    <?php else: ?>
                                        <?php $link = $each_slide->post_excerp ?>
                                    <?php endif; ?>
                                    <a href='<?php echo empty($link) ? $link : "#"; ?>' target="_blank">
                                        <img src='<?php echo $img->l; ?>' title='<?php echo $each_slide->post_title; ?>'>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <!--/.Slide-->                
                    </div>
                </div><!--/.slide_index-->

                <!--Link to Admission and Visitor Info-->
                <div class="row margin_top_20">
                    <div class="custom-col-xs-5">
                        <a href="<?php echo site_url('mfu/admission'); ?>" target="__blank"><img src="<?php echo image_asset_url("admission_banner_" . LANG . ".png"); ?>" class="img-responsive"></a>
                    </div>
                    <div class="custom-col-xs-4">
                        <a href="<?php echo site_url('mfu/visitor_info'); ?>"><img src="<?php echo image_asset_url("visitorinfo_banner_" . LANG . ".png"); ?>" class="img-responsive"></a>
                    </div>
                </div><!--Link to Admission and Visitor Info-->


                <div class="row margin_top_20">
                    <div class="col-xs-4">
                        <!--Event type-->
                        <?php foreach ($widget->a as $section): ?>
                            <div class="each_widget">
                                <?php $block = widget_process($section); ?>
                                <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
                                <?php if (count($block['post'])): ?>

                                    <!--section loop-->
                                    <?php foreach ($block['post'] as $counter_limit => $event_raws): ?>
                                        <?php $obj_event = widget_event($event_raws); ?>
                                        <div class="row">
                                            <div class="event_date txt_center th_san">
                                                <span style="margin: 0px; font-size: 36px; line-height: 20px;" class="txt_center"><?php echo date('d', $obj_event['date']) ?></span>
                                                <span class="red" style="font-size: 16px; line-height: 14px; margin-top: -5px; display: block;"><?php echo $locale_short_month[LANG][date('n', $obj_event['date'])] . " " . date('y', $obj_event['date']) ?></span>
                                            </div>
                                            <div class="event_info">
                                                <p class="txt_justify" style="font-size: 11px; margin-bottom: 5px;"><?php echo $obj_event['post_title']; ?></p>
                                                <p class="txt_right" style=" margin-bottom: 15px;"><a class="btn btn-xs btn-default" href="<?php echo $obj_event['url']; ?>"><?php echo lang("อ่านต่อ"); ?> &raquo;</a></p>
                                            </div>
                                        </div>

                                        <?php if ($counter_limit >= 5): ?>
                                            <?php break; ?>
                                        <?php endif; ?>

                                    <?php endforeach; ?>
                                    <!--/section loop-->

                                    <p class="txt_right">
                                        <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                                    </p>
                                <?php else: ?>
                                    <h4 class="red th_san txt_center">&dash;<?php echo lang("ไม่มีรายการในหมวดนี้"); ?>&dash;</h4>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                        <!--/Event type-->
                    </div>

                    <div class="col-xs-8">
                        <?php foreach ($widget->b as $section): ?>
                            <div class="each_widget">
                                <?php $block = widget_process($section); ?>
                                <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
                                <?php foreach ($block['post'] as $post): ?>
                                    <div class="row" style='margin-bottom: 15px;'>
                                        <?php $cate_post = widget_cate($post); ?>
                                        <div class="widget_cate_thumbnail">
                                            <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                                        </div>
                                        <div class="widget_cate_info">
                                            <h4 class="th_san red_1"><a title="<?php echo $cate_post['alt_title'] ?>" class="link_inherit" href='<?php echo $cate_post['url']; ?>'><?php echo $cate_post['post_title']; ?></a> &dash; <small><i><?php echo get_longdate_format($cate_post['date']); ?></i></small></h4>
                                            <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <p class="txt_right">
                                    <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                                </p>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
                <?php if ((count($widget->c) > 0) || (count($widget->d) > 0)): ?>
                    <div class="row">
                        <?php if (count($widget->c) > 0): ?>
                            <div class="<?php echo count($widget->d) > 0 ? "col-xs-6" : "col-xs-12" ?>">
                                <?php foreach ($widget->c as $section): ?>
                                    <div class="each_widget">
                                        <?php $block = widget_process($section); ?>
                                        <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
                                        <?php foreach ($block['post'] as $post): ?>
                                            <div class="row" style='margin-bottom: 15px;'>
                                                <?php $cate_post = widget_cate($post); ?>
                                                <div class="widget_cate_thumbnail">
                                                    <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                                                </div>
                                                <div class="widget_cate_info block_half">
                                                    <h4 class="th_san red_1 margin_top_0"><a title="<?php echo $cate_post['alt_title'] ?>" class="link_inherit" href='<?php echo $cate_post['url']; ?>'><?php echo $cate_post['post_title']; ?></a></h4>
                                                    <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                        <p class="txt_right">
                                            <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                                        </p>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                        <?php if (count($widget->d) > 0): ?>
                            <div class="<?php echo count($widget->c) > 0 ? "col-xs-6" : "col-xs-12" ?>">
                                <?php foreach ($widget->d as $section): ?>
                                    <div class="each_widget">
                                        <?php $block = widget_process($section); ?>
                                        <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
                                        <?php foreach ($block['post'] as $post): ?>
                                            <div class="row" style='margin-bottom: 15px;'>
                                                <?php $cate_post = widget_cate($post); ?>
                                                <div class="widget_cate_thumbnail">
                                                    <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                                                </div>
                                                <div class="widget_cate_info block_half">
                                                    <h4 class="th_san red_1 margin_top_0"><a class="link_inherit" title="<?php echo $cate_post['alt_title'] ?>" href='<?php echo $cate_post['url']; ?>'><?php echo $cate_post['post_title']; ?></a></h4>
                                                    <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                        <p class="txt_right">
                                            <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                                        </p>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                    </div>
                <?php endif; ?>
            </div>

            <div class="col-xs-3 sidebar">
                <?php echo $sidebar; ?>
            </div>

        </div>

        <?php if (count($widget->block_2_block_a) > 0 || count($widget->block_2_block_b) > 0): ?>
            <div class="row block_2">

                <?php if (count($widget->block_2_block_a) > 0): ?>
                    <div class="<?php echo count($widget->block_2_block_b) > 0 ? "col-xs-6" : "col-xs-12" ?>">
                        <?php foreach ($widget->block_2_block_a as $section): ?>
                            <div class="each_widget">
                                <?php $block = widget_process($section); ?>
                                <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
                                <?php foreach ($block['post'] as $post): ?>
                                    <div class="row" style='margin-bottom: 15px;'>
                                        <?php $cate_post = widget_cate($post); ?>
                                        <div class="widget_cate_thumbnail">
                                            <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                                        </div>
                                        <div class="widget_cate_info">
                                            <h4 class="th_san red_1 margin_top_0"><a class="link_inherit" title="<?php echo $cate_post['alt_title'] ?>" href='<?php echo $cate_post['url']; ?>'><?php echo $cate_post['post_title']; ?></a></h4>
                                            <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <p class="txt_right">
                                    <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                                </p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <?php if (count($widget->block_2_block_b) > 0): ?>
                    <div class="<?php echo count($widget->block_2_block_a) > 0 ? "col-xs-6" : "col-xs-12" ?>">
                        <?php foreach ($widget->block_2_block_b as $section): ?>
                            <div class="each_widget">
                                <?php $block = widget_process($section); ?>
                                <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
                                <?php foreach ($block['post'] as $post): ?>
                                    <div class="row" style='margin-bottom: 15px;'>
                                        <?php $cate_post = widget_cate($post); ?>
                                        <div class="widget_cate_thumbnail">
                                            <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                                        </div>
                                        <div class="widget_cate_info">
                                            <h4 class="th_san red_1 margin_top_0"><a class="link_inherit" title="<?php echo $cate_post['alt_title'] ?>" href='<?php echo $cate_post['url']; ?>'><?php echo $cate_post['post_title']; ?></a></h4>
                                            <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <p class="txt_right">
                                    <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                                </p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

            </div>
        <?php endif; ?>

        <?php if (count($widget->block_3_block_a) > 0 || count($widget->block_3_block_b) > 0 || count($widget->block_3_block_c) > 0): ?>
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("ข่าวอื่นๆ"); ?></span></h4>
                </div>
            </div>

            <div class="row block_3">

                <?php if (count($widget->block_3_block_a) > 0): ?>
                    <!--block_3_block_a-->
                    <div class="<?php echo template_section_block3($widget->block_3_block_a, $widget->block_3_block_b, $widget->block_3_block_c) ?>">
                        <?php foreach ($widget->block_3_block_a as $section): ?>
                            <div class="each_widget">
                                <?php $block = widget_process($section); ?>
                                <h4 class="txt_center"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo $block['title']; ?></h4>
                                <?php if (count($block['post']) > 0): ?>
                                    <ul class="post_in_cate_list">
                                        <?php foreach ($block['post'] as $post): ?>
                                            <?php $cate_post = widget_cate($post) ?>
                                            <li>
                                                <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>"><?php echo $cate_post['alt_title'] ?></a>
                                                <i style="color: #666; font-size: 11px;">&nbsp;&dash;&nbsp;<?php echo get_longdate_format($cate_post['date']); ?></i>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <p class="txt_right">
                                        <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                                    </p>
                                <?php else: ?>
                                    <p class="txt_center"><?php echo lang("ไม่มีข่าวในหมวดนี้"); ?></p>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!--/block_3_block_a-->
                <?php endif; ?>

                <!--block_3_block_b-->
                <div class="<?php echo template_section_block3($widget->block_3_block_a, $widget->block_3_block_b, $widget->block_3_block_c) ?>">
                    <?php foreach ($widget->block_3_block_b as $section): ?>
                        <div class="each_widget">
                            <?php $block = widget_process($section); ?>
                            <h4 class="txt_center"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo $block['title']; ?></h4>
                            <?php if (count($block['post']) > 0): ?>
                                <ul class="post_in_cate_list">
                                    <?php foreach ($block['post'] as $post): ?>
                                        <?php $cate_post = widget_cate($post) ?>
                                        <li>
                                            <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>"><?php echo $cate_post['alt_title'] ?></a>
                                            <i style="color: #666; font-size: 11px;">&nbsp;&dash;&nbsp;<?php echo get_longdate_format($cate_post['date']); ?></i>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <p class="txt_right">
                                    <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                                </p>
                            <?php else: ?>
                                <p class="txt_center"><?php echo lang("ไม่มีข่าวในหมวดนี้"); ?></p>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!--/block_3_block_b-->

                <!--block_3_block_c-->
                <div class="<?php echo template_section_block3($widget->block_3_block_a, $widget->block_3_block_b, $widget->block_3_block_c) ?>">
                    <?php foreach ($widget->block_3_block_c as $section): ?>
                        <div class="each_widget">
                            <?php $block = widget_process($section); ?>
                            <h4 class="txt_center"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo $block['title']; ?></h4>
                            <?php if (count($block['post']) > 0): ?>
                                <ul class="post_in_cate_list">
                                    <?php foreach ($block['post'] as $post): ?>
                                        <?php $cate_post = widget_cate($post) ?>
                                        <li>
                                            <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>"><?php echo $cate_post['alt_title'] ?></a>
                                            <i style="color: #666; font-size: 11px;">&nbsp;&dash;&nbsp;<?php echo get_longdate_format($cate_post['date']); ?></i>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <p class="txt_right">
                                    <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                                </p>
                            <?php else: ?>
                                <p class="txt_center"><?php echo lang("ไม่มีข่าวในหมวดนี้"); ?></p>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!--/block_3_block_c-->
            </div>
        <?php endif; ?>
    </div>
</div>
<!--/.body_display-->

<script type="text/javascript">
    $(function() {
        $('#slider').nivoSlider();
    });
</script>
<div class="container margin_top_20">    
    <div class="row">
        <div class="col-xs-12">
            <h1 class="red th_san margin_top_0 margin_bottom_0"><?php echo $cate_info->cate_name; ?></h1>
            <form>
                <div class="input-group input-group-sm box_search">
                    <!--month-->
                    <div class="input-group-btn">                            
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="replace_here"><?php echo lang("ทุกเดือน") ?></span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu month_list" role="menu">
                            <?php foreach ($locale_month[LANG] as $i => $each_month): ?>
                                <li <?php echo $month == $i ? "class='active'" : ""; ?>><a href="<?php echo "#{$i}"; ?>" data-month="<?php echo $i; ?>"><?php echo $i == 0 ? lang("ทุกเดือน") : $each_month ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div><!--month-->
                    <!--year-->
                    <div class="input-group-btn">      
                        <button type="button" class="btn btn-default dropdown-toggle border_radius_0 margin_right_minus_1" data-toggle="dropdown">
                            <span class="replace_here"><?php echo lang("ทุกปี") ?></span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu year_list" role="menu">
                            <li <?php echo $year == 0 ? "class='active'" : ""; ?>><a href="<?php echo "#0"; ?>" data-year="0"><?php echo lang("ทุกปี") ?></a></li>
                            <?php foreach ($all_year as $each_year): ?>
                                <?php $temp_year = date("Y", $each_year->post_year); ?>
                                <li <?php echo $year == $temp_year ? "class='active'" : ""; ?>><a href="<?php echo "#{$temp_year}"; ?>" data-year="<?php echo $temp_year; ?>"><?php echo $i == 0 ? lang("ทุกปี") : $temp_year ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div><!--year-->

                    <input type="text" class="form-control" name="q" value="<?php echo $q; ?>" placeholder="Search in this section">
                    <span class="input-group-btn">
                        <button class="btn btn-default"><i class=" glyphicon glyphicon-search"></i></button>
                    </span>
                </div>
                <input type="hidden" id="search_cate_id" name="cate_id" value="<?php echo $cate_id ?>">
                <input type="hidden" id="search_month" name="month" value="<?php echo $month ?>">
                <input type="hidden" id="search_year" name="year" value="<?php echo $year ?>">
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-9">

            <?php if ($total_page > 1): ?>
                <div class="row">
                    <div class="col-xs-12 txt_right">
                        <ul class="pagination pagination-sm">
                            <?php for ($i = 0; $i < $total_page; $i++): ?>
                                <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo "?page={$i}&month={$month}&year={$year}"; ?>"><?php echo $i + 1; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (count($all_post)): ?>
                <?php foreach ($all_post as $post): ?>
                    <div class="row" style='margin-bottom: 15px;'>
                        <?php $cate_post = widget_cate($post); ?>
                        <div class="widget_cate_thumbnail">
                            <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                        </div>
                        <div class="widget_cate_info cate_page">
                            <h3 class="th_san margin_top_0 red_1">
                                <a title="<?php echo $cate_post['alt_title'] ?>" href='<?php echo $cate_post['url']; ?>' class="link_inherit"><?php echo $cate_post['post_title']; ?></a> &dash;
                                <small><i><?php echo get_longdate_format($cate_post['date']); ?></i></small>
                            </h3>
                            <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <h3 class="txt_center th_san"><?php echo lang("ไม่มีข่าวในหมวดนี้"); ?></h3>
            <?php endif; ?>

            <?php if ($total_page > 1): ?>
                <div class="row">
                    <div class="col-xs-12 txt_right">
                        <ul class="pagination pagination-sm">
                            <?php for ($i = 0; $i < $total_page; $i++): ?>
                                <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo "?page={$i}&month={$month}&year={$year}"; ?>"><?php echo $i + 1; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($cate_info->api_id): ?>
                <p class="txt_right">
                    <?php $api = get_post_by_id($cate_info->api_id); ?>
                    <a href="<?php echo site_url("mfu/archive/{$cate_info->api_id}/" . slug($api->post_title)); ?>" target="_blank" class="btn btn-default">
                        <i class="glyphicon glyphicon-time"></i>&nbsp;<?php echo lang("อ่านข่าวจากเว็บไซต์เดิม"); ?>
                    </a>
                </p>
            <?php endif; ?>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>

<style>
    .pagination-sm{
        margin: 10px 0px;
    }
</style>

<script type="text/javascript">

    $(function() {

        $("ul.month_list li a").click(function() {
            var info = $(this).attr('data-month');
            var label = $(this).text();

            $("ul.month_list li").removeClass('active');
            $(this).parent('li').addClass('active');
            $("ul.month_list").siblings("button").find(".replace_here").text(label);
            $('#search_month').val(info);
            return false;
        });

        $("ul.year_list li a").click(function() {
            var info = $(this).attr('data-year');
            var label = $(this).text();

            $("ul.year_list li").removeClass('active');
            $(this).parent('li').addClass('active');
            $("ul.year_list").siblings("button").find(".replace_here").text(label);
            $('#search_year').val(info);
            return false;
        });

        $.each($('.box_search li.active'), function(idx, ele) {
            $(ele).find("a").trigger("click");
        });

        $('#filter_btn').click(function() {
            $('#submit_btn_hidden').trigger('click');
        });
    });
</script>
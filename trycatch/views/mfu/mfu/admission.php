<?php $post = get_post_by_id(602); ?>
<div class="img_top_holder admission">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 relative">
                <?php echo image_asset("Admission_header_text_" . LANG . ".png", '', array('class' => 'img-responsive')); ?>
                <div class='sub_inside_menu'>
                    <?php echo image_asset("admission_menu_head.png", '', array('style' => 'float:left;')); ?>
                    <ul class='nav nav-pills menu_interior'>
                        <?php $list = json_decode($post->post_detail); ?>
                        <?php foreach ($list as $each_link): ?>
                            <li><a href='<?php echo $each_link->url; ?>'><?php echo $each_link->title; ?></a></li>
                        <?php endforeach; ?>
                        <li>
                            <a class="fb_btn_submenu" href=<?php echo FB_ADMISSION; ?>>
                                <?php echo image_asset("admission_menu_FB.png", "", array('class' => 'img-responsive')) ?>
                            </a>
                        </li>
                    </ul>
                    <?php echo image_asset("admission_menu_tail.png", '', array('style' => 'float:left;')); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-9">
            <div class="row">
                <div class="col-xs-4">
                    <?php echo image_asset("Admission_coursedetail_icon_" . LANG . ".png", '', array('class' => 'img-responsive')); ?>
                    <?php $each_post = get_post_by_id(576); ?>
                    <p class="font_11"><?php echo $each_post->post_excerp ?></p>
                    <?php $list = @json_decode($each_post->post_detail); ?>
                    <?php if (is_array($list)): ?>
                        <?php if (count($list) <= 5): ?>
                            <ul class='hilight_link'>
                                <?php foreach ($list as $each_post): ?>
                                    <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url; ?>'><?php echo $each_post->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php else: ?>
                            <?php $l_element = array(); ?>
                            <?php $r_element = array(); ?>
                            <?php foreach ($list as $index => $each_element): ?>
                                <?php if ($index % 2): ?>
                                    <?php array_push($r_element, $each_element); ?>
                                <?php else: ?>
                                    <?php array_push($l_element, $each_element); ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <div class="row">
                                <div class="col-xs-6">
                                    <ul class='hilight_link' style="padding-left: 15px;">
                                        <?php foreach ($l_element as $each_post): ?>
                                            <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url; ?>'><?php echo $each_post->title; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <div class="col-xs-6">
                                    <ul class='hilight_link' style="padding-left: 15px;">
                                        <?php foreach ($r_element as $each_post): ?>
                                            <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url; ?>'><?php echo $each_post->title; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-4">
                    <?php echo image_asset("Admission_scholarship_icon_" . LANG . ".png", '', array('class' => 'img-responsive')); ?>
                    <?php $each_post = get_post_by_id(577); ?>
                    <p class="font_11"><?php echo $each_post->post_excerp ?></p>
                    <?php $list = @json_decode($each_post->post_detail); ?>
                    <?php if (is_array($list)): ?>
                        <?php if (count($list) <= 5): ?>
                            <ul class='hilight_link'>
                                <?php foreach ($list as $each_post): ?>
                                    <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url; ?>'><?php echo $each_post->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php else: ?>
                            <?php $l_element = array(); ?>
                            <?php $r_element = array(); ?>
                            <?php foreach ($list as $index => $each_element): ?>
                                <?php if ($index % 2): ?>
                                    <?php array_push($r_element, $each_element); ?>
                                <?php else: ?>
                                    <?php array_push($l_element, $each_element); ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <div class="row">
                                <div class="col-xs-6">
                                    <ul class='hilight_link' style="padding-left: 15px;">
                                        <?php foreach ($l_element as $each_post): ?>
                                            <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url; ?>'><?php echo $each_post->title; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <div class="col-xs-6">
                                    <ul class='hilight_link' style="padding-left: 15px;">
                                        <?php foreach ($r_element as $each_post): ?>
                                            <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url; ?>'><?php echo $each_post->title; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-4">
                    <?php echo image_asset("Admission_lifeoncampus_icon_" . LANG . ".png", '', array('class' => 'img-responsive')); ?>
                    <?php $each_post = get_post_by_id(578); ?>
                    <p class="font_11"><?php echo $each_post->post_excerp ?></p>
                    <?php $list = @json_decode($each_post->post_detail); ?>
                    <?php if (is_array($list)): ?>
                        <?php if (count($list) <= 5): ?>
                            <ul class='hilight_link'>
                                <?php foreach ($list as $each_post): ?>
                                    <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url; ?>'><?php echo $each_post->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php else: ?>
                            <?php $l_element = array(); ?>
                            <?php $r_element = array(); ?>
                            <?php foreach ($list as $index => $each_element): ?>
                                <?php if ($index % 2): ?>
                                    <?php array_push($r_element, $each_element); ?>
                                <?php else: ?>
                                    <?php array_push($l_element, $each_element); ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <div class="row">
                                <div class="col-xs-6">
                                    <ul class='hilight_link' style="padding-left: 15px;">
                                        <?php foreach ($l_element as $each_post): ?>
                                            <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url; ?>'><?php echo $each_post->title; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <div class="col-xs-6">
                                    <ul class='hilight_link' style="padding-left: 15px;">
                                        <?php foreach ($r_element as $each_post): ?>
                                            <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_post->url; ?>'><?php echo $each_post->title; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-4">
                    <?php $event_obj = new stdClass(); ?>
                    <?php $event_obj->data_type = "event"; ?>
                    <?php $event_obj->data_id = 50; ?>
                    <?php $all_event = widget_process($event_obj); ?>
                    <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $all_event['title']; ?></span></h4>
                    <?php if (count($all_event['post'])): ?>
                        <?php foreach ($all_event['post'] as $event_raws): ?>
                            <?php $obj_event = widget_event($event_raws); ?>
                            <div class="row">
                                <div class="event_date txt_center th_san">
                                    <span style="margin: 0px; font-size: 36px; line-height: 20px;" class="txt_center"><?php echo date('d', $obj_event['date']) ?></span>
                                    <span class="red" style="font-size: 16px; line-height: 14px; margin-top: -5px; display: block;"><?php echo $locale_short_month[LANG][date('n', $obj_event['date'])] . " " . date('y', $obj_event['date']) ?></span>
                                </div>
                                <div class="event_info">
                                    <p class="txt_justify" style="font-size: 11px; margin-bottom: 5px;"><?php echo $obj_event['post_title']; ?></p>
                                    <p class="txt_right" style=" margin-bottom: 15px;"><a class="btn btn-xs btn-default" href="<?php echo $obj_event['url']; ?>"><?php echo lang("อ่านต่อ"); ?> &raquo;</a></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <p class="txt_right">
                            <a class="btn btn-default" href="<?php echo site_url('mfu/all_event?cate_id=50'); ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                        </p>
                    <?php else: ?>
                        <h4 class="red th_san txt_center">&dash;<?php echo lang("ไม่มีข่าวในหมวดนี้") ?>&dash;</h4>
                    <?php endif; ?>
                </div> 
                <div class="col-xs-8">
                    <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("ข่าวรับสมัครล่าสุด") ?></span></h4>
                    <?php $post = get_latest_admission_news(10); ?>
                    <?php if (count($post)): ?>
                        <?php foreach ($post as $each_post): ?>
                            <?php $img = $each_post->link ? json_decode($each_post->link) : ""; ?>                    
                            <?php $dim = "portrait"; ?>
                            <?php if ($img != ""): ?>
                                <?php $path = "./uploads/{$each_post->mid_post_id}/{$each_post->media_name}"; ?>
                                <?php $img_info = getimagesize($path); ?>
                                <?php if ($img_info[0] > $img_info[1]): ?>
                                    <?php $dim = 'langscape'; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                            <div class="row" style='margin-bottom: 15px;'>
                                <div class="widget_cate_thumbnail">
                                    <a title="<?php echo $each_post->post_title ?>" href="<?php echo site_url("mfu/post/{$each_post->post_id}/" . slug($each_post->post_title)); ?>" style="background-image: url('<?php echo $img ? $img->s : image_asset_url("Blank_thumbnail.jpg"); ?>');" class="thumbnail_mfu <?php echo $dim; ?>"></a>
                                </div>
                                <div class="widget_cate_info">
                                    <h4 class="th_san">
                                        <a title="<?php echo $each_post->post_title; ?>" href='<?php echo site_url("mfu/post/{$each_post->post_id}/" . slug($each_post->post_title)); ?>'><?php echo $each_post->post_title; ?></a> &dash;
                                        <small><i><?php echo get_longdate_format($each_post->post_date); ?></i></small>
                                    </h4>
                                    <p class='txt_justify'><?php echo get_excerp($each_post, 240); ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <p class="txt_right"><a class="btn btn-default" href="<?php echo site_url('mfu/news_mashup'); ?>"><i class="glyphicon glyphicon-plus"></i>&nbsp;อ่านข่าวทั้งหมด</a></p>
                    <?php else: ?>
                        <h4 class="txt_center margin_top_10 red th_san">&dash;<?php echo lang("ไม่มีข่าวในหมวดนี้") ?>&dash;</h4>
                    <?php endif; ?>
                </div> 
            </div>


            <h4 class="spc_label margin_top_20" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("ข่าวการรับสมัครอื่นๆ"); ?></span></h4>

            <div class="row block_3 ">
                <div class="col-xs-4">
                    <div class="each_widget">
                        <?php $widget->data_id = 25; ?>
                        <?php $widget->data_type = 'category'; ?>
                        <?php $block = widget_process($widget); ?>
                        <h2 class="txt_center th_san margin_top_0"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("ปริญญาตรี"); ?></h2>
                        <?php if (count($block['post']) > 0): ?>
                            <ul class="post_in_cate_list">
                                <?php foreach ($block['post'] as $post): ?>
                                    <?php $cate_post = widget_cate($post) ?>
                                    <li>
                                        <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>"><?php echo $cate_post['alt_title'] ?></a>
                                        <i style="color: #666; font-size: 11px;">&nbsp;&dash;&nbsp;<?php echo get_longdate_format($cate_post['date']); ?></i>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <p class="txt_right">
                                <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                            </p>
                        <?php else: ?>
                            <h4 class="txt_center margin_top_10 red th_san">&dash;<?php echo lang("ไม่มีข่าวในหมวดนี้"); ?>&dash;</h4>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="each_widget">
                        <?php $widget->data_id = 26; ?>
                        <?php $widget->data_type = 'category'; ?>
                        <?php $block = widget_process($widget); ?>
                        <h2 class="txt_center th_san margin_top_0"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("ปริญญาโท"); ?></h2>
                        <?php if (count($block['post']) > 0): ?>
                            <ul class="post_in_cate_list">
                                <?php foreach ($block['post'] as $post): ?>
                                    <?php $cate_post = widget_cate($post) ?>
                                    <li>
                                        <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>"><?php echo $cate_post['alt_title'] ?></a>
                                        <i style="color: #666; font-size: 11px;">&nbsp;&dash;&nbsp;<?php echo get_longdate_format($cate_post['date']); ?></i>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <p class="txt_right">
                                <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                            </p>
                        <?php else: ?>
                            <h4 class="txt_center margin_top_10 red th_san">&dash;<?php echo lang("ไม่มีข่าวในหมวดนี้"); ?>&dash;</h4>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="each_widget">
                        <?php $widget->data_id = 27; ?>
                        <?php $widget->data_type = 'category'; ?>
                        <?php $block = widget_process($widget); ?>
                        <h2 class="txt_center th_san margin_top_0"><?php echo image_asset('bullet.png'); ?>&nbsp;<?php echo lang("ปริญญาเอก"); ?></h2>
                        <?php if (count($block['post']) > 0): ?>
                            <ul class="post_in_cate_list">
                                <?php foreach ($block['post'] as $post): ?>
                                    <?php $cate_post = widget_cate($post) ?>
                                    <li>
                                        <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>"><?php echo $cate_post['alt_title'] ?></a>
                                        <i style="color: #666; font-size: 11px;">&nbsp;&dash;&nbsp;<?php echo get_longdate_format($cate_post['date']); ?></i>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <p class="txt_right">
                                <a class="btn btn-default" href="<?php echo $block['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                            </p>
                        <?php else: ?>
                            <h4 class="txt_center margin_top_10 red th_san">&dash;<?php echo lang("ไม่มีข่าวในหมวดนี้"); ?>&dash;</h4>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-3 sidebar">
            <?php $all_event = get_event_by_status(); ?>
<!--            <div class="widget_header_type_1 txt_right th_san" style="margin-bottom: 20px;">
                <p>Information&amp;Overview</p>
                <p>ประชมสัมพันธ์ข้อมูลทั่วไป</p>
            </div>-->
            
            <div class="widget_header_type_1 txt_right th_san" style="margin-bottom: 20px;">
                <p>Information</p>
                <p>ข้อมูลทั่วไป</p>
            </div>

            <?php $list = get_post_by_id(446); ?>
            <?php $side_list = json_decode($list->post_detail) ?>
            <ul class="hilight_link">
                <?php foreach ($side_list as $link): ?>
                    <li><font class='red'>&raquo;&nbsp;</font><a href="<?php echo $link->url; ?>"><?php echo $link->title; ?></a></li>
                <?php endforeach; ?>
            </ul>


            <?php $widget->data_id = 0; ?>
            <?php $widget->data_type = 'school'; ?>
            <?php $block = widget_process($widget); ?>
            <div class="widget_header_type_1 txt_right th_san">
                <p>Schools &amp; Departments</p>
                <p>สำนักวิชาและหน่วยงาน</p>
            </div>
            <p class="label_widget_type_1 th_san"><?php echo lang('สำนักวิชา') ?></p>
            <ul class="school_list">
                <?php foreach ($block['post'] as $post): ?>
                    <li><a href="<?php echo site_url("mfu/school/{$post->post_id}/" . slug($post->post_title)); ?>"><?php echo $post->post_title; ?></a></li>
                <?php endforeach; ?>
            </ul>

            <?php $list = get_post_by_id(447); ?>
            <?php $side_list = json_decode($list->post_detail) ?>
            <h2 class="margin_top_0 th_san red"><?php echo $list->post_title; ?></h2>
            <ul class="hilight_link">
                <?php foreach ($side_list as $link): ?>
                    <li><font class='red'>&raquo;&nbsp;</font><a href="<?php echo $link->url; ?>"><?php echo $link->title; ?></a></li>
                    <?php endforeach; ?>
            </ul>

            <!--Fix link-->
            <?php echo $fix_link; ?>
            <!--/Fix Link-->
        </div>
    </div>
</div>
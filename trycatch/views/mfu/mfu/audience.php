<?php

function gen_tree_aud($each_group, $group) { ?>
    <?php if (array_key_exists($each_group->menu_id, $group)): ?>
        <?php foreach ($group[$each_group->menu_id] as $each_record): ?>
            <?php if ($each_record->type == "external_link"): ?>
                <?php $ext_link = get_post_by_id($each_record->ref_id); ?>
                <?php $ext_link = json_decode($ext_link->post_detail); ?>
                <?php foreach ($ext_link as $each_ext_link): ?>
                    <li>
                        <a href="<?php echo $each_ext_link->url ?>">
                            <?php echo $each_ext_link->title; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php elseif ($each_record->type == "other"): ?>
                <?php $school = get_all_post_by_type(TYPE_SCHOOL); ?>
                <?php foreach ($school as $each_school): ?>
                    <li>
                        <a href="<?php echo site_url("mfu/school/{$each_school->post_id}/" . slug($each_school->post_title)); ?>">
                            <?php echo $each_school->post_title ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <?php switch ($each_record->type): case 'category': ?>
                        <?php $url = site_url("mfu/category/{$each_record->ref_id}/" . slug($each_record->title)); ?>
                        <?php break; ?>
                    <?php case 'page': ?>
                        <?php $url = site_url("mfu/page/{$each_record->ref_id}/" . slug($each_record->title)); ?>
                        <?php break; ?>
                <?php endswitch; ?>

                <li>
                    <a href="<?php echo $url ?>">
                        <?php echo $each_record->title; ?>
                    </a>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
<?php } ?>

<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="red th_san"><?php echo $post->post_title; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-9">  
            <?php if (count($all_link)): ?>
                <?php $group = group_audience_content($all_link); ?>
                <?php foreach ($group['-1'] as $each_group): ?>
                    <?php switch ($each_group->type): case 'group_label': ?>
                            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $each_group->title; ?></span></h4>
                            <ul>
                                <?php gen_tree_aud($each_group, $group); ?>
                            </ul>
                            <?php break; ?>
                        <?php case 'category': ?>
                            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $each_group->title; ?></span></h4>
                            <ul>
                                <?php $post = get_all_post_in_cate($each_group->ref_id, 10); ?>
                                <?php foreach ($post as $each_post): ?>
                                    <?php $p = widget_cate($each_post); ?>
                                    <li>
                                        <a href="<?php echo $p['url']; ?>">
                                            <?php echo $p['alt_title']; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>

                                <?php gen_tree_aud($each_group, $group); ?>
                            </ul>
                            <?php break; ?>
                        <?php case 'external_link': ?>
                            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $each_group->title; ?></span></h4>
                            <?php $ext_link = get_post_by_id($each_group->ref_id); ?>
                            <?php $ext_link = json_decode($ext_link->post_detail); ?>

                            <ul>
                                <?php foreach ($ext_link as $each_ext_link): ?>
                                    <li>
                                        <a href="<?php echo $each_ext_link->url ?>">
                                            <?php echo $each_ext_link->title; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>

                                <?php gen_tree_aud($each_group, $group) ?>
                            </ul>
                            <?php break; ?>
                        <?php case 'redirect': ?>
                            <?php redirect($each_group->title); ?>
                            <?php break; ?>
                        <?php case 'other': ?>
                            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("สำนักวิชา"); ?></span></h4>
                            <?php $school = get_all_post_by_type(TYPE_SCHOOL); ?>

                            <ul>
                                <?php foreach ($school as $each_school): ?>
                                    <li>
                                        <a href="<?php echo site_url("mfu/school/{$each_school->post_id}/" . slug($each_school->post_title)); ?>">
                                            <?php echo $each_school->post_title ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>

                                <?php gen_tree_aud($each_group, $group) ?>
                            </ul>
                            <?php break; ?>
                    <?php endswitch; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <h4 class="txt_center th_san">&dash; <?php echo lang("ไม่มีข้อมูลในส่วนนี้"); ?> &dash;</h4>
            <?php endif; ?>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
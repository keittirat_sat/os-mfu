<!--Fix link-->

<!--Presidential Hotline-->
<div class="fix_link">
    <a href="#" data-toggle="modal" data-target="#presidential_hotline_box">
        <?php echo image_asset("sidebar_photline_" . LANG . ".png"); ?>
    </a>
</div><!--/Presidential Hotline-->


<div class="fix_link">
    <a href="https://www.google.com/a/mfu.ac.th/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/a/mfu.ac.th/&ss=1&ltmpl=default&ltmplcache=2" target="_blank">
        <?php echo image_asset("sidebar_mfumail.png"); ?>
    </a>
</div>

<div class="fix_link">
    <a href="<?php echo site_url('mfu/clipping'); ?>">
        <?php echo image_asset("sidebar_news_" . LANG . ".png"); ?>
    </a>
</div>

<div class="fix_link">
    <a href="<?php echo site_url('mfu/newsletter'); ?>">
        <?php echo image_asset("sidebar_newsletter_" . LANG . ".png"); ?>
    </a>
</div>

<div class="fix_link">
    <a href="http://www.mfu.ac.th/center/lib/">
        <?php echo image_asset("sidebar_onlinelib_" . LANG . ".png"); ?>
    </a>
</div>

<!--Gallery-->
<div class="fix_link">
    <a href="<?php echo site_url("mfu/gallery"); ?>">
        <?php echo image_asset("sidebar_gallery_" . LANG . ".png"); ?>
    </a>
</div><!--/Gallery-->

<div class="fix_link">
    <a href="<?php echo site_url("mfu/post/452/contactus") ?>">
        <?php echo image_asset("sidebar_contactinfo_" . LANG . ".png"); ?>
    </a>
</div>

<div class="fix_link clearfix">
    <a href="<?php echo FB_HOME; ?>">
        <?php echo image_asset("sidebar_social_fb.png", '', array('style' => "width: 60px;")); ?>
    </a>
    <a href="<?php echo YOUTUBE_LINK; ?>">
        <?php echo image_asset("sidebar_social_yt.png", '', array('style' => "width: 60px;")); ?>
    </a>
    <?php echo image_asset("sidebar_social_text.png", '', array('style' => "width: 75px;")); ?>
</div>

<!--/Fix Link-->
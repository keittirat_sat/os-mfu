<div class="img_top_holder pr_info">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('news_header_text.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>
<div class="container margin_top_20">

    <?php $category = get_all_category(CATE_CLIPPING); ?>
    <?php $cate_rearrange = array(); ?>
    <?php foreach ($category as $each_cate): ?>
        <?php $cate_rearrange[$each_cate->cate_id] = $each_cate ?>
    <?php endforeach; ?>

    <div class="row">
        <div class="col-xs-9">
            <div class="row">
                <div class="col-xs-8">
                    <h1 class="th_san red margin_top_0 margin_bottom_0">MFU Clipping</h1>
                </div>
                <div class="col-xs-4">
                    <form>
                        <div class="form-group has-feedback margin_bottom_0">
                            <input type="text" class="form-control" id="inputSuccess2" placeholder="MFU Clipping" name="q" value="<?php echo $q ?>">
                            <span class="glyphicon glyphicon-search form-control-feedback" style="top: 0px;"></span>
                        </div>

                        <input type="hidden" name="year" id="year_hidden" value="<?php echo $year ?>">
                        <input type="hidden" name="month" id="month_hidden" value="<?php echo $month ?>">
                        <input type="submit" style="display: none;" id="submit_btn_hidden">
                    </form>
                </div>
            </div>

            <div class="row margin_top_0 margin_bottom_20">
                <div class="col-xs-12 txt_right">
                    <label style="color: #ff0000; font-weight: normal;">Sort by :</label>
                    <div class="btn-group btn-group-xs">

                        <!--Month-->
                        <div class="btn-group btn-group-xs month_dropdown">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <font><?php echo $month == 0 ? lang("ทุกเดือน") : $locale_month[LANG][$month]; ?></font>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li <?php echo $month == 0 ? "class='active'" : "" ?>><a href="#" data-value="0"><?php echo lang("ทุกเดือน"); ?></a></li>
                                <?php foreach ($all_month as $each_month): ?>
                                    <?php $current_month = date("n", $each_month->post_month); ?>
                                    <li <?php echo $month == $current_month ? "class='active'" : "" ?>><a href="#" data-value="<?php echo date("n", $each_month->post_month) ?>"><?php echo $locale_month[LANG][date("n", $each_month->post_month)]; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div><!--Month-->

                        <!--Year-->
                        <div class="btn-group btn-group-xs  year_dropdown">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <font><?php echo $year == 0 ? lang("ทุกปี") : $year + (LANG == "th" ? 543 : 0) ?></font>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li <?php echo $year == 0 ? "class='active'" : "" ?>><a href="#" data-value="0"><?php echo lang("ทุกปี"); ?></a></li>
                                <?php foreach ($all_year as $each_year): ?>
                                    <?php $current_year = date("Y", $each_year->post_year); ?>
                                    <li <?php echo $year == $current_year ? "class='active'" : "" ?>><a href="#" data-value="<?php echo date("Y", $each_year->post_year) ?>"><?php echo date("Y", $each_year->post_year) + (LANG == "th" ? 543 : 0) ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div><!--Year-->

                        <button class="btn btn-danger" id="filter_btn">Submit</button>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <table class="table-condensed table">
                        <thead>
                            <tr>
                                <th width="514"><?php echo lang("หัวข้อ"); ?></th>
                                <th><?php echo lang("แหล่งข่าว"); ?></th>
                                <th><?php echo lang("ปี"); ?></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th><?php echo lang("หัวข้อ"); ?></th>
                                <th><?php echo lang("แหล่งข่าว"); ?></th>
                                <th><?php echo lang("ปี"); ?></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($post as $each_post): ?>
                                <?php $media = get_media_by_post_id($each_post->post_id) ?>
                                <?php $mid = $media[0]->mid; ?>
                                <?php $media_name = $media[0]->media_name; ?>
                                <?php $url = site_url("mfu/mfu_reader/{$mid}/" . slug($media_name)); ?>
                                <tr>
                                    <td><a href="<?php echo $url; ?>"><?php echo $each_post->post_title ?></a></td>
                                    <td><a href="?cate_id=<?php echo $each_post->cate_id ?>"><?php echo $cate_rearrange[$each_post->cate_id]->cate_name ?></a></td>
                                    <td><a href="?month=<?php echo date('n', $each_post->post_date) ?>&year=<?php echo date('Y', $each_post->post_date) ?>"><?php echo get_shortdate_format($each_post->post_date); ?></a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 txt_center">
                    <ul class="pagination pagination-centered pagination-sm">
                        <?php for ($i = 1; $i <= $total_page; $i++): ?>
                            <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        var month = $('#month_hidden').val();
        var year = $('#year_hidden').val();

        $('.month_dropdown a').click(function() {
            $('.month_dropdown li.active').removeClass('active');
            $(this).parents('li').addClass('active');
            $('.month_dropdown font').text($(this).text());
            month = $(this).attr('data-value');
            $('#month_hidden').val(month);
//            $('#filter_btn').attr('href', '?month=' + month + "&year=" + year);
            return false;
        });

        $('.year_dropdown a').click(function() {
            $('.year_dropdown li.active').removeClass('active');
            $(this).parents('li').addClass('active');
            $('.year_dropdown font').text($(this).text());
            year = $(this).attr('data-value');
            $('#year_hidden').val(year);
//            $('#filter_btn').attr('href', '?month=' + month + "&year=" + year);
            return false;
        });

        $('#filter_btn').click(function() {
            $('#submit_btn_hidden').trigger('click');
        });
    });
</script>
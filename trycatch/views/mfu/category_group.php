<div class="img_top_holder other_news">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('news_header_text.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>
<div class="container margin_top_20">
    <div class="row">        
        <div class="col-xs-3">
            <ul class="nav nav-pills nav-stacked">
                <?php foreach ($category as $each_cate): ?>
                    <li <?php echo $cate_id == $each_cate->cate_id ? "class='active'" : ""; ?>><a href="<?php echo "?cate_id={$each_cate->cate_id}" ?>"><?php echo $each_cate->title ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>        
        <div class="col-xs-6">
            <?php if ($total_page > 1): ?>
                <div class="row">
                    <div class="col-xs-12 txt_right">
                        <ul class="pagination pagination-sm">
                            <?php for ($i = 0; $i < $total_page; $i++): ?>
                                <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo "?page={$i}"; ?>"><?php echo $i + 1; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>

            <?php foreach ($all_post as $post): ?>
                <div class="row" style='margin-bottom: 15px;'>
                    <?php $cate_post = widget_cate($post); ?>
                    <div class="widget_cate_thumbnail">
                        <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                    </div>
                    <div class="widget_cate_info">
                        <h3 class="th_san margin_top_0 red_1"><a title="<?php echo $cate_post['alt_title'] ?>" href='<?php echo $cate_post['url']; ?>' class="link_inherit"><?php echo $cate_post['post_title']; ?></a> &dash; <small><i><?php echo date('F d Y', $cate_post['date']); ?></i></small></h3>
                        <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                    </div>
                </div>
            <?php endforeach; ?>

            <?php if ($total_page > 1): ?>
                <div class="row">
                    <div class="col-xs-12 txt_right">
                        <ul class="pagination pagination-sm">
                            <?php for ($i = 0; $i < $total_page; $i++): ?>
                                <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo "?page={$i}"; ?>"><?php echo $i + 1; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-xs-3 sidebar">            
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
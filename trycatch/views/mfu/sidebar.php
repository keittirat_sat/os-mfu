<!--Widget Blocking-->
<?php foreach ($widget->sidebar as $section): ?>
    <div class="each_widget">
        <?php $block = widget_process($section); ?>
        <?php switch ($block['type']): case "audience_guideline" : ?>
                <?php echo image_asset("sidebar_welcome_" . LANG . ".png"); ?>
                <ul class='ad_gl txt_right th_san'>
                    <?php foreach ($block['post'] as $post): ?>
                        <?php $each_link = widget_audience_guideline($post); ?>
                        <li><a href='<?php echo $each_link['url']; ?>'><?php echo $each_link['post_title']; ?>&nbsp;<?php echo image_asset("arrow.png") ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <?php break; ?>
            <?php case 'school': ?>
                <div class="widget_header_type_1 txt_right th_san">
                    <p>Schools & Departments</p>
                    <p>สำนักวิชาและหน่วยงาน</p>
                </div>
                <p class="label_widget_type_1 th_san"><?php echo lang("สำนักวิชา"); ?></p>
                <ul class="school_list">
                    <?php foreach ($block['post'] as $post): ?>
                        <li><a href="<?php echo site_url("mfu/school/{$post->post_id}/" . slug($post->post_title)); ?>"><?php echo $post->post_title; ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <?php break; ?>
            <?php case 'ext_link': ?>
                <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
                <?php $link = json_decode($block['post']) ?>
                <ul class="ext_link_list">
                    <?php foreach ($link as $post): ?>
                        <li><a target="_blank" href="<?php echo $post->url; ?>"><?php echo $post->title; ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <?php break; ?>
            <?php default : ?>
                <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $block['title']; ?></span></h4>
                <pre><?php print_r($block); ?></pre>
                <?php break; ?>
        <?php endswitch; ?>
    </div>
<?php endforeach; ?>
<!--/Widget Blocking-->

<?php echo $fix_link; ?>
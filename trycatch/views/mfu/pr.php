<div class="img_top_holder pr_info">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('news_header_text_' . LANG . '.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>

<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-9">
            <div class="row">
                <div class="col-xs-4">
                    <?php $event_obj = new stdClass(); ?>
                    <?php $event_obj->data_type = "event"; ?>
                    <?php $event_obj->data_id = 48; ?>
                    <?php $all_event = widget_process($event_obj); ?>
                    <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $all_event['title'] ?></span></h4>
                    <?php if (count($all_event['post'])): ?>
                        <?php foreach ($all_event['post'] as $event_raws): ?>
                            <?php $obj_event = widget_event($event_raws); ?>
                            <div class="row">
                                <div class="event_date txt_center th_san">
                                    <span style="margin: 0px; font-size: 36px; line-height: 20px;" class="txt_center"><?php echo date('d', $obj_event['date']) ?></span>
                                    <!--<span class="red" style="font-size: 16px; line-height: 14px; margin-top: -5px; display: block;"><?php echo date('M y', $obj_event['date']) ?></span>-->
                                    <span class="red" style="font-size: 16px; line-height: 14px; margin-top: -5px; display: block;"><?php echo $locale_short_month[LANG][date('n', $obj_event['date'])] . " " . date('y', $obj_event['date']) ?></span>
                                </div>
                                <div class="event_info">
                                    <p class="txt_justify" style="font-size: 11px; margin-bottom: 5px;"><?php echo $obj_event['post_title']; ?></p>
                                    <p class="txt_right" style=" margin-bottom: 15px;"><a class="btn btn-xs btn-default" href="<?php echo $obj_event['url']; ?>"><?php echo lang("อ่านต่อ"); ?> &raquo;</a></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <p class="txt_right">
                            <a class="btn btn-default" href="<?php echo $all_event['more_btn']; ?>"><i class="glyphicon glyphicon-plus"></i> <?php echo lang("ดูทั้งหมด"); ?></a>
                        </p>
                    <?php else: ?>
                        <h4 class="red th_san txt_center">&dash;<?php echo lang("ไม่มีข่าวในหมวดนี้"); ?>&dash;</h4>
                    <?php endif; ?>
                </div>
                <div class="col-xs-8">
                    <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("ข่าวสาร"); ?></span></h4>
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo image_asset('pr_1.jpg', '', array('class' => 'img-responsive')); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php $group_link = get_post_by_id(636); ?>
                            <h3 class="th_san red_1" style="margin: 0px 0px 5px;"><?php echo lang("ข่าวสารทั่วไป"); ?></h3>
                            <p style="font-size: 11px; margin-bottom: 5px;"><?php echo $group_link->post_excerp ?></p>
                            <ul class='hilight_link'>
                                <?php $link = json_decode($group_link->post_detail); ?>
                                <?php foreach ($link as $each_link): ?>
                                    <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_link->url; ?>'><?php echo $each_link->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="row margin_top_20">
                        <div class="col-xs-6">
                            <?php echo image_asset('pr_2.jpg', '', array('class' => 'img-responsive')); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php $group_link = get_post_by_id(637); ?>
                            <h3 class="th_san red_1" style="margin: 0px 0px 5px;"><?php echo lang("ข่าวประชาสัมพันธ์"); ?></h3>                            
                            <p style="font-size: 11px; margin-bottom: 5px;"><?php echo $group_link->post_excerp ?></p>
                            <ul class='hilight_link'>
                                <?php $link = json_decode($group_link->post_detail); ?>
                                <?php foreach ($link as $each_link): ?>
                                    <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_link->url; ?>'><?php echo $each_link->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="row margin_top_20">
                        <div class="col-xs-6">
                            <?php echo image_asset('pr_3.jpg', '', array('class' => 'img-responsive')); ?>
                        </div>                        
                        <div class="col-xs-6">
                            <?php $group_link = get_post_by_id(638); ?>
                            <h3 class="th_san red_1" style="margin: 0px 0px 5px;"><?php echo lang("กิจกรรมนักศึกษา"); ?></h3>                            
                            <p style="font-size: 11px; margin-bottom: 5px;"><?php echo $group_link->post_excerp ?></p>
                            <ul class='hilight_link'>
                                <?php $link = json_decode($group_link->post_detail); ?>
                                <?php foreach ($link as $each_link): ?>
                                    <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_link->url; ?>'><?php echo $each_link->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="row margin_top_20">
                        <div class="col-xs-6">
                            <?php echo image_asset('pr_4.jpg', '', array('class' => 'img-responsive')); ?>
                        </div>                        
                        <div class="col-xs-6">
                            <?php $group_link = get_post_by_id(639); ?>
                            <h3 class="th_san red_1" style="margin: 0px 0px 5px;"><?php echo lang("รางวัล"); ?></h3>                            
                            <p style="font-size: 11px; margin-bottom: 5px;"><?php echo $group_link->post_excerp ?></p>
                            <ul class='hilight_link'>
                                <?php $link = json_decode($group_link->post_detail); ?>
                                <?php foreach ($link as $each_link): ?>
                                    <li><font class='red'>&raquo;&nbsp;</font><a href='<?php echo $each_link->url; ?>'><?php echo $each_link->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo lang("ข่าวสารล่าสุด"); ?></span></h4>
            <div class="row">
                <div class="col-xs-12">
                    <?php $post = get_latest_news(10); ?>
                    <?php if (count($post)): ?>
                        <?php foreach ($post as $each_post): ?>
                            <?php $img = $each_post->link ? json_decode($each_post->link) : ""; ?>                    
                            <?php $dim = "portrait"; ?>
                            <?php if ($img != ""): ?>
                                <?php $path = "./uploads/{$each_post->mid_post_id}/{$each_post->media_name}"; ?>
                                <?php $img_info = getimagesize($path); ?>
                                <?php if ($img_info[0] > $img_info[1]): ?>
                                    <?php $dim = 'langscape'; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                            <div class="row" style='margin-bottom: 15px;'>
                                <div class="widget_cate_thumbnail">
                                    <a title="<?php echo $each_post->post_title ?>" href="<?php echo site_url("mfu/post/{$each_post->post_id}/" . slug($each_post->post_title)); ?>" style="background-image: url('<?php echo $img ? $img->s : image_asset_url("Blank_thumbnail.jpg"); ?>');" class="thumbnail_mfu <?php echo $dim; ?>"></a>
                                </div>
                                <div class="widget_cate_info" style="width: 596px;">
                                    <h4 class="th_san"><a title="<?php echo $each_post->post_title; ?>" href='<?php echo site_url("mfu/post/{$each_post->post_id}/" . slug($each_post->post_title)); ?>'><?php echo $each_post->post_title; ?></a> &dash; <small><i><?php echo date('F d Y', $each_post->post_date); ?></i></small></h4>
                                    <p class='txt_justify'><?php echo get_excerp($each_post, 240); ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <p class="txt_right"><a class="btn btn-default" href="<?php echo site_url('mfu/news_mashup'); ?>"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo lang("อ่านข่าวทั้งหมด"); ?></a></p>
                    <?php else: ?>
                        <h4 class="txt_center margin_top_10 red th_san">&dash;<?php echo lang("ไม่มีข่าวในหมวดนี้"); ?>&dash;</h4>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-xs-3 sidebar">
            <?php $all_event = get_event_by_status(); ?>
            <div class="widget_header_type_1 txt_right th_san" style="margin-bottom: 20px;">
                <p>by Category</p>
                <p>แยกตามหมวดหมู่</p>
            </div>

            <h3 class="red_1 th_san margin_top_0 margin_bottom_0"><?php echo lang("ข่าวสารทั่วไป"); ?></h3>
            <ul class='hilight_link'>
                <?php $group_link = get_post_by_id(636); ?>
                <?php $link = json_decode($group_link->post_detail); ?>
                <?php foreach ($link as $each_link): ?>
                    <li><a href='<?php echo $each_link->url; ?>'><?php echo $each_link->title; ?></a></li>
                <?php endforeach; ?>
            </ul>
            <h3 class="red_1 th_san margin_top_0 margin_bottom_0"><?php echo lang("ข่าวประชาสัมพันธ์"); ?></h3>
            <ul class='hilight_link'>
                <?php $group_link = get_post_by_id(637); ?>
                <?php $link = json_decode($group_link->post_detail); ?>
                <?php foreach ($link as $each_link): ?>
                    <li><a href='<?php echo $each_link->url; ?>'><?php echo $each_link->title; ?></a></li>
                <?php endforeach; ?>
            </ul>
            <h3 class="red_1 th_san margin_top_0 margin_bottom_0"><?php echo lang("กิจกรรมนักศึกษา"); ?></h3>
            <ul class='hilight_link'>
                <?php $group_link = get_post_by_id(638); ?>
                <?php $link = json_decode($group_link->post_detail); ?>
                <?php foreach ($link as $each_link): ?>
                    <li><a href='<?php echo $each_link->url; ?>'><?php echo $each_link->title; ?></a></li>
                <?php endforeach; ?>
            </ul>
            <h3 class="red_1 th_san margin_top_0 margin_bottom_0"><?php echo lang("รางวัล"); ?></h3>
            <ul class='hilight_link'>
                <?php $group_link = get_post_by_id(639); ?>
                <?php $link = json_decode($group_link->post_detail); ?>
                <?php foreach ($link as $each_link): ?>
                    <li><a href='<?php echo $each_link->url; ?>'><?php echo $each_link->title; ?></a></li>
                <?php endforeach; ?>
            </ul>

            <!--Fix link-->
            <?php echo $fix_link; ?>
            <!--/Fix Link-->
        </div>
    </div>
</div>
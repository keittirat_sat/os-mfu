<div class="img_top_holder pr_info">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('news_header_text.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>
<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-9">            
            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span>ข่าวสารทั้งหมด</span></h4>
            <?php if (count($post)): ?>           
                <?php if ($total_page > 1): ?>
                    <div class="row">
                        <div class="col-xs-12 txt_right">
                            <ul class="pagination pagination-sm">
                                <?php for ($i = 0; $i < $total_page; $i++): ?>
                                    <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo "?page={$i}"; ?>"><?php echo $i + 1; ?></a></li>
                                <?php endfor; ?>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
                <?php foreach ($post as $each_post): ?>
                    <div class="row" style='margin-bottom: 15px;'>
                        <?php $cate_post = widget_cate($each_post); ?>
                        <div class="widget_cate_thumbnail">
                            <a title="<?php echo $cate_post['alt_title'] ?>" href="<?php echo $cate_post['url'] ?>" style="background-image: url('<?php echo $cate_post['thumbnail']; ?>');" class="thumbnail_mfu <?php echo $cate_post['dim']; ?>"></a>
                        </div>
                        <div class="widget_cate_info cate_page">
                            <h3 class="th_san margin_top_0 red_1"><a title="<?php echo $cate_post['alt_title'] ?>" href='<?php echo $cate_post['url']; ?>' class="link_inherit"><?php echo $cate_post['post_title']; ?></a> &dash; <small><i><?php echo date('F d Y', $cate_post['date']); ?></i></small></h3>
                            <p class='txt_justify'><?php echo $cate_post['post_excerp']; ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>            
                <?php if ($total_page > 1): ?>
                    <div class="row">
                        <div class="col-xs-12 txt_right">
                            <ul class="pagination pagination-sm">
                                <?php for ($i = 0; $i < $total_page; $i++): ?>
                                    <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo "?page={$i}"; ?>"><?php echo $i + 1; ?></a></li>
                                <?php endfor; ?>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
            <?php else: ?>
                <h4 class="red th_san txt_center">&dash;ไม่มีรายการในหมวดนี้&dash;</h4>
            <?php endif; ?>
        </div>
        <div class="col-xs-3 sidebar">
            <?php $all_event = get_event_by_status(); ?>
            <div class="widget_header_type_1 txt_right th_san" style="margin-bottom: 20px;">
                <p>by Category</p>
                <p>แยกตามหมวดหมู่</p>
            </div>

            <h3 class="red_1 th_san margin_top_0 margin_bottom_0">ข่าวสาร</h3>
            <ul class='hilight_link'>
                <?php $cate = get_category_by_cate_id(21); ?>
                <li><a href='<?php echo site_url("mfu/category/{$cate->cate_id}/" . slug($cate->cate_name)); ?>'><?php echo $cate->cate_name; ?></a></li>
            </ul>
            <h3 class="red_1 th_san margin_top_0 margin_bottom_0">ข่าวประชาสัมพันธ์</h3>
            <ul class='hilight_link'>
                <?php $cate = get_category_by_cate_id(15); ?>
                <li><a href='<?php echo site_url("mfu/category/{$cate->cate_id}/" . slug($cate->cate_name)); ?>'><?php echo $cate->cate_name; ?></a></li>
                <?php $cate = get_category_by_cate_id(12); ?>
                <li><a href='<?php echo site_url("mfu/category/{$cate->cate_id}/" . slug($cate->cate_name)); ?>'><?php echo $cate->cate_name; ?></a></li>
                <?php $cate = get_category_by_cate_id(14); ?>
                <li><a href='<?php echo site_url("mfu/category/{$cate->cate_id}/" . slug($cate->cate_name)); ?>'><?php echo $cate->cate_name; ?></a></li>
            </ul>
            <h3 class="red_1 th_san margin_top_0 margin_bottom_0">กิจกรรมนักศึกษา</h3>
            <ul class='hilight_link'>
                <?php $cate = get_category_by_cate_id(22); ?>
                <li><a href='<?php echo site_url("mfu/category/{$cate->cate_id}/" . slug($cate->cate_name)); ?>'><?php echo $cate->cate_name; ?></a></li>
                <?php $cate = get_category_by_cate_id(24); ?>
                <li><a href='<?php echo site_url("mfu/category/{$cate->cate_id}/" . slug($cate->cate_name)); ?>'><?php echo $cate->cate_name; ?></a></li>
            </ul>
            <h3 class="red_1 th_san margin_top_0 margin_bottom_0">รางวัล</h3>
            <ul class='hilight_link'>
                <?php $cate = get_category_by_cate_id(23); ?>
                <li><a href='<?php echo site_url("mfu/category/{$cate->cate_id}/" . slug($cate->cate_name)); ?>'><?php echo $cate->cate_name; ?></a></li>
            </ul>

            <!--Fix link-->
            <?php echo $fix_link; ?>
            <!--/Fix Link-->
        </div>
    </div>
</div>
<div class="img_top_holder visitor_info">
    <div class="container">
        <div class="row">
            <div class="col-lg-12"><?php echo image_asset('visitorinfo_header_text.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>

<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-9">  
            <h4 class="spc_label" style="border-bottom: 1px solid #fd0200;"><span><?php echo $post->post_title; ?></span></h4>

            <!--Nav-->
            <ol class="breadcrumb mfu">
                <li><a href="<?php echo site_url(); ?>"><?php echo lang("หน้าแรก"); ?></a></li>
                <li><a href="<?php echo site_url('mfu/gallery'); ?>"><?php echo lang("แกลลอรี่"); ?></a></li>
                <li class="active"><?php echo $post->post_title; ?></li>
            </ol><!--Nav-->

            <!--Album Information-->
            <div class="row margin_bottom_20">
                <div class="col-xs-12">
                    <p><?php echo $post->post_excerp ?></p>
                </div>
            </div><!--Album Information-->

            <div class='row'>
                <?php foreach ($all_image as $image): ?>
                    <?php $media_info = json_decode($image->media_info); ?>
                    <?php $media_link = json_decode($image->link); ?>
                    <?php $dim = 'landscape'; ?>
                    <?php if ($media_info->image_width > $media_info->image_height): ?>
                        <?php $dim = 'portrait'; ?>
                    <?php endif; ?>
                    <div class="col-xs-4 margin_bottom_20">
                        <a href="<?php echo $media_link->l; ?>" class="cubic-md-4 <?php echo $dim; ?>" style="background-image: url('<?php echo $media_link->m ?>');" rel='album' title='<?php echo $image->media_name; ?>'>
                            <?php echo image_asset('Gallery_icon.png', '', array('class' => 'magnify')); ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <p class="txt_right"><span class="badge badge-important"><i class="glyphicon glyphicon-user"></i> <?php echo lang("จำนวนผู้เข้าชม") ?>: <font id="id_holder_stat">0</font></span></p>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar ?>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(function() {
        $('a.cubic-md-4').mouseenter(function() {
            $(this).find('.magnify').stop().animate({opacity: 1}, 200);
        }).mouseleave(function() {
            $(this).find('.magnify').stop().animate({opacity: 0}, 200);
        });

        $('a.cubic-md-4').colorbox();
    });
</script>
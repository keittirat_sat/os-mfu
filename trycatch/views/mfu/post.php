<div class="container margin_top_20">
    <!--    <div class="row">
            <div class="col-xs-12">
                <h1 class="red th_san margin_top_0 margin_bottom_20"><?php echo $post->post_title; ?></h1>
            </div>
        </div>-->
    <div class="row">
        <div class="col-xs-9">
            <h1 class="red th_san margin_top_0 margin_bottom_20"><?php echo $post->post_title; ?></h1>
            <ol class="breadcrumb mfu">
                <li><a href="<?php echo site_url(); ?>"><?php echo lang("หน้าแรก"); ?></a></li>
                <?php if ($post->cate_id != 0): ?>
                    <?php switch ($cate_info->cate_type): case CATE_POST: ?>
                            <?php $url = site_url("mfu/category/{$post->cate_id}/" . slug($cate_info->cate_name)) ?>
                            <?php break; ?>
                        <?php case CATE_EVENT: ?> 
                            <?php $url = site_url("mfu/all_event?cate_id={$post->cate_id}") ?>
                            <?php break; ?>
                        <?php case CATE_ALBUM: ?>
                            <?php $url = site_url("mfu/album/{$post->cate_id}/" . slug($cate_info->cate_name)) ?>
                            <?php break; ?>
                        <?php case CATE_CLIPPING: ?>
                            <?php $url = site_url("mfu/clipping?cate_id={$post->cate_id}") ?>
                            <?php break; ?>
                    <?php endswitch; ?>
                    <li><a href="<?php echo $url; ?>"><?php echo $cate_info->cate_name; ?></a></li>
                <?php else: ?>
                    <li><a href="<?php echo site_url('mfu/category/0/uncategory'); ?>">Uncategory</a></li>
                <?php endif; ?>
                <li class="active"><?php //echo ellipsize($post->post_title, 60, 0.5);   ?><?php echo $post->post_title; ?></li>
            </ol>
            <?php $today = date('U'); ?>
            <?php if ($post->sch_exp == 0 || $post->sch_exp > $today): ?>            
                <?php echo auto_typography($post->post_detail); ?>
            <?php else: ?>
                <h3 class="th_san red txt_center"><?php echo lang("ขออภัยประกาศนี้หมดอายุแล้ว"); ?></h3>
            <?php endif; ?>
            <p class="txt_right"><span class="badge badge-important"><i class="glyphicon glyphicon-user"></i> <?php echo lang("จำนวนผู้เข้าชม") ?>: <font id="id_holder_stat"></font></span></p>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
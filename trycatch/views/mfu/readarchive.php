<div class="container margin_top_20">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="red th_san margin_top_0 margin_bottom_20"><?php echo $post->title; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-9">
            <ol class="breadcrumb mfu">
                <li><a href="<?php echo site_url(); ?>"><?php echo lang("หน้าแรก"); ?></a></li>
                <?php if ($ref_id == 0): ?>
                    <li><a href="<?php echo site_url('mfu/category/0/uncategory'); ?>">Uncategory</a></li>
                <?php else: ?>
                    <?php $api_info = get_post_by_id($ref_id) ?>
                    <li><a href="<?php echo site_url("mfu/archive/{$ref_id}/" . slug($api_info->post_title)); ?>"><?php echo $api_info->post_title; ?></a></li>
                <?php endif; ?>
                <li class="active"><?php echo ellipsize($post->title, 60, 0.5); ?></li>
            </ol>      
            <?php echo auto_typography(html_entity_decode($post->detail)); ?>
            <p class="txt_right">
                <a href="<?php echo $post->link ?>" class="btn btn-danger btn-sm" target="_blank"><i class="glyphicon glyphicon-globe"></i>&nbsp;<?php echo lang('ดูข่าวจากระบบเดิม'); ?></a>
            </p>
            <p class="txt_right"><span class="badge badge-important"><i class="glyphicon glyphicon-user"></i> <?php echo lang("จำนวนผู้เข้าชม") ?>: <font id="id_holder_stat"></font></span></p>
        </div>
        <div class="col-xs-3 sidebar">
            <?php echo $sidebar; ?>
        </div>
    </div>
</div>
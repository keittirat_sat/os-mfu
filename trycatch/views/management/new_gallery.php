<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" method="post" action="<?php echo site_url('api/update_post'); ?>" id="form_update">
                        <p class="margin_bottom_20">
                            <label>Album title</label>
                            <input type="text" name="post_title" class="form-control" placeholder="Album title" required="" value="<?php echo $reserv_post->post_title; ?>">
                        </p>
                        <p>
                            <label>Album detail</label>
                            <textarea class="form-control" placeholder="Album detail" name="post_excerp" style="resize: vertical;" rows="6"><?php echo $reserv_post->post_excerp; ?></textarea>
                        </p>
                        <input type="hidden" value="<?php echo $reserv_post->post_status; ?>" name="post_status" id="post_status">
                        <input type="hidden" value="<?php echo $reserv_post->post_id; ?>" name="post_id" id="post_status">
                        <input type="hidden" value="<?php echo $reserv_post->cate_id; ?>" name="cate_id" id="cate_id_in_form">
                        <input type="submit" id="submit_btn" style="display: none;">
                    </form>
                </div>
            </div>
            <div class="row upload_holder">
                <div class="col-md-12">
                    <ul class="upload_progrerss_bar" id="upload_queue"></ul>
                </div>
            </div>
            <div class="row media-tank" style="height: auto;">
                <?php foreach ($all_image as $each_rows): ?>
                    <div class="row">
                        <?php foreach ($each_rows as $obj_media): ?>
                            <?php $permalink = json_decode($obj_media->link); ?>
                            <div class="col-md-3" id="block_<?php echo $obj_media->mid; ?>">
                                <div data-mime="<?php echo $obj_media->header_media; ?>" data-link="<?php echo $permalink->l; ?>" data-thumbnail='<?php echo $permalink->s; ?>' data-img-m='<?php echo $permalink->m; ?>' class="thumbnail" title="<?php echo $obj_media->media_name; ?>" data-type="<?php echo $obj_media->media_type ?>" data-mid="<?php echo $obj_media->mid; ?>" style="position: relative;">
                                    <button class="btn btn-danger btn-xs del_btn" data-mid="<?php echo $obj_media->mid; ?>" style="position: absolute;"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>
                                    <img data-src="holder.js/100%x180" src="<?php echo $permalink->m; ?>">
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-danger">
                <div class="panel-heading"><span class="panel-title"><i class="glyphicon glyphicon-tasks"></i>&nbsp;Update album</span></div>
                <div class="panel-body txt_center">
                    <div class="form-group">
                        <select class="form-control" id="fake_post_status">
                            <option value="<?php echo POST_DRAFT ?>" <?php echo POST_DRAFT == $reserv_post->post_status ? "selected" : ""; ?>>Draft</option>
                            <option value="<?php echo POST_PUBLISH ?>" <?php echo POST_PUBLISH == $reserv_post->post_status ? "selected" : ""; ?>>Publish</option>
                            <option value="<?php echo POST_TRASH ?>" <?php echo POST_TRASH == $reserv_post->post_status ? "selected" : ""; ?>>Trash</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="fake_cate_id" <?php echo count($category) ? "" : "disabled" ?>>
                            <?php if (count($category)): ?>
                                <option value="0" <?php echo $reserv_post->cate_id == 0 ? "selected" : ""; ?>>Uncategory</option>
                                <?php foreach ($category as $cate): ?>
                                    <option value="<?php echo $cate->cate_id ?>" <?php echo $reserv_post->cate_id == $cate->cate_id ? "selected" : ""; ?>><?php echo $cate->cate_name ?></option>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <option value="0">Please add category...</option>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger btn-sm" id="fake_submit_btn" data-loading-text="Saving&hellip;">
                            <i class="glyphicon glyphicon-save"></i> Save
                        </button>
                    </div>
                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading"><span class="panel-title"><i class="glyphicon glyphicon-picture"></i>&nbsp;Add image</span></div>
                <div class="panel-body txt_center">
                    <button class="btn btn-success btn-sm upload_fake_btn" style="position: relative; overflow: hidden;">
                        <i class="glyphicon glyphicon-upload"></i> Upload
                        <input type="file" name="upload_file[]" id="file_btn" multiple="multiple">
                    </button>
                </div>
            </div>

            <?php if (get_user_level() != USER_WRITER): ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-align-justify"></i> Category</div>
                    <div class="panel-body">
                        <form role="form" method="post" action="<?php echo site_url('operation/store_category'); ?>" id="cate_add_form">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="cate_name" required="required" placeholder="Category for News Clipping">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" id="cate_btn_add" type="submit">Add</button>
                                    </span>
                                </div><!-- /input-group -->
                                <input type="hidden" value="" name="cate_info">
                                <input type="hidden" value="<?php echo CATE_ALBUM; ?>" name="cate_type">
                            </div>
                        </form>
                        <?php if (count($category)): ?>
                            <ul class="nav nav-pills nav-stacked">
                                <?php foreach ($category as $each_cate): ?>
                                    <li><a href="#del" class="del_cate_btn" data-cate_id="<?php echo $each_cate->cate_id; ?>"><i class="glyphicon glyphicon-remove"></i>&nbsp;<font><?php echo $each_cate->cate_name; ?></font></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>

<div class="modal fade" id="status_notify" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body txt_center">
                <h4 id="status_holder" class="margin_bottom_20 margin_top_20"></h4>
            </div>
        </div>
    </div>
</div>

<style>
    #file_btn{
        position: absolute; 
        top:0; 
        left:0; 
        width:100%; 
        height: 100%; 
        opacity: 0;
    }

    .upload_holder{
        display: none;
        padding: 0px;
    }

    .upload_holder li{
        list-style: none;
    }

    ul.progress_each_file{
        padding: 0px;
    }

    a.box-gallery{
        height: 96px;
    }
</style>

<script type="text/javascript">
    $(function () {
        $('.upload_fake_btn').click(function () {
            $('#file_btn').trigger('click');
        });

        var success_upload = 0;
        var idx_generate = 0;

        $('#file_btn').change(function () {
            var myupload = this.files;
            success_upload = 0;
            $('.upload_holder').show();
            for (var index = 0; index < myupload.length; index++) {
                var each_file = myupload[index];
                var id_ref = "bar_progress_" + idx_generate;
                idx_generate++;
                var each_element = '<li id="' + id_ref + '">';
                each_element += '<ul class="progress_each_file">';
                each_element += '<li>' + each_file.name + '</li>';
                each_element += '<li>';
                each_element += '<div class="progress progress-striped active">';
                each_element += '<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">';
                each_element += '<span>0% Complete</span>';
                each_element += '</div>';
                each_element += '</div>';
                each_element += '</li>';
                each_element += '</ul>';
                each_element += '</li>';
                $('#upload_queue').append(each_element);

                var upload_form = new FormData();
                upload_form.append('img_upload', each_file);

                $.ajax({
                    url: '<?php echo site_url("api/upload_single_media?post_id={$reserv_post->post_id}"); ?>',
                    type: 'POST',
                    xhr: function () {
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) { // Check if upload property exists
                            xhr.upload.each_file = each_file;
                            xhr.upload.each_element = each_element;
                            xhr.upload.ref_id = id_ref;
                            xhr.upload.addEventListener('progress', function (e) {
                                var percent = parseInt(e.loaded / e.total * 100);
                                $("#" + this.ref_id).find(".progress-bar").width(percent + '%');
                                $('#' + this.ref_id).find('span').text(percent + '% Complete');
                                $('#' + this.ref_id).find('.progress-bar').attr('aria-valuenow', percent);
                                if (percent == 100) {
                                    $('#' + this.ref_id).find('span').text('Converting...');
                                }
                            }, false);
                        }
                        return xhr;
                    },
                    success: function (e) {
                        console.log(e);
                        var json = $.parseJSON(e);

                        if (json.status === "success") {
                            //                            console.log('remove #' + id_ref);
                            $('#' + id_ref).fadeOut(400, function () {
                                $(this).remove();
                            });
                            swal({title: "สำเร็จ", text: "การอัพโหลดภาพเสร็จสมบูรณ์", type: "success"});
                        } else {
                            //                            console.log(json.flag);
                            //                            $('#status_holder').html(json.flag);
                            //                            $('#status_notify').modal();
                            swal({title: "ล้มเหลว", text: "การอัพโหลดภาพล้มเหลว : " + json.flag, type: "error"});
                        }

                        success_upload++;

                        if (success_upload == myupload.length) {
                            $('.upload_holder').hide();

                            $.get('<?php echo site_url('api/get_raws_media_by_post_id'); ?>', {'post_id':<?php echo $reserv_post->post_id; ?>}, function (res) {
                                $('.media_in_this_post').remove();
                                //                                var tank = $('.media-tank').html();
                                //                                var new_data = res + tank;
                                $('.media-tank').html(res);
                            });
                        }
                    },
                    data: upload_form,
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

        });

        $('.del_btn').click(function () {
            var mid = $(this).attr('data-mid');
            $(this).attr('disabled', 'disabled').text('Deleting...');
            $.post('<?php echo site_url('api/delete_media'); ?>', {mid: mid}, function (res) {
                if (res.status === "success") {
                    console.log('#block_' + mid);
                    $('#block_' + mid).fadeOut(400, function () {
                        $(this).remove();
                    });
                } else {
                    $('#status_holder').html('<p>Cannot delete this image</p>');
                    $('#status_notify').modal();
                }
            }, 'json');
        });

        $('#fake_post_status').change(function () {
            var post_status = $(this).val();
            $('#post_status').val(post_status);
        });

        $('#fake_submit_btn').click(function () {
            $('#submit_btn').trigger('click');
        });

        var fake_submit_btn;
        $('#form_update').ajaxForm({
            beforeSend: function () {
                fake_submit_btn = $('#fake_submit_btn').html();
                $('#fake_submit_btn').attr('disabled', 'disabled').text($('#fake_submit_btn').attr('data-loading-text'));
            },
            complete: function (xhr) {
                console.log(xhr.responseText);
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    $('#status_holder').html('<p>Successful</p>');
                } else {
                    $('#status_holder').html('<p>Cannot update your request</p>');
                }

                $('#fake_submit_btn').removeAttr('disabled').html(fake_submit_btn);
                $('#status_notify').modal();
            }
        });

        $('#cate_add_form').ajaxForm({
            beforeSend: function () {
                $('#cate_btn_add').text('Adding...').attr('disabled', 'dsiabled');
            },
            complete: function (xhr) {
                console.log(xhr.responseText);
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.reload();
                } else {
                    alert('Category already existed');
                }
            }
        });

        $('#fake_cate_id').change(function () {
            $('#cate_id_in_form').val($(this).val());
        });

        $('.del_cate_btn').click(function () {
            if (confirm('Do you want to delete "' + $(this).find('font').text() + '"')) {
                var cate_id = $(this).attr('data-cate_id');
                var cate_for_delete = $(this).parents('li');
                $.post('<?php echo site_url('operation/del_category'); ?>', {cate_id: cate_id}, function (res) {
                    if (res.status === "success") {
                        $(cate_for_delete).fadeOut(300, function () {
                            $(this).remove();
                        });
                    } else {
                        alert('Cannot delete this category');
                    }
                }, 'json');
            }
            return false;
        });
    });
</script>

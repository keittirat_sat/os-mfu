<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-cog"></i> Add new</div>
                <div class="panel-body">
                    <form role="form" action="<?php echo site_url('api/upload_newsletter') ?>" method="post" id="upload_clip">
                        <label>Title</label>
                        <div class="form-group">
                            <input type="text" required="required" name="post_title" class="form-control" placeholder="Title">
                        </div>
                        <label>Detail</label>
                        <div class="form-group">
                            <input type="text" required="required" name="post_excerp" class="form-control" placeholder="Detail">
                        </div>
                        <label>Published date</label>
                        <div class="form-group">
                            <div id="datepicker" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy"></div>
                        </div>
                        <div class="form-group txt_right">
                            <button class="btn btn-success upload_btn" type="button">
                                <i class="glyphicon glyphicon-upload"></i>
                                <font>Upload</font>
                                <input type="file" name="img_upload" accept=".pdf" id="upload_btn">
                            </button>
                        </div>
                        <input type="hidden" value="<?php echo date('U'); ?>" id="post_date_id" name="post_date">
                        <input type="hidden" value="<?php echo date('U'); ?>" name="post_modify">
                        <input type="hidden" value="<?php echo TYPE_NEWSLETTER; ?>" name="post_type">
                        <input type="hidden" value="<?php echo POST_PUBLISH; ?>" name="post_status">
                        <input type="hidden" value="<?php echo get_user_uid(); ?>" name="post_author">
                        <input type="hidden" value="0" name="cate_id">
                        <input type="submit" id="submit_news_btn">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="margin_bottom_20">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>&nbsp;
                        <?php if ($user_id == 0): ?>
                            All User
                        <?php else: ?>
                            <?php $user_info = get_user_by_uid($user_id); ?>
                            <?php echo $user_info->real_name ?>
                        <?php endif; ?>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo "?cate_id={$cate_id}&user_id=0"; ?>">All User</a></li>
                        <?php ?>
                        <?php foreach ($all_user_in_system as $each_user): ?>
                            <li><a href="<?php echo "?cate_id={$cate_id}&user_id={$each_user->uid}"; ?>"><?php echo $each_user->real_name; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>

            <?php if (count($post)): ?>
                <?php foreach ($post as $each_post): ?>
                    <div class="panel panel-default" id="block_<?php echo $each_post->post_id; ?>">
                        <div class="panel-heading">
                            <?php echo $each_post->post_title; ?>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <!--<form method="post">-->
                                <?php $media = get_all_media($each_post->post_id); ?>
                                <?php if (count($media)): ?>
                                    <?php $mid = $media[0]->mid ?>
                                    <?php $pdf = json_decode($media[0]->media_info); ?>
                                    <?php if (count($pdf->pdf_image) > 1): ?>
                                        <?php foreach ($pdf->pdf_image as $each_img): ?>
                                            <?php if ($each_img->name === "page-0.jpg"): ?>
                                                <?php $thumb = $each_img->thumb; ?>
                                                <?php break; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <?php $first = $pdf->pdf_image; ?>
                                        <?php $thumb = $first[0]->thumb; ?>
                                    <?php endif; ?>
                                    <img src="<?php echo $thumb; ?>" class="img-responsive" style="margin: 0px auto;">
                                <?php else: ?>
                                    <h5>Not found PDF file</h5>
                                    <?php $mid = 0; ?>
                                <?php endif; ?>

                                <!--Re-upload-->
                                <p class="txt_center margin_top_10">
                                    <button class="btn btn-danger fake_file_btn" data-loading-text="Uploading&hellip;">
                                        <i class="glyphicon glyphicon-file"></i>&nbsp;<font>Upload new file</font>
                                        <input type="file" class="re_upload" accept=".pdf" name="re_img_upload" data-post_id="<?php echo $each_post->post_id ?>">
                                    </button>
                                </p>
                                <!--/Re-upload-->
                                <!--</form>-->
                            </div>
                            <div class="col-md-8">
                                <?php if (get_user_level() != USER_WRITER || get_user_uid() == $each_post->uid): ?>
                                    <?php $can_edit = true; ?>
                                <?php else: ?>
                                    <?php $can_edit = false; ?>
                                <?php endif; ?>
                                <form role="form" class="update_form" data-post_id="<?php echo $each_post->post_id ?>" method="post" action="<?php echo site_url("api/update_post"); ?>">
                                    <label>Title</label>
                                    <div class="form-group">
                                        <input type="text" name="post_title" value="<?php echo quotes_to_entities($each_post->post_title); ?>" class="form-control" <?php echo $can_edit ? "" : "disabled"; ?>>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" required="required" name="post_excerp" class="form-control" placeholder="Detail" value="<?php echo quotes_to_entities($each_post->post_excerp); ?>" <?php echo $can_edit ? "" : "disabled"; ?>>
                                    </div>
                                    <label>Publish date</label>
                                    <?php if ($can_edit): ?>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="datepicker_mfu" for="<?php echo "clip_{$each_post->post_id}"; ?>" data-post_id="<?php echo $each_post->post_id ?>" data-date="<?php echo $each_post->post_date != 0 ? date('d-m-Y', $each_post->post_date) : date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy"></div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <p><i class="glyphicon glyphicon-time"></i>&nbsp;<?php echo $each_post->post_date != 0 ? date('d-m-Y', $each_post->post_date) : date('d-m-Y'); ?></p>
                                    <?php endif; ?>
                                    <p><span class="label label-success"><?php echo $each_post->real_name; ?></span></p>
                                    <input type="hidden" value="<?php echo $each_post->post_date; ?>" id="<?php echo "clip_{$each_post->post_id}"; ?>" name="post_date">
                                    <input type="hidden" value="<?php echo date('U'); ?>" name="post_modify">
                                    <input type="hidden" value="<?php echo TYPE_NEWSLETTER; ?>" name="post_type">
                                    <input type="hidden" value="<?php echo POST_PUBLISH; ?>" name="post_status">
                                    <input type="hidden" value="<?php echo $each_post->uid; ?>" name="post_author">
                                    <input type="hidden" value="<?php echo $each_post->post_id; ?>" name="post_id">
                                    <input type="submit" style="display: none;" id="submit_news_<?php echo $each_post->post_id ?>">
                                </form>
                            </div>
                        </div>
                        <div class="panel-footer txt_right">
                            <?php if (count($media)): ?>
                                <a href="<?php echo $media[0]->link; ?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-file"></i> Download PDF</a>
                                <a href="<?php echo site_url('mfu/mfu_reader/' . $media[0]->mid . "/" . slug($media[0]->media_name)); ?>" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-book"></i> Read in MFU Reader</a>
                            <?php endif; ?>

                            <?php if ($can_edit): ?>
                                <button class="btn btn-sm btn-danger del_news_btn" data-mid="<?php echo $mid; ?>" data-post_id="<?php echo $each_post->post_id; ?>"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                <button class="btn btn-sm btn-default update_news_btn" id="fake_submit_news_<?php echo $each_post->post_id; ?>" data-post_id="<?php echo $each_post->post_id; ?>"><i class="glyphicon glyphicon-cog"></i> Update</button>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>

                <div class="margin_top_10">
                    <ul class="pagination pagination-sm pagination-centered">
                        <?php for ($i = 1; $i <= $total_page; $i++): ?>
                            <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo "?page={$i}&cate_id={$cate_id}&user_id={$user_id}"; ?>"><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            <?php else: ?>
                <div class="jumbotron">
                    <h3>Not found any post...</h3>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<style type="text/css">
    .upload_btn{
        position:relative;
        overflow: hidden;
        width: 89px;
        height: 34px;
    }

    .upload_btn input[type=file]{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }

    #datepicker, .datepicker_mfu{
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    #submit_news_btn{
        display: none;
    }

    .fake_file_btn{
        width: 141px;
        height: 34px;
        position: relative;
    }

    .re_upload{
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        opacity: 0;
    }
</style>

<script type="text/javascript">
    $(function() {
        $('#datepicker').datepicker().on('changeDate', function(e) {
            var unix = e.date.getTime() / 1000;
            console.log(unix);
            $('#post_date_id').val(unix);
        });

        $('.datepicker_mfu').datepicker().on('changeDate', function(e) {
            var unix = e.date.getTime() / 1000;
            var target = $(this).attr('for');
            $('#' + target).val(unix);
        });

        $('#cate_add_form').ajaxForm({
            beforeSend: function() {
                $('#cate_btn_add').text('Adding...').attr('disabled', 'dsiabled');
            },
            complete: function(xhr) {
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.reload();
                } else {
                    alert('Category already existed');
                }
            }
        });

        $('.del_cate_btn').click(function() {
            if (confirm('Do you want to delete "' + $(this).find('font').text() + '"')) {
                var cate_id = $(this).attr('data-cate_id');
                var cate_for_delete = $(this).parents('li');
                $.post('<?php echo site_url('operation/del_category'); ?>', {cate_id: cate_id}, function(res) {
                    if (res.status === "success") {
                        $(cate_for_delete).fadeOut(300, function() {
                            $(this).remove();
                        });
                    } else {
                        alert('Cannot delete this category');
                    }
                }, 'json');
            }
            return false;
        });

        $('#upload_btn').click(function() {
            if (!$(this).parents('form').find("[name=post_title]").val()) {
                alert('Title cannot leave blank');
                return false;
            }
        }).change(function() {
            var pdffile = $(this).val();
            if (pdffile != "") {
                $('#submit_news_btn').trigger('click');
            }
        });

        $('#upload_clip').ajaxForm({
            beforeSend: function() {
                $('.upload_btn').attr('disabled', 'disabled').text('Uploading...');
            },
            complete: function(xhr) {
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.reload();
                } else {
                    alert("Cannot upload new post : Access denied");
                }
            }
        });

        $('.update_news_btn').click(function() {
            var post_id = $(this).attr('data-post_id');
            $('#submit_news_' + post_id).trigger('click');
        });

        $('.update_form').each(function(index, ele) {
            $(ele).ajaxForm({
                beforeSend: function() {
                    var s_btn = $(ele).attr('data-post_id');
                    $('#fake_submit_news_' + s_btn).text('Updating...').attr('disabled', 'disabled');
                },
                complete: function(xhr) {
                    console.log(xhr.responseText);
                    var json = $.parseJSON(xhr.responseText);
                    if (json.status === "success") {
                        location.reload();
                    } else {
                        alert('Cannot update this post');
                    }
                }
            });
        });

        $('.del_news_btn').click(function() {
            if (confirm('Do you want to delete this clipping news ?')) {
                $(this).text("Deleting...").attr('disabled', 'disabled');
                var mid = $(this).attr('data-mid');
                var post_id = $(this).attr('data-post_id');
                $.post('<?php echo site_url('api/delete_clip'); ?>', {post_id: post_id, mid: mid}, function(res) {
                    console.log(res);
                    if (res) {
                        $('#block_' + post_id).fadeOut(400, function() {
                            $(this).remove();
                        });
                    }
                });
            }
        });

        $.each($('[name=re_img_upload]'), function(idx, ele) {
            $(ele).change(function() {
                var myupload = this.files;
                var post_id = $(this).attr('data-post_id');
                var btn = $(ele).siblings("font");

                if (myupload.length > 0) {
                    //Accept file to upload
                    var upload_form = new FormData();
                    upload_form.append("img_upload", myupload[0]);

                    btn.html("Uploading&hellip;");

                    $.ajax({
                        url: '<?php echo site_url("api/upload_single_media?post_id="); ?>' + post_id,
                        type: 'POST',
                        xhr: function() {
                            var xhr = $.ajaxSettings.xhr();
                            if (xhr.upload) { // Check if upload property exists
                                xhr.upload.each_element = btn;
                                xhr.upload.addEventListener('progress', function(e) {
                                    var percent = parseInt(e.loaded / e.total * 100);
                                    $(this.each_element).html("Uploading&hellip;" + percent);

                                    if (percent == 100) {
                                        $(this.each_element).html("Converting&hellip;");
                                    }
                                }, false);
                            }
                            return xhr;
                        },
                        success: function(e) {
                            console.log(e);
                            var json = $.parseJSON(e);
                            if (json.status === "success") {
                                $(ele).siblings("font").html("Upload new file");
                                location.reload();
                            } else {
                                console.log('Upload fail', e);
                            }


                        },
                        data: upload_form,
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                } else {
                    $(ele).siblings("font").html("Upload new file");
                }
            });
        });
    });
</script>
<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-stacked nav-pills">
                <li <?php echo POST_PUBLISH == $post_status ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_gallery?post_status=" . POST_PUBLISH); ?>">Publish</a></li>
                <li <?php echo POST_DRAFT == $post_status ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_gallery?post_status=" . POST_DRAFT); ?>">Draft</a></li>
                <li <?php echo POST_TRASH == $post_status ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_gallery?post_status=" . POST_TRASH); ?>">Trash</a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <?php if (count($post)): ?>
                <div class="row">
                    <div class="col-md-10">
                        <ul class="pagination" style="margin: 0px;">
                            <?php for ($i = 0; $i < $total_page; $i++): ?>
                                <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_gallery?page={$i}&post_status={$post_status}"); ?>"><?php echo $i + 1; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                    <div class="col-md-2 txt_right">
                        <a href="<?php echo site_url('management/new_gallery'); ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;New album</a>
                    </div>
                </div>

                <?php foreach ($post as $each_post): ?>
                    <?php $media = get_media_by_post_id($each_post->post_id); ?>
                    <div class="panel panel-default margin_top_10" id="<?php echo "block_{$each_post->post_id}"; ?>">
                        <div class="panel-heading"><span class="panel-title"><i class="glyphicon glyphicon-book"></i>&nbsp;<?php echo $each_post->post_title; ?></span></div>
                        <div class="panel-body">
                            <div class="row">
                                <?php if (count($media)): ?>
                                    <?php $temp_img = $media[0]; ?>
                                    <?php $link = json_decode($temp_img->link); ?>
                                    <div class="col-md-4">
                                        <a class="thumbnail" style="margin-bottom: 0px;" href="<?php echo site_url('management/new_gallery?post_id=' . $each_post->post_id); ?>">
                                            <img src="<?php echo $link->m; ?>">
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <div class="<?php echo count($media) > 0 ? "col-md-8" : "col-md-12"; ?>">
                                    <p><b>Album: </b><?php echo $each_post->post_title; ?></p>
                                    <p><b>Owner: </b><?php echo $each_post->real_name; ?></p>
                                    <p><b>Date: </b><?php echo date('d F Y H:i', $each_post->post_date); ?></p>
                                    <?php if (get_user_level() != USER_WRITER || get_user_uid() == $each_post->uid): ?>
                                        <p>
                                            <a href="<?php echo site_url('management/new_gallery?post_id=' . $each_post->post_id); ?>" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit</a>
                                            <button class="btn btn-danger btn-xs del_btn" data-trucate="<?php echo $post_status != POST_TRASH ? "0" : "1"; ?>" data-id="<?php echo $each_post->post_id ?>"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                <div class="row">
                    <div class="col-md-10">
                        <ul class="pagination" style="margin: 0px;">
                            <?php for ($i = 0; $i < $total_page; $i++): ?>
                                <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_gallery?page={$i}&post_status={$post_status}"); ?>"><?php echo $i + 1; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                    <div class="col-md-2 txt_right">
                        <a href="<?php echo site_url('management/new_gallery'); ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;New album</a>
                    </div>
                </div>
            <?php else: ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-danger">
                            <div class="panel-heading"><span class="panel-title"><i class="glyphicon glyphicon-import"></i>&nbsp;Gallery</span></div>
                            <div class="panel-body">
                                <h1 class="txt_center">Not found any album in this Section</h1>
                                <p class="txt_center"><a href="<?php echo site_url('management/new_gallery'); ?>" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;New album</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.del_btn').click(function() {
            if (confirm("Do you want to delete this album ?")) {
                var post_id = $(this).attr('data-id');
                var trucate = $(this).attr('data-trucate');
//                console.log(post_id, trucate);
                $.post("<?php echo site_url("api/delete_post"); ?>", {post_id: post_id, trucate: trucate}, function(res) {
                    if (res.status === "success") {
                        $('#block_' + post_id).fadeOut(300, function() {
                            $(this).remove();
                        });
                    } else {
                        alert("Cannot delete");
                    }
                }, 'json');
            }
        });
    });
</script>
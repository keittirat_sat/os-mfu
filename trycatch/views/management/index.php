<div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4" id="login_block">
    <p class="txt_center">
        <img src="<?php echo image_asset_url("backend_logo.png"); ?>" class="img-responsive">
    </p>
    <form role="form" method="post" action="<?php echo site_url('api/login'); ?>" id="login_form">
        <div class="form-group">
            <input type="email" class="form-control" placeholder="Email" name="email" required>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" name="password" required>
        </div>
        <p class="txt_right padding_bottom_10">
            <button type="submit" class="btn btn-danger" id="submit_btn" data-loading-text="Logging in..."><span class="glyphicon glyphicon-log-in"></span> LOGIN</button>
            <button type="reset" class="btn"><span class="glyphicon glyphicon-refresh"></span> RESET</button>
        </p>
    </form>
    <script type="text/javascript">
        $(function() {
            var btn_login;
            $('#login_form').ajaxForm({
                beforeSend: function() {
                    btn_login = $('#submit_btn').html();
                    $('#submit_btn').html($('#submit_btn').attr('data-loading-text')).attr('disabled', 'disabled');
                },
                complete: function(xhr) {
                    var json = $.parseJSON(xhr.responseText);
                    if (json.status === "success") {
                        location.href = json.routing;
                    } else {
                        $('#login_fail').modal();
                        $('#submit_btn').html(btn_login).removeAttr('disabled');
                    }
                }
            });
        });
    </script>
</div>

<div class="modal fade" id="login_fail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title th_san">Oops!!! Something wrong.</h2>
            </div>
            <div class="modal-body">
                <p><b>Cannot login</b></p>
                <p>Please check your Password and Email</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="container margin_top_20">
    <div class=" col-md-9">
        <h2 style='margin-top: 4px;'>All API</h2>
        <div class="panel-group" id="api_all">
            <?php foreach ($post as $each_post): ?>
                <div class='panel panel-default' id='<?php echo "del_set_" . $each_post->post_id ?>'>
                    <div class='panel-heading'>
                        <span class='panel-title'>
                            <a data-toggle="collapse" data-parent="#api_all" href='<?php echo "#api_{$each_post->post_id}"; ?>'><i class='glyphicon glyphicon-cog'></i>&nbsp;<?php echo $each_post->post_title; ?></a>
                        </span>
                    </div>
                    <div id="<?php echo "api_{$each_post->post_id}"; ?>" class="panel-collapse collapse">
                        <div class='panel-body'>
                            <div class='col-md-6 col-md-offset-3'>
                                <form role='form' method="post" class="update_form" action="<?php echo site_url('api/update_post'); ?>">
                                    <div class="form-group">                            
                                        <label class='control-label'>Name</label>
                                        <input type='text' class='form-control' placeholder="API name" name="post_title" value='<?php echo $each_post->post_title; ?>' required="required">
                                        <input type='hidden' value='<?php echo $each_post->post_id; ?>' name='post_id'>
                                    </div>
                                    <div class="form-group">                            
                                        <label class='control-label'>API Url</label>
                                        <input type="text" class='form-control' placeholder="API information" name="post_detail" value="<?php echo $each_post->post_detail; ?>">
                                    </div>
                                    <div class="form-group">                            
                                        <label class='control-label'>Description</label>
                                        <textarea class='form-control' placeholder="API information" name="post_excerp"><?php echo $each_post->post_excerp ?></textarea>
                                    </div>
                                    <div class="form-group txt_right" style='margin-bottom: 0px;'>
                                        <button class='btn btn-primary btn-sm update_btn' type='submit'><i class='glyphicon glyphicon-save'></i>&nbsp;Save</button>
                                        <button class='btn btn-default btn-sm' type='reset'><i class='glyphicon glyphicon-refresh'></i>&nbsp;Reset</button>
                                        <button class='btn btn-danger btn-sm del_btn' type='button' data-post_id='<?php echo $each_post->post_id; ?>'><i class='glyphicon glyphicon-trash'></i>&nbsp;Delete</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class=" col-md-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <span class="panel-title"><i class="glyphicon glyphicon-cog"></i>&nbsp;New API</span>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <form role="form" id="add_api" class="form-horizontal" method="post" action="<?php echo site_url('api/add_custom_post'); ?>">
                        <div class="form-group">
                            <input type="text" placeholder="API name" name="post_title" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="API Url" name="post_detail" class="form-control">
                        </div>
                        <div class="form-group">
                            <textarea placeholder="API detail" name="post_excerp" class="form-control"></textarea>
                        </div>
                        <div class="form-group txt_right" style="margin-bottom: 0px;">
                            <button class="btn btn-info btn-sm" id="add_btn"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Add</button>
                        </div>
                        <input type="hidden" name="post_type" value="<?php echo TYPE_API; ?>">
                        <input type="hidden" name="post_author" value="<?php echo get_user_uid(); ?>">
                        <input type="hidden" name="post_date" value="<?php echo date('U'); ?>">
                        <input type="hidden" name="post_modify" value="<?php echo date('U'); ?>">
                        <input type="hidden" name="post_status" value="<?php echo POST_PUBLISH; ?>">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#add_api').ajaxForm({
            beforeSend: function() {
                $('#add_btn').attr('disabled', 'disabled').text('Adding...');
            },
            complete: function(xhr) {
                console.log(xhr.responseText);
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.reload();
                } else {
                    $('#add_btn').removeAttr('disabled').html('<i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Add');
                    alert('Cannot process your request');
                }
            }
        });

        $('.update_form').each(function(id, ele) {
            var btn = $(ele).find('.update_btn');
            var btn_html = $(btn).html();
            $(ele).ajaxForm({
                beforeSend: function() {
                    $(btn).attr('disabled', 'disabled').text('Updating...');
                },
                complete: function(xhr) {
                    console.log(xhr.responseText)
                    var json = $.parseJSON(xhr.responseText);
                    if (json.status === "success") {
                        location.reload();
                    } else {
                        $(btn).removeAttr('disabled').html(btn_html);
                        alert('Cannot process your request');
                    }
                }
            });
        });

        $('.del_btn').click(function() {
            if (confirm('Do you want to delete this API ?')) {
                var post_id = $(this).attr('data-post_id');
                var btn_html = $(this).html();
                var btn = $(this);

                $(btn).attr('disabled', 'disabled').text('Deleting...');
                $.post('<?php echo site_url('api/delete_post'); ?>', {post_id: post_id, trucate: 1}, function(res) {
                    if (res.status === "success") {
                        $('#del_set_' + post_id).fadeOut(400, function() {
                            $(this).remove();
                        });
                    } else {
                        $(btn).removeAttr('disabled').html(btn_html);
                        alert('Cannot process your request');
                    }
                }, 'json');
            }
        });
    });
</script>
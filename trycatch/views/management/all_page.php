<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked">
                <li class="dropdown active">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="glyphicon glyphicon-list-alt"></i>&nbsp;
                        <?php switch ($post_status): case POST_DRAFT: ?>
                                Draft
                                <?php break; ?>
                            <?php case POST_PUBLISH: ?>
                                Published
                                <?php break; ?>
                            <?php case POST_TRASH: ?>
                                Trash
                                <?php break; ?>
                        <?php endswitch; ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li <?php echo $post_status == POST_PUBLISH ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_page?users={$users}&post_status=" . POST_PUBLISH); ?>">Publish</a></li>
                        <li <?php echo $post_status == POST_DRAFT ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_page?users={$users}&post_status=" . POST_DRAFT); ?>">Draft</a></li>
                        <li <?php echo $post_status == POST_TRASH ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_page?users={$users}&post_status=" . POST_TRASH); ?>">Trash</a></li>
                    </ul>
                </li>
                <li class="dropdown active">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="glyphicon glyphicon-user"></i>&nbsp;
                        <?php if ($users == 0): ?>
                            All User
                        <?php else: ?>
                            <?php foreach ($all_user_in_system as $user): ?>
                                <?php if ($user->uid == $users): ?>
                                    <?php echo $user->real_name; ?>
                                    <?php break; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li <?php echo $users == 0 ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_page?users=0&post_status={$post_status}"); ?>">All User</a></li>
                        <?php foreach ($all_user_in_system as $user): ?>
                            <li <?php echo $users == $user->uid ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_page?users={$user->uid}&post_status={$post_status}"); ?>"><?php echo $user->real_name ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-9">
            <?php if (count($post)): ?>
                <div class="row">
                    <div class="col-md-10">
                        <ul class="pagination margin_top_0">
                            <?php for ($i = 1; $i <= $total_page; $i++): ?>
                                <li <?php echo $page == $i ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_page?post_status={$post_status}&page={$i}"); ?>"><?php echo $i; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                    <?php if (get_user_level() != USER_WRITER): ?>
                        <div class="col-md-2 txt_right">
                            <a href="<?php echo site_url("management/page_operation"); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-pencil"></i>&nbsp;New Page</a>
                        </div>
                    <?php endif; ?>
                </div>
                <?php foreach ($post as $each_post_raws): ?>

                    <!--Revision checking-->
                    <?php $temp_id = $each_post_raws->post_id; ?>

                    <!--Real own-->
                    <?php $own_uid = $each_post_raws->uid; ?>
                    <?php $own_realname = $each_post_raws->real_name ?>

                    <?php $each_post = get_latest_post_revision($each_post_raws->post_id); ?>

                    <?php $each_post->post_id = $temp_id; ?>
                    <!--/Revision checking-->

                    <div class="panel panel-default" id="box_<?php echo $each_post->post_id; ?>">
                        <div class="panel-heading" id="post_title_id_<?php echo $each_post->post_title; ?>">
                            <?php echo trim($each_post->post_title) == "" ? "Not defined" : $each_post->post_title; ?>
                        </div>
                        <div class="panel-body">
                            <label>Sample excerp</label>
                            <p><?php echo get_excerp($each_post, 300); ?></p>

                            <label>Owner</label>
                            <p id="post_realname_<?php echo $each_post->post_id; ?>"><a href="<?php echo site_url("management/all_page?users={$each_post->uid}"); ?>"><?php echo $each_post->real_name; ?></a></p>

                            <label>Visitor</label>
                            <?php $counter = get_stat("post_{$each_post->post_id}"); ?>
                            <p><span class="badge badge-inverse"><?php echo $counter ? number_format($counter[0]->counter) : "0"; ?></p>
                        </div>
                        <div class="panel-footer txt_right">
                            <a target="_blank" href="<?php echo site_url("mfu/page/{$each_post->post_id}/" . slug($each_post->post_title)); ?>" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;View</a>
                            <?php if (get_user_level() != USER_WRITER || get_user_uid() == $own_uid): ?>
                                <a target="_blank" href="<?php echo site_url("management/page_operation?post_id={$each_post->post_id}"); ?>" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit</a>
                            <?php endif; ?>

                            <?php if (get_user_level() == USER_ADMIN): ?>
                                <?php $can_del = true; ?>
                            <?php else: ?>
                                <?php if (get_user_uid() == $own_uid): ?>
                                    <?php $can_del = true; ?>
                                <?php else: ?>
                                    <?php $can_del = false; ?>
                                <?php endif; ?>
                            <?php endif; ?>

                            <?php if ($can_del): ?>
                                <button data-set="<?php echo $each_post->post_id; ?>" data-trucate="<?php echo ($each_post->post_status == POST_TRASH) ? "1" : "0"; ?>" class="btn btn-sm btn-default btn-delete"><i class="glyphicon glyphicon-trash"></i>&nbsp;<?php echo ($each_post->post_status == POST_TRASH) ? "Permanent delete" : "Delete"; ?></button>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="row">
                    <div class="col-md-10">
                        <ul class="pagination margin_top_0">
                            <?php for ($i = 1; $i <= $total_page; $i++): ?>
                                <li <?php echo $page == $i ? "class='active'" : ""; ?>><a href="<?php echo site_url("management/all_page?post_status={$post_status}&page={$i}"); ?>"><?php echo $i; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                    <?php if (get_user_level() != USER_WRITER): ?>
                        <div class="col-md-2 txt_right">
                            <a href="<?php echo site_url("management/page_operation"); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-pencil"></i>&nbsp;New Page</a>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="jumbotron">
                    <h1>Not found :'(</h1>
                    <p>
                        <a class="btn btn-danger btn-lg" role="button" href='<?php echo site_url("management/page_operation"); ?>'><i class="glyphicon glyphicon-pencil"></i>&nbsp;New Page</a>
                    </p>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm_dialoque">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure ?</h4>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete this post ?</p>
                <h4 id="modal_placeholder_here" style="color:#428bca;">Title here !!!</h4>
                <p>by <b id="realname_placeholder">Name Here !!!</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-loading-text="Deleting..." id="confirm_delete_btn" data-set="0" data-trucate="0">Delete</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(function() {
        delete_success = false;
        $('.btn-delete').click(function() {
            var post_id = parseInt($(this).attr('data-set'));
            var trucate = parseInt($(this).attr('data-trucate'));
            var title = $('#post_title_id_' + post_id).text();
            var post_author = $('#post_realname_' + post_id).text();

            $('#modal_placeholder_here').text(title);
            $('#realname_placeholder').text(post_author);
            $('#confirm_delete_btn').attr('data-set', post_id);
            $('#confirm_delete_btn').attr('data-trucate', trucate);

            $('#confirm_dialoque').modal();
        });

        $('#confirm_delete_btn').click(function() {
            var post_id = parseInt($(this).attr('data-set'));
            var trucate = parseInt($(this).attr('data-trucate'));
            var btn = $(this);
            var btn_html = $(btn).html();
            $(btn).attr('disabled', 'disabled').text('Deleting...');

            $.post('<?php echo site_url('api/delete_post'); ?>', {post_id: post_id, trucate: trucate}, function(res) {
                if (res.status === "success") {
                    console.log(post_id);
                    $('#box_' + post_id).fadeOut(400, function() {
                        $(this).remove();
                    });
                    $('#confirm_dialoque').modal('hide');
                } else {
                    alert('Cannot process your request');
                    $(btn).button('reset');
                }
                $(btn).html(btn_html).removeAttr('disabled');
            }, 'json');
        });
    });
</script>
<?php $permission = get_disallow_permission(); ?>
<div class="row">
    <form role="form" action="<?php echo isset($form_url) ? $form_url : site_url('api/update_post'); ?>" method="post" id="add_post_form">
        <div class="col-md-9 col-sm-8">
            <h3><i class="glyphicon glyphicon-pencil"></i> 
                <?php echo ($reserv_post->post_title) ? "Edit" : "New"; ?>&nbsp;
                <?php switch ($reserv_post->post_type): case TYPE_PAGE: ?>
                        Page
                        <?php break; ?>
                    <?php case TYPE_EVENT: ?>
                        Event
                        <?php break; ?>
                    <?php case TYPE_CLIPPING: ?>
                        Clipping
                        <?php break; ?>
                    <?php case TYPE_RESEARCH: ?>
                        Researcher
                        <?php break; ?>
                    <?php default: ?>
                        Post
                        <?php break; ?>
                <?php endswitch; ?>
                <a href="<?php echo site_url('mfu/' . ($reserv_post->post_type == TYPE_PAGE ? "page" : "post") . '/' . $reserv_post->post_id); ?>">
                    <span class="badge"><?php echo site_url('mfu/' . ($reserv_post->post_type == TYPE_PAGE ? "page" : "post") . '/' . $reserv_post->post_id); ?></span>
                </a>
            </h3>

            <!--Post title-->
            <div class="form-group">
                <input type="text" class="form-control" name="post_title" id="post_title" placeholder="Enter title here" required value="<?php echo $reserv_post->post_title; ?>">
            </div><!--/Post title-->

            <!--Media button-->
            <div class="form-group">
                <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#media_library"><i class="glyphicon glyphicon-film"></i> Add Media</button>
            </div><!--/Media button-->

            <!--Post Detail-->
            <div class="form-group">
                <textarea class="tinymce_editor" name="post_detail" rows="14" required="required"><?php echo $reserv_post->post_detail; ?></textarea>
            </div><!--/Post Detail-->

            <?php if ($reserv_post->post_type == TYPE_POST): ?>
                <div class="form-group">
                    <!--Post excerp-->
                    <div class="panel panel-default">
                        <div class="panel-heading">Post excerp</div>
                        <div class="panel-body">
                            <textarea style="width: 100%; resize: vertical;" rows="4" placeholder="Enter post excerp here" name="post_excerp" class="form-control"><?php echo $reserv_post->post_excerp; ?></textarea>
                        </div>
                    </div><!--/Post excerp-->
                </div>
            <?php endif; ?>

            <?php if (isset($revision) && isset($post_revision)): ?>
                <div class="panel panel-warning">
                    <div class="panel-heading">Revision</div>
                    <div class="panel-body">
                        <ul>
                            <?php $i = true; ?>
                            <?php foreach ($post_revision as $rev_post): ?>
                                <?php $author = get_user_by_uid($rev_post->post_author); ?>
                                <?php switch ($reserv_post->post_type): case TYPE_POST: ?>
                                        <?php $rev_link = site_url("management/post_operation?post_id={$rev_post->post_id}&display=revision") ?>
                                        <?php break; ?>
                                    <?php case TYPE_PAGE: ?>
                                        <?php $rev_link = site_url("management/page_operation?post_id={$rev_post->post_id}&display=revision") ?>
                                        <?php break; ?>
                                    <?php case TYPE_EVENT: ?>
                                        <?php $rev_link = site_url("management/event_operation?post_id={$rev_post->post_id}&display=revision") ?>
                                        <?php break; ?>
                                <?php endswitch; ?>

                                <li id="<?php echo "rev_{$rev_post->post_id}"; ?>">
                                    <a href="<?php echo $rev_link; ?>"  data-toggle="tooltip" data-placement="top"  title="<?php echo "Edited by {$author->real_name}"; ?>" class="<?php echo $display == "revision" ? $this->input->get('post_id') == $rev_post->post_id ? "cyan" : "" : $real_revison_post_id == $rev_post->post_id ? "cyan" : "" ?> link_inherit a_rev">
                                        <i><?php echo "{$rev_post->post_title} (" . date('d-m-Y H:i:s', $rev_post->post_modify) . ")"; ?></i>
                                    </a>

                                    <?php if ($rev_post->revision != 0): ?>
                                        <button class="btn btn-danger btn-xs del_revision" type="button" data-post_id="<?php echo $rev_post->post_id ?>"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>
                                    <?php else: ?>
                                        &nbsp;&dash;<?php echo $rev_post->post_status == POST_PUBLISH ? "Publish" : "Unpublish" ?>&nbsp;
                                    <?php endif; ?>

                                    <!--edit tag-->
                                    <?php echo $display == "revision" ? $this->input->get('post_id') == $rev_post->post_id ? "<i class='glyphicon glyphicon-pencil'></i> editing&hellip;" : "" : $real_revison_post_id == $rev_post->post_id ? "<i class='glyphicon glyphicon-pencil'></i> editing&hellip;" : "" ?>
                                    <!--/edit tag-->

                                    <?php if ($rev_post->post_status == POST_PUBLISH && $i): ?>
                                        <i class="glyphicon glyphicon-flag"></i> current publish
                                        <?php $i = false; ?>
                                    <?php endif; ?>
                                    <p style="color: #666; font-size: 9px;"><?php echo "Edited by {$author->real_name}"; ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-md-3 col-sm-4">
            <!--Publications Block-->
            <div class="panel panel-danger" style="margin-top: 60px;">
                <div class="panel-heading">Publication</div>
                <div class="panel-body">
                    <?php if ($reserv_post->post_type != TYPE_EVENT): ?>
                        <p><b>Date: </b><?php echo date('d/m/Y H:i:s', $reserv_post->post_date); ?></p>
                    <?php endif; ?>

                    <!--Old operation-->
                    <!--                    <div class="form-group">
                                            <div class="btn-group">
                                                <button class="btn btn-danger" id="publish_btn" data-loading-text="<?php echo ($reserv_post->post_title) ? "Updating" : (get_user_level() == USER_WRITER ? "Sending" : "Publishing"); ?>&hellip;" type="button">
                                                    <i class="glyphicon glyphicon-floppy-saved"></i> <?php echo ($reserv_post->post_title) ? "Update" : (get_user_level() == USER_WRITER ? "Send to review" : "Publish"); ?>
                                                </button>
                                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li class="active"><a href="#" data-set="<?php echo get_user_level() == USER_WRITER ? POST_REVIEW : POST_PUBLISH ?>" class="operation_mode"><i class="glyphicon glyphicon-floppy-saved"></i> <?php echo ($reserv_post->post_title) ? "Update" : (get_user_level() == USER_WRITER ? "Send to review" : "Publish"); ?></a></li>
                                                    <li><a href="#" data-set="<?php echo POST_DRAFT ?>" class="operation_mode"><i class="glyphicon glyphicon-floppy-disk"></i> Save as draft</a></li>
                                                </ul>
                                            </div>
                                        </div>-->
                    <!--/Old operation-->

                    <!--New Operation-->
                    <div class="form-group">
                        <div class="btn-group">
                            <button class="btn btn-danger" id="publish_btn" type="button">
                                <?php if (get_user_level() == USER_WRITER): ?>
                                    <!--Writer Section-->
                                    <i class="glyphicon glyphicon-floppy-save"></i>&nbsp;
                                    Save as draft
                                <?php else: ?>
                                    <!--Admin and Director-->
                                    <i class="glyphicon glyphicon-flag"></i>&nbsp;
                                    Publish
                                <?php endif; ?>
                            </button>
                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">

                                <li>
                                    <a href="#<?php echo POST_DRAFT ?>" class="operation_mode" data-set="<?php echo POST_DRAFT ?>">
                                        <i class="glyphicon glyphicon-floppy-save"></i>&nbsp;
                                        Save as draft
                                    </a>
                                </li>

                                <?php if (get_user_level() == USER_WRITER): ?>
                                    <li>
                                        <a href="#<?php echo POST_REVIEW ?>" class="operation_mode" data-set="<?php echo POST_REVIEW ?>">
                                            <i class="glyphicon glyphicon-transfer"></i>&nbsp;
                                            Send to review
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if (get_user_level() != USER_WRITER): ?>
                                    <li>
                                        <a href="#<?php echo POST_REVIEW ?>" class="operation_mode" data-set="<?php echo POST_REVIEW ?>">
                                            <i class="glyphicon glyphicon-ban-circle"></i>&nbsp;
                                            Drop
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#<?php echo POST_PUBLISH ?>" class="operation_mode" data-set="<?php echo POST_PUBLISH ?>">
                                            <i class="glyphicon glyphicon-flag"></i>&nbsp;
                                            Publish
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <!--/New Operation-->

                </div>
            </div><!--/Publications Block-->

            <?php if ($reserv_post->post_type == TYPE_POST || $reserv_post->post_type == TYPE_EVENT): ?>
                <!--Feature image Block-->
                <div class="panel panel-default">
                    <div class="panel-heading">Post Thumbnail</div>
                    <div class="panel-body txt_center">
                        <div id="show_thumbnail" class="txt_center">
                            <?php if ($reserv_post->post_thumbnail != 0): ?>
                                <?php $media = get_media_mid($reserv_post->post_thumbnail); ?>
                                <?php $link = json_decode($media->link); ?>
                                <img src='<?php echo $link->m; ?>' class='img-thumbnail' style='margin-bottom:15px;'>
                            <?php endif; ?>
                        </div>
                        <div id="set_thumbnail_sidebar">
                            <button type="button" id="set_thumbnail_sidebar_btn" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-picture"></i> Set thumbnail</button>
                            <button type="button" id="remove_thumbnail_sidebar_btn" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-trash"></i> Remove thumbnail</button>
                        </div>
                        <input type="hidden" name="post_thumbnail" value="<?php echo $reserv_post->post_thumbnail; ?>">
                    </div>
                </div><!--/Feature image Block-->
            <?php endif; ?>

            <?php if ($reserv_post->post_type == TYPE_EVENT): ?>
                <?php $category = get_all_category(CATE_EVENT); ?>
                <?php $re_cate = group_cate_by_parent($category); ?>
                <div id="initial_category"><?php echo json_encode($re_cate); ?></div>

                <div class="panel panel-default">
                    <div class="panel-heading">Event type</div>
                    <div class="panel-body">
                        <?php $cate_info = get_category_by_cate_id($reserv_post->cate_id); ?>
                        <select class="form-control" id="cate_type">
                            <?php if ($reserv_post->cate_id == 0): ?>
                                <option value="0">--- Please select event type ---</option>
                            <?php endif; ?>
                            <?php foreach ($re_cate[0] as $each_cate): ?>
                                <?php if (!in_array($each_cate->cate_id, $permission)): ?>
                                    <option value="<?php echo $each_cate->cate_id; ?>" <?php echo $cate_info->parent_id == $each_cate->cate_id ? "selected" : ""; ?>><?php echo $each_cate->cate_name; ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Event Category</div>
                    <div class="panel-body">
                        <select class="form-control" name="cate_id">
                            <?php if ($reserv_post->cate_id != 0): ?>
                                <?php foreach ($re_cate[$cate_info->parent_id] as $each_cate): ?>
                                    <option value="<?php echo $each_cate->cate_id; ?>" <?php echo $reserv_post->cate_id == $each_cate->cate_id ? "selected" : ""; ?>><?php echo $each_cate->cate_name; ?></option>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <option value="0">--- No category ---</option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Set event date</div>
                    <div class="panel-body">
                        <div id="datepicker" data-date="<?php echo $reserv_post->post_date != 0 ? date('d-m-Y', $reserv_post->post_date) : date('d-m-Y'); ?>"data-date-format="dd-mm-yyyy"></div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($reserv_post->post_type == TYPE_POST): ?>
                <!--Slide-->
                <div class="panel panel-success">
                    <div class="panel-heading">Slide</div>
                    <div class="panel-body">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="post_slide" value="1" <?php echo $reserv_post->post_slide == 1 ? "checked" : ""; ?>>
                                Display in slide
                            </label>
                        </div>
                    </div>
                </div>
                <!--/Slide-->

                <!--Scheduling-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="checkbox  margin_bottom_0 margin_top_0">
                            <label>
                                <input type="checkbox" for="sch_start" original-data="<?php echo $reserv_post->sch_start; ?>" name="enable_schedule" <?php echo $reserv_post->sch_start != 0 ? "checked" : ""; ?>>
                                Start date
                            </label>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="datepicker_mfu" for="sch_start" data-date="<?php echo $reserv_post->sch_start != 0 ? date('d-m-Y', $reserv_post->sch_start) : date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy"></div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="checkbox margin_bottom_0 margin_top_0">
                            <label>
                                <input type="checkbox" for="sch_exp" original-data="<?php echo $reserv_post->sch_exp; ?>" name="hidden_expire" <?php echo $reserv_post->sch_exp != 0 ? "checked" : ""; ?>>
                                Expire date
                            </label>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="datepicker_mfu" for="sch_exp" data-date="<?php echo $reserv_post->sch_exp != 0 ? date('d-m-Y', $reserv_post->sch_exp) : date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy"></div>
                    </div>
                </div>
                <!--/Scheduling-->

                <!--Category Block-->
                <div class="panel panel-default">
                    <div class="panel-heading">Categories</div>
                    <div class="panel-body">
                        <?php $cate = get_all_category(); ?>
                        <div class="radio">
                            <label>
                                <input type="radio" name="cate_id" value="0" checked>
                                Uncategories
                            </label>
                        </div>

                        <?php foreach ($cate as $each_cate): ?>
                            <?php if (!in_array($each_cate->cate_id, $permission)): ?>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="cate_id" value="<?php echo $each_cate->cate_id; ?>" <?php echo($reserv_post->cate_id == $each_cate->cate_id) ? "checked" : ""; ?>>
                                        <?php echo $each_cate->cate_name; ?>
                                    </label>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div><!--/Category Block-->
            <?php endif; ?>

        </div>
        <input type="hidden" name="post_status" value="<?php echo get_user_level() == USER_WRITER ? POST_DRAFT : POST_PUBLISH ?>" id="post_status_id">
        <input type="hidden" name="post_date" value="<?php echo $reserv_post->post_date; ?>" id="post_date_id">
        <input type="hidden" name="sch_start" value="<?php echo $reserv_post->sch_start; ?>" id="sch_start">
        <input type="hidden" name="sch_exp" value="<?php echo $reserv_post->sch_exp; ?>" id="sch_exp">

        <?php if (isset($revision)): ?>
            <!--Revision-->
            <input name="post_type" type="hidden" value="<?php echo $revision ?>">
            <input name="post_modify" type="hidden" value="<?php echo date('U'); ?>">
            <input name="post_author" type="hidden" value="<?php echo get_user_uid() ?>">

            <?php if (isset($real_revison_post_id)): ?>
                <input name="real_revison_post_id" type="hidden" value="<?php echo $real_revison_post_id; ?>">
            <?php endif; ?>

            <input type="hidden" value="<?php echo $reserv_post->revision != 0 ? $reserv_post->revision : $reserv_post->post_id; ?>" name="revision" id="post_id">
        <?php else: ?>
            <!--Post id-->
            <input type="hidden" value="<?php echo $reserv_post->post_id; ?>" name="post_id" id="post_id">
        <?php endif; ?>

        <input type="submit" id="submit_form_hidden" style="display: none;">
    </form>
</div>

<div class="modal fade" id="media_library">
    <div class="modal-dialog" style="width: 95%;">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 style="margin-top: 0px;"><i class="glyphicon glyphicon-picture"></i> Media management</h3>
                <div class="row">
                    <div class="col-md-2">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills nav-stacked" id="tab_control">
                            <li class="active"><a href="#media_library_tab" data-toggle="tab" id="media_library_tab_event"><i class="glyphicon glyphicon-picture"></i> Media Library</a></li>
                            <li><a href="#upload_files_tab" data-toggle="tab"><i class="glyphicon glyphicon-upload"></i> Upload Files</a></li>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane relative fade in active" id="media_library_tab">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 inside_modal">
                                        <!-- Extra small button group -->
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm dropdown-toggle filters" type="button" data-toggle="dropdown">
                                                <span class="mode-armed"><i class="glyphicon glyphicon-picture"></i> All Media in library</span> <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#" class="mode-filters" data-filter="all"><i class="glyphicon glyphicon-picture"></i> All Media in library</a></li>
                                                <li><a href="#" class="mode-filters" data-filter="not_all"><i class="glyphicon glyphicon-upload"></i> Uploaded into this post</a></li>
                                            </ul>
                                        </div>
                                        <div class="media-tank clearfix">
                                            <?php $all_media = get_all_media(); ?>
                                            <?php foreach ($all_media as $rec): ?>
                                                <?php get_media_block($rec, $reserv_post->post_id); ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 inside_modal">
                                        <h4><i class="glyphicon glyphicon-info-sign"></i> Media information</h4>
                                        <div id="each_media_info" style="overflow: auto; height: 388px;">
                                            <h4 style="margin: 10px 0px;"><i class="glyphicon glyphicon-file"></i> <span id="media_title">Title</span></h4>
                                            <p style="margin: 10px 0px;" id="image_standing"></p>
                                            <div class="input-group input-group-sm" style="margin: 0px 0px 10px;" >
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-link"></i></span>
                                                <input type="text" name="link_into" id="link_into" class="form-control" placeholder="Link" readonly>
                                            </div>
                                            <p>
                                                <?php if ($reserv_post->post_type == TYPE_POST || $reserv_post->post_type == TYPE_EVENT): ?>
                                                    <button style="margin: 0px 0px 10px;" id="media_block_set_thumbnail" type="button" class="btn btn-danger btn-sm" data-mid="0"><i class="glyphicon glyphicon-picture"></i> Set thumbnail</button>
                                                <?php endif; ?>
                                                <button style="margin: 0px 0px 10px;" id="media_block_remove" type="button" class="btn btn-default btn-sm" data-mid="0"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                            </p>

                                            <h4 id="selected-number">0 selected</h4>
                                            <div class="list_of_selected"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane relative fade" id="upload_files_tab">
                                <div class="row" id="panel_progress_upload">
                                    <!--upload queue-->
                                    <ul id="upload_queue">

                                    </ul>
                                </div>
                                <div class="row" id="panel_for_upload">
                                    <div class="col-md-12" style="height: 300px;">
                                        <!--btn_upload section-->
                                        <button class="btn btn-default absolute_center upload_fake_btn"  data-loading-text="Uploadin ...." style="height: 41px; width: 170px;">
                                            <i class="glyphicon glyphicon-upload"></i> CLICK TO UPLOAD
                                            <input class="absolute_center" type="file" name="upload_btn[]" multiple="multiple" style="opacity: 0;" id="file_btn">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" id="media_block_insert_into_post"><i class="glyphicon glyphicon-download"></i> insert into post</button>
                <button class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="status_report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="txt_center margin_top_20 margin_bottom_20">Upload / Update complete.</h4>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<style>
    #initial_category{
        display: none;
    }
</style>
<?php echo js_asset("tinymce/tinymce.min.js"); ?>
<script type="text/javascript">
    var img_list = [];
    tinymce.init({
        selector: "textarea.tinymce_editor",
        theme: "modern",
        relative_urls: false,
        remove_script_host: false,
//        plugins: [
//            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
//            "searchreplace wordcount visualblocks visualchars code fullscreen",
//            "insertdatetime media nonbreaking save table contextmenu directionality",
//            "emoticons template paste textcolor"
//        ],
        plugins: 'link image code autolink searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime emoticons template paste textcolor table',
        toolbar1: "insertfile undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media emoticons link",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        image_list: img_list
    });
    function add_html(html) {
        tinyMCE.execCommand('mceInsertContent', false, html);
    }

    function setup_add_newpost() {
        $('#remove_thumbnail_sidebar_btn').hide();
        $('#each_media_info').hide();
    }

    $(function() {
        //initial function 
        setup_add_newpost();
        $('#tab_control a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $('a.operation_mode').click(function() {
            var post_status = $(this).attr('data-set');
            var mode_operation = $(this).html();
            $('a.operation_mode').parent('li').removeClass('active');
            $(this).parent('li').addClass('active');
            $('#publish_btn').html(mode_operation);
            $('#post_status_id').val(post_status);
        });
        //        var hasfile = 0;

        $('#media_library').on('shown.bs.modal', function() {
            size_control();
            size_control_gallery_page();
        });
        $('#media_library_tab_event').click(function() {
            setTimeout(function() {
                size_control();
                size_control_gallery_page();
            }, 500);
            $('.mode-filters').first().trigger('click');
        });
        var idx_generate = 0;
        $('#panel_progress_upload').hide();
        $('#panel_for_upload').show();

        $('.upload_fake_btn').click(function() {
            $('#file_btn').trigger('click');
        });
        $('#file_btn').change(function() {
            var myupload = this.files;
            success_upload = 0;
            $('#panel_for_upload').hide();
            $('#panel_progress_upload').show();
            for (var index = 0; index < myupload.length; index++) {
                var each_file = myupload[index];
                var id_ref = "bar_progress_" + idx_generate;
                idx_generate++;
                var each_element = '<li id="' + id_ref + '">';
                each_element += '<ul class="progress_each_file">';
                each_element += '<li>' + each_file.name + '</li>';
                each_element += '<li>';
                each_element += '<div class="progress progress-striped active">';
                each_element += '<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">';
                each_element += '<span>0% Complete</span>';
                each_element += '</div>';
                each_element += '</div>';
                each_element += '</li>';
                each_element += '</ul>';
                each_element += '</li>';
                $('#upload_queue').append(each_element);
                var upload_form = new FormData();
                upload_form.append('img_upload', each_file);
                $.ajax({
                    url: '<?php echo site_url("api/upload_single_media?post_id={$reserv_post->post_id}"); ?>',
                    type: 'POST',
                    xhr: function() {
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) { // Check if upload property exists
                            xhr.upload.each_file = each_file;
                            xhr.upload.each_element = each_element;
                            xhr.upload.ref_id = id_ref;
                            xhr.upload.addEventListener('progress', function(e) {
                                var percent = parseInt(e.loaded / e.total * 100);
                                $("#" + this.ref_id).find(".progress-bar").width(percent + '%');
                                $('#' + this.ref_id).find('span').text(percent + '% Complete');
                                $('#' + this.ref_id).find('.progress-bar').attr('aria-valuenow', percent);
                                if (percent == 100) {
                                    $('#' + this.ref_id).find('span').text('Converting...');
                                }
                            }, false);
                        }
                        return xhr;
                    },
                    success: function(e) {
                        console.log(e);
                        var json = $.parseJSON(e);
                        if (json.status === "success") {
                            $('#' + id_ref).fadeOut(400, function() {
                                $(this).remove();
                            });
                        } else {
                            console.log('Upload fail', e);
                        }

                        success_upload++;
                        if (success_upload == myupload.length) {
                            $('#panel_progress_upload').hide();
                            $('#panel_for_upload').show();
                            $.get('<?php echo site_url('api/get_media_by_post_id'); ?>', {'post_id':<?php echo $reserv_post->post_id; ?>}, function(res) {
                                $('.media_in_this_post').remove();
                                var tank = $('.media-tank').html();
                                var new_data = res + tank;
                                $('.media-tank').html(new_data);
                                $('#media_library_tab_event').trigger('click');
                            });
                        }
                    },
                    data: upload_form,
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

        });
        $('.mode-filters').click(function() {
            var mode = $(this).html();
            $('.filters .mode-armed').html(mode);
            var mode = $(this).attr('data-filter');
            if (mode === "all") {
                $('.media_other_post').show();
            } else {
                $('.media_other_post').hide();
            }
            return false;
        });
        $('#link_into').click(function() {
            $(this).select();
        });
        $('.media-lib-block .filter_media_block').live({
            click: function(e) {
                var parent = $(this).parents('.media-lib-block');
                if ($(parent).is('.selected')) {
                    $(parent).removeClass('selected');
                } else {
                    if (!e.ctrlKey) {
                        $('.selected').removeClass('selected');
                    }
                    $(parent).addClass('selected');
                    //Get info start here
                    var link_into = $(this).siblings('a').attr('data-link');
                    var media_title = $(this).siblings('a').attr('title');
                    var mid = $(this).siblings('a').attr('data-mid');
                    var img = "<img class='img-stand img-thumbnail' src='" + $(this).siblings('a').attr('data-img-m') + "' title='" + media_title + "'>";
                    $('#link_into').val(link_into);
                    $('#media_title').text(media_title);
                    $('#image_standing').html(img);
                    //Set MID Operation 
                    if ($(this).siblings('a').attr('data-type') === "img") {
                        $('#media_block_set_thumbnail').attr('data-mid', mid).show();
                    } else {
                        $('#media_block_set_thumbnail').hide();
                    }
                    $('#media_block_remove').attr('data-mid', mid);
                }

                var selected_total = $('.selected').length;
                if (selected_total > 0) {
                    $('#each_media_info').show();
                    $('#selected-number').html("<i class='glyphicon glyphicon-tags'></i>&nbsp;" + selected_total + " selected");
                    $('.list_of_selected').html("");
                    $('.selected').each(function(id, element) {
                        var info = $(element).children('a');
                        var ele;
                        var thumbnail;
                        var title = info.attr('title');
                        var image_present;
                        if (info.is('.height_enclose')) {
                            image_present = "height_enclose";
                        } else {
                            image_present = "width_enclose";
                        }

                        thumbnail = info.attr('data-thumbnail');
                        ele = "<div title='" + title + "' class='thumbnail_mini_media_block " + image_present + "' style='background-image:url(\"" + thumbnail + "\")'></div>";
                        $('.list_of_selected').append(ele);
                    });
                } else {
                    $('#each_media_info').hide();
                }

                return false;
            }
        });

        //prepare obj for insertion into post
        $('#media_block_insert_into_post').click(function() {
            var element = "";
            $('.selected').each(function(idx, ele) {
                var link = $(ele).children('a').attr('data-link');
                switch ($(ele).children('a').attr('data-type')) {
                    case 'img':
                        element += "<p><img width='450px' src='" + link + "' alt='" + $(ele).children('a').attr('title') + "' title='" + $(ele).children('a').attr('title') + "'></p>";
                        break;
                    case 'vdo':
                        element += "<p>";
                        element += "<video width='450' controls>";
                        element += "<source src='" + link + "' type='" + $(ele).children('a').attr('data-mime') + "'>";
                        element += "Your browser does not support the video tag.";
                        element += "</video>";
                        element += "</p>";
                        break;
                    default:
                        element += "<p><a href='" + link + "'>" + $(ele).children('a').attr('title') + "</a>";
                        break;
                }
            });
            add_html(element);
            $('#media_library').modal('hide');
        });

        $('.a_rev').tooltip();

        $('.del_revision').click(function() {
            var post_id = $(this).attr("data-post_id");
            if (confirm("Do you want to delete this Revision ?")) {
                var btn = $(this);
                var btn_html = $(this).html();
                $(btn).attr('disabled', 'disabled').text('Deleting...');
                $.post('<?php echo site_url('api/delete_post'); ?>', {post_id: post_id, trucate: 1}, function(res) {
                    if (res.status === "success") {
//                        console.log(post_id);
                        $('#rev_' + post_id).fadeOut(400, function() {
                            $(this).remove();
                        });
                        $('#confirm_dialoque').modal('hide');
                    } else {
                        alert('Cannot process your request');
                        $(btn).button('reset');
                    }
                    $(btn).html(btn_html).removeAttr('disabled');

                }, 'json');
            }
        });

        //Set thumbnail         
        $('#media_block_set_thumbnail').click(function() {

            $('#media_library').modal('hide');
            var mid = $(this).attr('data-mid');
            var target_element = $('#block_' + mid);
            var element = "<img src='" + $(target_element).children('a').attr('data-thumbnail') + "' class='img-thumbnail' style='margin-bottom:15px;'>";
            $('#show_thumbnail').html(element);

            //Set MID into fieldset
            $("[name=post_thumbnail]").val(mid);
            $('#set_thumbnail_sidebar_btn').hide();
            $('#remove_thumbnail_sidebar_btn').show();
        });

        //Remove thumbnail
        $('#remove_thumbnail_sidebar_btn').click(function() {
            $('#show_thumbnail').html("");
            $('#set_thumbnail_sidebar_btn').show();
            $('#remove_thumbnail_sidebar_btn').hide();
            $("[name=post_thumbnail]").val(0);
            return false;
        });

        $('#set_thumbnail_sidebar_btn').click(function() {
            $('#media_library').modal('show');
        });

        $('#media_block_remove').click(function() {
            if (confirm("Are you sure to delete this file?")) {
                var mid = $(this).attr('data-mid');
                $.post('<?php echo site_url('api/delete_media'); ?>', {mid: mid}, function(res) {
                    if (res.status === "success") {
                        $('#block_' + mid).remove();
                        $('#each_media_info').hide();
                    } else {
                        alert("Cannot process your request");
                    }
                }, 'json');
            }
        });

        $('#publish_btn').click(function() {
            var temp = tinyMCE.activeEditor.getContent();
            $('.tinymce_editor').text(temp);
            $('#submit_form_hidden').trigger('click');
        });
        var publish_btn_txt;
        $('#add_post_form').ajaxForm({
            beforeSend: function() {
                publish_btn_txt = $('#publish_btn').html();
                $('#publish_btn').attr('disabled', 'disabled');
                $('#publish_btn').html($('#publish_btn').attr('data-loading-text'));
            },
            complete: function(response) {
                console.log(response.responseText);
                var json = $.parseJSON(response.responseText);
                if (json.status === "success") {
                    //Modal Here
                    $("#status_report").modal();

                } else {
                    $('#publish_btn').removeAttr('disabled');
                    $('#publish_btn').html(publish_btn_txt);
                    alert('Cannot process your request');
                }
                console.log(json, response.responseText);
            }
        });

        $('#status_report').on('hidden.bs.modal', function(e) {
<?php if (isset($revision)): ?>
                location.reload();
<?php else: ?>
                location.href = json.redirect;
<?php endif; ?>
        })
<?php if ($reserv_post->post_type == TYPE_EVENT): ?>
            $('#datepicker').datepicker().on('changeDate', function(e) {
                var unix = e.date.getTime() / 1000;
                //                console.log(unix);
                $('#post_date_id').val(unix);
            });

            if ($('#cate_type').val() == 0) {
                $('select[name=cate_id]').attr({'disabled': 'disabled'});
            }

            $('#cate_type').change(function() {
                var parent = $(this).val();
                var all_cate = $.parseJSON($('#initial_category').text());
                var child_cate = all_cate[parent];
                var html = "";

                if (parent == 0) {
                    $('select[name=cate_id]').attr({'disabled': 'disabled'});
                } else {
                    $('select[name=cate_id]').removeAttr('disabled');
                }

                $.each(child_cate, function(id, ele) {
                    html += "<option value='" + ele.cate_id + "'>";
                    html += ele.cate_name;
                    html += "</option>";
                });
                //                console.log(html);
                $('select[name=cate_id]').html(html);
            });
<?php endif; ?>

<?php if ($reserv_post->post_type == TYPE_POST): ?>
            $('.datepicker_mfu').datepicker().on('changeDate', function(e) {
                var unix = e.date.getTime() / 1000;
                var target = $(this).attr('for');
                if (target === "sch_start") {
                    $('[name=enable_schedule]').attr('original-data', unix);
                    if ($('[name=enable_schedule]').is(':checked')) {
                        $('#' + target).val(unix);
                    }
                } else {
                    $('[name=hidden_expire]').attr('original-data', unix);
                    if ($('[name=hidden_expire]').is(':checked')) {
                        $('#' + target).val(unix);
                    }
                    if (parseInt($('#sch_start').val()) >= parseInt($('#sch_exp').val())) {
                        alert('End date Cannot equre or less than start date');
                    }
                }
            });
            $('[name=enable_schedule], [name=hidden_expire]').change(function() {
                var target = $(this).attr('for');
                if ($(this).is(':checked')) {
                    $('#' + target).val($(this).attr('original-data'));
                } else {
                    $('#' + target).val(0);
                }
            });
<?php endif; ?>
    });</script>
<?php if ($reserv_post->post_thumbnail != 0): ?>    
    <script tyle="text/javascript">
        $(function() {
            $('#set_thumbnail_sidebar_btn').hide();
            $('#remove_thumbnail_sidebar_btn').show();
        });
    </script>
<?php endif; ?>

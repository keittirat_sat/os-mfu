<div class="row margin_top_20">
    <div class="col-md-3">

        <?php $re_cate = array(); ?>
        <?php foreach ($category as $each_cate): ?>
            <?php $re_cate[$each_cate->cate_id] = $each_cate; ?>
        <?php endforeach; ?>

        <ul class="nav nav-pills nav-stacked">
            <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-time"></i>&nbsp;
                    <?php switch ($post_status): case POST_PUBLISH: ?>
                            Publish Event
                            <?php break; ?>
                        <?php case POST_DRAFT: ?>
                            Draft Event
                            <?php break; ?>
                        <?php case POST_TRASH: ?>
                            Trash Event
                            <?php break; ?>
                    <?php endswitch; ?>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li class="<?php echo $post_status == POST_PUBLISH ? "active" : "" ?>"><a href="<?php echo "?post_status=" . POST_PUBLISH; ?>">Publish Event</a></li>
                    <li class="<?php echo $post_status == POST_DRAFT ? "active" : "" ?>"><a href="<?php echo "?post_status=" . POST_DRAFT; ?>">Draft Event</a></li>
                    <li class="<?php echo $post_status == POST_TRASH ? "active" : "" ?>"><a href="<?php echo "?post_status=" . POST_TRASH; ?>">Trash Event</a></li>
                </ul>
            </li>
            <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-th-list"></i>&nbsp;
                    <?php if ($cate_id == 0): ?>
                        All Category
                    <?php else: ?>
                        <?php $temp = $re_cate[$cate_id]; ?>
                        <?php echo $temp->cate_name; ?>
                    <?php endif; ?>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li class="<?php echo $each_cate->cate_id == $cate_id ? "active" : "" ?>"><a href="<?php echo "?post_status={$post_status}&cate_id=0" ?>">All Category</a></li>
                    <?php foreach ($category as $each_cate): ?>
                        <li class="<?php echo $each_cate->cate_id == $cate_id ? "active" : "" ?>"><a href="<?php echo "?post_status={$post_status}&cate_id={$each_cate->cate_id}" ?>"><?php echo $each_cate->cate_name ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                    <?php if ($uid != 0): ?>
                        <?php foreach ($all_user_in_system as $each_user): ?>
                            <?php if ($each_user->uid == $uid): ?>
                                <?php echo $each_user->real_name ?>
                                <?php break; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        All User
                    <?php endif; ?>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li <?php echo $uid == 0 ? "class='active'" : "" ?>><a href="<?php echo "?uid=0&post_status={$post_status}"; ?>">All User</a></li>
                    <?php foreach ($all_user_in_system as $each_user): ?>
                        <li <?php echo $uid == $each_user->uid ? "class='active'" : "" ?>><a href="<?php echo "?uid={$each_user->uid}&post_status={$post_status}"; ?>"><?php echo $each_user->real_name ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
        </ul>
    </div>
    <div class="col-md-9">
        <div class="row margin_bottom_20">
            <div class="col-md-12">
                <ul class="pagination pagination-sm margin_bottom_0 margin_top_0">
                    <?php for ($i = 1; $i <= $total_page; $i++): ?>
                        <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo "?page={$i}&post_status={$post_status}&cate_id={$cate_id}&uid={$uid}"; ?>"><?php echo $i; ?></a></li>
                    <?php endfor; ?>
                </ul>
                <a class="btn btn-danger pull-right" href="<?php echo site_url('management/event_operation'); ?>">New event</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--Content here-->
                <?php if (count($post) > 0): ?>
                    <?php foreach ($post as $each_post): ?>

                        <div class="panel panel-default" id="<?php echo "event_{$each_post->post_id}" ?>">
                            <div class="panel-heading">
                                <span id="post_title_id_<?php echo $each_post->post_id; ?>"><?php echo trim($each_post->post_title) != "" ? $each_post->post_title : "Untitle"; ?></span> <i>(<?php echo date('d-m-Y', $each_post->post_modify) ?>)</i>
                            </div>
                            <div class="panel-body" style="font-size: 13px;">
                                <p><?php echo trim($each_post->post_excerp) == "" ? trim($each_post->post_detail) == "" ? "No content" : ellipsize(strip_tags($each_post->post_detail), 60) . '&hellip;' : $this->typography->auto_typography($each_post->post_excerp) . '&hellip;'; ?></p>
                                <p>
                                    <i class="glyphicon glyphicon-th-list"></i>&nbsp;
                                    <?php if ($each_post->cate_id != 0): ?>
                                        <a href="<?php echo "?cate_id={$each_post->cate_id}"; ?>"><?php echo $re_cate[$each_post->cate_id]->cate_name; ?></a>
                                    <?php else: ?>
                                        Uncategory
                                    <?php endif; ?>
                                </p>
                                <p id="post_realname_<?php echo $each_post->post_id; ?>" style="margin: 10px 0px 0px;"><i class="glyphicon glyphicon-user"></i>&nbsp;<a href="<?php echo "?uid={$each_post->uid}&post_status={$post_status}&cate_id={$cate_id}"; ?>"><?php echo $each_post->real_name; ?></a></p>
                            </div>
                            <?php if (get_user_level() != USER_WRITER || $each_post->uid == get_user_uid()): ?>
                                <div class="panel-footer txt_right">
                                    <a href="<?php echo site_url("management/event_operation?post_id={$each_post->post_id}"); ?>" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <button data-set="<?php echo $each_post->post_id; ?>" data-trucate="<?php echo $each_post->post_status == POST_TRASH ? '1' : '0'; ?>" class="btn btn-xs btn-default btn-delete"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>
                                </div>
                            <?php endif; ?>
                        </div>

                    <?php endforeach; ?>
                <?php else: ?>
                    <h2 class="txt_center">Not found any post</h2>
                <?php endif; ?>
                <!--/Content here-->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm_dialoque">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure ?</h4>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete this post ?</p>
                <h4 id="modal_placeholder_here" style="color:#428bca;">Title here !!!</h4>
                <p>by <b id="realname_placeholder">Name Here !!!</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-loading-text="Deleting..." id="confirm_delete_btn" data-set="0" data-trucate="0">Delete</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(function() {
        delete_success = false;

        $('.btn-delete').click(function() {
            var post_id = parseInt($(this).attr('data-set'));
            var trucate = parseInt($(this).attr('data-trucate'));
            var title = $('#post_title_id_' + post_id).text();
            var post_author = $('#post_realname_' + post_id).text();

            $('#modal_placeholder_here').text(title);
            $('#realname_placeholder').text(post_author);
            $('#confirm_delete_btn').attr('data-set', post_id);
            $('#confirm_delete_btn').attr('data-trucate', trucate);

            $('#confirm_dialoque').modal();
        });

        $('#confirm_delete_btn').click(function() {
            var post_id = parseInt($(this).attr('data-set'));
            var trucate = parseInt($(this).attr('data-trucate'));
            var btn = $(this);

            $(btn).button('loading');

            $.post('<?php echo site_url('api/delete_post'); ?>', {post_id: post_id, trucate: trucate}, function(res) {
                if (res.status === "success") {
                    $('#confirm_dialoque').modal('hide');
                    $(btn).button('reset');
                    $('#event_' + post_id).fadeOut(300, function() {
                        $(this).remove();
                    });
                } else {
                    alert('Cannot process your request');
                    $(btn).button('reset');
                }
                console.log(res);
            }, 'json');
        });
    });
</script>
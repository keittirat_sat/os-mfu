<div class="container margin_top_20">
    <legend><h1>Site summary by Google's report</h1></legend>
    <div class="row"> 
        <div class="col-md-12">
            <div class='panel panel-default'>
                <div class='panel-heading'><i class='glyphicon glyphicon-exclamation-sign'></i> Website's statistic</div>
                <div class='panel-body'>
                    <p><b>Period:</b>&nbsp;<?php echo "{$visitor_pageview['summary']->startDate} ~ {$visitor_pageview['summary']->endDate}"; ?></p>
                    <p><b>Pageviews:</b>&nbsp;<?php echo "{$visitor_pageview['pageviews']} Time"; ?></p>
                    <p><b>Visitor:</b>&nbsp;<?php echo "{$visitor_pageview['visits']} People"; ?></p>
                    <p><b>Webstat:</b>&nbsp;<?php echo "{$local_stat} People"; ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class='panel panel-default'>
                <div class='panel-heading'><i class='glyphicon glyphicon-flag'></i> Most sites referrer</div>
                <div class='panel-body'>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="span1" style="text-align: center;">#</th>
                                <th class="span5">Site</th>
                                <th class="span2" style="text-align: center;">Behavior</th>
                                <th class="span2" style="text-align: center;">Visitor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $index = 0; ?>
                            <?php foreach ($site_referrer as $i => $v): ?>
                                <?php if ($i != "summary"): ?>
                                    <?php foreach ($v as $i_lv2 => $v_lv2): ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo $index; ?></td>
                                            <td><?php echo $i; ?></td>
                                            <td style="text-align: center;"><?php echo $i_lv2; ?></td>
                                            <td style="text-align: center;"><?php echo $v_lv2->visits; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <?php $index++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class='panel panel-default'>
                <div class='panel-heading'><i class='glyphicon glyphicon-bold'></i> Google Keyword</div>
                <div class='panel-body'>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="span1" style="text-align: center;">#</th>
                                <th class="span9">Keyword</th>
                                <th class="span2" style="text-align: center;">Visitor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $index = 0; ?>
                            <?php foreach ($keyword as $i => $v): ?>
                                <?php if ($i != "summary"): ?>
                                    <tr>
                                        <td style="text-align: center;"><?php echo $index; ?></td>
                                        <td><a href="<?php echo "http://www.google.co.th/search?q=" . urldecode($i); ?>" target="_blank"><?php echo $i; ?></a></td>
                                        <td style="text-align: center;"><?php echo $v->visits; ?></td>
                                    </tr>
                                <?php endif; ?>
                                <?php $index++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

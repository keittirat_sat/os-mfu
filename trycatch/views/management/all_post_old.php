<div class="row">
    <div class="col-md-3">
        <legend><h3 style='margin-top: 35px;'><i class="glyphicon glyphicon-cog"></i> Display filter</h3></legend>
        <ul class="nav nav-pills nav-stacked">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="glyphicon glyphicon-list-alt"></i>&nbsp;<span id="post_type_placeholder">Published</span>&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="<?php echo ($post_status == POST_PUBLISH) ? "active" : ""; ?>"><a href="#post_published" data-set="1" class="post_status">Published</a></li>
                    <li class="<?php echo ($post_status == POST_DRAFT) ? "active" : ""; ?>"><a href="#post_draft" data-set="2" class="post_status">Draft</a></li>
                    <li class="<?php echo ($post_status == POST_TRASH) ? "active" : ""; ?>"><a href="#post_trash" data-set="0" class="post_status">Trash</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="glyphicon glyphicon-th-list"></i>&nbsp;<span id="post_category_placeholder">All Category</span>&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="<?php echo ($category == 0) ? "active" : ""; ?>"><a href="#cate_all" data-set="0" class="post_category">All Category </a></li>
                    <?php $cate = get_all_category(); ?>
                    <?php foreach ($cate as $each_cate): ?>
                        <li class="<?php echo ($category == $each_cate->cate_id) ? "active" : ""; ?>"><a href="#cate_<?php echo $each_cate->cate_id; ?>" data-set="<?php echo $each_cate->cate_id; ?>" class="post_category"><?php echo $each_cate->cate_name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="glyphicon glyphicon-user"></i>&nbsp;<span id="post_author_placeholder">All User</span>&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="<?php echo ($users == 0) ? "active" : ""; ?>"><a href="#user_all" data-set="0" class="post_author">All User</a></li>
                    <?php foreach ($all_user_in_system as $each_user): ?>
                        <li class="<?php echo ($users == $each_user->uid) ? "active" : ""; ?>"><a href="#user_<?php echo $each_user->uid; ?>" data-set="<?php echo $each_user->uid; ?>" class="post_author"><?php echo $each_user->real_name ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
        </ul>
        <button class="btn btn-danger" id="filter_all_post"><i class="glyphicon glyphicon-sort-by-attributes-alt"></i>&nbspApply filter</button>
    </div>
    <div class="col-md-9">
        <div class="table-cover">
            <?php if (count($post)): ?>
                <?php if ($total_page > 0): ?>
                    <ul class="pagination pagination-sm pull-right" style='margin:0px;'>
                        <?php for ($i = 0; $i < $total_page; $i++): ?>
                            <li <?php echo ($page == ($i + 1)) ? "class='active'" : ""; ?>><a href="<?php echo "?users={$users}&category={$category}&post_status={$post_status}&page=" . ($i + 1); ?>"><?php echo $i + 1; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                <?php endif; ?>
                <table class="table table-condensed">
                    <thead>
                    <th>Title</th>
                    <?php if ($type_post != TYPE_SCHOOL): ?>
                        <th style="max-width: 200px;">Category</th>
                    <?php endif; ?>
                    <th style="max-width: 250px;">Author</th>
                    <th style="max-width: 150px;">Date</th>
                    </thead>
                    <tfoot>
                    <th>Title</th>
                    <?php if ($type_post != TYPE_SCHOOL): ?>
                        <th>Category</th>
                    <?php endif; ?>
                    <th>Author</th>
                    <th>Date</th>
                    </tfoot>
                    <tbody>                
                        <?php foreach ($post as $rec => $each_post): ?>
                            <tr>
                                <td>
                                    <p style="margin-bottom: 5px; margin-top: 0px;" >
                                        <a href="<?php echo site_url("management/{$type_link}?post_id={$each_post->post_id}"); ?>">
                                            <b id="post_title_id_<?php echo $each_post->post_id; ?>">
                                                <?php echo (empty($each_post->post_title)) ? "Untitle post" : $each_post->post_title ?>
                                            </b>
                                        </a>
                                        <?php if ($each_post->post_status == 2): ?>
                                            &mdash;<i style="color: #ff0000;">&nbsp;draft</i>
                                        <?php endif; ?>
                                    </p>
                                    <p style="margin-bottom: 0px; margin-top: 5px;" class="txt_right">
                                        <a href="<?php echo site_url("management/{$type_link}?post_id={$each_post->post_id}"); ?>" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit</a>
                                        <a href="<?php echo site_url("post/{$each_post->post_id}"); ?>" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;View</a>
                                        <button data-set="<?php echo $each_post->post_id; ?>" data-trucate="<?php echo ($each_post->post_status == 0) ? "1" : "0"; ?>" class="btn btn-xs btn-default btn-delete"><i class="glyphicon glyphicon-trash"></i>&nbsp;<?php echo ($each_post->post_status == 0) ? "Permanent delete" : "Delete"; ?></button>
                                    </p>
                                </td>
                                <?php if ($type_post != TYPE_SCHOOL): ?>
                                    <td><?php echo ($each_post->cate_id == 0) ? "Uncategory" : trim($each_post->cate_name); ?></td>
                                <?php endif; ?>
                                <td id="post_realname_<?php echo $each_post->post_id; ?>"><?php echo $each_post->real_name; ?></td>
                                <td><a href="#" class='post_date' data-toggle="tooltip" title="<?php echo date('Y/m/d H:i:s', $each_post->post_date); ?>"><?php echo date('Y/m/d', $each_post->post_date); ?></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php if ($total_page > 0): ?>
                    <ul class="pagination pagination-sm pull-right">
                        <?php for ($i = 0; $i < $total_page; $i++): ?>
                            <li <?php echo ($page == ($i + 1)) ? "class='active'" : ""; ?>><a href="<?php echo "?users={$users}&category={$category}&post_status={$post_status}&page=" . ($i + 1); ?>"><?php echo $i + 1; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                <?php endif; ?>
            <?php else: ?>
                <div class="jumbotron">
                    <h2>Don't have any <?php echo strtolower($operation_mode); ?> yet.</h2>
                    <p>Want to add a new <?php echo $operation_mode; ?> ? <a href='<?php echo site_url('management/'.$type_link); ?>' class='btn btn-danger'>Click here</a></p>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm_dialoque">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure ?</h4>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete this post ?</p>
                <h4 id="modal_placeholder_here" style="color:#428bca;">Title here !!!</h4>
                <p>by <b id="realname_placeholder">Name Here !!!</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-loading-text="Deleting..." id="confirm_delete_btn" data-set="0" data-trucate="0">Delete</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    var delete_success;
    $(function() {

        delete_success = false;

        $('.btn-delete').click(function() {
            var post_id = parseInt($(this).attr('data-set'));
            var trucate = parseInt($(this).attr('data-trucate'));
            var title = $('#post_title_id_' + post_id).text();
            var post_author = $('#post_realname_' + post_id).text();

            $('#modal_placeholder_here').text(title);
            $('#realname_placeholder').text(post_author);
            $('#confirm_delete_btn').attr('data-set', post_id);
            $('#confirm_delete_btn').attr('data-trucate', trucate);

            $('#confirm_dialoque').modal();
        });

        $('#confirm_delete_btn').click(function() {
            var post_id = parseInt($(this).attr('data-set'));
            var trucate = parseInt($(this).attr('data-trucate'));
            var btn = $(this);

            $(btn).button('loading');

            $.post('<?php echo site_url('api/delete_post'); ?>', {post_id: post_id, trucate: trucate}, function(res) {
                if (res.status === "success") {
                    $('#confirm_dialoque').modal('hide');
                    location.reload();
                } else {
                    alert('Cannot process your request');
                    $(btn).button('reset');
                }
            }, 'json');
        });

        $('.post_status').click(function() {
            $('.post_status').parent('li').removeClass('active');
            $(this).parent('li').addClass('active');
            var txt = $(this).text();
            $('#post_type_placeholder').text(txt);
        });

        $('.post_category').click(function() {
            $('.post_category').parent('li').removeClass('active');
            $(this).parent('li').addClass('active');
            var txt = $(this).text();
            $('#post_category_placeholder').text(txt);
        });

        $('.post_author').click(function() {
            $('.post_author').parent('li').removeClass('active');
            $(this).parent('li').addClass('active');
            var txt = $(this).text();
            $('#post_author_placeholder').text(txt);
        });

        $('#filter_all_post').click(function() {
            var link = location.origin + location.pathname + "?";
            //Author
            link += "users=" + $('li.active a.post_author').attr('data-set');

            //Category
            link += "&category=" + $('li.active a.post_category').attr('data-set');

            //Post type
            link += "&post_status=" + $('li.active a.post_status').attr('data-set');

            location.href = link;
//            console.log(link);
        });

        $('li.active a.post_author').trigger('click');
        $('li.active a.post_category').trigger('click');
        $('li.active a.post_status').trigger('click');

        $('.post_date').tooltip();
    });
</script>
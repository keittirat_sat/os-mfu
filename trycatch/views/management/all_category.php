<div class="row">
    <div class="col-md-5 col-sm-5">
        <legend><h2 class="th_san">New Category</h2></legend>
        <form class="form-inline" role="form" id="add_new_category_form" method="post" action="<?php echo site_url('operation/store_category'); ?>">
            <fieldset>
                <p><input type="text" id="new_category" class="form-control" placeholder="New Category name" name="category_name"></p>
                <p>
                    <button class="btn btn-danger" type="submit" id="add_cate_btn" data-loading-text="Adding..."><span class="glyphicon glyphicon-plus"></span> ADD</button>
                    <button class="btn" type="reset" id="reset_btn"><span class="glyphicon glyphicon-refresh"></span> RESET</button>
                </p>
            </fieldset>
        </form>
    </div>
    <div class="col-md-7 col-sm-7">
        <legend><h2 class="th_san">All Exist Category</h2></legend>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="txt_left">Name</th>
                    <th class="txt_center" style="width: 100px;">Total Post</th>
                    <th class="txt_center" style="width: 100px;">Edit</th>
                    <th class="txt_center" style="width: 100px;">Delete</th>
                </tr>
            </thead>
            <tbody id="display_all_category">
                <?php $temp = get_all_category(); ?>
                <?php if (count($temp)): ?>
                    <?php foreach ($temp as $cate): ?>
                        <tr>
                            <td id="cate_<?php echo $cate->cate_id; ?>"><?php echo $cate->cate_name; ?></td>
                            <td class="txt_center"><a href="<?php echo site_url('management/all_post/' . $cate->cate_id); ?>"><?php echo get_number_of_post_in_cate($cate->cate_id); ?></a></td>
                            <td class="txt_center"><a href="<?php echo site_url('operation/edit_category/' . $cate->cate_id); ?>" class="edit_link" for="cate_<?php echo $cate->cate_id; ?>"><span class="glyphicon glyphicon-edit"></span> EDIT</a></td>
                            <td class="txt_center"><a href="<?php echo site_url('operation/del_category/' . $cate->cate_id); ?>" class="del_link"><span class="glyphicon glyphicon-remove"></span> DELETE</a></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3" class="txt_center"><h3 class="th_san">Not found any category.</h3></td>
                    </tr>
                <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th class="txt_left">Name</th>
                    <th class="txt_center" style="width: 100px;">Total Post</th>
                    <th class="txt_center" style="width: 100px;">Edit</th>
                    <th class="txt_center" style="width: 100px;">Delete</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<!--#update_fail-->
<div class="modal fade" id="update_fail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title th_san">Oops!!! Cannot add new category.</h2>
            </div>
            <div class="modal-body">
                <h3 class="th_san">Category has already exist (Name duplicate).</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--/#update_fail-->

<!--#update_category-->
<div class="modal fade" id="update_category">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title th_san">Update Category</h2>
            </div>
            <div class="modal-body">
                <form method="post" action="" id="update_category_form">
                    <fieldset>
                        <label for="update_category_box">Category name</label>
                        <p><input type="text" name="cate_name" id="update_category_box" class="form-control"></p>
                        <p class="txt_right">
                            <button type="submit" class="btn btn-primary" id="update_cate_btn" data-loading-text="Updating..."><span class="glyphicon glyphicon-check"></span> Update</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
                        </p>
                    </fieldset>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--/#update_category-->

<script type="text/javascript">
    function get_cate_all() {
        $.get('<?php echo site_url('api/get_all_category'); ?>', {}, function(res) {
            $('#display_all_category').html("");
            $.each(res, function(idx, ele) {
                var temp = "<tr>";
                temp += "<td id='cate_" + ele.cate_id + "'>" + ele.cate_name + "</td>";
                temp += "<td class='txt_center'><a href='" + ele.all_post + "'>" + ele.total_post + "</a></td>";
                temp += "<td class='txt_center'><a href='" + ele.edit_link + "' class='edit_link' for='cate_" + ele.cate_id + "'><span class='glyphicon glyphicon-edit'></span> EDIT</a></td>";
                temp += "<td class='txt_center'><a href='" + ele.del_link + "' class='del_link'><span class='glyphicon glyphicon-remove'></span> DELETE</a></td>";
                temp += "</tr>";
                $('#display_all_category').append(temp);
            });
        }, 'json');
    }
    $(function() {
        $('#add_new_category_form').ajaxForm({
            beforeSend: function() {
                $('#add_cate_btn').button('loading');
            },
            complete: function(res) {
                var json = $.parseJSON(res.responseText);
                if (json.status === "success") {
                    get_cate_all();
                    $('#reset_btn').trigger('click');
                } else if (json.status === "relogin") {
                    location.reload();
                } else {
                    $('#update_fail').modal();
                }
                $('#add_cate_btn').button('reset');
            }
        });

        $('#update_category_form').ajaxForm({
            beforeSend: function() {
                $('#update_cate_btn').button('loading');
            },
            complete: function(res) {
                var json = $.parseJSON(res.responseText);
                console.log(json);
                if (json.status === 'success') {
                    get_cate_all();
                    $('#update_category').modal('hide');
                }else{
                    alert('Update Fail');
                }
                $('#update_cate_btn').button('reset');
            }
        });

        $('.edit_link').live({
            click: function() {
                var url = $(this).attr('href');
                var cate_name_current = $(this).attr('for');
                $('#update_category_form').attr('action', url);
                $('#update_category_box').val($('#' + cate_name_current).text());
                $('#update_category').modal();
                return false;
            }
        });

        $('.del_link').live({
            click: function() {
                console.log($(this).attr('href'));
                return false;
            }
        });
    });
</script>
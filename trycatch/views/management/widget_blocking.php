<div class="row" style="margin-top: 10px;">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default" id="block_a">
                            <div class="panel-heading">
                                <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> Block A</span>
                            </div>
                            <div class="panel-body">
                                <ul class='installed_widget' data-block='a'>
                                    <?php widget_backend($widget->a); ?>
                                </ul>
                                <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="block_a" data-block="a"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default" id="block_b">
                            <div class="panel-heading">
                                <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> Block B</span>
                            </div>
                            <div class="panel-body">
                                <ul class='installed_widget' data-block='b'>
                                    <?php widget_backend($widget->b); ?>
                                </ul>
                                <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="block_b"  data-block="b"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default" id="block_c">
                            <div class="panel-heading">
                                <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> Block C</span>
                            </div>
                            <div class="panel-body">
                                <ul class='installed_widget' data-block='c'>
                                    <?php widget_backend($widget->c); ?>
                                </ul>
                                <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="block_c"  data-block="c"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default" id="block_d">
                            <div class="panel-heading">
                                <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> Block D</span>
                            </div>
                            <div class="panel-body">
                                <ul class='installed_widget' data-block='d'>
                                    <?php widget_backend($widget->d); ?>
                                </ul>
                                <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="block_d"  data-block="d"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default" id="block_2_block">
                            <div class="panel-heading">
                                <span class="panel-title"><i class="glyphicon glyphicon-folder-close"></i> News 2 block</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel panel-default" id="block_2_block_a">
                                            <div class="panel-heading">
                                                <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> News 2 A</span>
                                            </div>
                                            <div class="panel-body">
                                                <ul class='installed_widget' data-block='block_2_block_a'>
                                                    <?php widget_backend($widget->block_2_block_a); ?>
                                                </ul>
                                                <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="block_2_block_a"  data-block="2_block_a"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default" id="block_2_block_b">
                                            <div class="panel-heading">
                                                <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> News 2 B</span>
                                            </div>
                                            <div class="panel-body">
                                                <ul class='installed_widget' data-block='block_2_block_b'>
                                                    <?php widget_backend($widget->block_2_block_b); ?>
                                                </ul>
                                                <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="block_2_block_b"  data-block="2_block_b"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default" id="block_3_block">
                            <div class="panel-heading">
                                <span class="panel-title"><i class="glyphicon glyphicon-folder-close"></i> News 3 block</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="panel panel-default" id="block_3_block_a">
                                            <div class="panel-heading">
                                                <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> News 3 A</span>
                                            </div>
                                            <div class="panel-body">
                                                <ul class='installed_widget' data-block='block_3_block_a'>
                                                    <?php widget_backend($widget->block_3_block_a); ?>
                                                </ul>
                                                <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="block_3_block_a"  data-block="3_block_a"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-default" id="block_3_block_b">
                                            <div class="panel-heading">
                                                <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> News 3 B</span>
                                            </div>
                                            <div class="panel-body">
                                                <ul class='installed_widget' data-block='block_3_block_b'>
                                                    <?php widget_backend($widget->block_3_block_b); ?>
                                                </ul>
                                                <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="block_3_block_b"  data-block="3_block_b"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-default" id="block_3_block_c">
                                            <div class="panel-heading">
                                                <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> News 3 C</span>
                                            </div>
                                            <div class="panel-body">
                                                <ul class='installed_widget' data-block='block_3_block_c'>
                                                    <?php widget_backend($widget->block_3_block_c); ?>
                                                </ul>
                                                <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="block_3_block_c"  data-block="3_block_c"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span class="panel-title"><i class="glyphicon glyphicon-bullhorn"></i> Publish</span>
                    </div>
                    <div class="panel-body txt_center">
                        <button class="btn btn-danger" data-loading-text="Saving&hellip;" id="saving_btn"><i class="glyphicon glyphicon-save"></i> Save</button>
                        <button class="btn btn-default" id="reload_btn" data-loading-text="Reloading&hellip;"><i class="glyphicon glyphicon-refresh"></i> Reset</button>
                    </div>
                </div>
                <div class="panel panel-default" id="sidebar">
                    <div class="panel-heading">
                        <span class="panel-title"><i class="glyphicon glyphicon-bookmark"></i> Sidebar</span>
                    </div>
                    <div class="panel-body">
                        <ul class='installed_widget' data-block='sidebar'>
                            <?php widget_backend($widget->sidebar); ?>
                        </ul>
                        <div class="alert alert-block txt_center notice_widget">Please <button class="btn btn-xs btn-danger add_widget" data-target="sidebar" data-block="sidebar"><i class="glyphicon glyphicon-plus-sign"></i> Add</button> widget</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bs-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <span class="panel-title">Category</span>
                            </div>
                            <div class="panel-body panel_list">
                                <?php $cate = get_all_category(); ?>
                                <?php foreach ($cate as $each_cate): ?>
                                    <a href="#" data-id="<?php echo $each_cate->cate_id; ?>" data-type="category" class="each_widget <?php echo $each_cate->api_id != 0 ? "green" : ""; ?>"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;<span><?php echo $each_cate->cate_name; ?></span></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <span class="panel-title">External Link</span>
                            </div>
                            <div class="panel-body panel_list">
                                <?php foreach ($all_link as $each_link): ?>
                                    <a href="#" data-id="<?php echo $each_link->post_id; ?>" data-type="ext_link" class="each_widget"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;<span><?php echo $each_link->post_title; ?></span></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <span class="panel-title">Event and Calendar</span>
                            </div>    
                            <div class="panel-body panel_list" style="height: 164px;">
                                <?php $event_main_cate = get_all_category(CATE_EVENT) ?>
                                <a href="#" data-id="0" data-type="event" class="each_widget"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;<span>All Event</span></a>
                                <?php foreach ($event_main_cate as $each_event): ?>
                                    <?php if ($each_event->parent_id == 0): ?>
                                        <a href="#" data-id="<?php echo $each_event->cate_id; ?>" data-type="event" class="each_widget"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;<span><?php echo $each_event->cate_name; ?></span></a>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <span class="panel-title">Other widget</span>
                            </div>
                            <div class="panel-body panel_list">
                                <a href="#" data-id="0" data-type="audience_guideline" class="each_widget"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;<span>Audience Guideline</span></a>

                                <a href="#" data-id="0" data-type="external_link" class="each_widget"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;<span>External link group</span></a>

                                <a href="#" data-id="0" data-type="school" class="each_widget"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;<span>School</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- Modal -->


<!-- Modal -->
<div class="modal fade" id="update_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 id="status_response" class="txt_center margin_bottom_20 margin_top_20"></h4>
            </div>
        </div>
    </div>
</div><!-- Modal -->

<script>
    $(function() {
        var target_id;
        var save_btn_html, save_btn;
        $('.add_widget').click(function() {
            var title = $(this).attr('data-target');
            target_id = title;
            var title_txt = $('#' + title).find('.panel-title').html();
            $('#myModalLabel').html(title_txt);
            $('#myModal').modal();
        });

        $('a.each_widget').click(function() {
            var element = '<a href="#" data-id="' + $(this).attr('data-id') + '" data-type="' + $(this).attr('data-type') + '" class="each_elemet_widget"><i class="glyphicon glyphicon-minus-sign"></i>&nbsp;<span>' + $(this).find('span').text() + '<i>&nbsp;&dash;&nbsp;' + $(this).attr('data-type') + '</i></span></a>';
            $('#' + target_id).find('.installed_widget').append('<li>' + element + '</li>');
            $('#myModal').modal('hide');
            return false;
        });

        $('a.each_elemet_widget').live({
            click: function() {
                if (confirm('Confirm widget remove')) {
                    $(this).fadeOut(400, function() {
                        $(this).parent('li').remove();
                    });
                }
            }
        });

        $('#saving_btn').click(function() {
            save_btn_html = $(this).html();
            save_btn = $(this);
            $(save_btn).html('Saving...').attr('disabled', 'disabled');
            var all_box = {a: '', b: '', c: '', d: '', block_2_block_a: '', block_2_block_b: '', block_3_block_a: '', block_3_block_b: '', block_3_block_c: '', sidebar: ''};
            $('.installed_widget').each(function(id, ele) {

                var box_type = $(ele).attr('data-block');
                var widget = [];
                $(this).find('li').each(function(index, target_element) {
                    var a_elm = $(target_element).find('a');
                    var elm = {"data_type": $(a_elm).attr('data-type'), "data_id": $(a_elm).attr('data-id')};
                    widget.push(elm);
                });

                switch (box_type) {
                    case 'a' :
                        all_box.a = widget;
                        break;
                    case 'b' :
                        all_box.b = widget;
                        break;
                    case 'c' :
                        all_box.c = widget;
                        break;
                    case 'd' :
                        all_box.d = widget;
                        break;
                    case 'block_2_block_a' :
                        all_box.block_2_block_a = widget;
                        break;
                    case 'block_2_block_b' :
                        all_box.block_2_block_b = widget;
                        break;
                    case 'block_3_block_a' :
                        all_box.block_3_block_a = widget;
                        break;
                    case 'block_3_block_b' :
                        all_box.block_3_block_b = widget;
                        break;
                    case 'block_3_block_c' :
                        all_box.block_3_block_c = widget;
                        break;
                    case 'sidebar' :
                        all_box.sidebar = widget;
                        break;
                }
            });

            var obj_post = JSON.stringify(all_box);
            $.post('<?php echo site_url('api/update_block'); ?>', {widget: obj_post}, function(res) {
                if (res.status === 'success') {
                    $('#status_response').text('Widget updated successful.');
                    $(save_btn).html(save_btn_html).removeAttr('disabled');
                } else {
                    $('#status_response').text('Cannot update your request.');
                    $(save_btn).html(save_btn_html).removeAttr('disabled');
                }
                $('#update_status').modal();
            }, 'json');
        });

        $('#reload_btn').click(function() {
            $(this).button('loading');
            location.reload();
        });

        $('.installed_widget').sortable({
            revert: true
        });
    });
</script>
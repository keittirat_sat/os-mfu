<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-stacked nav-pills">
                <li class="dropdown active">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="glyphicon glyphicon-user"></i>&nbsp;
                        <?php if ($position == 0): ?>
                            ทุกตำแหน่ง
                        <?php else: ?>
                            <?php switch ($position): case POSITION_PROF: ?>
                                    ศาสตราจารย์
                                    <?php break; ?>
                                <?php case POSITION_ASSIS_PROF: ?>
                                    รองศาสตราจารย์
                                    <?php break; ?>
                                <?php case POSITION_ASSOC_PROF: ?>
                                    ผู้ช่วยศาสตราจารย์
                                    <?php break; ?>
                            <?php endswitch; ?>
                        <?php endif; ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li <?php echo $position == 0 ? "class='active'" : ""; ?>><a href="?position=0">ทุกตำแหน่ง</a></li>
                        <li <?php echo $position == POSITION_PROF ? "class='active'" : ""; ?>><a href="<?php echo "?position=" . POSITION_PROF . "&cate_id={$cate_id}"; ?>">ศาสตราจารย์</a></li>
                        <li <?php echo $position == POSITION_ASSIS_PROF ? "class='active'" : ""; ?>><a href="<?php echo "?position=" . POSITION_ASSIS_PROF . "&cate_id={$cate_id}"; ?>">รองศาสตราจารย์</a></li>
                        <li <?php echo $position == POSITION_ASSOC_PROF ? "class='active'" : ""; ?>><a href="<?php echo "?position=" . POSITION_ASSOC_PROF . "&cate_id={$cate_id}"; ?>">ผู้ช่วยศาสตราจารย์</a></li>
                    </ul>
                </li>
                <li class="dropdown active">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="glyphicon glyphicon-home"></i>&nbsp;
                        <?php if ($cate_id == 0): ?>
                            ทุกสำนัก
                        <?php else: ?>
                            <?php foreach ($category as $each_cate): ?>
                                <?php if ($each_cate->post_id == $cate_id): ?>
                                    <?php echo $each_cate->post_title; ?>
                                    <?php break ?>
                                <?php endif; ?>
                            <?php endforeach ?>
                        <?php endif; ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li <?php echo $cate_id == 0 ? "class='active'" : ""; ?>><a href="<?php echo "?cate_id=0&position={$position}"; ?>">ทุกสำนัก</a></li>
                        <?php foreach ($category as $each_cate): ?>
                            <li <?php echo $cate_id == $each_cate->post_id ? "class='active'" : ""; ?>><a href="<?php echo "?cate_id={$each_cate->post_id}&position={$position}"; ?>"><?php echo $each_cate->post_title ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12 ">
                    <ul class="pagination pagination-sm margin_bottom_0 margin_top_0">
                        <?php
                        for ($i = 1; $i <= $total_page; $i++):
                            ?>
                            <li <?php echo $i == $page ? "class='active'" : "" ?>><a href="<?php echo "?page={$i}&cate_id={$cate_id}&position={$position}"; ?>"><?php echo $i; ?></a></li>
                        <?php endfor ?>
                    </ul>
                    <a href="<?php echo site_url('management/new_researcher'); ?>" class="btn btn-danger pull-right">Add Researcher</a>
                </div>
            </div>
            <div class="row margin_top_20">
                <div class="col-md-12">
                    <?php if (count($post)): ?>
                        <?php $re_cate = array(); ?>
                        <?php foreach ($category as $each_cate): ?>
                            <?php $re_cate[$each_cate->post_id] = $each_cate; ?>
                        <?php endforeach; ?>

                        <?php foreach ($post as $each_post): ?>
                            <?php $post_title = json_decode($each_post->post_title); ?>
                            <div class="panel panel-default" id="<?php echo "block_{$each_post->post_id}"; ?>">
                                <div class="panel-heading">
                                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                                    <?php echo $post_title->en . " | " . $post_title->th ?>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="thumbnail">
                                                <?php if ($each_post->post_thumbnail == 0): ?>
                                                    <?php $img = image_asset_url("page.jpg"); ?>
                                                <?php else: ?>
                                                    <?php $img_info = get_media_mid($each_post->post_thumbnail); ?>
                                                    <?php $img = json_decode($img_info->link) ?>
                                                    <?php $img = $img->m ?>
                                                <?php endif; ?>
                                                <img src="<?php echo $img ?>" class="img-responsive" id="profile_picture">
                                            </div>
                                            <?php $pdf = get_all_media($each_post->post_id, 'desc', null, null, 'pdf') ?>
                                            <?php if (count($pdf) > 0): ?>
                                                <label>PDF list</label>
                                                <ul style="padding-left: 17px; font-size: 12px;">
                                                    <?php foreach ($pdf as $each_pdf): ?>
                                                        <li>
                                                            <a target="_blank" href="<?php echo site_url("mfu/mfu_reader/{$each_pdf->mid}/" . slug($each_pdf->media_name)) ?>"><?php echo $each_pdf->media_name; ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php else: ?>
                                                <p class="txt_center">No PDF found</p>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-8">
                                            <?php $name = json_decode($each_post->post_title) ?>
                                            <?php $info = json_decode($each_post->post_detail); ?>
                                            <div class="form-horizontal">

                                                <div class="form-group row margin_bottom_10">
                                                    <div class="col-xs-12">
                                                        <label>ชื่อ-สกุล (ไทย)</label>
                                                        <input type="text" id="name_th" required="required" class="form-control" placeholder="Name in Thai" value="<?php echo quotes_to_entities($name->th) ?>" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label>ชื่อ-สกุล (อังกฤษ)</label>
                                                        <input type="text" id="name_en" required="required" class="form-control" placeholder="Name in English" value="<?php echo quotes_to_entities($name->en) ?>" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-xs-4 control-label">ตำแหน่งทางวิชาการ</label>
                                                    <div class="col-xs-8 padding_top_7">
                                                        <a href="<?php echo "?position={$each_post->post_excerp}&cate_id={$cate_id}" ?>" class="badge">
                                                            <?php switch ($each_post->post_excerp): case POSITION_ASSOC_PROF: ?>
                                                                    ผู้ช่วยศาสตรจารย์
                                                                    <?php break; ?>
                                                                <?php case POSITION_ASSIS_PROF: ?>
                                                                    รองศาสตราจารย์
                                                                    <?php break; ?>
                                                                <?php case POSITION_PROF: ?>
                                                                    ผู้ช่วยศาสตราจารย์
                                                                    <?php break; ?>
                                                            <?php endswitch; ?>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-xs-4 control-label">สำนักวิชา</label>
                                                    <div class="col-xs-8 padding_top_7">
                                                        <a href="<?php echo "?position={$position}&cate_id={$each_post->cate_id}" ?>" class="badge">
                                                            <?php $cate = $re_cate[$each_post->cate_id] ?>
                                                            <?php echo $cate->post_title; ?>
                                                        </a>
                                                    </div>
                                                </div>

                                                <?php if ($info->tel_work || $info->tel_mobile): ?>
                                                    <div class="form-group">
                                                        <label class="col-xs-4 control-label">หมายเลขโทรศัพท์</label>
                                                        <div class="col-xs-8 padding_top_7">
                                                            <span class="badge"><i class="glyphicon glyphicon-phone"></i>&nbsp;<?php echo quotes_to_entities($info->tel_work) ?></span>
                                                        </div>
                                                    </div>
                                                    <?php if ($info->tel_mobile): ?>
                                                        <div class="form-group">
                                                            <label class="col-xs-4 control-label"></label>
                                                            <div class="col-xs-8 padding_top_7">
                                                                <span class="badge"><i class="glyphicon glyphicon-phone"></i>&nbsp;<?php echo quotes_to_entities($info->tel_mobile) ?></span>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>

                                                <div class="form-group">
                                                    <label class="col-xs-4 control-label">Email</label>
                                                    <div class="col-xs-8 padding_top_7">
                                                        <?php echo safe_mailto($info->email, $info->email, 'class="badge"'); ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-xs-4 control-label">Owner</label>
                                                    <div class="col-xs-8 padding_top_7">
                                                        <span class="label label-success"><?php echo $each_post->real_name; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if (get_user_level() != USER_WRITER || get_user_uid() == $each_post->uid): ?>
                                    <div class="panel-footer txt_right">
                                        <a href="<?php echo site_url('management/new_researcher?post_id=' . $each_post->post_id); ?>" class="btn btn-default btn-sm">Edit</a>
                                        <button class="btn btn-default btn-sm btn_del" data-id="<?php echo $each_post->post_id ?>">Delete</button>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <h1 class="txt_center">Not found any researcher in this section</h1>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.btn_del').click(function() {
            if (confirm("Do you want to delete ?")) {
                var post_id = $(this).attr('data-id');
                $.post('<?php echo site_url('api/delete_post'); ?>', {post_id: post_id, trucate: 1}, function(res) {
                    if (res.status === "success") {
                        $('#block_' + post_id).fadeOut(300, function() {
                            $(this).remove();
                        });
                    } else {
                        alert("Request fail");
                    }
                }, 'json');
            }
        });
    });
</script>
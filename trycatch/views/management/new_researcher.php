<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Researcher Info</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <?php if ($reserve_post->post_thumbnail == 0): ?>
                                            <?php $img = image_asset_url("page.jpg"); ?>
                                        <?php else: ?>
                                            <?php $img_info = get_media_mid($reserve_post->post_thumbnail); ?>
                                            <?php $img = json_decode($img_info->link) ?>
                                            <?php $img = $img->m ?>
                                        <?php endif; ?>
                                        <img src="<?php echo $img ?>" class="img-responsive" id="profile_picture">
                                    </div>
                                    <p class="txt_center">
                                        <button class="btn btn-sm btn-danger upload_profile" type="button" data-loading-text="Uploading...">
                                            <i class="glyphicon glyphicon-upload"></i>&nbsp;<font>Upload thumbnail</font>                            
                                            <input type="file" accept=".jpg, .png, .jpeg" id="update_profile_btn">
                                        </button>
                                    </p>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-horizontal" role="form">
                                        <?php $name = json_decode($reserve_post->post_title) ?>
                                        <?php $info = json_decode($reserve_post->post_detail); ?>
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">ชื่อ-สกุล (ไทย)</label>
                                            <div class="col-xs-8">
                                                <input type="text" id="name_th" required="required" class="form-control" placeholder="Name in Thai" value="<?php echo @quotes_to_entities($name->th) ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">ชื่อ-สกุล (อังกฤษ)</label>
                                            <div class="col-xs-8">
                                                <input type="text" id="name_en" required="required" class="form-control" placeholder="Name in English" value="<?php echo @quotes_to_entities($name->en) ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">ตำแหน่งทางวิชาการ</label>
                                            <div class="col-xs-8">
                                                <select class="form-control" id="position">
                                                    <option <?php echo @$reserve_post->post_excerp == POSITION_ASSOC_PROF ? "selected" : "" ?> value="<?php echo POSITION_ASSOC_PROF ?>">ผู้ช่วยศาสตรจารย์</option>
                                                    <option <?php echo @$reserve_post->post_excerp == POSITION_ASSIS_PROF ? "selected" : "" ?> value="<?php echo POSITION_ASSIS_PROF ?>">รองศาสตราจารย์</option>
                                                    <option <?php echo @$reserve_post->post_excerp == POSITION_PROF ? "selected" : "" ?> value="<?php echo POSITION_PROF ?>">ผู้ช่วยศาสตราจารย์</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">สำนักวิชา</label>
                                            <div class="col-xs-8">
                                                <select class="form-control" id="cate_id">
                                                    <?php foreach ($category as $each_cate): ?>
                                                        <option value="<?php echo $each_cate->post_id ?>" <?php echo $reserve_post->cate_id == $each_cate->post_id ? "selected" : ""; ?>><?php echo $each_cate->post_title ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">หมายเลขโทรศัพท์</label>
                                            <div class="col-xs-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-phone-alt"></i>
                                                    </span>
                                                    <input type="text" id="tel_work" class="form-control" placeholder="Work" value="<?php echo @quotes_to_entities($info->tel_work) ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label"></label>
                                            <div class="col-xs-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-phone"></i>
                                                    </span>
                                                    <input type="text" id="tel_mobile" class="form-control" placeholder="Mobile" value="<?php echo @quotes_to_entities($info->tel_mobile) ?>">
                                                </div>

                                                <label class="custom_label_checkbox">
                                                    <input type="checkbox" id="tel_mobile_flag" value="1" <?php echo (@$info->tel_mobile_flag == 1) ? "checked" : ""; ?>><i>&nbsp;Display</i>
                                                </label>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Email</label>
                                            <div class="col-xs-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-envelope"></i>
                                                    </span>
                                                    <input type="email" id="email" class="form-control" placeholder="Email Address" value="<?php echo @quotes_to_entities($info->email) ?>">
                                                </div>

                                                <label class="custom_label_checkbox">
                                                    <input type="checkbox" id="email_flag" value="1" <?php echo (@$info->email_flag == 1) ? "checked" : ""; ?>><i>&nbsp;Display</i>
                                                </label>

                                            </div>
                                        </div>
                                        <label class="control-label">ความเชี่ยวชาญ / ชำนาญ ด้านการวิจัย</label>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <textarea name="special_ability" rows="8" class="form-control" id="special_ability"><?php echo @quotes_to_entities($info->special_ability) ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">PDF file (Limited 20Mb for each)</div>
                        <div class="panel-body">
                            <div id="panel_for_upload">
                                <div class="cover_list_menu">
                                    <ul class="installed_widget" style="padding: 10px;">
                                        <?php if (count($pdf_file) > 0): ?>
                                            <?php foreach ($pdf_file as $each_pdf): ?>
                                                <li>
                                                    <a href="#del" data-mid="<?php echo $each_pdf->mid; ?>" class="red link_inherit pdf_link_del"><i class="glyphicon glyphicon-remove"></i></a>&nbsp;
                                                    <a href="<?php echo site_url("mfu/mfu_reader/{$each_pdf->mid}/" . slug($each_pdf->media_name)); ?>"><?php echo $each_pdf->media_name; ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <li id="non_holder">์PDF not found</li>
                                        <?php endif ?>
                                    </ul>
                                </div>

                                <button class="btn btn-default upload_pdf_btn">
                                    <i class="glyphicon glyphicon-upload"></i>&nbsp;Upload
                                    <input type="file" accept=".pdf" id="update_pdf_btn" multiple="multiple">
                                </button>
                            </div>
                            <div id="panel_progress_upload">
                                <ul id="upload_queue"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Update block</div>
                <div class="panel-body txt_center">
                    <div class="form-group">
                        <select class="form-control" id="post_status_side">
                            <option value="<?php echo POST_PUBLISH ?>">Publish</option>
                            <option value="<?php echo POST_DRAFT ?>">Save as draft</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger btn-sm" id="fake_submit_btn"><i class="glyphicon glyphicon-check"></i>&nbsp;Update</button>
                        <button class="btn btn-default btn-sm" id="delete_btn"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="post" action="<?php echo site_url('api/update_post'); ?>" id="update_post_form">
        <?php foreach ($reserve_post as $index => $value): ?>
            <?php if ($index == "post_modify"): ?>
                <input type="hidden" value="<?php echo date('U'); ?>" name="<?php echo $index ?>" id="<?php echo "fake_" . $index ?>">
            <?php else: ?>
                <input type="hidden" value="<?php echo quotes_to_entities($value); ?>" name="<?php echo $index ?>" id="<?php echo "fake_" . $index ?>">
            <?php endif; ?>
        <?php endforeach; ?>
        <input type="submit" id="fake_submit_btn_real" style="display: none;">
    </form>
</div>

<style>
    .upload_pdf_btn{
        width: 89px;
        height: 34px;
        position: relative;
        overflow: hidden;
    }

    .upload_pdf_btn input[type=file], .upload_profile input[type=file]{
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        opacity: 0;
    }

    .upload_profile{
        width: 134px;
        height: 30px;
        position: relative;
        overflow: hidden;
    }
</style>

<script type="text/javascript">
    $(function() {
        var idx_generate = 0;

        $('#panel_progress_upload').hide();

        $('#update_profile_btn').change(function() {
            var profile = this.files;
            console.log(profile);
            for (var i = 0; i < profile.length; i++) {
                $('.upload_profile').attr('disabled', 'disabled').find('font').text("Uploading...");
                var upload_form = new FormData();
                var each_img = profile[i];
                console.log(each_img);
                upload_form.append('img_upload', each_img);
                $.ajax({
                    url: '<?php echo site_url("api/upload_single_media?post_id={$reserve_post->post_id}"); ?>',
                    type: 'POST',
                    xhr: function() {
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) {
                            xhr.upload.addEventListener('progress', function(e) {
                                var percent = parseInt(e.loaded / e.total * 100);
                                //Progress here
                                console.log(percent);
                                if (percent < 100) {
                                    $('.upload_profile').find('font').text(percent + "%");
                                } else {
                                    $('.upload_profile').find('font').text("Spliting...");
                                }
                            }, false);
                        }
                        return xhr;
                    },
                    success: function(e) {
                        console.log(e);
                        var json = $.parseJSON(e);
                        if (json.status === "success") {
                            var site_url = location.origin + "/";
                            $('.upload_profile').removeAttr('disabled').find('font').text("Upload thumbnail");
                            console.log(json);
                            $('#profile_picture').attr('src', json.link.m);
                            $('#fake_post_thumbnail').val(json.mid);
                        } else {
                            console.log('Upload fail', e);
                        }
                    },
                    data: upload_form,
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });

        $('#update_pdf_btn').change(function() {
            var myupload = this.files;
            success_upload = 0;
            $('#panel_for_upload').hide();
            $('#panel_progress_upload').show();
            for (var index = 0; index < myupload.length; index++) {
                var each_file = myupload[index];
                var id_ref = "bar_progress_" + idx_generate;
                idx_generate++;
                var each_element = '<li id="' + id_ref + '">';
                each_element += '<ul class="progress_each_file">';
                each_element += '<li>' + each_file.name + '</li>';
                each_element += '<li>';
                each_element += '<div class="progress progress-striped active">';
                each_element += '<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">';
                each_element += '<span>0% Complete</span>';
                each_element += '</div>';
                each_element += '</div>';
                each_element += '</li>';
                each_element += '</ul>';
                each_element += '</li>';
                $('#upload_queue').append(each_element);
                var upload_form = new FormData();
                upload_form.append('img_upload', each_file);
                $.ajax({
                    url: '<?php echo site_url("api/upload_single_media?post_id={$reserve_post->post_id}"); ?>',
                    type: 'POST',
                    xhr: function() {
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) { // Check if upload property exists
                            xhr.upload.each_file = each_file;
                            xhr.upload.each_element = each_element;
                            xhr.upload.ref_id = id_ref;
                            xhr.upload.addEventListener('progress', function(e) {
                                var percent = parseInt(e.loaded / e.total * 100);
                                $("#" + this.ref_id).find(".progress-bar").width(percent + '%');
                                $('#' + this.ref_id).find('span').text(percent + '% Complete');
                                $('#' + this.ref_id).find('.progress-bar').attr('aria-valuenow', percent);
                                if (percent == 100) {
                                    $('#' + this.ref_id).find('span').text('Converting...');
                                }
                            }, false);
                        }
                        return xhr;
                    },
                    success: function(e) {
                        console.log(e);
                        var json = $.parseJSON(e);
                        if (json.status === "success") {
                            $('#' + id_ref).fadeOut(400, function() {
                                $(this).remove();
                            });
                        } else {
                            console.log('Upload fail', e);
                        }

                        success_upload++;
                        if (success_upload === myupload.length) {
                            $('#panel_progress_upload').hide();
                            $('#panel_for_upload').show();
                            var site_url = location.origin + "/";
                            $.get('<?php echo site_url('api/get_raws_upload_media'); ?>', {'post_id':<?php echo $reserve_post->post_id; ?>, 'media_type': 'pdf'}, function(res) {
                                console.log(res);
                                var html = "";
                                $(res).each(function(id, data) {
                                    html += "<li>";

                                    html += "<a href='#del_ajax' data-mid='" + data.mid + "' class='red link_inherit pdf_link_del'>";
                                    html += "<i class='glyphicon glyphicon-remove'></i>&nbsp;";
                                    html += "</a>";

                                    html += "<a href='" + site_url + "mfu/mfu_reader/" + data.mid + "/preview'>" + data.media_name + "</a>";
                                    html += "</li>";
                                });
                                $('#panel_for_upload ul').html(html);
                            }, 'json');
                        }
                    },
                    data: upload_form,
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

        });

        $('.pdf_link_del').live({
            click: function() {
                var ele = $(this);
                if (confirm('Do you want to delete this document file ?')) {
                    var mid = $(this).attr('data-mid');
                    $.post('<?php echo site_url('api/delete_media'); ?>', {mid: mid}, function(res) {
                        if (res.status === "success") {
                            $(ele).parents('li').fadeOut(400, function() {
                                $(this).remove();
                            });
                        } else {
                            alert('Operation requested fail');
                        }
                    }, 'json');
                }
            }
        });

        $('#fake_submit_btn').click(function() {
            var post_title = {};
            var post_detail = {};

            post_title.en = $('#name_en').val();
            post_title.th = $('#name_th').val();

            $('#fake_post_title').val(JSON.stringify(post_title));

            //CATE ID
            $('#fake_cate_id').val($('#cate_id').val());

            //Excerp
            $('#fake_post_excerp').val($('#position').val());

            //Detail
            post_detail.tel_work = $('#tel_work').val();
            post_detail.tel_mobile = $('#tel_mobile').val();
            post_detail.email = $('#email').val();
            post_detail.special_ability = $('#special_ability').val();

            //Permission Enable
            post_detail.tel_mobile_flag = $('#tel_mobile_flag').is(":checked") ? 1 : 0;
            post_detail.email_flag = $('#email_flag').is(":checked") ? 1 : 0;

            $('#fake_post_detail').val(JSON.stringify(post_detail));

            //Post_status
            $('#fake_post_status').val($('#post_status_side').val());

            $('#fake_submit_btn_real').trigger('click');
        });

        $('#update_post_form').ajaxForm({
            beforeSend: function() {
                $('#fake_submit_btn').attr('disabled', 'disabled').text('Updating...');
            },
            complete: function(xhr) {
                console.log(xhr.responseText);
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.href = location.origin + location.pathname + "?post_id=<?php echo $reserve_post->post_id ?>";
                } else {
                    $('#fake_submit_btn').removeAttr('disabled').html('<i class="glyphicon glyphicon-check"></i>&nbsp;Update');
                }
            }
        });

        $('#delete_btn').click(function() {
            if (confirm("Please confirm to delete this post")) {
                var post_id = <?php echo $reserve_post->post_id ?>;
                $.post('<?php echo site_url('api/delete_post'); ?>', {post_id: post_id, trucate: 1}, function(res) {
                    if (res.status === "success") {
                        alert("Delete complete");
                        location.href = "<?php echo site_url("management/research"); ?>";
                    } else {
                        alert("Request fail");
                    }
                }, 'json');
            }
        });
    });
</script>
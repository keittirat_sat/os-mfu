<div class="container" stle="margin-top: 20px;">
    <div class="row">

        <div class="col-xs-4">
            <h3>Media Library</h3>
            <div class="panel panel-default">
                <div class="panel-heading">Media type</div>
                <div class="panel-body">
                    <ul class="nav nav-stacked nav-pills">
                        <li class="<?php echo $type === 0 ? "active" : ""; ?>">
                            <a href="<?php echo "?type=0"; ?>">ALL</a>
                        </li>

                        <li class="<?php echo $type === "img" ? "active" : ""; ?>">
                            <a href="<?php echo "?type=img"; ?>">IMAGE</a>
                        </li>

                        <li class="<?php echo $type === "pdf" ? "active" : ""; ?>">
                            <a href="<?php echo "?type=pdf"; ?>">PDF</a>
                        </li>

                        <li class="<?php echo $type === "vdo" ? "active" : ""; ?>">
                            <a href="<?php echo "?type=vdo"; ?>">VDO</a>
                        </li>

                        <li class="<?php echo $type === "other" ? "active" : ""; ?>">
                            <a href="<?php echo "?type=other"; ?>">OTHER</a>
                        </li>
                    </ul>
                </div>
            </div>

            <form method = "get">
                <div class = "panel panel-default">
                    <div class = "panel-heading"><i class = "glyphicon glyphicon-search"></i> Search</div>
                    <div class = "panel-body">
                        <div class="input-group">
                            <input type = "text" placeholder = "Search" class = "form-control" name="q" value="<?php echo $q ?>">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class = "glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="<?php echo $type ?>" name="type">
            </form>
        </div>

        <div class = "col-xs-8">
            <!--pagination top-->
            <div class = "row">
                <div class = "col-md-12">
                    <!--Normal Pagination-->
                    <ul class = "pagination pagination-sm">
                        <?php $endPage = ($startpage + 10) < $total_page ? ($startpage + 10) : $total_page ?>

                        <?php for ($i = $startpage; $i < $endPage; $i++): ?>
                            <li <?php echo $page == $i ? "class='active'" : ""; ?>>
                                <a href="<?php echo "?q={$q}&type={$type}&page={$i}&startpage={$startpage}"; ?>"><?php echo $i + 1; ?></a>
                            </li>
                        <?php endfor; ?>

                        <?php if ($total_page > 10): ?>
                            <li class="dropdown">
                                <a href="#" class="" id="dropdownMenu1" data-toggle="dropdown">
                                    Jump to page (<?php echo $total_page ?>)
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" style="margin: 30px 0 0 -72px;">
                                    <?php for ($p = 0; $p < $total_page; $p+=10): ?>    
                                        <li role="presentation" <?php echo $startpage == $p ? "class='active'" : ""; ?>>
                                            <?php $p_index = $p + 1; ?>
                                            <?php $p_endpoint = ($p + 10) <= $total_page ? ($p + 10) : $total_page ?>
                                            <a role="menuitem" tabindex="-1" href="<?php echo "?q={$q}&page={$p}&startpage={$p}&type={$type}"; ?>"><?php echo "{$p_index} - {$p_endpoint}"; ?></a>
                                        </li>
                                    <?php endfor ?>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul><!--/Normal Pagination-->
                </div>
            </div>
            <!--/pagination top-->

            <?php if (count($post) > 0): ?>
                <?php foreach ($post as $each_media): ?>
                    <?php switch ($each_media->media_type): case 'img': ?>
                            <?php $panel = "panel-default"; ?>
                            <?php $gly = "glyphicon-picture"; ?>
                            <?php break; ?>
                        <?php case 'pdf': ?>
                            <?php $panel = "panel-danger"; ?>
                            <?php $gly = "glyphicon-book"; ?>
                            <?php break; ?>
                        <?php case 'vdo': ?>
                            <?php $panel = "panel-info"; ?>
                            <?php $gly = "glyphicon-film"; ?>
                            <?php break; ?>
                        <?php default: ?>
                            <?php $panel = "panel-warning"; ?>
                            <?php $gly = "glyphicon-question-sign"; ?>
                            <?php break; ?>
                    <?php endswitch; ?>
                    <div class="row" id="<?php echo "mid_" . $each_media->mid; ?>">
                        <div class="col-md-12">
                            <div class="panel <?php echo $panel; ?>">
                                <div class="panel-heading">
                                    <span class="panel-title"><i class="glyphicon <?php echo $gly; ?>"></i>&nbsp;<?php echo $each_media->media_name; ?></span>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-4">
                                        <?php switch ($each_media->media_type): case "img": ?>
                                                <?php $img = json_decode($each_media->link); ?>
                                                <img src="<?php echo $img->m ?>" class="img-responsive" style="margin: 0px auto;">
                                                <?php break; ?>
                                            <?php case "pdf": ?>
                                                <img src="<?php echo image_asset_url("pdf.png"); ?>" class="img-responsive" style="margin: 0px auto;">
                                                <?php break; ?>
                                            <?php case "vdo": ?>
                                                <video class="img-responsive" style="margin: 0px auto;" controls="controls"><source src="<?php echo $each_media->link; ?>" type="<?php echo $each_media->header_media; ?>">
                                                    Your browser does not support the video tag.
                                                </video>
                                                <?php break; ?>
                                            <?php default: ?>
                                                <img src="<?php echo image_asset_url("other.png"); ?>" class="img-responsive" style="margin: 0px auto;">
                                                <?php break; ?>
                                        <?php endswitch; ?>
                                    </div>
                                    <div class="col-md-8">
                                        <label>Name</label>
                                        <p><?php echo $each_media->media_name; ?></p>

                                        <label>Link</label>
                                        <?php if ($each_media->media_type != "img"): ?>
                                            <p><input type="text" class="form-control" value="<?php echo $each_media->link; ?>" readonly="readonly"></p>
                                        <?php else: ?>
                                            <?php $link_img = json_decode($each_media->link); ?>
                                            <p><input type="text" class="form-control" value="<?php echo $link_img->s; ?>" readonly="readonly"></p>
                                            <p><input type="text" class="form-control" value="<?php echo $link_img->m; ?>" readonly="readonly"></p>
                                            <p><input type="text" class="form-control" value="<?php echo $link_img->l; ?>" readonly="readonly"></p>
                                        <?php endif; ?>

                                        <label>Date</label>
                                        <p><?php echo date('d F Y H:i', $each_media->upload_date); ?></p>

                                        <label>Owner</label>
                                        <p><?php echo $each_media->real_name; ?></p>
                                        <?php if (get_user_level() != USER_WRITER || get_user_uid() == $each_media->uid): ?>
                                            <p><button class="btn btn-danger del_btn" data-mid="<?php echo $each_media->mid; ?>"><i class=" glyphicon glyphicon-trash"></i>&nbsp;Delete</button></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <h3 class="txt_center">Media not found</h3>
            <?php endif; ?>

            <!--pagination bottom-->
            <div class="row">
                <div class="col-md-12">
                    <!--Normal Pagination-->
                    <ul class = "pagination pagination-sm">
                        <?php $endPage = ($startpage + 10) < $total_page ? ($startpage + 10) : $total_page ?>

                        <?php for ($i = $startpage; $i < $endPage; $i++): ?>
                            <li <?php echo $page == $i ? "class='active'" : ""; ?>>
                                <a href="<?php echo "?q={$q}&type={$type}&page={$i}&startpage={$startpage}"; ?>"><?php echo $i + 1; ?></a>
                            </li>
                        <?php endfor; ?>

                        <?php if ($total_page > 10): ?>
                            <li class="dropdown">
                                <a href="#" class="" id="dropdownMenu1" data-toggle="dropdown">
                                    Jump to page (<?php echo $total_page ?>)
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" style="margin: 30px 0 0 -72px;">
                                    <?php for ($p = 0; $p < $total_page; $p+=10): ?>    
                                        <li role="presentation" <?php echo $startpage == $p ? "class='active'" : ""; ?>>
                                            <?php $p_index = $p + 1; ?>
                                            <?php $p_endpoint = ($p + 10) <= $total_page ? ($p + 10) : $total_page ?>
                                            <a role="menuitem" tabindex="-1" href="<?php echo "?q={$q}&page={$p}&startpage={$p}&type={$type}"; ?>"><?php echo "{$p_index} - {$p_endpoint}"; ?></a>
                                        </li>
                                    <?php endfor ?>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul><!--/Normal Pagination-->
                </div>
            </div>
            <!--/pagination bottom-->
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.del_btn').click(function() {
            if (confirm("Are you sure ?")) {
                var mid = $(this).attr('data-mid');
                $.post('<?php echo site_url('api/delete_media'); ?>', {'mid': mid}, function(res) {
                    console.log(res);
                    res = $.parseJSON(res);
                    if (res.status === "success") {
                        $('#mid_' + mid).fadeOut(400, function() {
                            $('#mid_' + mid).remove();
                        });
                    }
                });
            }
        });
    });
</script>
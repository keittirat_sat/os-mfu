<div class="row"style="margin-top: 10px;">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading"><i class="glyphicon glyphicon-link"></i> Custom link</div>
            <div class="panel-body">
                <form id="add_custom_link_form">
                    <p>
                        <input type="text" placeholder="Lable text" class="form-control input-sm" required id="link_label">
                    </p>
                    <p>
                        <input type="text" class="form-control  input-sm" placeholder="<?php echo site_url(); ?>" id="link_to" required>
                    </p>
                    <p class="txt_right" style="margin: 0px;">
                        <button class="btn btn-default btn-sm" type="submit" id="add_link"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                    </p>
                </form>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><i class="glyphicon glyphicon-file"></i> Page</div>
            <div class="panel-body">
                <?php $page = get_all_post_by_type(TYPE_PAGE); ?>
                <div class="cover_list_menu page_list">
                    <?php foreach ($page as $each_page): ?>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="<?php echo $each_page->post_title; ?>" data-link="<?php echo site_url("mfu/page/{$each_page->post_id}/" . slug($each_page->post_title)); ?>" data-type="<?php echo $each_page->post_type; ?>"> <?php echo $each_page->post_title; ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
                <p class="txt_right" style="margin: 0px;">
                    <button class="btn btn-default btn-sm" type="submit" id="add_page"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                </p>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><i class="glyphicon glyphicon-list-alt"></i> Category</div>
            <div class="panel-body">
                <?php $cate = get_all_category(); ?>
                <div class="cover_list_menu category_list">
                    <?php foreach ($cate as $each_cate): ?>
                        <div class="checkbox  <?php echo $each_cate->api_id != 0 ? "green" : ""; ?>">
                            <label>
                                <input type="checkbox" data-title="<?php echo $each_cate->cate_name; ?>" data-link="<?php echo site_url("mfu/category/{$each_cate->cate_id}/" . slug($each_cate->cate_name)); ?>" data-type="category"> <?php echo $each_cate->cate_name; ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
                <p class="txt_right" style="margin: 0px;">
                    <button class="btn btn-default btn-sm" type="submit" id="add_category"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                </p>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><i class="glyphicon glyphicon-list-alt"></i> Event and Calendar</div>
            <div class="panel-body">
                <?php $cate = get_all_category(CATE_EVENT); ?>
                <div class="cover_list_menu calendar_list">                    
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="All Event" data-link="<?php echo site_url("mfu/all_event"); ?>" data-type="event"> All Event
                        </label>
                    </div>
                    <?php foreach ($cate as $each_cate): ?>
                        <?php if ($each_cate->parent_id == 0): ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" data-title="<?php echo $each_cate->cate_name; ?>" data-link="<?php echo site_url("mfu/all_event?cate_id={$each_cate->cate_id}"); ?>" data-type="event"> <?php echo $each_cate->cate_name; ?>
                                </label>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <p class="txt_right" style="margin: 0px;">
                    <button class="btn btn-default btn-sm" type="submit" id="add_event"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                </p>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><i class="glyphicon glyphicon-list-alt"></i> External Link Group</div>
            <div class="panel-body">
                <div class="cover_list_menu category_group_list">
                    <?php foreach ($all_link as $each_link): ?>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="<?php echo $each_link->post_title; ?>" data-link="<?php echo site_url("mfu/category_group/{$each_link->post_id}/" . slug($each_link->post_title)); ?>" data-type="category_group"> <?php echo $each_link->post_title; ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
                <p class="txt_right" style="margin: 0px;">
                    <button class="btn btn-default btn-sm" type="submit" id="add_category_group"><i class="glyphicon glyphicon-import"></i> Add to group</button>
                </p>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><i class=" glyphicon glyphicon-list"></i> Reserved page</div>
            <div class="panel-body">
                <div class="cover_list_menu reserved_list">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU PR" data-link="<?php echo site_url("mfu/pr"); ?>" data-type="reserved_list"> MFU PR
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Admission" data-link="<?php echo site_url("mfu/admission"); ?>" data-type="reserved_list"> MFU Admission
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Admission news" data-link="<?php echo site_url("mfu/admission_news"); ?>" data-type="reserved_list"> MFU Admission news
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Visitor info" data-link="<?php echo site_url("mfu/visitor_info"); ?>" data-type="reserved_list"> MFU Visitor info
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Research" data-link="<?php echo site_url("mfu/research"); ?>" data-type="reserved_list"> MFU Research
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Newsletter" data-link="<?php echo site_url("mfu/newsletter"); ?>" data-type="reserved_list"> MFU Newsletter
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Clipping" data-link="<?php echo site_url("mfu/clipping"); ?>" data-type="reserved_list"> MFU Clipping
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Gallery" data-link="<?php echo site_url("mfu/gallery"); ?>" data-type="reserved_list"> MFU Gallery
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Researcher" data-link="<?php echo site_url("mfu/researcher"); ?>" data-type="reserved_list"> MFU Researcher
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Organization" data-link="<?php echo site_url("mfu/organization"); ?>" data-type="reserved_list"> MFU Organization
                        </label>
                    </div>
                    
                    <!--Program-->
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Bachelor Program" data-link="<?php echo site_url("mfu/bachelor"); ?>" data-type="reserved_list"> MFU Bachelor Program
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Master Program" data-link="<?php echo site_url("mfu/master"); ?>" data-type="reserved_list"> MFU Master Program
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" data-title="MFU Doctorate Program" data-link="<?php echo site_url("mfu/doctorate"); ?>" data-type="reserved_list"> MFU Doctorate Program
                        </label>
                    </div>
                    <!--/Program-->
                </div>
                <p class="txt_right" style="margin: 0px;">
                    <button class="btn btn-default btn-sm" type="submit" id="add_reserved_page"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                </p>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><i class="glyphicon glyphicon-list-alt"></i> School</div>
            <div class="panel-body">
                <?php $school = get_all_post_by_type(TYPE_SCHOOL); ?>
                <div class="cover_list_menu school_list">
                    <?php foreach ($school as $each_school): ?>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="<?php echo $each_school->post_title; ?>" data-link="<?php echo site_url("mfu/school/{$each_school->post_id}/" . slug($each_school->post_title)); ?>" data-type="school"> <?php echo $each_school->post_title; ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
                <p class="txt_right" style="margin: 0px;">
                    <button class="btn btn-default btn-sm" type="submit" id="add_school"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-9"> 
        <div class="jumbotron">
            <div class="container">
                <h2><i class=" glyphicon glyphicon-list-alt"></i> Menu Structure</h2>

                <p style="font-size:18px;">Drag each item into the order you prefer. Click the arrow on the right of the item to reveal additional configuration options.</p>

                <ul class="sortable nav nav-pills nav-stacked">
                    <?php $json = $this->post->get_meta('menu'); ?>
                    <?php $json = json_decode($json[0]->meta_info); ?>
                    <?php foreach ($json as $index => $menu): ?>
                        <?php if ($menu->menu_parent_id == -1): ?>
                            <?php $class = "ui-state-default"; ?>
                            <?php $panel = "panel-default"; ?>
                        <?php else: ?>
                            <?php $parent_menu = $json[$menu->menu_parent_id]; ?>
                            <?php if ($parent_menu->menu_parent_id == -1): ?>
                                <!--Orphan-->
                                <?php $class = "ui-state-default sub-menu_lv1"; ?>
                                <?php $panel = "panel-info"; ?>
                            <?php else: ?>
                                <!--Has Parent-->
                                <?php $class = "ui-state-default sub-menu_lv2"; ?>
                                <?php $panel = "panel-danger"; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <li class="ux_tc <?php echo $class; ?>">
                            <div class="panel-group" id="accordion_<?php echo $index; ?>">
                                <div class="panel <?php echo $panel; ?>">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $index; ?>">
                                                <span class="_sub-menu"><i class="glyphicon glyphicon-indent-left"></i>&nbsp;</span>
                                                <span class="tc_label"><?php echo $menu->menu_title; ?></span>
                                                <span class="test_post"></span>
                                                <i class="caret pull-right caret_menu_cutom"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_<?php echo $index; ?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="form-horizontal form_menu_config">
                                                <div class="form-group">
                                                    <label for="link_title__<?php echo $index; ?>" class="col-sm-2 control-label">Link label</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control input-sm" id="link_title__<?php echo $index; ?>" placeholder="Link label" value="<?php echo $menu->menu_title; ?>" name="link_title">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="link_to__<?php echo $index; ?>" class="col-sm-2 control-label">URL</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control input-sm" id="link_to__<?php echo $index; ?>" placeholder="URL" value="<?php echo $menu->menu_link; ?>" name="link_to" <?php echo ($menu->menu_type != "custom_link") ? "readonly" : ""; ?>>
                                                        <input type="hidden" class="menu_type" value="<?php echo $menu->menu_type; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-10 col-sm-offset-2">
                                                        <button class="btn btn-danger btn-sm del_menu"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <p class="txt_right">
                    <button type="button" class="btn btn-danger" id="save_operation"><i class="glyphicon glyphicon-save"></i> Save menu</button>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="status_report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="status_title txt_shadow">Status</h2>
                <p class="status_txt">One fine body&hellip;</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    var counter_id;
    function get_element_index(index) {
        var take = null;
        $('.sortable li').each(function(id, element) {
            if (index === id) {
                take = element;
            }
        });

        return take;
    }

    function isValidURL(url) {
        var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

        if (RegExp.test(url)) {
            return true;
        } else {
            return false;
        }
    }

    function sub_lv1(target_element) {
        $(target_element).removeClass('sub-menu_lv2').addClass('sub-menu_lv1');
        $(target_element).find('.panel').removeClass('panel-danger').addClass('panel-info').removeClass('panel-default');
        $('.ui-sortable-placeholder').removeClass('sub-menu_lv2').addClass('sub-menu_lv1');
    }

    function sub_lv2(target_element) {
        $(target_element).removeClass('sub-menu_lv1').addClass('sub-menu_lv2');
        $(target_element).find('.panel').addClass('panel-danger').removeClass('panel-info').removeClass('panel-default');
        $('.ui-sortable-placeholder').removeClass('sub-menu_lv1').addClass('sub-menu_lv2');
    }

    function root_menu(target_element) {
        $(target_element).removeClass('sub-menu_lv1').removeClass('sub-menu_lv2');
        $(target_element).find('.panel').removeClass('panel-danger').removeClass('panel-info').addClass('panel-default');
        $('.ui-sortable-placeholder').removeClass('sub-menu_lv1').removeClass('sub-menu_lv2');
    }

    function checkbox_execution(target) {
        var element;
        $(target).each(function(id, ele) {
            var link = $(ele).attr('data-link');
            var title = $(ele).attr('data-title');
            var type = $(ele).attr('data-type');
            element = generate_element(title, link, type);

            $(ele).removeAttr('checked');

            $('.sortable').append(element);
        });
    }

    function generate_element(title, link, type) {
        var element;
        counter_id++;
        var next_total = new Date();
        var random_id = next_total.getTime() + counter_id;
        element = "<li class='ux_tc ui-state-default'>";
        element += "<div class='panel-group' id='accordion" + random_id + "'>";
        element += "<div class='panel panel-default'>";
        element += "<div class='panel-heading'>";
        element += "<h4 class='panel-title'>";
        element += "<a data-toggle='collapse' data-parent='#accordion' href='#collapse" + random_id + "'>";
        element += "<span class='_sub-menu'><i class='glyphicon glyphicon-indent-left'></i>&nbsp;</span><span class='tc_label'>" + title + "</span> <span class='test_post'></span> <i class='caret pull-right caret_menu_cutom'></i>";
        element += "</a>";
        element += "</h4>";
        element += "</div>";
        element += "<div id='collapse" + random_id + "' class='panel-collapse collapse'>";
        element += "<div class='panel-body'>";

        //Inside Element
        element += "<div class='form-horizontal form_menu_config'>";

        //First Line
        element += "<div class='form-group'>";
        element += "<label for='link_title_" + random_id + "' class='col-sm-2 control-label'>Link label</label>";
        element += "<div class='col-sm-10'>";
        element += "<input type='text' class='form-control input-sm' id='link_title_" + random_id + "' placeholder='Link label' value='" + title + "' name='link_title'>";
        element += "</div>";
        element += "</div>";

        //Second Line
        element += "<div class='form-group'>";
        element += "<label for='link_to_" + random_id + "' class='col-sm-2 control-label'>URL</label>";
        element += "<div class='col-sm-10'>";

        element += "<input type='text' class='form-control input-sm' id='link_to_" + random_id + "' placeholder='URL' value='" + link + "' name='link_to' ";
        if (type !== 'custom_link') {
            element += " readonly ";
        }
        element += ">";

        element += "<input type='hidden' class='menu_type' value='" + type + "'>";
        element += "</div>";
        element += "</div>";

        //Third Line - Submit form
        element += "<div class='form-group'>";
        element += "<div class='col-sm-10 col-sm-offset-2'>";

        //Submit - form
        element += "<button class='btn btn-danger btn-sm del_menu'><i class='glyphicon glyphicon-trash'></i>&nbsp;Delete</button>";

        element += "</div>";
        element += "</div>";
        element += "</div>";

        element += "</div>";
        element += "</div>";
        element += "</div>";
        element += "</div>";
        element += "</li>";
        return element;
    }

    $(function() {
        var target_element;
        counter_id = 0;

        //Retrieve element Ids
        $('.ux_tc').live({
            mousedown: function() {
                target_element = $(this);
            }
        });

        $('.sortable').sortable({
            revert: true,
            cursorAt: {left: 20},
            update: function(event, ui) {

                //First index rules
                var first_index = get_element_index(0);
                root_menu(first_index);

                //Sort check rule
                var old = false;
                $('.sortable li').each(function(idx, ele) {
                    if (!$(ele).is('.ui-sortable-helper')) {
                        if (old) {
//                            console.log($(old).find('.tc_label').text(), $(ele).find('.tc_label').text());
                            if (!$(old).is('.sub-menu_lv1') && !$(old).is('.sub-menu_lv2')) {
                                //Main menu
                                if ($(ele).is('.sub-menu_lv2')) {
                                    sub_lv1(ele);
                                }
                            }
                        }
                    }
                    old = ele;
                });
            },
            over: function(event, ui) {
                var compare_index = $('.ui-sortable-placeholder').index();
                var selected_index = $(target_element).index();
                if (compare_index === 0 || selected_index === 0) {
                    //Top-menu
                    $(target_element).css({'margin-left': 0});
                    $('.ui-sortable-placeholder').css({'margin-left': 0});
                } else {
                    //Second Menu
                    var mark_compare = get_element_index(compare_index - 2);

                    if (ui.position.left > 30) {
                        if ($(mark_compare).is('.sub-menu_lv1')) {
                            if (ui.position.left > 60) {
                                //lv1 -> lv2
                                sub_lv2(target_element);
                            } else {
                                //lv2 -> lv1
                                sub_lv1(target_element);
                            }
                        } else if ($(mark_compare).is('.sub-menu_lv2')) {
                            if (ui.position.left > 60) {
                                //lv1 -> lv2
                                sub_lv2(target_element);
                            } else {
                                //lv2 -> lv1
                                sub_lv1(target_element);
                            }
                        } else {
                            //lv2 -> lv1
                            sub_lv1(target_element);
                        }

                    } else {
                        root_menu(target_element);
                    }
                }
            }
        });

        $('form#add_custom_link_form').submit(function() {
            var title = $('#link_label').val();
            var link = $('#link_to').val();

            var element = "";
            if (isValidURL(link)) {
                element = generate_element(title, link, 'custom_link');

                $('.sortable').append(element);

                $(this).trigger('reset');
            } else {
                alert('URL invalid');
            }

            return false;
        });

        $('.del_menu').live({
            click: function() {
                if (confirm('Do you want to delete this menu ?')) {
                    var element = $(this).parents('li.ux_tc');
                    $(element).animate({opacity: 0}, 400, function() {
                        $(element).remove();
                    });

                    //Re-Adjust
                    //Delete root
                    if ((!$(element).is('.sub-menu_lv1') && !$(element).is('.sub-menu_lv2')) && ($(element).next().is('.sub-menu_lv1'))) {
                        var next_1 = $(element).next();
                        root_menu(next_1);

                        if ($(next_1).next().is('.sub-menu_lv2')) {
                            sub_lv1($(next_1).next());
                        }
                    }
                    else if (($(element).is('.sub-menu_lv1') && !$(element).is('.sub-menu_lv2')) && ($(element).next().is('.sub-menu_lv2'))) {
                        var next_1 = $(element).next();
                        sub_lv1(next_1);
                    }
                }
            }
        });

        $('#add_category').click(function() {
            checkbox_execution($('.category_list input[type=checkbox]:checked'));
        });

        $('#add_school').click(function() {
            checkbox_execution($('.school_list input[type=checkbox]:checked'));
        });

        $('#add_page').click(function() {
            checkbox_execution($('.page_list input[type=checkbox]:checked'));
        });

        $('#add_reserved_page').click(function() {
            checkbox_execution($('.reserved_list input[type=checkbox]:checked'));
        });

        $('#add_event').click(function() {
            checkbox_execution($('.calendar_list input[type=checkbox]:checked'));
        });

        $('#add_category_group').click(function() {
            checkbox_execution($('.category_group_list input[type=checkbox]:checked'));
        });


        $('#save_operation').click(function() {
            var root, lv1;
            var dataset = [];
            $('.ux_tc').each(function(idx, ele) {
                //Generate ids.
                $(ele).attr('menu-id', idx);

                var atom = {};
                atom['menu_id'] = idx;
                atom['menu_title'] = $(ele).find('input[name=link_title]').val();
                atom['menu_link'] = $(ele).find('input[name=link_to]').val();
                atom['menu_type'] = $(ele).find('.menu_type').val();

                //Generate tree relation
                if (!$(ele).is('.sub-menu_lv1') && !$(ele).is('.sub-menu_lv2')) {
                    //Root
                    root = idx;
                    $(ele).attr('menu-parent-id', -1);
                    atom['menu_parent_id'] = -1;
                } else if ($(ele).is('.sub-menu_lv1')) {
                    //Sub-menu_lv1
                    lv1 = idx;
                    $(ele).attr('menu-parent-id', root);
                    atom['menu_parent_id'] = root;
                } else if ($(ele).is('.sub-menu_lv2')) {
                    //Sub-menu_lv2
                    $(ele).attr('menu-parent-id', lv1);
                    atom['menu_parent_id'] = lv1;
                }

                dataset.push(atom);

            });

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('api/update_menu'); ?>",
                data: {'dataset': dataset},
//                contentType: "application/json; charset=utf-8",
                dataType: "html",
                cache: false,
                success: function(res) {
                    var json = $.parseJSON(res);
                    if (json.status === "success") {
                        $('.status_title').text('Update complete');
                        $('.status_txt').text('Menu update successful');
                    } else {
                        $('.status_title').text('Update fail');
                        $('.status_txt').text('Cannot process your request');
                    }
                    $('#status_report').modal();
                },
                failure: function(res) {
                    $('.status_title').text('Update fail');
                    $('.status_txt').text('Internal error');
                    $('#status_report').modal();
                }
            });
        });
    });
</script>
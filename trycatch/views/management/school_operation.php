<?php if (!empty($post_detail)): ?>
    <?php extract($post_detail); ?>
<?php endif; ?>
<div class="row">
    <form role="form" action="<?php echo site_url('api/update_post_school'); ?>" method="post" id="add_post_form">
        <fieldset>
            <div class="col-md-9 col-sm-8">
                <h3><i class="glyphicon glyphicon-pencil"></i> <?php echo ($reserv_post->post_title) ? "Edit" : "New"; ?> School <a href="<?php echo site_url('shool/' . $reserv_post->post_id); ?>"><span class="badge"><?php echo site_url('school/' . $reserv_post->post_id); ?></span></a></h3>

                <!--Post id-->
                <input type="hidden" value="<?php echo $reserv_post->post_id; ?>" name="post_id" id="post_id">
                <!--/Post id-->

                <!--Post title-->
                <div class="form-group">
                    <input type="text" class="form-control" name="post_title" id="post_title" placeholder="Enter title here" required value="<?php echo $reserv_post->post_title; ?>">
                </div><!--/Post title-->

                <!--Media button-->
                <div class="form-group">
                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#media_library"><i class="glyphicon glyphicon-film"></i> Add Media</button>
                </div><!--/Media button-->

                <!--Post excerp : School information-->
                <div class="panel panel-danger">
                    <div class="panel-heading">School info</div>
                    <div class="panel-body">
                        <textarea style="width: 100%; resize: vertical;" rows="4" class="form-control" placeholder="Enter post excerp here" name="post_excerp"><?php echo $reserv_post->post_excerp; ?></textarea>
                    </div>
                </div><!--/Post excerp : School information-->

                <!--Other info Home-->
                <div class="panel panel-danger">
                    <div class="panel-heading">Home</div>
                    <div class="panel-body">
                        <textarea style="width: 100%; resize: vertical;" rows="14" name="post_other_home" class="tab_home" id="tab_home"><?php echo (empty($post_other_home)) ? "" : $post_other_home; ?></textarea>
                    </div>
                </div><!--/Other info Home-->

                <!--Other About the school / Vision-->
                <div class="panel panel-danger">
                    <div class="panel-heading">About the school / Vision</div>
                    <div class="panel-body">
                        <textarea style="width: 100%; resize: vertical;" rows="14" name="post_other_about" class="tab_about" id="tab_about"><?php echo (empty($post_other_about)) ? "" : $post_other_about; ?></textarea>
                    </div>
                </div><!--/Other About the school / Vision-->

                <!--Other Research Interests-->
                <div class="panel panel-danger">
                    <div class="panel-heading">Research Interests</div>
                    <div class="panel-body">
                        <textarea style="width: 100%; resize: vertical;" rows="14" name="post_other_research" class="tab_research" id="tab_research"><?php echo (empty($post_other_research)) ? "" : $post_other_research; ?></textarea>
                    </div>
                </div><!--/Other Research Interests-->

                <!--Other Career Opportunities-->
                <div class="panel panel-danger">
                    <div class="panel-heading">Career Opportunities</div>
                    <div class="panel-body">
                        <textarea style="width: 100%; resize: vertical;" rows="14" name="post_other_career" class="tab_career" id="tab_career"><?php echo (empty($post_other_career)) ? "" : $post_other_career; ?></textarea>
                    </div>
                </div><!--/Other Career Opportunities-->

                <!--Other International Collaboration-->
                <div class="panel panel-danger">
                    <div class="panel-heading">International Collaboration</div>
                    <div class="panel-body">
                        <textarea style="width: 100%; resize: vertical;" rows="14" name="post_other_collaboration" class="tab_collaboration" id="tab_collaboration"><?php echo (empty($post_other_collaboration)) ? "" : $post_other_collaboration; ?></textarea>
                    </div>
                </div><!--/Other International Collaboration-->

                <!--Other Programmes-->
                <div class="panel panel-danger">
                    <div class="panel-heading">Programmes</div>
                    <div class="panel-body">
                        <textarea style="width: 100%; resize: vertical;" rows="14" name="post_other_programmes" class="tab_programmes" id="tab_programmes"><?php echo (empty($post_other_programmes)) ? "" : $post_other_programmes; ?></textarea>
                    </div>
                </div><!--/Other Programmes-->

                <!--Other Contact Information-->
                <div class="panel panel-danger">
                    <div class="panel-heading">Contact Information</div>
                    <div class="panel-body form-horizontal">
                        <div class="form-group">
                            <label for="bst_phone" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="bst_phone" placeholder="Phone" name="post_other_phone" value="<?php echo (empty($post_other_phone)) ? "" : $post_other_phone; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bst_fax" class="col-sm-2 control-label">Fax</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="bst_fax" placeholder="Fax" name="post_other_fax" value="<?php echo (empty($post_other_fax)) ? "" : $post_other_fax; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bst_email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="bst_email" placeholder="Email" name="post_other_email" value="<?php echo (empty($post_other_email)) ? "" : $post_other_email; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bst_website" class="col-sm-2 control-label">Website</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="bst_website" placeholder="Website" name="post_other_website" value="<?php echo (empty($post_other_website)) ? "" : $post_other_website; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bst_address" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                                <textarea style="width: 100%; resize: vertical;" class="form-control" id="bst_address" name="post_other_address" placeholder="Address"><?php echo (empty($post_other_address)) ? "" : $post_other_address; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div><!--/Other Contact Information-->

            </div>
            <div class="col-md-3 col-sm-4">
                <!--Publications Block-->
                <div class="panel panel-danger" style="margin-top: 60px;">
                    <div class="panel-heading">Publication</div>
                    <div class="panel-body">
                        <p><b>Date: </b><?php echo date('d/m/Y H:i:s', $reserv_post->post_date); ?></p>
                        <div class="btn-group">
                            <button class="btn btn-danger" id="publish_btn" data-loading-text="<?php echo ($reserv_post->post_title) ? "Updating" : "Publishing"; ?> ..." type="button">
                                <i class="glyphicon glyphicon-floppy-saved"></i> <?php echo ($reserv_post->post_title) ? "Update" : "Publish"; ?>
                            </button>
                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li class="active"><a href="#" data-set="1" class="operation_mode"><i class="glyphicon glyphicon-floppy-saved"></i> <?php echo ($reserv_post->post_title) ? "Update" : "Publish"; ?></a></li>
                                <li><a href="#" data-set="2" class="operation_mode"><i class="glyphicon glyphicon-floppy-disk"></i> Save as draft</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!--/Publications Block-->
            </div>
            <input type="hidden" name="post_status" value="1" id="post_status_id">
            <input type="submit" id="submit_form_hidden" style="display: none;">
        </fieldset>
    </form>
</div>

<div class="modal fade" id="media_library">
    <div class="modal-dialog" style="width: 95%;">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 style="margin-top: 0px;"><i class="glyphicon glyphicon-picture"></i> Media management</h3>
                <div class="row">
                    <div class="col-md-2">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills nav-stacked" id="tab_control">
                            <li class="active"><a href="#media_library_tab" data-toggle="tab" id="media_library_tab_event"><i class="glyphicon glyphicon-picture"></i> Media Library</a></li>
                            <li><a href="#upload_files_tab" data-toggle="tab"><i class="glyphicon glyphicon-upload"></i> Upload Files</a></li>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane relative fade in active" id="media_library_tab">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 inside_modal">
                                        <!-- Extra small button group -->
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm dropdown-toggle filters" type="button" data-toggle="dropdown">
                                                <span class="mode-armed"><i class="glyphicon glyphicon-picture"></i> All Media in library</span> <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#" class="mode-filters" data-filter="all"><i class="glyphicon glyphicon-picture"></i> All Media in library</a></li>
                                                <li><a href="#" class="mode-filters" data-filter="not_all"><i class="glyphicon glyphicon-upload"></i> Uploaded into this post</a></li>
                                            </ul>
                                        </div>
                                        <div class="media-tank clearfix">
                                            <?php $all_media = get_all_media(); ?>
                                            <?php foreach ($all_media as $rec): ?>
                                                <?php get_media_block($rec, $reserv_post->post_id); ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 inside_modal">
                                        <h4><i class="glyphicon glyphicon-info-sign"></i> Media information</h4>
                                        <div id="each_media_info" style="overflow: auto; height: 388px;">
                                            <h4 style="margin: 10px 0px;"><i class="glyphicon glyphicon-file"></i> <span id="media_title">Title</span></h4>
                                            <p style="margin: 10px 0px;" id="image_standing"></p>
                                            <div class="input-group input-group-sm" style="margin: 0px 0px 10px;" >
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-link"></i></span>
                                                <input type="text" name="link_into" id="link_into" class="form-control" placeholder="Link" readonly>
                                            </div>
                                            <p>
                                                <?php if ($reserv_post->post_type == TYPE_POST): ?>
                                                    <button style="margin: 0px 0px 10px;" id="media_block_set_thumbnail" type="button" class="btn btn-danger btn-sm" data-mid="0"><i class="glyphicon glyphicon-picture"></i> Set thumbnail</button>
                                                <?php endif; ?>
                                                <button style="margin: 0px 0px 10px;" id="media_block_remove" type="button" class="btn btn-default btn-sm" data-mid="0"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                            </p>

                                            <h4 id="selected-number">0 selected</h4>
                                            <div class="list_of_selected"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane relative fade" id="upload_files_tab">
                                <div class="row" id="panel_progress_upload">
                                    <!--upload queue-->
                                    <ul id="upload_queue">

                                    </ul>
                                </div>
                                <div class="row" id="panel_for_upload">
                                    <div class="col-md-12" style="height: 300px;">
                                        <!--btn_upload section-->
                                        <button class="btn btn-default absolute_center upload_fake_btn"  data-loading-text="Uploadin ...." style="height: 41px; width: 170px;">
                                            <i class="glyphicon glyphicon-upload"></i> CLICK TO UPLOAD
                                            <input class="absolute_center" type="file" name="upload_btn[]" multiple="multiple" style="opacity: 0;" id="file_btn">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <label>Section: </label>
                <select class="form-control" style="width: 200px; display: inline;" id="target_block">
                    <option value="tab_home">Home</option>
                    <option value="tab_about">About the school / Vision</option>
                    <option value="tab_research">Research Interests</option>
                    <option value="tab_career">Career Opportunities</option>
                    <option value="tab_collaboration">International Collaboration</option>
                    <option value="tab_programmes">Programmes</option>
                </select>
                <button class="btn btn-primary" id="media_block_insert_into_post"><i class="glyphicon glyphicon-download"></i> insert into post</button>
                <button class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="status_report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="txt_center margin_top_20 margin_bottom_20">Upload / Update complete.</h4>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php echo js_asset("tinymce/tinymce.min.js"); ?>
<script type="text/javascript">
    var img_list = [];
    tinymce.init({
        selector: "textarea.tab_home, textarea.tab_about, textarea.tab_research, textarea.tab_career, textarea.tab_collaboration, textarea.tab_programmes",
        theme: "modern",
        relative_urls: false,
        convert_urls: false,
        remove_script_host: false,
//        plugins: [
//            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
//            "searchreplace wordcount visualblocks visualchars code fullscreen",
//            "insertdatetime media nonbreaking save table contextmenu directionality",
//            "emoticons template paste textcolor"
//        ],
        plugins: 'link image code autolink searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime emoticons template paste textcolor table',
        toolbar1: "insertfile undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media emoticons link",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        image_list: img_list
    });

    function add_html(target, html) {
        tinyMCE.get(target).execCommand('mceInsertContent', false, html);
    }

    function setup_add_newpost() {
        $('#remove_thumbnail_sidebar_btn').hide();
        $('#each_media_info').hide();

    }

    function get_content_from_editor(target) {
        return tinyMCE.get(target).getContent();
    }

    $(function() {
        //initial function 
        setup_add_newpost();

        $('#tab_control a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $('a.operation_mode').click(function() {
            var post_status = $(this).attr('data-set');
            var mode_operation = $(this).html();
            $('a.operation_mode').parent('li').removeClass('active');
            $(this).parent('li').addClass('active');
            $('#publish_btn').html(mode_operation);
            $('#post_status_id').val(post_status);
        });


        var hasfile = 0;

        $('#media_library').on('shown.bs.modal', function() {
            size_control();
            size_control_gallery_page();
        });

        $('#media_library_tab_event').click(function() {
            setTimeout(function() {
                size_control();
                size_control_gallery_page();
            }, 500);
            $('.mode-filters').first().trigger('click');
        });

        var idx_generate = 0;
        $('#panel_progress_upload').hide();
        $('#panel_for_upload').show();

        $('.upload_fake_btn').click(function() {
            $('#file_btn').trigger('click');
        });

        $('#file_btn').change(function() {
            var myupload = this.files;
            success_upload = 0;
            $('#panel_for_upload').hide();
            $('#panel_progress_upload').show();
            for (var index = 0; index < myupload.length; index++) {
                var each_file = myupload[index];
                var id_ref = "bar_progress_" + idx_generate;
                idx_generate++;
                var each_element = '<li id="' + id_ref + '">';
                each_element += '<ul class="progress_each_file">';
                each_element += '<li>' + each_file.name + '</li>';
                each_element += '<li>';
                each_element += '<div class="progress progress-striped active">';
                each_element += '<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">';
                each_element += '<span>0% Complete</span>';
                each_element += '</div>';
                each_element += '</div>';
                each_element += '</li>';
                each_element += '</ul>';
                each_element += '</li>';
                $('#upload_queue').append(each_element);
                var upload_form = new FormData();
                upload_form.append('img_upload', each_file);
                $.ajax({
                    url: '<?php echo site_url("api/upload_single_media?post_id={$reserv_post->post_id}"); ?>',
                    type: 'POST',
                    xhr: function() {
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) { // Check if upload property exists
                            xhr.upload.each_file = each_file;
                            xhr.upload.each_element = each_element;
                            xhr.upload.ref_id = id_ref;
                            xhr.upload.addEventListener('progress', function(e) {
                                var percent = parseInt(e.loaded / e.total * 100);
                                $("#" + this.ref_id).find(".progress-bar").width(percent + '%');
                                $('#' + this.ref_id).find('span').text(percent + '% Complete');
                                $('#' + this.ref_id).find('.progress-bar').attr('aria-valuenow', percent);
                                if (percent == 100) {
                                    $('#' + this.ref_id).find('span').text('Converting...');
                                }
                            }, false);
                        }
                        return xhr;
                    },
                    success: function(e) {
                        console.log(e);
                        var json = $.parseJSON(e);
                        if (json.status === "success") {
                            $('#' + id_ref).fadeOut(400, function() {
                                $(this).remove();
                            });
                        } else {
                            console.log('Upload fail', e);
                        }

                        success_upload++;
                        if (success_upload == myupload.length) {
                            $('#panel_progress_upload').hide();
                            $('#panel_for_upload').show();
                            $.get('<?php echo site_url('api/get_media_by_post_id'); ?>', {'post_id':<?php echo $reserv_post->post_id; ?>}, function(res) {
                                $('.media_in_this_post').remove();
                                var tank = $('.media-tank').html();
                                var new_data = res + tank;
                                $('.media-tank').html(new_data);
                                $('#media_library_tab_event').trigger('click');
                            });
                        }
                    },
                    data: upload_form,
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

        });

        $('.mode-filters').click(function() {
            var mode = $(this).html();
            $('.filters .mode-armed').html(mode);
            var mode = $(this).attr('data-filter');
            if (mode === "all") {
                $('.media_other_post').show();
            } else {
                $('.media_other_post').hide();
            }
            return false;
        });

        $('#link_into').click(function() {
            $(this).select();
        });

        $('.media-lib-block .filter_media_block').live({
            click: function(e) {
                var parent = $(this).parents('.media-lib-block');
                if ($(parent).is('.selected')) {
                    $(parent).removeClass('selected');
                } else {
                    if (!e.ctrlKey) {
                        $('.selected').removeClass('selected');
                    }
                    $(parent).addClass('selected');

                    //Get info start here
                    var link_into = $(this).siblings('a').attr('data-link');
                    var media_title = $(this).siblings('a').attr('title');
                    var mid = $(this).siblings('a').attr('data-mid');
                    var img = "<img class='img-stand img-thumbnail' src='" + $(this).siblings('a').attr('data-img-m') + "' title='" + media_title + "'>";
                    $('#link_into').val(link_into);
                    $('#media_title').text(media_title);
                    $('#image_standing').html(img);

                    //Set MID Operation 
                    if ($(this).siblings('a').attr('data-type') === "img") {
                        $('#media_block_set_thumbnail').attr('data-mid', mid).show();
                    } else {
                        $('#media_block_set_thumbnail').hide();
                    }
                    $('#media_block_remove').attr('data-mid', mid);
                }

                var selected_total = $('.selected').length;


                if (selected_total > 0) {
                    $('#each_media_info').show();
                    $('#selected-number').html("<i class='glyphicon glyphicon-tags'></i>&nbsp;" + selected_total + " selected");
                    $('.list_of_selected').html("");
                    $('.selected').each(function(id, element) {
                        var info = $(element).children('a');
                        var ele;
                        var thumbnail;
                        var title = info.attr('title');
                        var image_present;
                        if (info.is('.height_enclose')) {
                            image_present = "height_enclose";
                        } else {
                            image_present = "width_enclose";
                        }

                        thumbnail = info.attr('data-thumbnail');
                        ele = "<div title='" + title + "' class='thumbnail_mini_media_block " + image_present + "' style='background-image:url(\"" + thumbnail + "\")'></div>";

                        $('.list_of_selected').append(ele);
                    });
                } else {
                    $('#each_media_info').hide();
                }

                return false;
            }
        });

        //prepare obj for insertion into post
        $('#media_block_insert_into_post').click(function() {
            var element = "";
            $('.selected').each(function(idx, ele) {
                var link = $(ele).children('a').attr('data-link');
                switch ($(ele).children('a').attr('data-type')) {
                    case 'img':
                        element += "<p><img width='450px' src='" + link + "' alt='" + $(ele).children('a').attr('title') + "' title='" + $(ele).children('a').attr('title') + "'></p>";
                        break;
                    case 'vdo':
                        element += "<p>";
                        element += "<video width='450' controls>";
                        element += "<source src='" + link + "' type='" + $(ele).children('a').attr('data-mime') + "'>";
                        element += "Your browser does not support the video tag.";
                        element += "</video>";
                        element += "</p>";
                        break;
                    default:
                        element += "<p><a href='" + link + "'>" + $(ele).children('a').attr('title') + "</a>";
                        break;
                }
            });
            var target_block = $('#target_block').val();
            add_html(target_block, element);
            $('#media_library').modal('hide');
        });

        //Set thumbnail
        $('#media_block_set_thumbnail').click(function() {
            $('#media_library').modal('hide');
            var mid = $(this).attr('data-mid');
            var target_element = $('#block_' + mid);

            var element = "<img src='" + $(target_element).children('a').attr('data-thumbnail') + "' class='img-thumbnail' style='margin-bottom:15px;'>";
            $('#show_thumbnail').html(element);

            //Set MID into fieldset
            $("[name=post_thumbnail]").val(mid);
            $('#set_thumbnail_sidebar_btn').hide();
            $('#remove_thumbnail_sidebar_btn').show();
        });

        //Remove thumbnail
        $('#remove_thumbnail_sidebar_btn').click(function() {
            $('#show_thumbnail').html("");
            $('#set_thumbnail_sidebar_btn').show();
            $('#remove_thumbnail_sidebar_btn').hide();
            $("[name=post_thumbnail]").val(0);
            return false;
        });

        $('#set_thumbnail_sidebar_btn').click(function() {
            $('#media_library').modal('show');
        });

        $('#media_block_remove').click(function() {
            if (confirm("Are you sure to delete this file?")) {
                var mid = $(this).attr('data-mid');
                $.post('<?php echo site_url('api/delete_media'); ?>', {mid: mid}, function(res) {
                    if (res.status === "success") {
                        $('#block_' + mid).remove();
                        $('#each_media_info').hide();
                    } else {
                        alert("Cannot process your request");
                    }
                }, 'json');
            }
        });

        $('#publish_btn').click(function() {
            $('.tab_home').text(get_content_from_editor('tab_home'));
            $('.tab_about').text(get_content_from_editor('tab_about'));
            $('.tab_research').text(get_content_from_editor('tab_research'));
            $('.tab_career').text(get_content_from_editor('tab_career'));
            $('.tab_collaboration').text(get_content_from_editor('tab_collaboration'));
            $('.tab_programmes').text(get_content_from_editor('tab_programmes'));

            $('#submit_form_hidden').trigger('click');
        });

        $('#add_post_form').ajaxForm({
            beforeSend: function() {
                $('#publish_btn').attr({'disabled': 'disabled'}).text('Updating...');
            },
            complete: function(response) {
                var json = $.parseJSON(response.responseText);
                if (json.status === "success") {
                    $("#status_report").modal();
                    $('#status_report').on('hidden.bs.modal', function(e) {
                        var temp_url = location.origin + location.pathname;
                        location.href = temp_url + "?post_id=" + json.post_id;
                    });
                } else {
                    alert('Cannot process your request');
                }
                $('#publish_btn').button('reset');
            }
        });
    });
</script>
<?php if ($reserv_post->post_thumbnail != 0): ?>    
    <script tyle="text/javascript">
        $(function() {
            $('#set_thumbnail_sidebar_btn').hide();
            $('#remove_thumbnail_sidebar_btn').show();
        });
    </script>
<?php endif; ?>

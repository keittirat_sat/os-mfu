<div class="row">
    <div class="col-md-4 col-lg-3">
        <?php if ($post_id): ?>        
            <div class="row margin_top_20">
                <div class="col-md-12">
                    <!--Force redirect-->
                    <div class="panel panel-mfu">
                        <div class="panel-heading"><i class="glyphicon glyphicon-link"></i> Force redirect</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <input type="text" name="link_redirect" id="link_redirect" placeholder="URL" class="form-control">
                            </div>

                            <div class="form-group txt_right" style="margin-bottom: 0px">
                                <button class="btn btn-default btn-sm btn_add_to_list" type="submit" data-target="link_redirect"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                            </div>
                        </div>
                    </div>
                    <!--/Force redirect-->

                    <!--Group Label-->
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="glyphicon glyphicon-info-sign"></i> Group label</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <input type="text" name="group_label" id="group_label" placeholder="Label" class="form-control">
                            </div>

                            <div class="form-group txt_right" style="margin-bottom: 0px">
                                <button class="btn btn-default btn-sm btn_add_to_list" type="submit" data-target="group_label"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                            </div>
                        </div>
                    </div>
                    <!--/Group Label-->

                    <!--Other service-->
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="glyphicon glyphicon-gbp"></i> Other</div>
                        <div class="panel-body">
                            <div class="cover_list_menu" id="etc_list_side_bar">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" data-title="School" data-id="0" data-type="other"> School
                                    </label>
                                </div>
                            </div>
                            <p class="txt_right" style="margin: 0px;">
                                <button class="btn btn-default btn-sm btn_add_to_list" type="submit" data-target="etc_list_side_bar"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                            </p>
                        </div>
                    </div>
                    <!--/Other service-->

                    <!--Page Section-->
                    <div class="panel panel-warning">
                        <div class="panel-heading"><i class="glyphicon glyphicon-file"></i> Page</div>
                        <div class="panel-body">
                            <?php $page = get_all_post_by_type(TYPE_PAGE); ?>
                            <div class="cover_list_menu" id="page_list_side_bar">
                                <?php foreach ($page as $each_page): ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" data-title="<?php echo $each_page->post_title; ?>" data-id="<?php echo $each_page->post_id; ?>" data-type="page"> <?php echo $each_page->post_title; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <p class="txt_right" style="margin: 0px;">
                                <button class="btn btn-default btn-sm btn_add_to_list" type="submit" data-target="page_list_side_bar"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                            </p>
                        </div>
                    </div><!--/.Page Section-->

                    <!--Category Section-->
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-list-alt"></i> Category
                        </div>
                        <div class="panel-body">
                            <?php $cate = get_all_category(); ?>
                            <div class="cover_list_menu" id="category_list_side_bar">
                                <?php foreach ($cate as $each_cate): ?>
                                    <div class="checkbox <?php echo $each_cate->api_id != 0 ? "green" : ""; ?>">
                                        <label>
                                            <input type="checkbox" data-title="<?php echo $each_cate->cate_name; ?>" data-id="<?php echo $each_cate->cate_id; ?>" data-type="category"> <?php echo $each_cate->cate_name; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <p class="txt_right" style="margin: 0px;">
                                <button class="btn btn-default btn-sm btn_add_to_list" type="submit" data-target="category_list_side_bar"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                            </p>
                        </div>
                    </div><!--/.Category Section-->

                    <!--Group link Section-->
                    <div class="panel panel-success">
                        <div class="panel-heading"><i class="glyphicon glyphicon-link"></i> External link group</div>
                        <div class="panel-body">
                            <?php $page = get_all_post_by_type(TYPE_MENUGROUP); ?>
                            <div class="cover_list_menu" id="external_list_side_bar">
                                <?php foreach ($page as $each_page): ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" data-title="<?php echo $each_page->post_title; ?>" data-id="<?php echo $each_page->post_id; ?>" data-type="external_link"> <?php echo $each_page->post_title; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <p class="txt_right" style="margin: 0px;">
                                <button class="btn btn-default btn-sm btn_add_to_list" type="submit" data-target="external_list_side_bar"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                            </p>
                        </div>
                    </div><!--/.Group link Section-->
                </div> 
            </div>
        <?php endif; ?>
    </div>
    <div class="col-md-8 col-lg-9">
        <div class="margin_top_20">
            <label>Audience's group</label>
            <div class="row">
                <div class='col-lg-5'>
                    <select class="form-control" id="menu_list">
                        <?php if (count($all_link)): ?>
                            <?php foreach ($all_link as $each_link): ?>
                                <option value="<?php echo $each_link->post_id; ?>" <?php echo ($each_link->post_id == $post_id) ? "selected" : ""; ?>><?php echo $each_link->post_title; ?></option>
                                <?php if ($each_link->post_id == $post_id): ?>
                                    <?php $group_title = $each_link->post_title; ?>
                                    <?php $content = $each_link->post_detail; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <option value='-1'>--- No available group ---</option>
                        <?php endif; ?>
                    </select>
                </div>

                <button class='btn btn-primary' <?php echo (!$post_id) ? "disabled" : ""; ?> id="btn_select_menu">Select</button>
                <button class='btn btn-danger' id='create_new_group'>Create new group</button>
            </div>
            <div class="row margin_top_10">
                <div class='col-md-12'>
                    <?php if ($post_id): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-bullhorn"></i> Group management : <b><i id="nav_name"><?php echo $group_title; ?></i></b>
                                <?php if (count($post_id)): ?>
                                    <button class="btn btn-default btn-xs pull-right" id="btn_delete_menu"><i class="glyphicon glyphicon-trash"></i> Delete this group</button>
                                <?php endif; ?>
                            </div>
                            <div class="panel-body">
                                <?php if ($post_id): ?>
                                    <!--Sorting panel-->
                                    <ul class="sortable">
                                        <?php if (trim($content)): ?>
                                            <?php $list_link = json_decode($content); ?>
                                            <?php foreach ($list_link as $index => $link_each): ?>
                                                <?php $class = "ui-state-default"; ?>
                                                <?php if ($link_each->menu_parent_id != -1): ?>
                                                    <?php $class .= " sub-menu_lv1"; ?>
                                                <?php endif; ?>
                                                <?php switch ($link_each->type): case 'category': ?>
                                                        <?php $panel = "panel-info"; ?>
                                                        <?php break; ?>
                                                    <?php case 'external_link': ?>
                                                        <?php $panel = "panel-success"; ?>
                                                        <?php break; ?>
                                                    <?php case 'page': ?>
                                                        <?php $panel = "panel-warning"; ?>
                                                        <?php break; ?>
                                                    <?php case 'redirect': ?>
                                                        <?php $panel = "panel-mfu"; ?>
                                                        <?php break; ?>
                                                    <?php default: ?>
                                                        <?php $panel = "panel-default"; ?>
                                                        <?php break; ?>
                                                <?php endswitch; ?>
                                                <li class="ux_tc <?php echo $class; ?>">
                                                    <div class="panel-group" id="accordion_<?php echo $index; ?>">
                                                        <div class="panel <?php echo $panel; ?>">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $index; ?>">
                                                                        <span class="_sub-menu"><i class="glyphicon glyphicon-indent-left"></i>&nbsp;</span>
                                                                        <span class="tc_label"><?php echo $link_each->title; ?></span>
                                                                        <span class="test_post"></span>
                                                                        <i class="caret pull-right caret_menu_cutom"></i>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse_<?php echo $index; ?>" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal form_menu_config">
                                                                        <div class="form-group">
                                                                            <label for="link_title__<?php echo $index; ?>" class="col-sm-2 control-label">Link label</label>
                                                                            <div class="col-sm-10">
                                                                                <input type="text" class="form-control input-sm" id="link_title__<?php echo $index; ?>" placeholder="Link label" value="<?php echo $link_each->title; ?>" name="link_title">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="link_id__<?php echo $index; ?>" class="col-sm-2 control-label">Ref ID</label>
                                                                            <div class="col-sm-10">
                                                                                <input type="text" class="form-control input-sm" id="link_to__<?php echo $index; ?>" placeholder="Ref ID" value="<?php echo $link_each->ref_id; ?>" name="link_id" readonly>
                                                                                <input type="hidden" class="menu_type" value="<?php echo $link_each->type; ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-10 col-sm-offset-2">
                                                                                <button class="btn btn-danger btn-sm del_menu"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul> 
                                    <!--./Sorting panel-->
                                <?php else: ?>
                                    <h3 class="txt_center" style="color: #999;">Please create a new group.</h3>
                                <?php endif; ?>
                            </div>
                            <div class="panel-footer txt_right">
                                <button class="btn btn-danger btn-sm" id="update_list_group" data-loading-text="Updating..." ><i class="glyphicon glyphicon-save"></i> Update group</button>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" value="<?php echo $post_id; ?>" name="post_id" id="post_id_menu">

<div class="modal fade" id='add_group'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Add new Audience group</h3>
            </div>
            <div class="modal-body">
                <form role='form' action='<?php echo site_url('api/add_audience'); ?>' method="post" id="form_add_link">
                    <div class='form-group' style="margin-bottom: 0px;">
                        <label for='group_name_modal'>Group name <span id="error_report"></span></label>
                        <input type='text' name='group_name' id='group_name_modal' class='form-control' placeholder="Group name" required="">
                        <input type="submit" id="submit_form_add_new_group" style="display: none;">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger btn-sm" id="save_group" data-loading-text="Adding...">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="status_report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="status_txt txt_center margin_top_20 margin_bottom_20">One fine body&hellip;</h4>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type='text/javascript'>
    var counter_id = 0;

    function get_element_index(index) {
        var take = null;
        $('.sortable li').each(function(id, element) {
            if (index === id) {
                take = element;
            }
        });

        return take;
    }

    function isValidURL(url) {
        var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

        if (RegExp.test(url)) {
            return true;
        } else {
            return false;
        }
    }

    function sub_lv1(target_element) {
        $(target_element).removeClass('sub-menu_lv2').addClass('sub-menu_lv1');
        $('.ui-sortable-placeholder').removeClass('sub-menu_lv2').addClass('sub-menu_lv1');
    }

    function root_menu(target_element) {
        $(target_element).removeClass('sub-menu_lv1').removeClass('sub-menu_lv2');
        $('.ui-sortable-placeholder').removeClass('sub-menu_lv1').removeClass('sub-menu_lv2');
    }

    function generate_element(title, ref_id, type) {
        var element;
        counter_id++;
        var next_total = new Date();
        var random_id = next_total.getTime() + counter_id;
        element = "<li class='ux_tc ui-state-default'>";
        element += "<div class='panel-group' id='accordion" + random_id + "'>";
        switch (type) {
            case 'page':
                element += "<div class='panel panel-warning'>";
                break;
            case 'category':
                element += "<div class='panel panel-info'>";
                break;
            case 'external_link':
                element += "<div class='panel panel-success'>";
                break;
            case 'redirect':
                element += "<div class='panel panel-mfu'>";
                break;
            default:
                element += "<div class='panel panel-default'>";
                break;
        }
        element += "<div class='panel-heading'>";
        element += "<h4 class='panel-title'>";
        element += "<a data-toggle='collapse' data-parent='#accordion' href='#collapse" + random_id + "'>";
        element += "<span class='_sub-menu'><i class='glyphicon glyphicon-indent-left'></i>&nbsp;</span><span class='tc_label'>" + title + "</span> <span class='test_post'></span> <i class='caret pull-right caret_menu_cutom'></i>";
        element += "</a>";
        element += "</h4>";
        element += "</div>";
        element += "<div id='collapse" + random_id + "' class='panel-collapse collapse'>";
        element += "<div class='panel-body'>";

        //Inside Element
        element += "<div class='form-horizontal form_menu_config'>";

        //First Line
        element += "<div class='form-group'>";
        element += "<label for='link_title_" + random_id + "' class='col-sm-2 control-label'>Link label</label>";
        element += "<div class='col-sm-10'>";
        element += "<input type='text' class='form-control input-sm' id='link_title_" + random_id + "' placeholder='Link label' value='" + title + "' name='link_title'>";
        element += "</div>";
        element += "</div>";

        //Second Line
        element += "<div class='form-group'>";
        element += "<label for='link_id_" + random_id + "' class='col-sm-2 control-label'>Ref ID</label>";
        element += "<div class='col-sm-10'>";

        element += "<input type='text' class='form-control input-sm' id='link_id_" + random_id + "' placeholder='URL' value='" + ref_id + "' name='link_id' readonly>";

        element += "<input type='hidden' class='menu_type' value='" + type + "'>";
        element += "</div>";
        element += "</div>";

        //Third Line - Submit form
        element += "<div class='form-group'>";
        element += "<div class='col-sm-10 col-sm-offset-2'>";

        //Submit - form
        element += "<button class='btn btn-danger btn-sm del_menu'><i class='glyphicon glyphicon-trash'></i>&nbsp;Delete</button>";

        element += "</div>";
        element += "</div>";
        element += "</div>";

        element += "</div>";
        element += "</div>";
        element += "</div>";
        element += "</div>";
        element += "</li>";
        return element;
    }

    function redirect_url(post_id) {
        var redirect = location.origin + location.pathname;
        redirect += "?post_id=" + post_id;
        location.href = redirect;
    }

    $(function() {
        var target_element;
        counter_id = 0;
        //Retrieve element Ids
        $('.ux_tc').live({
            mousedown: function() {
                target_element = $(this);
            }
        });
        $('#create_new_group').click(function() {
            $('#add_group').modal();
        });
        $('#save_group').click(function() {
            $('#submit_form_add_new_group').trigger('click');
        });
        var save_group_btn;
        $('#form_add_link').ajaxForm({
            beforeSend: function() {
//                $('#save_group').button('loading');
                save_group_btn = $('#save_group').html();
                $('#save_group').html($('#save_group').attr('data-loading-text')).attr('disabled', 'disabled');
                $('#error_report').text("- Processing your request").css({'color': 'gray'});
            },
            complete: function(xhr) {
                var raws = xhr.responseText;
                console.log(raws);
                var json = $.parseJSON(raws);
                if (json.status === "success") {
                    $('#error_report').text("- Success").css({'color': 'green'});
                    $('#save_group').html('Added');
                    setTimeout(function() {
                        redirect_url(json.post_id);
                    }, 1000);
                } else {
                    $('#save_group').html('Error');
                    $('#error_report').text("- Cannot process your request").css({'color': 'red'});
                    setTimeout(function() {
                        $('#save_group').html(save_group_btn).removeAttr('disabled');
                    }, 1000);
                }
            }
        });
        $('.sortable').sortable({
            revert: true,
            cursorAt: {left: 20},
            update: function(event, ui) {

                //First index rules
                var first_index = get_element_index(0);
                root_menu(first_index);
                //Sort check rule
                var old = false;
                $('.sortable li').each(function(idx, ele) {
                    if (!$(ele).is('.ui-sortable-helper')) {
                        if (old) {
//                            console.log($(old).find('.tc_label').text(), $(ele).find('.tc_label').text());
                            if (!$(old).is('.sub-menu_lv1') && !$(old).is('.sub-menu_lv2')) {
                                //Main menu
                                if ($(ele).is('.sub-menu_lv2')) {
                                    sub_lv1(ele);
                                }
                            }
                        }
                    }
                    old = ele;
                });
            },
            over: function(event, ui) {
                var compare_index = $('.ui-sortable-placeholder').index();
                var selected_index = $(target_element).index();
                if (compare_index === 0 || selected_index === 0) {
                    //Top-menu
                    $(target_element).css({'margin-left': 0});
                    $('.ui-sortable-placeholder').css({'margin-left': 0});
                } else {
                    //Second Menu
                    var mark_compare = get_element_index(compare_index - 2);
                    if (ui.position.left > 30) {
                        sub_lv1(target_element);
                    } else {
                        root_menu(target_element);
                    }
                }
            }
        });
        $('#btn_select_menu').click(function() {
            var post_id = $('#menu_list').val();
            $(this).attr('disabled', 'disabled');
            $(this).text('Swapping...');
            setTimeout(function() {
                redirect_url(post_id);
            }, 1000);
        });

        $('#update_list_group').click(function() {

            var btn_txt = $(this).html();
            $(this).html($(this).attr('data-loading-text')).attr('disabled', 'disabled');

            var root, lv1;
            var dataset = [];
            $('.ux_tc').each(function(idx, ele) {
                //Generate ids.
                $(ele).attr('menu-id', idx);

                var atom = {};
                atom['menu_id'] = idx;
                atom['title'] = $(ele).find('[name=link_title]').val();
                atom['ref_id'] = $(ele).find('[name=link_id]').val();
                atom['type'] = $(ele).find('.menu_type').val();

                //Generate tree relation
                if (!$(ele).is('.sub-menu_lv1') && !$(ele).is('.sub-menu_lv2')) {
                    //Root
                    root = idx;
                    $(ele).attr('menu-parent-id', -1);
                    atom['menu_parent_id'] = -1;
                } else if ($(ele).is('.sub-menu_lv1')) {
                    //Sub-menu_lv1
                    lv1 = idx;
                    $(ele).attr('menu-parent-id', root);
                    atom['menu_parent_id'] = root;
                } else if ($(ele).is('.sub-menu_lv2')) {
                    //Sub-menu_lv2
                    $(ele).attr('menu-parent-id', lv1);
                    atom['menu_parent_id'] = lv1;
                }

                dataset.push(atom);

            });

            console.log(dataset);

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('api/update_audience'); ?>",
                data: {'dataset': dataset, post_id: $('#post_id_menu').val()},
                dataType: "html",
                cache: false,
                success: function(res) {
                    var json = $.parseJSON(res);
                    console.log(json);
                    if (json.status === "success") {
                        $('.status_txt').text('Group update successful');
                    } else {
                        $('.status_txt').text('Cannot process your request');
                    }
                    $('#status_report').modal();
                    $('#update_list_group').html(btn_txt).removeAttr('disabled');
                },
                failure: function(res) {
                    $('.status_txt').text('Internal error');
                    $('#status_report').modal();
                    $('#update_list_group').html(btn_txt).removeAttr('disabled');
                }
            });
        });

        $('#btn_delete_menu').click(function() {
            if (confirm('Do you really want to delete this: ' + $('#nav_name').text() + '?')) {
                var post_id = $('#post_id_menu').val();
                var html = $(this).html();
                var btn = $(this);
                $(btn).attr('disabled', 'disabled').html('Deleting...');
                $.post('<?php echo site_url('api/delete_post'); ?>', {'trucate': true, 'post_id': post_id}, function(res) {
                    if (res.status === "success") {
                        var url = location.origin + location.pathname;
                        location.href = url;
                    } else {
                        $(btn).html('Cannot process your request: delete fail');
                        setTimeout(function() {
                            $(btn).removeAttr('disabled').html(html);
                        }, 1500);
                    }
                }, 'json');
            }
        });

        $('.del_menu').live({
            click: function() {
                var current = $(this).parents('li.ux_tc');
                $(current).fadeOut(400, function() {
                    $(current).remove();
                });

                //Re-Adsjust
                if (!$(current).is('.sub-menu_lv1') && $(current).next().is('.sub-menu_lv1')) {
                    root_menu($(current).next());
                }
            }
        });

        $('.btn_add_to_list').click(function() {
            var target_id = $(this).attr('data-target');
            if (target_id === "link_redirect") {
                if (confirm('Are you sure ? all record will be removed and replace with force redirect record')) {
                    var html = generate_element($('#' + target_id).val(), "0", "redirect");
                    $('.sortable').html(html);
                }
            } else if (target_id === "group_label") {
                var html = generate_element($('#' + target_id).val(), "0", target_id);
                $('.sortable').append(html);
            } else {
                $('#' + target_id).find('input[type=checkbox]:checked').each(function(id, ele) {
                    var html = generate_element($(ele).attr('data-title'), $(ele).attr('data-id'), $(ele).attr('data-type'));
                    $(ele).removeAttr('checked');
                    $('.sortable').append(html);
                });
            }
        });
    });
</script>
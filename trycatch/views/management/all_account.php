<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-stacked nav-pills">
                <li <?php echo $level == 0 ? "class='active'" : ""; ?>><a href="?level=0">All User</a></li>
                <li <?php echo $level == USER_ADMIN ? "class='active'" : ""; ?>><a href="?level=<?php echo USER_ADMIN ?>">Admin</a></li>
                <li <?php echo $level == USER_DIRECTOR ? "class='active'" : ""; ?>><a href="?level=<?php echo USER_DIRECTOR ?>">Director</a></li>
                <li <?php echo $level == USER_WRITER ? "class='active'" : ""; ?>><a href="?level=<?php echo USER_WRITER ?>">Writer</a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <?php if (get_user_level() != USER_WRITER): ?>
                <p class="txt_right"><a href="<?php echo site_url("management/new_account"); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-user"></i> New user</a></p>
            <?php endif; ?>

            <?php foreach ($users as $each_user): ?>
                <?php switch ($each_user->level): case USER_ADMIN: ?>
                        <?php $c = "panel-danger"; ?>
                        <?php $l = "Administrator"; ?>
                        <?php break; ?>
                    <?php case USER_DIRECTOR: ?>
                        <?php $c = "panel-success"; ?>
                        <?php $l = "Director"; ?>
                        <?php break; ?>
                    <?php case USER_WRITER: ?>
                        <?php $c = "panel-default"; ?>
                        <?php $l = "Writer"; ?>
                        <?php break; ?>
                <?php endswitch; ?>
                <div class="panel <?php echo $c ?>">
                    <div class="panel-heading"><?php echo $each_user->status == 1 ? "" : "<i style='color: #ff0000;' class='glyphicon glyphicon-ban-circle'></i>"; ?>&nbsp;<?php echo $each_user->real_name; ?></div>
                    <div class="panel-body">
                        <p><b>Name: </b> <?php echo $each_user->real_name; ?></p>

                        <p><b>Email: </b> <?php echo $each_user->email; ?></p>

                        <p><b>Level: </b> <a href="?level=<?php echo $each_user->level; ?>"><?php echo $l; ?></a></p>

                        <p><b>Total Post: </b><a href="<?php echo site_url("management/all_post?users={$each_user->uid}"); ?>"><?php echo $each_user->total_post; ?></a></p>

                        <p><b>Total Page: </b><a href="<?php echo site_url("management/all_page?users={$each_user->uid}"); ?>"><?php echo $each_user->total_page; ?></a></p>
                    </div>
                    <div class="panel-footer txt_right">
                        <a class="btn btn-sm btn-default" href="<?php echo site_url('management/edit_account?uid=' . $each_user->uid); ?>"><i class="glyphicon glyphicon-edit"></i> Edit</a>
        <!--                        <button class="btn btn-sm btn-success" data-uid="<?php echo $each_user->uid ?>"><i class="glyphicon glyphicon-check"></i> Make it Online</button>
                            <button class="btn btn-sm btn-default" data-uid="<?php echo $each_user->uid ?>"><i class="glyphicon glyphicon-ban-circle"></i> Temporary</button>-->
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
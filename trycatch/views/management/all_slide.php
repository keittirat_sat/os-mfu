<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-map-marker"></i>&nbsp;Slide status</div>
                <div class="panel-body">
                    <ul class="nav nav-stacked nav-pills">
                        <li <?php echo $status == "allslide" ? "class='active'" : ""; ?>>
                            <a href="?status=allslide">All Slide</a>
                        </li>
                        <li <?php echo $status == "online" ? "class='active'" : ""; ?>>
                            <a href="?status=online">Online</a>
                        </li>
                        <li <?php echo $status == "offline" ? "class='active'" : ""; ?>>
                            <a href="?status=offline">Expired</a>
                        </li>
                    </ul>
                </div>
            </div>

            <form action="<?php echo site_url('api/upload_slide'); ?>" method="post" class="form-horizontal" id="slide_add_form">

                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-picture"></i>&nbsp;Custom slide</div>
                    <div class="panel-body">                    
                        <label for="post_title">Caption</label>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <textarea id="post_title" name="post_title" class="form-control" placeholder="caption here" required="required"></textarea>
                            </div>
                        </div>

                        <label for="post_excerp">URL</label>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="text" name="post_excerp" class="form-control" id="post_excerp" placeholder="<?php echo site_url(); ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="post_date" value="<?php echo date('U'); ?>" id="post_date_id">
                <input type="hidden" name="sch_start" value="0" id="sch_start">
                <input type="hidden" name="sch_exp" value="0" id="sch_exp">
                <input type="hidden" name="post_type" value="<?php echo TYPE_SLIDE ?>">
                <input type="hidden" name="post_status" value="<?php echo POST_PUBLISH ?>">
                <input type="submit" id="submit_news_btn" style="display: none;">

                <!--Scheduling-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="checkbox  margin_bottom_0 margin_top_0">
                            <label>
                                <input type="checkbox" for="sch_start" original-data="0" name="enable_schedule">
                                Start date
                            </label>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="datepicker_mfu" for="sch_start" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy"></div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="checkbox margin_bottom_0 margin_top_0">
                            <label>
                                <input type="checkbox" for="sch_exp" original-data="0" name="hidden_expire">
                                Expire date
                            </label>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="datepicker_mfu" for="sch_exp" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy"></div>
                    </div>
                </div>
                <!--/Scheduling-->

                <div class="panel panel-default">
                    <div class="form-group txt_center margin_bottom_10 margin_top_10">
                        <div class="col-xs-12">
                            <button class="btn btn-success upload_btn" type="button">
                                <i class="glyphicon glyphicon-upload"></i>
                                Upload
                                <input type="file" name="img_upload" accept=".jpg, .png" id="upload_btn">
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-9">
            <section name="search box" class="margin_bottom_20">
                <form>
                    <div class="input-group  input-group-sm ">
                        <!--status-->
                        <div class="input-group-btn">                            
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="replace_here">All Slide</span>
                                <span class="caret"></span>
                            </button>
                            <ul role="menu" class="dropdown-menu search_list" data-target="status">
                                <li <?php echo $status == "allslide" ? "class='active'" : ""; ?>><a href="#0" data-value="allslide">All Slide</a></li>
                                <li <?php echo $status == "online" ? "class='active'" : ""; ?>><a href="#online" data-value="online">Online</a></li>
                                <li <?php echo $status == "offline" ? "class='active'" : ""; ?>><a href="#offline" data-value="offline">Expired</a></li>
                            </ul>
                        </div><!--status-->

                        <!--type_list-->
                        <div class="input-group-btn">                            
                            <button type="button" class="btn btn-default dropdown-toggle border_radius_0 margin_right_minus_1" data-toggle="dropdown">
                                <span class="replace_here">All Type</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu search_list" data-target="type">
                                <li <?php echo $type == 0 ? "class='active'" : ""; ?>><a href="#0" data-info="0">All Type</a></li>
                                <li <?php echo $type == TYPE_POST ? "class='active'" : ""; ?>><a href="#post_slide" data-value="<?php echo TYPE_POST ?>">Post Thumbnail</a></li>
                                <li <?php echo $type == TYPE_SLIDE ? "class='active'" : ""; ?>><a href="#custom_slide" data-value="<?php echo TYPE_SLIDE ?>">Custom slide</a></li>
                            </ul>
                        </div><!--type_list-->

                        <!--month-->
                        <div class="input-group-btn">                            
                            <button type="button" class="btn btn-default dropdown-toggle border_radius_0 margin_right_minus_1" data-toggle="dropdown">
                                <span class="replace_here"><?php echo "All Month"; ?></span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu search_list" data-target="month">
                                <?php foreach ($locale_month['en'] as $i => $each_month): ?>
                                    <li <?php echo $month == $i ? "class='active'" : ""; ?>><a href="<?php echo "#{$i}"; ?>" data-value="<?php echo $i; ?>"><?php echo $i == 0 ? "All Month" : $each_month ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div><!--month-->

                        <!--year-->
                        <div class="input-group-btn">      
                            <button type="button" class="btn btn-default dropdown-toggle border_radius_0 margin_right_minus_1" data-toggle="dropdown">
                                <span class="replace_here"><?php echo "All Year" ?></span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu search_list" data-target="year">
                                <li <?php echo $year == 0 ? "class='active'" : ""; ?>><a href="<?php echo "#0"; ?>" data-year="0"><?php echo "All Year" ?></a></li>
                                <?php foreach ($group_year as $each_year): ?>
                                    <li <?php echo $year == $each_year->year ? "class='active'" : ""; ?>><a href="<?php echo "#{$each_year->year}"; ?>" data-value="<?php echo $each_year->year; ?>"><?php echo $i == 0 ? lang("ทุกปี") : $each_year->year ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div><!--year-->

                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>" placeholder="Search in this section">
                        <span class="input-group-btn">
                            <button class="btn btn-default"><i class=" glyphicon glyphicon-search"></i></button>
                        </span>
                    </div>

                    <!--hidden element-->
                    <input type="hidden" name="status" value="<?php echo $status ?>">
                    <input type="hidden" name="type" value="<?php echo $type ?>">
                    <input type="hidden" name="month" value="<?php echo $month ?>">
                    <input type="hidden" name="year" value="<?php echo $year ?>">
                    <!--hidden element-->
                </form>
            </section>

            <section name="display_area">

                <?php if (count($slide)): ?>
                    <?php $today = date("U"); ?>
                    <?php foreach ($slide as $each_slide): ?>
                        <div class='panel <?php echo $each_slide->post_type == TYPE_POST ? "panel-default" : "panel-info"; ?>' id="<?php echo "post_" . $each_slide->post_id ?>">
                            <div class='panel-heading'>

                                <span class='panel-title <?php echo $each_slide->sch_exp < $today ? "red" : ""; ?>'><i class='glyphicon glyphicon-picture'></i>&nbsp;<?php echo $each_slide->post_title; ?> <?php echo $each_slide->sch_exp < $today ? "[EXPIRED]" : ""; ?></span>

                            </div>
                            <div class='panel-body'>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <?php $img = json_decode($each_slide->link); ?>
                                        <img src='<?php echo $img->l; ?>' class='img-responsive' id="<?php echo "slide_{$each_slide->post_id}_thumb"; ?>">
                                    </div>
                                    <div class="col-xs-8">
                                        <label>Caption</label>
                                        <p><?php echo $each_slide->post_title; ?></p>

                                        <label>URL</label>
                                        <?php if ($each_slide->post_type == TYPE_POST): ?>
                                            <?php $link = site_url("mfu/post/{$each_slide->post_id}/" . slug($each_slide->post_title)); ?>
                                        <?php else: ?>
                                            <?php $link = $each_slide->post_excerp ?>
                                        <?php endif; ?>
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $link; ?>" class="form-control" readonly="readonly">
                                        </div>

                                        <label>Post date</label>
                                        <p><?php echo date("d-m-Y", $each_slide->post_date) ?></p>                                        

                                        <?php if ($each_slide->sch_start != 0): ?>
                                            <label>Start date</label>
                                            <p><?php echo date("d-m-Y", $each_slide->sch_start) ?></p>
                                        <?php endif; ?>

                                        <?php if ($each_slide->sch_exp != 0): ?>
                                            <label>End date</label>
                                            <p><?php echo date("d-m-Y", $each_slide->sch_exp) ?></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class='panel-footer txt_right'>
                                <?php if ((get_user_level() != USER_WRITER || get_user_uid() == $each_slide->post_author)): ?>
                                    <?php if ($each_slide->post_type == TYPE_POST): ?>
                                        <!--Edit with post operation-->
                                        <a href="<?php echo site_url("management/post_operation?post_id={$each_slide->post_id}"); ?>" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <?php else: ?>
                                        <!--Edit with live custom operation-->
                                        <button class="btn btn-xs btn-info live_edit" data-hidden="<?php echo "slide_{$each_slide->post_id}"; ?>" data-toggle="modal" data-target="#edit_slide_custom"><i class="glyphicon glyphicon-adjust"></i>&nbsp;Edit</button>
                                        <input type="hidden" class="<?php echo "slide_{$each_slide->post_id}"; ?>" data-type="post_title" value="<?php echo $each_slide->post_title ?>">
                                        <input type="hidden" class="<?php echo "slide_{$each_slide->post_id}"; ?>" data-type="post_id" value="<?php echo $each_slide->post_id ?>">
                                        <input type="hidden" class="<?php echo "slide_{$each_slide->post_id}"; ?>" data-type="post_excerp" value="<?php echo $each_slide->post_excerp ?>">
                                        <input type="hidden" class="<?php echo "slide_{$each_slide->post_id}"; ?>" data-type="sch_start" value="<?php echo $each_slide->sch_start ?>">
                                        <input type="hidden" class="<?php echo "slide_{$each_slide->post_id}"; ?>" data-type="sch_exp" value="<?php echo $each_slide->sch_exp ?>">                                    <?php endif; ?>
                                <?php endif; ?>
                                <button class='btn btn-danger btn-xs del_btn' data-post_id='<?php echo $each_slide->post_id; ?>' data-post_type="<?php echo $each_slide->post_type; ?>"><i class='glyphicon glyphicon-minus-sign'></i> Remove</button>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <script type="text/javascript">
                        function setup_livebox(target_id, unix) {
                            unix = parseInt(unix) * 1000;
                            if (unix > 0) {
                                var time = new Date(unix);
                                $('.' + target_id).find("[type=checkbox]").attr("checked", "checked");
                                $.each($('.' + target_id + " select"), function(idx, ele) {
                                    switch ($(ele).attr('for')) {
                                        case 'day':
                                            $(this).val(time.getDate());
                                            break;
                                        case 'month':
                                            $(this).val(time.getMonth() + 1);
                                            break
                                        case 'year':
                                            $(this).val(time.getFullYear());
                                            break
                                    }
                                });
                            } else {
                                $('.' + target_id).find("[type=checkbox]").removeAttr("checked");
                            }
                        }

                        function control_operation(this_ele) {
                            var parent_element = $(this_ele).parent().find('select');
                            var day, month, year;
                            day = month = year = 0;
                            $.each($(parent_element), function(i, sl) {
                                var p = $(sl).attr('for');
                                var v = parseInt($(sl).val());
                                switch (p) {
                                    case 'day':
                                        day = v;
                                        break;
                                    case 'month':
                                        month = v - 1;
                                        break;
                                    case 'year':
                                        year = v;
                                        break;
                                }
                            });

                            var date = new Date(year, month, day);
                            if (date.getMonth() == month) {
                                $(this_ele).parent().removeClass('has-error');
                                var unix = date.getTime();
                                unix /= 1000;
                                unix = unix.toString();

                                if ($(this_ele).parents('.input-group').is('.start_date_slide')) {
                                    if ($('.start_date_slide [type=checkbox]').is(":checked")) {
                                        $('#startDate_live').val(unix);
                                        console.log("startDate", unix);
                                        $('.start_date_slide').removeClass('has-error');
                                    }
                                } else {
                                    if ($('.end_date_slide [type=checkbox]').is(":checked")) {
                                        $('#endDate').val(unix);
                                        console.log("endDate", unix);
                                        $('.end_date_slide').removeClass('has-error');
                                    }
                                }

                                if ($('.end_date_slide [type=checkbox]').is(":checked")) {
                                    if ($('#startDate_live').val() > $('#endDate').val()) {
                                        if (!$('.end_date_slide').is('.has-error')) {
                                            $('.end_date_slide').addClass('has-error');
                                        }

                                        if (!$('.start_date_slide').is('.has-error')) {
                                            $('.start_date_slide').addClass('has-error');
                                        }
                                    } else {
                                        if ($('.end_date_slide').is('.has-error')) {
                                            $('.end_date_slide').removeClass('has-error');
                                        }

                                        if ($('.start_date_slide').is('.has-error')) {
                                            $('.start_date_slide').removeClass('has-error');
                                        }
                                    }
                                }
                            } else {
                                $(this_ele).parent().addClass('has-error');
                            }
                        }

                        function date_live_box_control(ele) {
                            $(ele).change(function() {
                                control_operation($(this));
                            });
                        }

                        $(function() {
                            $('.del_btn').click(function() {
                                if (confirm('Do you want to remove this slide ?')) {
                                    var post_id = $(this).attr('data-post_id');
                                    $('#post_' + post_id).fadeOut(400, function() {
                                        $(this).remove();
                                    });

                                    $.post('<?php echo site_url('api/remove_slide'); ?>', {post_id: post_id}, function(res) {
                                        if (res.status === "success") {
                                            $('#post_' + post_id).fadeOut(400, function() {
                                                $(this).remove();
                                            });
                                        }
                                    }, 'json');
                                }
                            });

                            $('.live_edit').click(function() {
                                var post_id = $(this).attr("data-hidden");

                                //setup image data
                                var img_ori_url = $('#' + post_id + "_thumb").attr("src");
                                $('#preview_image').attr('src', img_ori_url);

                                $.each($("." + post_id), function(idx, ele) {
                                    var value = $(ele).val();
                                    var target_id = $(ele).attr("data-type");
                                    switch (target_id) {
                                        case 'sch_start':
                                        case 'sch_exp':
                                            if (target_id == "sch_start") {
                                                setup_livebox("start_date_slide", value);
                                            } else {
                                                setup_livebox("end_date_slide", value);
                                            }
                                            value = value.toString();
                                        default:
                                            $('#live_edit_form [name=' + target_id + ']').val(value);
                                            break;
                                    }
                                });
                            });

                            $('#new_thumb').change(function() {
                                console.log("active");
                                var input = this;
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();
                                    console.log(input.files);

                                    reader.onload = function(e) {
                                        $('#preview_image').attr('src', e.target.result);
                                    };

                                    reader.readAsDataURL(input.files[0]);
                                }
                            });

                            $('.start_date_slide [type=checkbox]').change(function() {
                                if ($(this).is(':checked')) {
                                    $.each($('.start_date_slide select'), function(idx, ele) {
                                        control_operation(ele);
                                    });
                                } else {
                                    $('#startDate_live').val(0);
                                }
                            });

                            $('.end_date_slide [type=checkbox]').change(function() {
                                if ($(this).is(':checked')) {
                                    $.each($('.end_date_slide select'), function(idx, ele) {
                                        control_operation(ele);
                                    });
                                } else {
                                    $('#endDate').val(0);
                                }
                            });

                            $.each($('.start_date_slide select, .end_date_slide select'), function(idx, ele) {
                                date_live_box_control(ele);
                            });
                        });

                        //date picker
                        $('.datepicker_mfu').datepicker().on('changeDate', function(e) {
                            var unix = e.date.getTime() / 1000;
                            var target = $(this).attr('for');
                            if (target === "sch_start") {
                                $('[name=enable_schedule]').attr('original-data', unix);
                                if ($('[name=enable_schedule]').is(':checked')) {
                                    $('#' + target).val(unix);
                                }
                            } else {
                                $('[name=hidden_expire]').attr('original-data', unix);
                                if ($('[name=hidden_expire]').is(':checked')) {
                                    $('#' + target).val(unix);
                                }

                                if (parseInt($('#sch_start').val()) >= parseInt($('#sch_exp').val())) {
                                    alert('End date Cannot equre or less than start date');
                                }
                            }
                        });

                        $('[name=enable_schedule], [name=hidden_expire]').change(function() {
                            var target = $(this).attr('for');
                            if ($(this).is(':checked')) {
                                $('#' + target).val($(this).attr('original-data'));
                            } else {
                                $('#' + target).val(0);
                            }
                        });

                        $('#upload_btn').click(function() {
                            if (!$(this).parents('form').find("[name=post_title]").val()) {
                                alert('Title cannot leave blank');
                                return false;
                            }
                        }).change(function() {
                            var pdffile = $(this).val();
                            if (pdffile != "") {
                                $('#submit_news_btn').trigger('click');
                            }
                        });

                        $('#slide_add_form').ajaxForm({
                            beforeSend: function() {
                                $('.upload_btn').attr('disabled', 'disabled').text('Uploading...');
                            },
                            complete: function(xhr) {
                                //                                console.log(xhr.responseText);
                                var json = $.parseJSON(xhr.responseText);
                                if (json.status === "success") {
                                    location.reload();
                                } else {
                                    alert("Cannot upload new post : Access denied");
                                }
                            }
                        });

                        $('#live_edit_form').ajaxForm({
                            beforeSend: function() {
                                $('.upload_btn_live').attr('disabled', 'disabled').text('Updating...');
                            },
                            complete: function(xhr) {
                                //                                console.log(xhr.responseText);
                                var json = $.parseJSON(xhr.responseText);
                                if (json.status === "success") {
                                    location.reload();
                                } else {
                                    alert("Cannot update slide");
                                }
                            }
                        });
                    </script>
                <?php else: ?>
                    <h1 class='txt_center th_san'><i class='glyphicon glyphicon-folder-open'></i> Don't found any slide</h1>
                <?php endif; ?>
            </section>
        </div>
    </div>
</div>

<!-- Modal -->
<form id="live_edit_form" method="post" action='<?php echo site_url("api/update_slide"); ?>'  enctype="multipart/form-data">
    <div class="modal fade" id="edit_slide_custom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="thumbnail"><img src="#" id="preview_image" class="img-responsive"></div>
                            <p>
                                <button class="btn btn-default" style="height: 34px; width: 120px; position: relative;">
                                    Change Image
                                    <input type="file" id="new_thumb" accept=".jpg, .png"  name="img_upload">
                                </button>                            
                            </p>
                        </div>
                        <div class="col-xs-8">
                            <label>Caption</label>
                            <p><input type="text" value="" name="post_title" class="form-control"></p>

                            <label>Url</label>
                            <p><input type="text" value="" name="post_excerp" class="form-control"></p>

                            <label>Start date</label>
                            <div class="input-group start_date_slide">
                                <div class="input-group-addon-mfu">
                                    <input type="checkbox">
                                </div>
                                <div class="input-group-btn" style="width: 325px;">
                                    <select class="form-control" style="width:109px; margin-right: -1px; border-radius: 0px;" for="day">
                                        <?php for ($i = 1; $i <= 31; $i++): ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <select class="form-control" style="width:109px; margin-right: -1px;" for="month">
                                        <?php foreach ($locale_month['en'] as $index => $month): ?>
                                            <?php if ($index != 0): ?>
                                                <option value="<?php echo $index ?>"><?php echo $month; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                    <?php $current_year = date("Y"); ?>
                                    <select class="form-control" style="width:107px;" for="year">
                                        <?php for ($year = $current_year - 10; $year <= $current_year + 10; $year ++): ?>
                                            <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>

                            <label>End date</label>
                            <div class="input-group end_date_slide">
                                <div class="input-group-addon-mfu">
                                    <input type="checkbox">
                                </div>
                                <div class="input-group-btn" style="width: 325px;">
                                    <select class="form-control" style="width:109px; margin-right: -1px; border-radius: 0px;" for="day">
                                        <?php for ($i = 1; $i <= 31; $i++): ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <select class="form-control" style="width:109px; margin-right: -1px;" for="month">
                                        <?php foreach ($locale_month['en'] as $index => $month): ?>
                                            <?php if ($index != 0): ?>
                                                <option value="<?php echo $index ?>"><?php echo $month; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                    <?php $current_year = date("Y"); ?>
                                    <select class="form-control" style="width:107px;" for="year">
                                        <?php for ($year = $current_year - 10; $year <= $current_year + 10; $year ++): ?>
                                            <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="post_id" value=""> 
                            <input type="hidden" value="" name="sch_start" class="form-control" id="startDate_live">
                            <input type="hidden" value="" name="sch_exp" class="form-control" id="endDate">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary upload_btn_live">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>

<style>
    .upload_btn{
        position:relative;
        overflow: hidden;
        width: 89px;
        height: 34px;
    }

    .upload_btn input[type=file]{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }

    #datepicker, .datepicker_mfu{
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    #submit_news_btn{
        display: none;
    }

    .input-group-addon-mfu{
        display: table-cell;
        width: 39px;
        vertical-align: middle;
        text-align: center;
        border-radius: 4px 0px 0px 4px;
        background-color: #eee;
        border: 1px solid #ccc;
        border-right: 0px;
    }

    #new_thumb{
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
</style>

<script type="text/javascript">
    $(function() {
        $.each($('.search_list'), function(index, ele) {

            var main_form = $(ele).parents('form');

            $(ele).find('a').click(function() {
                //setup data                            
                var input_element = $(this).parents("ul.search_list").attr('data-target');
                var value = $(this).attr('data-value');
                var label = $(this).text();

                $(this).parents("ul.search_list").find("li").removeClass("active");
                $(this).parents('li').addClass("active");
                $(this).parents("ul.search_list").siblings('button').find('.replace_here').text(label);

                $(main_form).find("[name=" + input_element + "]").val(value);

                return false;
            });

            $(ele).find('li.active>a').trigger("click");

            $('#startDate').datepicker();
        });
    });
</script>
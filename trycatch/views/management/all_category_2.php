<?php

function category_block($each_parent, $cate_type, $all_cate, $raws_cate, $all_api) { ?> 
    <?php foreach ($each_parent as $each_cate): ?>
        <?php $total_post = get_number_of_post_in_cate($each_cate->cate_id, null); ?>
        <div class='panel <?php echo $each_cate->parent_id != 0 ? "panel-warning margin_left_30" : "panel-default"; ?>' id='<?php echo "del_set_" . $each_cate->cate_id ?>'>
            <div class='panel-heading'>
                <span class='panel-title'>
                    <a data-toggle="collapse" data-parent="#cate_all" href='<?php echo "#cate_{$each_cate->cate_id}"; ?>'>
                        <i class='glyphicon glyphicon-align-justify'></i>&nbsp;
                        <?php echo $each_cate->cate_name; ?>
                    </a>
                </span>

                <?php echo ($total_post > 0 ? "<span class='label label-danger'>Local feed: {$total_post}</span>" : ""); ?>
                <?php if ($each_cate->api_id != 0): ?>
                    <span class='label label-success'>API Feed</span>
                <?php endif; ?>

            </div>
            <div id="<?php echo "cate_{$each_cate->cate_id}"; ?>" class="panel-collapse collapse">
                <div class='panel-body'>
                    <div class='col-md-6 col-md-offset-3'>
                        <form role='form' method="post" class="update_form" action="<?php echo site_url('operation/edit_category'); ?>">
                            <div class="form-group">                            
                                <label class='control-label'>Name</label>
                                <input type='text' class='form-control' placeholder="Category name" name="cate_name" value='<?php echo $each_cate->cate_name; ?>' required="required">
                                <input type='hidden' value='<?php echo $each_cate->cate_id; ?>' name='cate_id'>
                            </div>
                            <div class="form-group">                            
                                <label class='control-label'>Description</label>
                                <textarea class='form-control' placeholder="Category information" name="cate_info"><?php echo $each_cate->cate_info ?></textarea>
                            </div>


                            <?php if ($cate_type == CATE_POST): ?>
                                <div class="form-group">                            
                                    <label class='control-label'>News Feed</label>
                                    <select class="form-control" name="api_id">
                                        <option value="0" <?php echo $each_cate->api_id == 0 ? "selected" : ""; ?>>Local system</option>
                                        <?php foreach ($all_api as $api): ?>
                                            <option value="<?php echo $api->post_id ?>" <?php echo $each_cate->api_id == $api->post_id ? "selected" : ""; ?>><?php echo $api->post_title; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>

                            <?php if ($cate_type == CATE_EVENT): ?>
                                <div class="form-group">                            
                                    <label class='control-label'>Parent</label>
                                    <select class="form-control" name="parent_id">
                                        <option value="0" <?php echo $each_cate->parent_id == 0 ? "selected" : ""; ?>>No Parent</option>
                                        <?php foreach ($raws_cate as $each_self_cate): ?>
                                            <?php if ($each_self_cate->cate_id != $each_cate->cate_id && $each_self_cate->parent_id == 0): ?>
                                                <option value="<?php echo $each_self_cate->cate_id ?>" <?php echo $each_self_cate->cate_id == $each_cate->parent_id ? "selected" : ""; ?>><?php echo $each_self_cate->cate_name; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>

                            <div class="form-group txt_right" style='margin-bottom: 0px;'>
                                <?php if (($total_post > 0 || $each_cate->api_id != 0) && ($each_cate->cate_type == CATE_POST)): ?>
                                    <a target='_blank' class='btn btn-sm btn-warning' href="<?php echo site_url("mfu/category/{$each_cate->cate_id}/" . slug($each_cate->cate_name)); ?>"><i class='glyphicon glyphicon-eye-open'></i>&nbsp;View total post <?php echo $each_cate->api_id == 0 ? "({$total_post})" : ""; ?></a>
                                <?php endif; ?>
                                <button class='btn btn-primary btn-sm update_btn' type='submit'><i class='glyphicon glyphicon-save'></i>&nbsp;Save</button>
                                <button class='btn btn-default btn-sm' type='reset'><i class='glyphicon glyphicon-refresh'></i>&nbsp;Reset</button>
                                <button class='btn btn-danger btn-sm del_btn' type='button' data-cate_id='<?php echo $each_cate->cate_id; ?>'><i class='glyphicon glyphicon-trash'></i>&nbsp;Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php if (array_key_exists($each_cate->cate_id, $all_cate)): ?>
            <?php category_block($all_cate[$each_cate->cate_id], $cate_type, $all_cate, $raws_cate, $all_api); ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php } ?>

<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-th-list"></i>&nbsp;Category type</div>
                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li <?php echo $cate_type == CATE_POST ? "class='active'" : "" ?>>
                            <a href="<?php echo "?cate_type=" . CATE_POST ?>">POST</a>
                        </li>
                        <li <?php echo $cate_type == CATE_EVENT ? "class='active'" : "" ?>>
                            <a href="<?php echo "?cate_type=" . CATE_EVENT ?>">EVENT</a>
                        </li>
                        <li <?php echo $cate_type == CATE_CLIPPING ? "class='active'" : "" ?>>
                            <a href="<?php echo "?cate_type=" . CATE_CLIPPING ?>">CLIPPING</a>
                        </li>
                        <li <?php echo $cate_type == CATE_ALBUM ? "class='active'" : "" ?>>
                            <a href="<?php echo "?cate_type=" . CATE_ALBUM ?>">ALBUM</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <span class="panel-title"><i class="glyphicon glyphicon-upload"></i>&nbsp;Add category</span>
                </div>
                <div class="panel-body">
                    <div class='col-md-12'>
                        <form class="form-horizontal add_form" method="post" role="form" action="<?php echo site_url('operation/store_category'); ?>">
                            <div class="form-group">                            
                                <label class='control-label'>Name</label>
                                <input type='text' class='form-control' placeholder="Category name" name="cate_name">
                            </div>
                            <?php if ($cate_type == CATE_EVENT): ?>
                                <div class="form-group">
                                    <label class='control-label'>Parent</label>
                                    <select name="parent_id" class="form-control">
                                        <option value="0">No Parent</option>
                                        <?php foreach ($temp as $each_self_cate): ?>
                                            <?php if ($each_self_cate->parent_id == 0): ?>
                                                <option value="<?php echo $each_self_cate->cate_id ?>"><?php echo $each_self_cate->cate_name; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">                            
                                <label class='control-label'>Description</label>
                                <textarea class='form-control' placeholder="Category information" name="cate_info"></textarea>
                            </div>
                            <div class="form-group txt_right" style='margin-bottom: 0px;'>                            
                                <button class='btn btn-info' id='add_btn' data-loading-text="Adding..."><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Add</button>
                            </div>                            
                            <input type="hidden" value="<?php echo $cate_type; ?>" name="cate_type">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <h2 style='margin-top: 4px;'>Category</h2>
            <div class="panel-group" id="cate_all">
                <?php $group_by_parent = group_cate_by_parent($temp); ?>
                <?php category_block($group_by_parent[0], $cate_type, $group_by_parent, $temp, $all_api); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        var add_btn;
        $('.add_form').ajaxForm({
            beforeSend: function() {
                add_btn = $('#add_btn').html();
                $('#add_btn').attr('disabled', 'desabled').text('Adding...');
            },
            complete: function(xhr) {
                console.log(xhr.responseText)
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.reload();
                } else {
                    $('#add_btn').removeAttr('disabled').html(add_btn);
                    alert('Cannot process your request');
                }
            }
        });

        $('.update_form').each(function(id, ele) {
            var btn = $(ele).find('.update_btn');
            var btn_html = $(btn).html();
            $(ele).ajaxForm({
                beforeSend: function() {
                    $(btn).attr('disabled', 'disabled').text('Updating...');
                },
                complete: function(xhr) {
                    console.log(xhr.responseText)
                    var json = $.parseJSON(xhr.responseText);
                    if (json.status === "success") {
                        location.reload();
                    } else {
                        $(btn).removeAttr('disabled').html(btn_html);
                        alert('Cannot process your request');
                    }
                }
            });
        });

        $('.del_btn').click(function() {
            if (confirm('Do you want to delete this category ?')) {
                var cate_id = $(this).attr('data-cate_id');
                var btn_html = $(this).html();
                var btn = $(this);

                $(btn).attr('disabled', 'disabled').text('Deleting...');
                $.post('<?php echo site_url('operation/del_category'); ?>', {cate_id: cate_id}, function(res) {
                    if (res.status === "success") {
                        $('#del_set_' + cate_id).fadeOut(400, function() {
                            $(this).remove();
                        });
                    } else {
                        $(btn).removeAttr('disabled').html(btn_html);
                        alert('Cannot process your request');
                    }
                }, 'json');
            }
        });
    });
</script>
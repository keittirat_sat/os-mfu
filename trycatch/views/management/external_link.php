<div class="row margin_top_20">
    <div class="col-md-4 col-lg-3">
        <?php if ($post_id): ?>
            <!--Custom panel-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-link"></i> Add new link
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <p><input type="text" id="add_new_link_title" name="link_title" class="form-control" placeholder="Link title"></p>
                        <p><input type="text" id="add_new_link_url" name="link_url" class="form-control" placeholder="Link Url"></p>
                        <p class="txt_right"><button id="add_new_link" class="btn btn-danger btn-sm" data-loading-text="Adding..." data-complete-text="Added"><i class="glyphicon glyphicon-magnet"></i> Add to group</button></p>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-link"></i> All Page
                </div>
                <div class="panel-body">
                    <?php $page = get_all_post_by_type(TYPE_PAGE); ?>
                    <div class="cover_list_menu page_list">
                        <?php foreach ($page as $each_page): ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" data-title="<?php echo $each_page->post_title; ?>" data-link="<?php echo site_url("mfu/page/{$each_page->post_id}/" . slug($each_page->post_title)); ?>" data-type="<?php echo $each_page->post_type; ?>"> <?php echo $each_page->post_title; ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <p class="txt_right" style="margin: 0px;">
                        <button class="btn btn-danger btn-sm" type="submit" id="add_page"><i class="glyphicon glyphicon-magnet"></i> Add to group</button>
                    </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-list-alt"></i> Category</div>
                <div class="panel-body">
                    <?php $cate = get_all_category(); ?>
                    <div class="cover_list_menu category_list">
                        <?php foreach ($cate as $each_cate): ?>
                            <div class="checkbox  <?php echo $each_cate->api_id != 0 ? "green" : ""; ?>">
                                <label>
                                    <input type="checkbox" data-title="<?php echo $each_cate->cate_name; ?>" data-link="<?php echo site_url("mfu/category/{$each_cate->cate_id}/" . slug($each_cate->cate_name)); ?>" data-type="category"> <?php echo $each_cate->cate_name; ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <p class="txt_right" style="margin: 0px;">
                        <button class="btn btn-default btn-sm" type="submit" id="add_category"><i class="glyphicon glyphicon-import"></i> Add to group</button>
                    </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-list-alt"></i> External Link Group</div>
                <div class="panel-body">
                    <div class="cover_list_menu category_group_list">
                        <?php foreach ($all_link as $each_link): ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" data-title="<?php echo $each_link->post_title; ?>" data-link="<?php echo site_url("mfu/category_group/{$each_link->post_id}/" . slug($each_link->post_title)); ?>" data-type="category_group"> <?php echo $each_link->post_title; ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <p class="txt_right" style="margin: 0px;">
                        <button class="btn btn-default btn-sm" type="submit" id="add_category_group"><i class="glyphicon glyphicon-import"></i> Add to group</button>
                    </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-list-alt"></i> Event and Calendar</div>
                <div class="panel-body">
                    <?php $cate = get_all_category(CATE_EVENT); ?>
                    <div class="cover_list_menu calendar_list">                    
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="All Event" data-link="<?php echo site_url("mfu/all_event"); ?>" data-type="event"> All Event
                            </label>
                        </div>
                        <?php foreach ($cate as $each_cate): ?>
                            <?php if ($each_cate->parent_id == 0): ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" data-title="<?php echo $each_cate->cate_name; ?>" data-link="<?php echo site_url("mfu/all_event?cate_id={$each_cate->cate_id}"); ?>" data-type="event"> <?php echo $each_cate->cate_name; ?>
                                    </label>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <p class="txt_right" style="margin: 0px;">
                        <button class="btn btn-default btn-sm" type="submit" id="add_event"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                    </p>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><i class=" glyphicon glyphicon-list"></i> Reserved page</div>
                <div class="panel-body">
                    <div class="cover_list_menu reserved_list">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU PR" data-link="<?php echo site_url("mfu/pr"); ?>" data-type="reserved_list"> MFU PR
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Admission" data-link="<?php echo site_url("mfu/admission"); ?>" data-type="reserved_list"> MFU Admission
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Admission news" data-link="<?php echo site_url("mfu/admission_news"); ?>" data-type="reserved_list"> MFU Admission news
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Visitor info" data-link="<?php echo site_url("mfu/visitor_info"); ?>" data-type="reserved_list"> MFU Visitor info
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Research" data-link="<?php echo site_url("mfu/research"); ?>" data-type="reserved_list"> MFU Research
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Newsletter" data-link="<?php echo site_url("mfu/newsletter"); ?>" data-type="reserved_list"> MFU Newsletter
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Clipping" data-link="<?php echo site_url("mfu/clipping"); ?>" data-type="reserved_list"> MFU Clipping
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Gallery" data-link="<?php echo site_url("mfu/gallery"); ?>" data-type="reserved_list"> MFU Gallery
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Researcher" data-link="<?php echo site_url("mfu/researcher"); ?>" data-type="reserved_list"> MFU Researcher
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Organization" data-link="<?php echo site_url("mfu/organization"); ?>" data-type="reserved_list"> MFU Organization
                            </label>
                        </div>

                        <!--Program-->
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Bachelor Program" data-link="<?php echo site_url("mfu/bachelor"); ?>" data-type="reserved_list"> MFU Bachelor Program
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Master Program" data-link="<?php echo site_url("mfu/master"); ?>" data-type="reserved_list"> MFU Master Program
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-title="MFU Doctorate Program" data-link="<?php echo site_url("mfu/doctorate"); ?>" data-type="reserved_list"> MFU Doctorate Program
                            </label>
                        </div>
                        <!--/Program-->
                    </div>
                    <p class="txt_right" style="margin: 0px;">
                        <button class="btn btn-default btn-sm" type="submit" id="add_reserved_page"><i class="glyphicon glyphicon-import"></i> Add to menu</button>
                    </p>
                </div>
            </div>
            <!--/Custom panel-->
        <?php endif; ?>
    </div>
    <div class="col-md-8 col-lg-9">
        <label>External links' group</label>
        <div class="row">
            <div class='col-lg-5'>
                <select class="form-control" id="menu_list">
                    <?php if (count($all_link)): ?>
                        <?php foreach ($all_link as $each_link): ?>
                            <option value="<?php echo $each_link->post_id; ?>" <?php echo ($each_link->post_id == $post_id) ? "selected" : ""; ?>><?php echo $each_link->post_title; ?></option>
                            <?php if ($each_link->post_id == $post_id): ?>
                                <?php $temp_link = $each_link ?>
                                <?php $group_title = $each_link->post_title; ?>
                                <?php $content = $each_link->post_detail; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <option value='-1'>--- No available group ---</option>
                    <?php endif; ?>
                </select>
            </div>

            <button class='btn btn-primary' <?php echo (!$post_id) ? "disabled" : ""; ?> id="btn_select_menu">Select</button>
            <button class='btn btn-danger' id='create_new_group'>Create new group</button>
        </div>
        <div class="row margin_top_10">
            <div class='col-md-12'>
                <?php if ($post_id): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-bullhorn"></i> Group management : <b><i id="nav_name"><?php echo $group_title; ?></i></b>
                            <?php if (count($post_id)): ?>
                                <button class="btn btn-default btn-xs pull-right" id="btn_delete_menu"><i class="glyphicon glyphicon-trash"></i> Delete this group</button>
                            <?php endif; ?>
                        </div>
                        <div class="panel-body">
                            <?php if ($post_id): ?>
                                <!--Post Detail-->
                                <label><i class="glyphicon glyphicon-info-sign"></i> Group info</label>
                                <div class="form-group">
                                    <textarea class="form-control" id="post_excerp"><?php echo $temp_link->post_excerp; ?></textarea>
                                </div>
                                <!--/Post Detail-->

                                <!--Sorting panel-->
                                <div class="form-group">
                                    <ul class="sortable">
                                        <?php if (trim($content)): ?>
                                            <?php $list_link = json_decode($content); ?>
                                            <?php $list_link = is_array($list_link) ? $list_link : array() ?>
                                            <?php $class = "ui-state-default"; ?>
                                            <?php $panel = "panel-default"; ?>
                                            <?php foreach ($list_link as $index => $link_each): ?>
                                                <li class="ux_tc <?php echo $class; ?>">
                                                    <div class="panel-group" id="accordion_<?php echo $index; ?>">
                                                        <div class="panel <?php echo $panel; ?>">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $index; ?>">
                                                                        <span class="_sub-menu"><i class="glyphicon glyphicon-indent-left"></i>&nbsp;</span>
                                                                        <span class="tc_label"><?php echo $link_each->title; ?></span>
                                                                        <span class="test_post"></span>
                                                                        <i class="caret pull-right caret_menu_cutom"></i>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse_<?php echo $index; ?>" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal form_menu_config">
                                                                        <div class="form-group">
                                                                            <label for="link_title__<?php echo $index; ?>" class="col-sm-2 control-label">Link label</label>
                                                                            <div class="col-sm-10">
                                                                                <input type="text" class="form-control input-sm" id="link_title__<?php echo $index; ?>" placeholder="Link label" value="<?php echo $link_each->title; ?>" name="link_title">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="link_to__<?php echo $index; ?>" class="col-sm-2 control-label">URL</label>
                                                                            <div class="col-sm-10">
                                                                                <input type="text" class="form-control input-sm" id="link_to__<?php echo $index; ?>" placeholder="URL" value="<?php echo $link_each->url; ?>" name="link_to" <?php echo ($link_each->type != "custom_link") ? "readonly" : ""; ?>>
                                                                                <input type="hidden" class="menu_type" value="<?php echo $link_each->type; ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="detail__<?php echo $index; ?>" class="col-sm-2 control-label">Detail</label>
                                                                            <div class="col-sm-10">
                                                                                <input type="text" class="form-control input-sm" id="detail__<?php echo $index; ?>" placeholder="Detail" value="<?php echo @$link_each->detail; ?>" name="detail">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="detail2__<?php echo $index; ?>" class="col-sm-2 control-label">Detail#2</label>
                                                                            <div class="col-sm-10">
                                                                                <input type="text" class="form-control input-sm" id="detail2__<?php echo $index; ?>" placeholder="Detail#2" value="<?php echo @$link_each->detail2; ?>" name="detail2">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-10 col-sm-offset-2">
                                                                                <button class="btn btn-danger btn-sm del_menu"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul> 
                                </div>
                                <!--./Sorting panel-->
                            <?php else: ?>
                                <h3 class="txt_center" style="color: #999;">Please create a new group.</h3>
                            <?php endif; ?>
                        </div>
                        <div class="panel-footer txt_right">
                            <button class="btn btn-danger btn-sm" id="update_list_group" data-loading-text="Updating..." ><i class="glyphicon glyphicon-save"></i> Update group</button>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<input type="hidden" value="<?php echo $post_id; ?>" name="post_id" id="post_id_menu">

<div class="modal fade" id='add_group'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Add new group</h3>
            </div>
            <div class="modal-body">
                <form role='form' action='<?php echo site_url('api/add_link_group'); ?>' method="post" id="form_add_link">
                    <div class='form-group' style="margin-bottom: 0px;">
                        <label for='group_name_modal'>Group name <span id="error_report"></span></label>
                        <input type='text' name='group_name' id='group_name_modal' class='form-control' placeholder="Group name" required="">
                        <input type="submit" id="submit_form_add_new_group" style="display: none;">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger btn-sm" id="save_group" data-loading-text="Adding...">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="status_report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="status_txt txt_center margin_top_20 margin_bottom_20">One fine body&hellip;</h4>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type='text/javascript'>
    var counter_id = 0;
    function isValidURL(url) {
        var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

        if (RegExp.test(url)) {
            return true;
        } else {
            return false;
        }
    }

    function generate_element(title, link, type) {
        var element;
        counter_id++;
        var next_total = new Date();
        var random_id = next_total.getTime() + counter_id;
        element = "<li class='ux_tc ui-state-default'>";
        element += "<div class='panel-group' id='accordion" + random_id + "'>";
        element += "<div class='panel panel-default'>";
        element += "<div class='panel-heading'>";
        element += "<h4 class='panel-title'>";
        element += "<a data-toggle='collapse' data-parent='#accordion' href='#collapse" + random_id + "'>";
        element += "<span class='_sub-menu'><i class='glyphicon glyphicon-indent-left'></i>&nbsp;</span><span class='tc_label'>" + title + "</span> <span class='test_post'></span> <i class='caret pull-right caret_menu_cutom'></i>";
        element += "</a>";
        element += "</h4>";
        element += "</div>";
        element += "<div id='collapse" + random_id + "' class='panel-collapse collapse'>";
        element += "<div class='panel-body'>";

        //Inside Element
        element += "<div class='form-horizontal form_menu_config'>";

        //First Line
        element += "<div class='form-group'>";
        element += "<label for='link_title_" + random_id + "' class='col-sm-2 control-label'>Link label</label>";
        element += "<div class='col-sm-10'>";
        element += "<input type='text' class='form-control input-sm' id='link_title_" + random_id + "' placeholder='Link label' value='" + title + "' name='link_title'>";
        element += "</div>";
        element += "</div>";

        //Second Line
        element += "<div class='form-group'>";
        element += "<label for='link_to_" + random_id + "' class='col-sm-2 control-label'>URL</label>";
        element += "<div class='col-sm-10'>";

        element += "<input type='text' class='form-control input-sm' id='link_to_" + random_id + "' placeholder='URL' value='" + link + "' name='link_to' ";
        if (type !== 'custom_link') {
            element += " readonly ";
        }
        element += ">";

        element += "<input type='hidden' class='menu_type' value='" + type + "'>";
        element += "</div>";
        element += "</div>";

        //Detail
        element += "<div class='form-group'>";
        element += "<label for='detail__" + random_id + "' class='col-sm-2 control-label'>Detail</label>";
        element += "<div class='col-sm-10'>";
        element += "<input type='text' class='form-control input-sm' id='detail__" + random_id + "' placeholder='Detail' value='' name='detail'>";
        element += "</div>";
        element += "</div>";

        //Detail2
        element += "<div class='form-group'>";
        element += "<label for='detail2__" + random_id + "' class='col-sm-2 control-label'>Detail</label>";
        element += "<div class='col-sm-10'>";
        element += "<input type='text' class='form-control input-sm' id='detail2__" + random_id + "' placeholder='Detail#2' value='' name='detail2'>";
        element += "</div>";
        element += "</div>";

        //Third Line - Submit form
        element += "<div class='form-group'>";
        element += "<div class='col-sm-10 col-sm-offset-2'>";

        //Submit - form
        element += "<button class='btn btn-danger btn-sm del_menu' data-id=''><i class='glyphicon glyphicon-trash'></i>&nbsp;Delete</button>";

        element += "</div>";
        element += "</div>";
        element += "</div>";

        element += "</div>";
        element += "</div>";
        element += "</div>";
        element += "</div>";
        element += "</li>";
        return element;
    }

    function redirect_url(post_id) {
        var redirect = location.origin + location.pathname;
        redirect += "?post_id=" + post_id;
        location.href = redirect;
    }

    function checkbox_execution(target) {
        var element;
        $(target).each(function(id, ele) {
            var link = $(ele).attr('data-link');
            var title = $(ele).attr('data-title');
            var type = $(ele).attr('data-type');
            element = generate_element(title, link, type);

            $(ele).removeAttr('checked');

            $('.sortable').append(element);
        });
    }

    $(function() {
        $('#create_new_group').click(function() {
            $('#add_group').modal();
        });

        $('#save_group').click(function() {
            $('#submit_form_add_new_group').trigger('click');
        });

        var save_group_btn;
        $('#form_add_link').ajaxForm({
            beforeSend: function() {
//                $('#save_group').button('loading');
                save_group_btn = $('#save_group').html();
                $('#save_group').html($('#save_group').attr('data-loading-text')).attr('disabled', 'disabled');
                $('#error_report').text("- Processing your request").css({'color': 'gray'});
            },
            complete: function(xhr) {
                var raws = xhr.responseText;
                var json = $.parseJSON(raws);
                if (json.status === "success") {
                    $('#error_report').text("- Success").css({'color': 'green'});
                    $('#save_group').html('Added');
                    setTimeout(function() {
                        redirect_url(json.post_id);
                    }, 1000);
                } else {
                    $('#save_group').html('Error');
                    $('#error_report').text("- Cannot process your request").css({'color': 'red'});
                    setTimeout(function() {
                        $('#save_group').html(save_group_btn).removeAttr('disabled');
                    }, 1000);
                }
            }
        });

        $('#add_new_link').click(function() {
            var title = $('#add_new_link_title').val();
            var url = $('#add_new_link_url').val();
            var type = "custom_link";
            var btn = $(this);
            var btn_txt = $(btn).html();
            if (isValidURL(url)) {
                var html = generate_element(title, url, type);
                $('.sortable').append(html);
                $(this).trigger('reset');

                $(btn).html($(btn).attr('data-complete.text'));
                $(btn).attr('disabled', 'disabled');
                $('#add_new_link_title').val('');
                $('#add_new_link_url').val('');
                setTimeout(function() {
                    $(btn).html(btn_txt);
                    $(btn).removeAttr('disabled');
                }, 1000);
            } else {
                $(btn).html('Link url invalid');
                $(btn).attr('disabled', 'disabled');
                setTimeout(function() {
                    $(btn).html(btn_txt);
                    $(btn).removeAttr('disabled');
                }, 1000);
            }
            return false;
        });

        $('.sortable').sortable({
            revert: true
        });

        $('#btn_select_menu').click(function() {
            var post_id = $('#menu_list').val();
            $(this).attr('disabled', 'disabled');
            $(this).text('Swapping...');
            setTimeout(function() {
                redirect_url(post_id);
            }, 1000);
        });

        $('#update_list_group').click(function() {
            var all_data = [];
            var btn_txt = $(this).html();
            $(this).html($(this).attr('data-loading-text')).attr('disabled', 'disabled');
            $('.ux_tc').each(function(id, ele) {
                var json = {};
                json['title'] = $.trim($(ele).find('[name=link_title]').val());
                json['url'] = $.trim($(ele).find('[name=link_to]').val());
                json['type'] = $.trim($(ele).find('.menu_type').val());
                json['detail'] = $.trim($(ele).find('[name=detail]').val());
                json['detail2'] = $.trim($(ele).find('[name=detail2]').val());
                //add to json and store to DB
                all_data.push(json);
            });

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('api/update_group'); ?>",
                data: {dataset: all_data, post_id: $('#post_id_menu').val(), post_excerp: $('#post_excerp').val()},
//                contentType: "application/json; charset=utf-8",
                dataType: "html",
                cache: false,
                success: function(res) {
                    console.log(res);
                    var json = $.parseJSON(res);
                    console.log(json);
                    if (json.status === "success") {
//                        $('.status_title').text('Update complete');
                        $('.status_txt').text('Group update successful');
                    } else {
//                        $('.status_title').text('Update fail');
                        $('.status_txt').text('Cannot process your request');
                    }
                    $('#status_report').modal();
                    $('#update_list_group').html(btn_txt).removeAttr('disabled');
                },
                failure: function(res) {
//                    $('.status_title').text('Update fail');
                    $('.status_txt').text('Internal error');
                    $('#status_report').modal();
                    $('#update_list_group').html(btn_txt).removeAttr('disabled');
                }
            });


        });

        $('#btn_delete_menu').click(function() {
            if (confirm('Do you really want to delete this: ' + $('#nav_name').text() + '?')) {
                var post_id = $('#post_id_menu').val();
                var html = $(this).html();
                var btn = $(this);
                $(btn).attr('disabled', 'disabled').html('Deleting...');
                $.post('<?php echo site_url('api/delete_post'); ?>', {'trucate': true, 'post_id': post_id}, function(res) {
                    if (res.status === "success") {
                        var url = location.origin + location.pathname;
                        location.href = url;
                    } else {
                        $(btn).html('Cannot process your request: delete fail');
                        setTimeout(function() {
                            $(btn).removeAttr('disabled').html(html);
                        }, 1500);
                    }
                }, 'json');
            }
        });

        $('#add_page').click(function() {
            checkbox_execution($('.page_list input[type=checkbox]:checked'));
        });

        $('#add_category').click(function() {
            checkbox_execution($('.category_list input[type=checkbox]:checked'));
        });

        $('#add_category_group').click(function() {
            checkbox_execution($('.category_group_list input[type=checkbox]:checked'));
        });

        $('#add_reserved_page').click(function() {
            checkbox_execution($('.reserved_list input[type=checkbox]:checked'));
        });

        $('#add_event').click(function() {
            checkbox_execution($('.calendar_list input[type=checkbox]:checked'));
        });

        $('.del_menu').live({
            click: function() {
                $(this).parents('li').fadeOut(300, function() {
                    $(this).remove();
                });
            }
        });
    });
</script>

<?php

function get_cate_ref($category, $disallow, $is_cate = true, $permission = "permission_category") { ?>

    <?php $cate = array(); ?>
    <?php foreach ($category as $index => $each_cate): ?>
        <?php if ($is_cate): ?>
            <?php if ($each_cate->parent_id == 0): ?>
                <?php array_push($cate, $each_cate); ?>
            <?php endif; ?>
        <?php else: ?>
            <?php array_push($cate, $each_cate); ?>
        <?php endif; ?>
    <?php endforeach; ?>

    <div class="row">
        <div class="col-md-6">
            <?php foreach ($cate as $index => $each_cate): ?>
                <?php if (!($index % 2)): ?>
                    <div class="checkbox">
                        <label>
                            <?php if ($is_cate): ?>
                                <input type="checkbox" class="<?php echo $permission; ?>" name="category[]" value="<?php echo $each_cate->cate_id; ?>" <?php echo!in_array($each_cate->cate_id, $disallow) ? "checked" : ""; ?>>
                                <?php echo $each_cate->cate_name ?>
                            <?php else: ?>
                                <input type="checkbox" class="<?php echo $permission; ?>" name="category[]" value="<?php echo $each_cate->post_id; ?>" <?php echo!in_array($each_cate->post_id, $disallow) ? "checked" : ""; ?>>
                                <?php echo $each_cate->post_title ?>
                            <?php endif; ?>
                        </label>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <div class="col-md-6">
            <?php foreach ($cate as $index => $each_cate): ?>
                <?php if ($index % 2): ?>
                    <div class="checkbox">
                        <label>
                            <?php if ($is_cate): ?>
                                <input type="checkbox" class="<?php echo $permission; ?>" name="category[]" value="<?php echo $each_cate->cate_id; ?>" <?php echo!in_array($each_cate->cate_id, $disallow) ? "checked" : ""; ?>>
                                <?php echo $each_cate->cate_name ?>
                            <?php else: ?>
                                <input type="checkbox" class="<?php echo $permission; ?>" name="category[]" value="<?php echo $each_cate->post_id; ?>" <?php echo!in_array($each_cate->post_id, $disallow) ? "checked" : ""; ?>>
                                <?php echo $each_cate->post_title ?>
                            <?php endif; ?>
                        </label>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php } ?>

<div class="container margin_top_20">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">            
            <form role="form" method="post" action="<?php echo site_url('api/update_account'); ?>" class="margin_bottom_40" id="update_acc_form">
                <h2>Personal Information</h2>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        User information
                    </div>
                    <div class="panel-body">
                        <label>Name</label>
                        <div class="form-group">
                            <input type="text" name="real_name" class="form-control" required="required" placeholder="Real name" value="<?php echo $user_info->real_name; ?>">
                        </div>
                        <label>Email/Username</label>
                        <div class="form-group">
                            <input type="email" class="form-control" value="<?php echo $user_info->email ?>" name="username" disabled="disabled">                            
                        </div>
                        <label>New Password</label>
                        <div class="form-group">
                            <input type="password" name="password_1" class="form-control" placeholder="New Password">
                        </div>
                        <label>Re-type password</label>
                        <div class="form-group">
                            <input type="password" name="password_2" class="form-control" placeholder="New Password">
                        </div>
                    </div>
                </div>

                <?php if (($user_info->uid != get_user_uid()) && (get_user_level() != USER_WRITER)): ?>
                    <h2>Account status</h2>
                    <?php $disallow = json_decode($user_info->disallow); ?>
                    <div class="row">

                        <div class="<?php echo $user_info->level == USER_WRITER ? "col-md-4" : "col-md-6"; ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading">User level</div>
                                <div class="panel-body">
                                    <select class="form-control" name="level">
                                        <option value="<?php echo USER_ADMIN ?>" <?php echo $user_info->level == USER_ADMIN ? "selected" : ""; ?>>Administrator</option>
                                        <option value="<?php echo USER_DIRECTOR ?>" <?php echo $user_info->level == USER_DIRECTOR ? "selected" : ""; ?>>Director</option>
                                        <option value="<?php echo USER_WRITER ?>" <?php echo $user_info->level == USER_WRITER ? "selected" : ""; ?>>Writer</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div  <?php echo $user_info->level == USER_WRITER ? "class='col-md-4'" : "style='display:none;'"; ?>" >
                            <div class="panel panel-default">
                                <div class="panel-heading">Post reviewer</div>
                                <div class="panel-body">
                                    <select class="form-control" name="parent_uid">
                                        <option value="0" <?php echo $user_info->parent_uid == 0 ? "selected" : "" ?>>No Director</option>
                                        <?php foreach ($all_director as $each_director): ?>
                                            <?php if ($each_director->status != USER_TEMPORARY): ?>
                                                <option value="<?php echo $each_director->uid ?>" <?php echo $user_info->parent_uid == $each_director->uid ? "selected" : ""; ?>><?php echo $each_director->real_name ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="<?php echo $user_info->level == USER_WRITER ? "col-md-4" : "col-md-6"; ?>">
                            <div class="panel <?php echo $user_info->status == USER_ACTIVE ? "panel-default" : "panel-danger" ?>">
                                <div class="panel-heading">User status</div>
                                <div class="panel-body">
                                    <select class="form-control" name="status">
                                        <option value="1" <?php echo $user_info->status == USER_ACTIVE ? "selected" : ""; ?>>Active</option>
                                        <option value="0" <?php echo $user_info->status == USER_TEMPORARY ? "selected" : ""; ?>>Temporary</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php if ($user_info->level != USER_ADMIN): ?>
                        <h2>Permission</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><i class="glyphicon glyphicon-cog"></i>&nbsp;General</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="section" value="research" <?php echo!in_array("research", $disallow->section) ? "checked" : ""; ?>> Research
                                                    </label>
                                                </div>

                                                <?php /*if ($user_info->level != USER_WRITER): ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="section" value="school" <?php echo!in_array("school", $disallow->section) ? "checked" : ""; ?>> School
                                                        </label>
                                                    </div>
                                                <?php endif;*/ ?>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="section" value="page" <?php echo!in_array("page", $disallow->section) ? "checked" : ""; ?>> Page
                                                    </label>
                                                </div>

                                                <?php if ($user_info->level != USER_WRITER): ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="section" value="api" <?php echo!in_array("api", $disallow->section) ? "checked" : ""; ?>> API
                                                        </label>
                                                    </div>

                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="section" value="external_link" <?php echo!in_array("external_link", $disallow->section) ? "checked" : ""; ?>> External link
                                                        </label>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?php if ($user_info->level != USER_WRITER): ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="section" value="category" <?php echo!in_array("category", $disallow->section) ? "checked" : ""; ?>> Category
                                                        </label>
                                                    </div>
                                                <?php endif; ?>

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="section" value="newsletter" <?php echo!in_array("newsletter", $disallow->section) ? "checked" : ""; ?>> Newsletter
                                                    </label>
                                                </div>                                                

                                                <?php if ($user_info->level != USER_WRITER): ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="section" value="audience_guideline" <?php echo!in_array("audience_guideline", $disallow->section) ? "checked" : ""; ?>> Audience guideline
                                                        </label>
                                                    </div>

                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="section" value="menu_main" <?php echo!in_array("menu_main", $disallow->section) ? "checked" : ""; ?>> Menu
                                                        </label>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php $all_school = get_all_post_by_type(TYPE_SCHOOL); ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="checkbox margin_bottom_0 margin_top_0">
                                            <label>
                                                <input type="checkbox" class="section" value="school" <?php echo!in_array("school", $disallow->section) ? "checked" : ""; ?>> School
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <?php get_cate_ref($all_school, isset($disallow->permission_school) ? $disallow->permission_school : array(), false, "permission_school"); ?>
                                    </div>
                                </div>

                                <?php $category = get_all_category(CATE_POST); ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="checkbox margin_bottom_0 margin_top_0">
                                            <label>
                                                <input type="checkbox" class="section" value="post" <?php echo!in_array("post", $disallow->section) ? "checked" : ""; ?>> Post section
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <?php get_cate_ref($category, $disallow->permission); ?>
                                    </div>
                                </div>

                                <?php $category = get_all_category(CATE_EVENT); ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">                                        
                                        <div class="checkbox margin_bottom_0 margin_top_0">
                                            <label>
                                                <input type="checkbox" class="section" value="event" <?php echo!in_array("event", $disallow->section) ? "checked" : ""; ?>> Event section
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <?php get_cate_ref($category, $disallow->permission); ?>
                                    </div>
                                </div>

                                <?php $category = get_all_category(CATE_CLIPPING); ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="checkbox margin_bottom_0 margin_top_0">
                                            <label>
                                                <input type="checkbox" class="section" value="clipping" <?php echo!in_array("clipping", $disallow->section) ? "checked" : ""; ?>> Clipping section
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <?php get_cate_ref($category, $disallow->permission); ?>
                                    </div>
                                </div>

                                <?php $category = get_all_category(CATE_ALBUM); ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="checkbox margin_bottom_0 margin_top_0">
                                            <label>
                                                <input type="checkbox" class="section" value="album" <?php echo!in_array("album", $disallow->section) ? "checked" : ""; ?>> Album section
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <?php get_cate_ref($category, $disallow->permission); ?>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="disallow" value="<?php echo quotes_to_entities(json_encode($disallow)); ?>">
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12 txt_right">
                        <input type="submit" id="submit_real_btn">
                        <button type="button" id="submit_btn" class="btn btn-success"><i class="glyphicon glyphicon-check"></i> UPDATE</button>
                    </div>
                </div>
                <input type="hidden" value="<?php echo $user_info->uid; ?>" name="uid">
            </form>
        </div>
    </div>
</div>

<style>
    #submit_real_btn{
        display: none;
    }
</style>

<script type="text/javascript">
    function summary_permission() {
        var permission = [];
        var section = [];
        var school = [];
        
        var obj = {};
        $('.permission_category:not(:checked)').each(function(id, ele) {
            permission.push($(ele).val());
        });

        $('.section:not(:checked)').each(function(id, ele) {
            section.push($(ele).val());
        });

        $('.permission_school:not(:checked)').each(function(id, ele) {
            school.push($(ele).val());
        });

        obj['section'] = section;
        obj['permission'] = permission;
        obj['permission_school'] = school;

        console.log(obj);

        $('[name=disallow]').val(JSON.stringify(obj));

        return obj;
    }

    $(function() {
//        $('.permission_category, .section, .permission_school').change(function() {
//            summary_permission();
//        });

        $('#submit_btn').click(function() {
            var pass_1 = $('[name=password_1]').val();
            var pass_2 = $('[name=password_2]').val();
            if (pass_1 === pass_2) {
                if (summary_permission()) {
                    $('#submit_real_btn').trigger('click');
                }
            } else {
                alert('Password mismatch');
            }
        });

        $('#update_acc_form').ajaxForm({
            beforeSend: function() {
                $('#submit_btn').text("Updaing...").attr('disabled', 'disabled');
            },
            complete: function(xhr) {
                console.log(xhr.responseText);
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.reload();
                } else {
                    alert("update fail");
                }
            }
        });

        $('.panel-heading input.section').change(function() {
            var pbody = $(this).parents(".panel-heading");
            var pinput = pbody.siblings(".panel-body").find("input");

            if (!$(this).is(":checked")) {
                //uncheck all
                $.each(pinput, function(idx, ele) {
                    if ($(ele).is(":checked")) {
                        $(ele).removeAttr("checked");
                    }
                });
            } else {
                //check all
                $.each(pinput, function(idx, ele) {
                    if (!$(ele).is(":checked")) {
                        $(ele).attr("checked", "checked");
                    }
                });
            }
        });

        $(".panel-body input.permission_category").change(function() {
            if ($(this).is(":checked")) {
                var pbody = $(this).parents(".panel-body");
                var pinput = pbody.siblings(".panel-heading").find("input");

                if (!$(pinput).is(":checked")) {
                    $(pinput).attr("checked", "checked");
                }
            }
        });
    });
</script>
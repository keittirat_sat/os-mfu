<div class='row'>
    <div class='col-md-8 col-md-offset-2'>
        <legend>
            <h2><i class='glyphicon glyphicon-user'></i>&nbsp;Add new account</h2>
        </legend>
        <form class="form-horizontal" role="form" method='post' id="register_form" action='<?php echo site_url('api/add_users'); ?>'>
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputName" placeholder="Name" name="real_name" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password" required>
                </div>
            </div>
            <div class="form-group">
                <label for="user_level" class="col-sm-2 control-label">Permission</label>
                <div class="col-sm-10">
                    <select class="form-control" id='user_level' name="level">
                        <?php if (get_user_level() == USER_ADMIN || get_user_level() == USER_DIRECTOR): ?>
                            <option value='writer'>Writer</option>
                        <?php endif; ?>

                        <?php if (get_user_level() == USER_ADMIN): ?>
                            <option value='director'>Director</option>
                        <?php endif; ?>

                        <?php if (get_user_level() == USER_ADMIN): ?>
                            <option value='admin'>Administrator</option>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
            <div class='form-group'>
                <div class='col-sm-10 col-sm-offset-2'>
                    <section class='info_level writer_detail'>
                        <ul>
                            <li>
                                <b>Add operation</b>
                                <ol>
                                    <li>Post</li>
                                    <li>File</li>
                                    <li>Category</li>
                                </ol>
                            </li>
                            <li>
                                <b>Edit operation</b>
                                <ol>
                                    <li>Page</li>
                                    <li><b>Own</b> post</li>
                                    <li><b>Own</b> file</li>
                                    <li>Category</li>
                                </ol>
                            </li>
                        </ul>
                    </section>
                    <section class='info_level director_detail'>
                        <ul>
                            <li>
                                <b>Add operation</b>
                                <ol>
                                    <li>Post</li>
                                    <li>Page</li>
                                    <li>File</li>
                                    <li>Category</li>
                                </ol>
                            </li>
                            <li>
                                <b>Edit operation</b>
                                <ol>
                                    <li>Post</li>
                                    <li>Page</li>
                                    <li>File</li>
                                    <li>Category</li>
                                </ol>
                            </li>
                            <li>
                                <b>Template management</b>
                                <ol>
                                    <li>Add Page</li>
                                    <li>Menu management</li>
                                </ol>
                            </li>
                            <li>
                                <b>User management</b>
                                <ol>
                                    <li>Add <b>writer</b> account</li>
                                    <li>Ban <b>writer</b> account</li>
                                    <li>Unban <b>writer</b> account</li>
                                </ol>
                            </li>
                        </ul>
                    </section>
                    <section class='info_level admin_detail'>
                        <ul>
                            <li>
                                <b>Add operation</b>
                                <ol>
                                    <li>Post</li>
                                    <li>Page</li>
                                    <li>File</li>
                                    <li>Category</li>
                                </ol>
                            </li>
                            <li>
                                <b>Edit operation</b>
                                <ol>
                                    <li>Post</li>
                                    <li>Page</li>
                                    <li>File</li>
                                    <li>Category</li>
                                </ol>
                            </li>
                            <li>
                                <b>Template management</b>
                                <ol>
                                    <li>Add Page</li>
                                    <li>Menu management</li>
                                </ol>
                            </li>
                            <li>
                                <b>User management</b>
                                <ol>
                                    <li>Add new account</li>
                                    <li>Ban user account</li>
                                    <li>Unban user account</li>
                                </ol>
                            </li>
                        </ul>
                    </section>
                </div>
            </div>
            <input type="hidden" name="status" value="1">
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10 txt_right">
                    <button type="submit" class="btn btn-danger" id="submit_btn" data-loading-text="Verification ...">
                        <i class='glyphicon glyphicon-plus'></i> Add new user
                    </button>
                    <button type="reset" class="btn btn-default">
                        <i class='glyphicon glyphicon-refresh'></i> Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type='text/javascript'>
    $(function() {
        var init_val = $('#user_level').val();
        $('.info_level').hide();
        $('.' + init_val + '_detail').show();

        $('#user_level').change(function() {
            var init_val = $(this).val();
            $('.info_level').hide();
            $('.' + init_val + '_detail').show();
        });

        $('#register_form').ajaxForm({
            beforeSend: function() {
                $('#submit_btn').button('loading');
            },
            complete: function(xhr) {
                $('#submit_btn').button('reset');
                var json = $.parseJSON(xhr.responseText);
                console.log(json);
                if (json.status === "success") {
                    location.href = json.link;
                } else {
                    alert(json.error);
                }
            }
        });
    });
</script>
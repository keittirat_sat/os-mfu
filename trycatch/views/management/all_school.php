<?php
$current_uid = get_user_uid();
$user_info = get_user_by_uid($current_uid);
$permission = json_decode($user_info->disallow);
$permission_school = isset($permission->permission_school) ? $permission->permission_school : array();
?>

<div class='container margin_top_20'>
    <div class='row'>
        <?php if (get_user_level() == USER_ADMIN): ?>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Save school order
                    </div>
                    <div class="panel-body">
                        <button id="save_order" class="btn btn-danger">Save Order</button>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                function assign_order() {
                    $.each($(".panel-danger"), function(idx, ele) {
                        $(ele).attr("data-order", idx);
                    });
                }

                $(function() {
                    assign_order();

                    $("#all_school").sortable({
                        update: function(event, ui) {
                            assign_order();
                        }
                    });

                    $('#save_order').click(function() {
                        var order = [];
                        $.each($(".panel-danger"), function(idx, ele) {
                            var temp = {};
                            temp['post_id'] = $(ele).attr("data-id");
                            temp['post_thumbnail'] = $(ele).attr("data-order");
                            order.push(temp);
                        });

                        var final_order = JSON.stringify(order);
                        $.post("<?php echo site_url("api/update_slide_order"); ?>", {"order": final_order}, function(res) {
                            if (res.status === "success") {
                                $('#status_report').modal();
                            } else {
                                alert("Update order fail");
                            }
                        }, "json");
                    });
                });
            </script>

        <?php endif; ?>

        <div class='<?php echo (get_user_level() == USER_ADMIN) ? "col-md-9" : "col-md-8 col-md-offset-2"; ?>'>
            <?php if (get_user_level() == USER_ADMIN || get_user_level() == USER_DIRECTOR): ?>
                <p class="txt_right">
                    <a href="<?php echo site_url('management/school_operation'); ?>" class="btn btn-danger">New School</a>
                </p>
            <?php endif; ?>
            <ul class="panel-group" id="all_school">
                <?php foreach ($post as $idx => $each_post): ?>
                    <?php if (!in_array($each_post->post_id, $permission_school)): ?>
                        <li class="panel panel-danger" id='box_<?php echo $each_post->post_id; ?>' data-id="<?php echo $each_post->post_id; ?>"  data-order="<?php echo $each_post->post_thumbnail ?>">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a id='post_title_id_<?php echo $each_post->post_id; ?>' data-toggle="collapse" data-parent="#all_school" href="#school_<?php echo $each_post->post_id; ?>">
                                        <?php if (get_user_level() == USER_ADMIN): ?>
                                            <i class="glyphicon glyphicon-resize-vertical"></i>&nbsp;
                                        <?php endif; ?>
                                        <?php echo $each_post->post_title; ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="school_<?php echo $each_post->post_id; ?>" class="panel-collapse collapse">
                                <div class="panel-body">                      
                                    <label>Owner</label>
                                    <p id='post_realname_<?php echo $each_post->post_id; ?>'><?php echo $each_post->real_name; ?></p>
                                </div>
                                <div class='panel-footer txt_right'>
                                    <a target="_blank" href="<?php echo site_url("mfu/school/{$each_post->post_id}/" . slug($each_post->post_title)); ?>" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;View</a>
                                    <?php if (get_user_level() == USER_ADMIN): ?>
                                        <?php $can_del = true; ?>
                                    <?php else: ?>
                                        <?php if (get_user_uid() == $each_post->uid): ?>
                                            <?php $can_del = true; ?>
                                        <?php else: ?>
                                            <?php $can_del = false; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>


                                    <a target="_blank" href="<?php echo site_url("management/school_operation?post_id={$each_post->post_id}"); ?>" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit</a>
                                    <?php if ($can_del): ?>
                                        <button data-set="<?php echo $each_post->post_id; ?>" data-trucate="1" class="btn btn-sm btn-default btn-delete"><i class="glyphicon glyphicon-trash"></i>&nbsp;<?php echo ($each_post->post_status == POST_TRASH) ? "Permanent delete" : "Delete"; ?></button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>                
            </ul>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm_dialoque">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure ?</h4>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete this post ?</p>
                <h4 id="modal_placeholder_here" style="color:#428bca;">Title here !!!</h4>
                <p>by <b id="realname_placeholder">Name Here !!!</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-loading-text="Deleting..." id="confirm_delete_btn" data-set="0" data-trucate="0">Delete</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="status_report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="txt_center margin_top_20 margin_bottom_20">Upload / Update complete.</h4>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<style type="text/css">
    #all_school{
        margin-left: 0px;
        padding-left: 0px;
    }

    .ui-sortable-placeholder{
        padding: 10px;
    }
</style>

<script type="text/javascript">
    $(function() {
        delete_success = false;
        $('.btn-delete').click(function() {
            var post_id = parseInt($(this).attr('data-set'));
            var trucate = parseInt($(this).attr('data-trucate'));
            var title = $('#post_title_id_' + post_id).text();
            var post_author = $('#post_realname_' + post_id).text();

            $('#modal_placeholder_here').text(title);
            $('#realname_placeholder').text(post_author);
            $('#confirm_delete_btn').attr('data-set', post_id);
            $('#confirm_delete_btn').attr('data-trucate', trucate);

            $('#confirm_dialoque').modal();
        });

        $('#confirm_delete_btn').click(function() {
            var post_id = parseInt($(this).attr('data-set'));
            var trucate = parseInt($(this).attr('data-trucate'));
            var btn = $(this);
            var btn_html = $(btn).html();
            $(btn).attr('disabled', 'disabled').text('Deleting...');

            $.post('<?php echo site_url('api/delete_post'); ?>', {post_id: post_id, trucate: trucate}, function(res) {
                if (res.status === "success") {
                    console.log(post_id);
                    $('#box_' + post_id).fadeOut(400, function() {
                        $(this).remove();
                    });
                    $('#confirm_dialoque').modal('hide');
                } else {
                    alert('Cannot process your request');
                    $(btn).button('reset');
                }
                $(btn).html(btn_html).removeAttr('disabled');
            }, 'json');
        });
    });
</script>
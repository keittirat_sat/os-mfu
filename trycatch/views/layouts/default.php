<!DOCTYPE html>
<html lang="<?php echo LANG ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo (empty($title)) ? "MFU | Content Management System" : "{$title} | " . (!empty($site_detail) ? "Mae Fah Luang University Thailand Chiangrai" : "MFU Content Management System"); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo css_asset("bootstrap.min.css"); ?>  
        <?php echo css_asset("sweet-alert.css"); ?>  
        <?php echo (empty($css)) ? "" : $css; ?>

        <?php echo js_asset("jquery-1.7.min.js") ?>
        <?php echo js_asset("bootstrap.min.js"); ?>
        <?php echo js_asset("jquery-ui-1.8.18.min.js"); ?>       

        <?php echo js_asset("jquery.form.js"); ?>
        <?php echo js_asset("jquery.metadata.js"); ?>
        <?php echo js_asset("ratio_controller.js"); ?>      
        <?php echo js_asset("sweet-alert.min.js"); ?>      

        <!--GSAP-->
        <?php echo js_asset("TweenMax.min.js"); ?>
        <?php echo js_asset("jquery.gsap.min.js"); ?>
        <!--/GSAP-->

        <?php echo (empty($js)) ? "" : $js; ?> 

        <?php mfu_header(); ?>
    </head>
    <body <?php mfu_body_class(); ?>>
        <div class="container">
            <?php echo $contents; ?>
        </div>
        <?php mfu_footer(); ?>
    </body>
</html>
<!DOCTYPE html>
<html lang="<?php echo LANG ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <META NAME="Author" CONTENT="Mae Fah Luang University,Thailand,Chiangrai">
        <?php if (LANG == "en"): ?>
            <META NAME="Keywords" lang="en"  CONTENT="Mae Fah Luang, mfu, chiangrai, thai , thailand , Academic, education , Mae Fah Luang University,Thailand, University,  thai  University, School, Campuses, Faculty, Technology, Engineering, Science, MBA, Food, Nurse, Anti Aging, Research">
        <?php else: ?>
            <META NAME="Keywords" lang="th"  CONTENT="มหาวิทยาลัย , มหาวิทยาลัยแม่ฟ้าหลวง , แม่ฟ้าหลวง, มฟล, มฟล., ม.แม่ฟ้าหลวง, วิจัย,  สาธิต, โครงงาน, ประเทศไทย, สถาบันการศึกษา, คุณภาพ, เชียงราย,มหาวิทยาลัยไทย">
            <META NAME="Description" CONTENT="มหาวิทยาลัยแม่ฟ้าหลวงเป็นมหาวิทยาลัยขนาดกลางที่มีคุณภาพและมาตรฐานการศึกษาในระดับสากล มีความเป็นเลิศในศิลปะและวิทยาการสาขาต่างๆ เพื่อเป็นแหล่งผลิตและพัฒนาทรัพยากรมนุษย์ที่มีคุณภาพของประเทศ และอนุภูมิภาคลุ่มแม่น้ำโขง">
        <?php endif; ?>

        <title>
            <?php if (LANG == "en"): ?>
                <?php echo (empty($title)) ? "MFU | Content Management System" : "{$title} | " . (empty($site_detail) ? "Mae Fah Luang University Thailand Chiangrai" : $site_detail); ?>
            <?php else: ?>
                <?php echo (empty($title)) ? "MFU | Content Management System" : "{$title} | " . (empty($site_detail) ? "มหาวิทยาลัยแม่ฟ้าหลวง" : $site_detail); ?>
            <?php endif; ?>
        </title>

        <?php echo css_asset("bootstrap.min.css") ?>
        <?php echo css_asset("sweet-alert.css"); ?>  
        <?php echo (empty($css)) ? "" : $css; ?>

        <?php echo js_asset("jquery-1.7.min.js") ?>
        <?php echo js_asset("bootstrap.min.js") ?>
        <?php echo js_asset("jquery-ui-1.8.18.min.js"); ?>   
        <?php echo js_asset("sweet-alert.min.js"); ?>          

        <?php echo js_asset("jquery.form.js"); ?>
        <?php echo js_asset("jquery.metadata.js"); ?>
        <?php echo js_asset("ratio_controller.js"); ?>   

        <!--GSAP-->
        <?php echo js_asset("TweenMax.min.js"); ?>
        <?php echo js_asset("jquery.gsap.min.js"); ?>
        <!--/GSAP-->

        <?php echo (empty($js)) ? "" : $js; ?> 

        <!--Overide CSS-->
        <style>
            /*Responsive disabled*/
            body{
                min-width: 970px;
            }

            .container {
                max-width: none !important;
                width: 970px;
            }
        </style>
    </head>
    <body>
        <!--.header-->
        <div class="header">
            <div class="container">
                <div class="row" style="margin-top: 10px; height: 134px;">
                    <div class="col-xs-7">
                        <a href="<?php echo site_url(); ?>">
                            <?php if (LANG == 'th'): ?>
                                <?php echo image_asset('header_logo.png', '', array('class' => 'img-responsive', 'title' => 'มหาวิทยาลัยแม่ฟ้าหลวง')); ?>
                            <?php else: ?>
                                <?php echo image_asset('header_logo_eng.png', '', array('class' => 'img-responsive', 'title' => 'Mae Fah Luang University')); ?>
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="col-xs-3 pull-right">
                        <div class="row txt_right margin_top_20">
                            <ul class="clearfix lang_bar">
                                <!--<li><?php echo image_asset("lang_text.png"); ?></li>-->
                                <li><a href="<?php echo SITE_EN; ?>"><?php echo image_asset("lang_uk.png") ?></a></li>
                                <li><a href="<?php echo SITE_TH; ?>"><?php echo image_asset("lang_th.png") ?></a></li>
                            </ul>
                        </div>
                        <div class="row">
                            <form id="cse-search-box" action="<?php echo site_url('mfu/search'); ?>" role="form" class="form-horizontal" name="form">
                                <div class="input-group  input-group-sm">                                    
                                    <input type="hidden" name="cx" value="001450871320183011274:o9gcchahnzk" />
                                    <input type="hidden" name="cof" value="FORID:10" />
                                    <input type="hidden" name="ie" value="UTF-8" />
                                    <input type="text" name="q" class="form-control" placeholder="Search here" required="required">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" value="Search" type="submit" name="sa"><i class="glyphicon glyphicon-search"></i></button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 menu_section">
                        <?php echo get_main_menu_with_format(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--/.header-->

        <?php echo $contents; ?>

        <!--.footer-->
        <div class="footer margin_top_20">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="txt_center">
                            <?php echo image_asset("footer_line.png"); ?>
                        </p>
                    </div>
                </div>
                <div class="row" style="padding: 0px 0px 10px;">
                    <div class="col-xs-4">
                        <?php echo image_asset("footer_logo_nologo.png"); ?>
                    </div>
                    <div class="col-xs-8 txt_right footer_address">
                        <?php if (LANG == "th"): ?>
                            <p>333 หมู่ 1 ต.ท่าสุด อ.เมือง จ. เชียงราย 57100</p>
                            <p>โทรศัพท์ 0-5391-6000, 0-5391-7034 โทรสาร 0-5391-6034, 0-5391-7049</p>
                            <p>สำนักงานมหาวิทยาลัยแม่ฟ้าหลวง กรุงเทพฯ โทรศัพท์ 0-2679-0038-9 โทรสาร 0-2679-0038</p>
                        <?php else: ?>
                            <p>International Affairs Division | Mae Fah Luang University</p>
                            <p>Muang, Chiang Rai, Thailand 57100</p>
                            <p>Tel (66)5391-6000 Fax (66)5391-6023 Email <?php echo safe_mailto("inter@mfu.ac.th"); ?></p>
                        <?php endif; ?>
                        <p><?php echo lang("จำนวนผู้เยี่ยมชม") ?> <span class="badge badge-inverse" id="id_web_stat"><i class="glyphicon glyphicon-refresh"></i> Retriving...</span></p>
                    </div>
                </div>
            </div>
        </div>
        <!--/.footer-->

        <!-- Modal -->
        <div class="modal fade" id="presidential_hotline_box" tabindex="-1" role="dialog" aria-labelledby="presidential_hotline" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title red" id="presidential_hotline">Presidential Hotline | สายตรงอธิการบดี</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <form class="form-horizontal" id="hotline_form">
                                    <p><input type="text" placeholder="Name" name="contact_name" class="form-control" required="required"></p>
                                    <p><input type="email" placeholder="Email" name="contact_email" class="form-control" required="required"></p>
                                    <p>
                                        <textarea name="contact_detail" class="form-control" rows="12" placeholder="Message" required="required"></textarea>
                                    </p>
                                    <input type="submit" id="submit_btn" style="display: none;">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer margin_top_0">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;Close</button>
                        <button type="button" class="btn btn-danger" id="fake_submit_btn" data-loading-text="Sending&hellip;"><i class="glyphicon glyphicon-envelope"></i>&nbsp;Send message</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="hotline_report" tabindex="-1" role="dialog" aria-labelledby="presidential_hotline_status" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title red" id="presidential_hotline_status">Status report</h4>
                    </div>
                    <div class="modal-body">
                        <h5 id="status_txt"></h5>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function() {
                $('#hotline_form').attr({'action': "<?php echo site_url('uncached/sendmail'); ?>", 'method': 'post'});
                $('#fake_submit_btn').click(function() {
                    $('#submit_btn').trigger('click');
                });

                var btn_html;
                var status_modal_active = false;
                $('#hotline_form').ajaxForm({
                    beforeSend: function() {
                        btn_html = $('#fake_submit_btn').html();
                        $('#fake_submit_btn').attr('disabled', 'disabled').html('Sending&hellip;');
                        status_modal_active = true;

                    },
                    complete: function(xhr) {
                        console.log(xhr.responseText);
                        var json = $.parseJSON(xhr.responseText);
                        if (json.status === "success") {
                            $('#status_txt').text('Message sent.');
                        } else {
                            $('#status_txt').text('Cannot send your message.');
                        }
                        $('#fake_submit_btn').removeAttr('disabled').html(btn_html);
                        $('#presidential_hotline_box').modal('hide');
                    }
                });

                $('#presidential_hotline_box').on('hidden.bs.modal', function() {
                    if (status_modal_active) {
                        status_modal_active = false;
                        $('#hotline_report').modal();
                    }
                });

                $.get('<?php echo site_url('uncached/stat'); ?>', function(res) {
                    $('#id_web_stat').text(res.counter);
                }, 'json');
            });
        </script>
        <?php if (isset($post_stat_id)): ?>
            <script type="text/javascript">
                $(function() {
                    $.get('<?php echo site_url('uncached/stat'); ?>', {key: '<?php echo $post_stat_id ?>'}, function(res) {
                        $('#id_holder_stat').text(res.counter);
                    }, 'json');
                });
            </script>
        <?php endif; ?>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', '<?php echo LANG == 'th' ? tracking_th : tracking_en ?>', 'mfu.ac.th');
            ga('send', 'pageview');

        </script>
    </body>
</html>
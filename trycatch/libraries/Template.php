<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Template
{

    private $CI;
    private $layout;
    private $view_data = array();

    public function __construct()
    {
        // get CI instance
        $this->CI = &get_instance();
    }

    public function set($name, $value)
    {
        $this->view_data[$name] = $value;
    }

    public function set_layout($layout)
    {
        $this->layout = $this->layout_path($layout);
    }

    public function element($view = '', $view_data = array())
    {
        $view = $this->view_path($view);
        
        if (!empty($view_data))
        {
            $this->view_data = array_merge($this->view_data, $view_data);
        }

        return $this->CI->load->view($view, $this->view_data, TRUE);
    }

    public function load($view = '', $layout = '', $view_data = array(), $return = FALSE)
    {
        // determind view path
        $view = $this->view_path($view);

        if (!empty($view_data))
        {
            $this->view_data = array_merge($this->view_data, $view_data);
        }

        // send data to view and get back for display in layout
        $contents = $this->CI->load->view($view, $this->view_data, TRUE);

        $this->view_data['contents'] = $contents;

        $this->layout = $this->layout_path($layout);

        return $this->CI->load->view($this->layout, $this->view_data, $return);
    }

    private function view_path($view)
    {
        if (empty($view))
        {
            // auto assign folder by class name
            $class_name = $this->CI->router->fetch_class();

            // auto assign view by action name
            $method_name = $this->CI->router->fetch_method();

            return "{$class_name}/{$method_name}";
        }
        else
        {
            return "{$view}";
        }
    }

    private function layout_path($layout)
    {
        if (empty($layout))
        {
            // auto assign layout default
            return "layouts/default";
        }
        else
        {
            return "layouts/{$layout}";
        }
    }
}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */
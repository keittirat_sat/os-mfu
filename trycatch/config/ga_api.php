<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['profile_id'] = (LANG == "en" ? ga_profile_id_en : ga_profile_id_th); // GA profile id
$config['email'] = google_user; // GA Account mail
$config['password'] = google_pass; // GA Account password

$config['cache_data'] = false; // request will be cached
$config['cache_folder'] = '/cache'; // read/write
$config['clear_cache'] = array('date', '1 day ago'); // keep files 1 day

$config['debug'] = false; // print request url if true
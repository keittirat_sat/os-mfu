<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


//Set Timezone to Thailand
date_default_timezone_set('Asia/Bangkok');

//System Locale
//define('LANG', 'th');

//Define post type
define('TYPE_POST', 1);
define('TYPE_PAGE', 2);
define('TYPE_SCHOOL', 3);
define('TYPE_MENUGROUP', 4);
define('TYPE_AUDIENCE', 5);
define('TYPE_EVENT', 6);
define('TYPE_GALLERY', 7);
define('TYPE_API', 8);
define('TYPE_CLIPPING', 9);
define('TYPE_NEWSLETTER', 10);
define('TYPE_RESEARCH', 11);
define('TYPE_SLIDE', 12);
define('TYPE_REVISION', 13);

//Cate Type
define('CATE_POST', 0);
define('CATE_EVENT', 1);
define('CATE_CLIPPING', 2);
define('CATE_ALBUM', 4);

//Event
define('EVENT_OTHER', 0);
define('EVENT_GUEST', -1);
define('EVENT_ADMISSION', -2);

//Define Post status
define('POST_PUBLISH', 1);
define('POST_DRAFT', 2);
define('POST_REVIEW', 3);
define('POST_TRASH', 0);

//Define user type
define('USER_ADMIN', 1);
define('USER_DIRECTOR', 2);
define('USER_WRITER', 3);

define('USER_ACTIVE', 1);
define('USER_TEMPORARY', 0);

define('POST_PER_PAGE', 10);

define("YOUTUBE_LINK", "http://www.youtube.com/user/mfuChannel");
define("FB_ADMISSION", "https://www.facebook.com/pages/RegistrarMFU/269691783054697");
define("FB_HOME", "https://www.facebook.com/pages/%E0%B8%AA%E0%B9%88%E0%B8%A7%E0%B8%99%E0%B8%A3%E0%B8%B1%E0%B8%9A%E0%B8%99%E0%B8%B1%E0%B8%81%E0%B8%A8%E0%B8%B6%E0%B8%81%E0%B8%A9%E0%B8%B2-%E0%B8%A1%E0%B8%AB%E0%B8%B2%E0%B8%A7%E0%B8%B4%E0%B8%97%E0%B8%A2%E0%B8%B2%E0%B8%A5%E0%B8%B1%E0%B8%A2%E0%B9%81%E0%B8%A1%E0%B9%88%E0%B8%9F%E0%B9%89%E0%B8%B2%E0%B8%AB%E0%B8%A5%E0%B8%A7%E0%B8%87/322237694557736");

//Research Type
define("POSITION_PROF", 1);
define("POSITION_ASSIS_PROF", 2);
define("POSITION_ASSOC_PROF", 3);

//Date
$full_month = array(
    'th' => array('', 'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'),
    'en' => array('', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December')
);

$short_month = array(
    'th' => array('', 'ม.ค', 'ก.พ', 'มี.ค', 'เม.ย', 'พ.ค', 'มิ.ย', 'ก.ค', 'ส.ค', 'ก.ย', 'ต.ค', 'พ.ย', 'ธ.ค'),
    'en' => array('', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec')
);

define("FULL_MONTH", serialize($full_month));
define("SHORT_MONTH", serialize($short_month));

//Google Account for Main System
define("google_user", "web.mfu@gmail.com");
define("google_pass", "2014mfuweb2557");

//Google Analytic
define("tracking_th", "UA-49524108-1");
define("tracking_en", "UA-49524108-2");

//Google Analytic Profile ID
define("ga_profile_id_th", "84137728");
define("ga_profile_id_en", "85382888");

/* End of file constants.php */
/* Location: ./application/config/constants.php */
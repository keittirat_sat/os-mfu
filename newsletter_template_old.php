<div class="row">
	<?php foreach ($post as $each_post): ?>
	    <?php $media = get_media_by_post_id($each_post->post_id) ?>
	    <?php if (array_key_exists(0, $media)): ?>
		<?php $mid = $media[0]->mid; ?>
		<?php $media_name = $media[0]->media_name; ?>
		<?php $pdf_info = json_decode($media[0]->media_info); ?>
		<div class="col-xs-4 txt_center">
		    <h3 class="red th_san margin_bottom_0 margin_top_0"><?php echo $each_post->post_title; ?></h3>
		    <p><?php echo $each_post->post_excerp ?></p>
		    <?php if (count($pdf_info->pdf_image) > 1): ?>
		        <?php foreach ($pdf_info->pdf_image as $each_page): ?>
		            <?php if ($each_page->name == "page-0.jpg"): ?>
		                <?php $thumb = $each_page ?>
		                <?php break; ?>
		            <?php endif; ?>
		        <?php endforeach; ?>
		    <?php else: ?>
		        <?php $thumb = $pdf_info->pdf_image; ?>
		        <?php $thumb = $thumb[0] ?>
		    <?php endif; ?>
		    <a href="<?php echo site_url("mfu/mfu_reader/{$mid}/" . slug($media_name)); ?>" target="_blank" class="thumbnail"><img src="<?php echo $thumb->thumb ?>" style="max-height: 285px; max-width: 203px;"></a>
		</div>
	    <?php endif; ?>
	<?php endforeach; ?>
</div>

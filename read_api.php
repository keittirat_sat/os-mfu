<?php
//----------------------------------------------------
//Retrieve news_id id from GET request
$id = $_GET['id'];

//Input Validation
$id = is_numeric($id) ? $id : 0;


//----------------------------------------------------

$host = "intranet.mfu.ac.th";
$username = "mfuapp";
$password = "2013mfuapp";
$database = "mfuportal";

// Create connection
$con = mysqli_connect($host,$username,$password,$database);

// Check connection
if (mysqli_connect_errno($con)){
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


//----------------------------------------------------
//Edit dynamic query
$query = "SELECT n.news_id as id,
	n.cat_id as cid,
	n.title, 
	n.short_detail as intro, 
	n.detail as description, 
	n.activate, 
	n.publish_date, 
	n.flag_show, 
	n.internet, 
	n.engnews, 
	n.picture,

	t.file_pic as thumbnail
	FROM tbnews n LEFT JOIN tbpicattach t ON n.news_id = t.news_id
	WHERE n.news_id = {$id}";

//----------------------------------------------------

$result = mysqli_query($con, $query);

$data = array();
while($row = mysqli_fetch_array($result)){
	$ext = explode(".", $row['thumbnail']);
	if($ext[1] == "jpg" || $ext[1] == "png" || $ext[1] == "gif"){
		// $row['thumbnail'] = "";
	}else{
		$row['thumbnail'] = "";
	}

	//Add Page into record obj.
	$item = array(
		"id" => $row['id'],
		"page" => $page,
		"cat_id" => $row['cid'],
		"title" => $row['title'],
		"detail" => $row['description'],
		"thumbnail"  => $row['thumbnail'] == ""? "":"http://intranet.mfu.ac.th/file_attach/mfunews/".$row['thumbnail'],
		"intro" => $row['intro'],
		"publish" => $row['publish_date'],
		"link" => " http://intranet.mfu.ac.th/system/mfunews/prnews01.php?newsid=".$row['id']		

	);
	array_push($data, $item);
}

mysqli_close($con);

//Export Result as JSON
echo json_encode($data);

?>

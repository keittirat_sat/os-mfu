<?php
//----------------------------------------------------
//Retrieve page number and category id from GET request
$page = $_GET['page'];
$cate_id = $_GET['cate_id'];

//Flag for education news
$edunewstype = $_GET['edunewstype'];

//Flag for get total record
$num_rows = $_GET['num_rows'];

//Input Validation
$page = is_numeric($page) ? $page : 0;
$cate_id = is_numeric($cate_id) ? $cate_id : 1;
$num_rows = is_numeric($num_rows) ? $num_rows : 0;

//Set total item to query
$limit = 15;
$start_record = $page * $limit;



//----------------------------------------------------

$host = "intranet.mfu.ac.th";
$username = "mfuapp";
$password = "2013mfuapp";
$database = "mfuportal";

// Create connection
$con = mysqli_connect($host,$username,$password,$database);

// Check connection
if (mysqli_connect_errno($con)){
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

/*
$query = "SELECT n.news_id as id,
	n.cat_id as cid,
	n.title, 
	n.short_detail as intro, 
	n.detail as description, 
	n.activate, 
	n.publish_date, 
	n.flag_show, 
	n.internet, 
	n.engnews, 
	n.picture,

	t.file_pic as thumbnail
	FROM tbnews n LEFT JOIN tbpicattach t ON n.news_id = t.news_id
	WHERE n.cat_id = 1
	ORDER BY n.news_id DESC
	LIMIT 0,10";
*/

//----------------------------------------------------
//Edit dynamic query
switch($cate_id){
	case 5: 
		$query = "SELECT n.news_id as id,
			n.cat_id as cid,
			n.title, 
			n.short_detail as intro, 
			n.detail as description, 
			n.activate, 
			n.publish_date, 
			n.flag_show, 
			n.internet, 
			n.engnews, 
			n.picture,
			n.edunewstype,
			t.file_pic as thumbnail
	
			FROM tbnews n LEFT JOIN tbpicattach t ON n.news_id = t.news_id
			WHERE n.cat_id = {$cate_id} and n.activate = 'Y' and n.edunewstype like '%{$edunewstype}%'
			ORDER BY n.publish_date DESC";
		break;
	default:
		$query = "SELECT n.news_id as id,
			n.cat_id as cid,
			n.title, 
			n.short_detail as intro, 
			n.detail as description, 
			n.activate, 
			n.publish_date, 
			n.flag_show, 
			n.internet, 
			n.engnews, 
			n.picture,

			t.file_pic as thumbnail
			FROM tbnews n LEFT JOIN tbpicattach t ON n.news_id = t.news_id
			WHERE n.cat_id = {$cate_id}
			ORDER BY n.news_id DESC";
		break;
}

if($num_rows == 0){
	$query .= " LIMIT {$start_record},{$limit}";
}

//----------------------------------------------------

$result = mysqli_query($con, $query);

if($num_rows != 0){
	$all_rec = mysqli_num_rows($result);
	$data = array("cate_id" => $cate_id, "total_record" => $all_rec, "total_page" => ceil($all_rec / $limit));
}else{
	$data = array();
	while($row = mysqli_fetch_array($result)){
		$ext = explode(".", $row['thumbnail']);
		if($ext[1] == "jpg" || $ext[1] == "png" || $ext[1] == "gif"){
			// $row['thumbnail'] = "";
		}else{
			$row['thumbnail'] = "";
		}

		//Add Page into record obj.
		$item = array(
			"id" => $row['id'],
			"page" => $page,
			"cat_id" => $row['cid'],
			"title" => $row['title'],
			"detail" => $row['description'],
			"thumbnail"  => $row['thumbnail'] == ""? "":"http://intranet.mfu.ac.th/file_attach/mfunews/".$row['thumbnail'],
			"intro" => $row['intro'],
			"publish" => $row['publish_date'],
			"link" => " http://intranet.mfu.ac.th/system/mfunews/prnews01.php?newsid=".$row['id']		
		
		);
		array_push($data, $item);
	}
}

mysqli_close($con);

//Export Result as JSON
echo json_encode($data);

?>
